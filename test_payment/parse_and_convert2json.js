var fs = require('fs');
var _ = require("underscore");

var parseMonth = function (str) {
	switch (str) {
    	case "Jan": return 1;
    	case "Feb": return 2;
    	case "Mar": return 3;
    	case "Apr": return 4;
    	case "May": return 5;
    	case "Jun": return 6;
    	case "Jul": return 7;
    	case "Aug": return 8;
    	case "Sep": return 9;
    	case "Oct": return 10;
    	case "Nov": return 11;
    	case "Dec": return 12;
    	default: return -1;
	}
}

/*
Parsing the old payment
*/
var array = fs.readFileSync('old_payment_raw.txt').toString().split("\n");
var old_sale_payments = [];
for (i in array) {
    var result = array[i].trim().split("\t");

    // FORMAT of old payment: Date - Album - User - Money - Paid?
    if (result.length != 5) {
    	continue;
    }
    var time = result[0].split("-");
    var year = parseInt(time[0]);
    var month = parseInt(time[1]);
    var albumName = result[1].trim();
    var userName = result[2].trim();
    var money = parseFloat(result[3].substring(1));
    var paid = result[4].trim();
    old_sale_payments.push({
    	year: year,
    	month: month,
    	album: albumName,
    	user: userName,
    	money: money,
    	paid: paid
    });
}

/*
Parsing the new payment
*/
var array2 = fs.readFileSync('new_payment_raw.txt').toString().split("\n");
var new_sale_payments = [];
for (i in array2) {
    var result = array2[i].trim().split("\t");

    // FORMAT of old payment: Date - Album - User - Role - Money - Paid?
    if (result.length != 6) {
    	continue;
    }
    var time = result[0].split("-");
    var month = parseMonth(time[0].trim());
    var year = parseInt(time[1]);
    var albumName = result[1].trim();
    var userName = result[2].trim();
    var role = result[3].trim();
    var money = parseFloat(result[4]);
    var paid = result[5].trim();

    new_sale_payments.push({
    	year: year,
    	month: month,
    	album: albumName,
    	user: userName,
    	role: role,
    	money: money,
    	paid: paid,
    })
}

var group = function (arr, selector) {return _.groupBy(arr, selector);}
var byUser = function (obj) {return obj.user;};
var byAlbum = function (obj) {return obj.album;};
var byYear = function (obj) {return obj.year;};
var byMonth = function (obj) {return obj.month;};

var groupAndWrite2Json = function (payments, output) {
	var users = group(payments, byUser);
	var mapping = {};
	for (var user in users) {
		mapping[user] = {};
		var albumList = group(users[user], byAlbum);
		for (var album in albumList) {
			mapping[user][album] = {};
			var years = group(albumList[album], byYear);
			for (var year in years) {
				mapping[user][album][year] = {};
				var months = group(years[year], byMonth);
				for(var m in months) {
					mapping[user][album][year][m] = months[m];
				}
			}
		}
	}
	var json = JSON.stringify(mapping)
	console.log(json);

	fs.writeFile(output, json, function(err) {
	  if (err) throw err;
	});
};

console.log("\n\nOLD PAYMENT:\n");
groupAndWrite2Json(old_sale_payments, "old_payment_json.txt");

console.log("\n\nNEW PAYMENT:\n");
groupAndWrite2Json(new_sale_payments, "new_payment_json.txt");


