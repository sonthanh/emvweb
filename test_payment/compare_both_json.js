var fs = require('fs');
var _ = require("underscore");

var old_str = fs.readFileSync('old_payment_json.txt').toString();
var new_str = fs.readFileSync('new_payment_json.txt').toString();

var old_payment_json = JSON.parse(old_str);
var new_payment_json = JSON.parse(new_str);

for (var user in new_payment_json) {
	for (var album in new_payment_json[user]) {
		for (var year in new_payment_json[user][album]) {
			for(var month in new_payment_json[user][album][year]) {
				if (user === "EpicMusicVn Label") {
					continue;
				}
				var user_for_oldpayment = user;
				if (user === "M lee") {
					user_for_oldpayment = "Mlee";
				}
				var payments = new_payment_json[user][album][year][month];
				var old_payments = old_payment_json[user_for_oldpayment][album][year][month];
				var total = _.reduce(payments, function (acc, e) {return acc + e.money;}, 0);
				
				var difference = Math.abs(parseFloat(total) - parseFloat(old_payments[0].money));
				var old_paid = old_payments[0].paid
				var same_paid = payments.every(function (e) {return e.paid.toLowerCase() === old_paid.toLowerCase();});
				
				if (difference > 0.001) {
					console.log("\n\nMAYBE WRONG in MONEY of [" + user + ", " + album + ", " + year + ", " + month + "] ===> ");
					console.log("\tNew total payments = " + total);
					console.log("\tOld payments = " + old_payments[0].money);
					console.log("\tFULL NEW PAYMENTS:");
					console.log(payments);
					console.log("\tFULL OLD PAYMENTS:");
					console.log(old_payments);
				}
				if (!same_paid) {
					console.log("\n\nMAYBE WRONG in PAID of [" + user + ", " + album + ", " + year + ", " + month + "] ===> ");
					console.log(old_payments);
					console.log(payments);
				}
			}
		}
	}
}
