'use strict';

var mongoose = require('mongoose');
var chalk = require('chalk');

exports.openDBTests = function () {
    // Bootstrap db connection
    mongoose.connect('mongodb://104.236.183.57/mean-dev-test', function(err) {
        if (err) {
            console.error(chalk.red('Could not connect to MongoDB!'));
            console.log(chalk.red(err));
        }
    });
    mongoose.connection.on('error', function(err) {
            console.error(chalk.red('MongoDB connection error: ' + err));
            process.exit(-1);
        }
    );
};
