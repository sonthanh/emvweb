'use strict';

var mongoose = require('mongoose');
var requireDir = require('require-dir');
var dir = requireDir('./../../app/models');
var TrackShare = mongoose.model('TrackShare');
var AlbumInfo = mongoose.model('AlbumInfo');


var user1 = '551eb9b1f83f29db534d65b3';

var user2 = '551eb9b1f83f29db534d65b4';

var user3 = '555f752514cf099c3ebc5d56';

var composer = 'composer';

var soundeng = 'sound engineer';

var label = 'label';

var pulsarId = '559a83b79f8294127374dccc';
var pulsarName = 'Pulsar (Epicmusicvn)';
var track1InPulsar = 'Dream Catcher';
var track1InPulsarId = '559a83b6408c00253a79e9df';
var track2InPulsar = 'Wrath of Gods';
var track2InPulsarId = '559a83b6408c00253a79e9e1';

var icandoId = '559a83b79f8294127374dcc6';
var icandoName = 'I Can Do';
var trackInIcando = 'I Can Do';
var trackInIcandoId = '559a83b6408c00253a79e9c5';

var createTrackList = function (trackName, trackId) {
    return {
        trackName: trackName,
        trackId: trackId
    };
};

var createAlbum = function (albumId, albumName, trackList) {
    return {
        _id: albumId,
        name: albumName,
        trackList: trackList
    };
};

var createTrackShare = function (trackId, albumId, userId, role, commission, realCommission, shareWith, payee) {
    return {
        trackId: trackId,
        albumId: albumId,
        userId: userId,
        role: role,
        commission: commission,
        shareWith: shareWith,
        payee: payee,
        realCommission: realCommission
    };
};

var pulsarObj = createAlbum(pulsarId, pulsarName, [createTrackList(track1InPulsar, track1InPulsarId), createTrackList(track2InPulsar, track2InPulsarId)]);
var icandoObj = createAlbum(icandoId, icandoName, [createTrackList(trackInIcando, trackInIcandoId)]);

exports.pulsar = pulsarObj;
exports.icando = icandoObj;

exports.setupAlbum = function (done) {
    console.log('Setup 2 album Pulsar & I can do for testing');
    return AlbumInfo.create([pulsarObj, icandoObj]).then(function () { done(); }).end();
};
