'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Role Schema
 */
var RoleSchema = new Schema({
    role: {
        type: String,
        required: true,
        index: true
    },
    value: {
        type: Number,
        required: true,
        index: true
    },
    netValue: {
        type: Number,
        required: true,
        index: true,
        'default': 0
    },
    trackId: {
        type: Schema.ObjectId,
        index: true
    },
    commission: Number,
    netCommission: Number,
    ispaid: {
        type: Boolean,
        'default': false
    }
}, { _id: false });

/**
 * AllCommission Schema
 */
var AllCommissionSchema = new Schema({
    userId: {
        type: Schema.ObjectId,
        required: true,
        ref: 'User',
        index: true
    },
    detail: [RoleSchema],
    totalValue: {
        type: Number,
        required: true,
        index: true
    }
}, { _id: false });

/**
 * SaleCommission Schema
 */
var SaleCommissionSchema = new Schema({
    album_artist: { type: String, required: '{PATH} is required!' },
    track_artist: { type: String, required: '{PATH} is required!' },
    label: { type: String, required: '{PATH} is required!' },
    album_name: { type: String, index: true, required: '{PATH} is required!' },
    track_name: { type: String, index: true, required: '{PATH} is required!' },
    partner: { type: String, index: true },
    dms: { type: String },
    type: String,
    quantity: { type: Number, required: '{PATH} is required!' },
    unit: { type: Number, required: '{PATH} is required!' },
    profit: { type: Number, required: '{PATH} is required!' },
    currency: { type: String, required: '{PATH} is required!' },
    country: { type: String, required: '{PATH} is required!' },
    sale_date: { type: Date },
    import_date: { type: Date },
    allCommissions: [AllCommissionSchema]
});

mongoose.model('SaleCommission', SaleCommissionSchema);
