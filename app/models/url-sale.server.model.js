'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * URL Sale Schema
 */
var UrlSaleSchema = new Schema({
	userId: {
		type: Schema.ObjectId,
		ref: 'User',
		required: true,
	},
	sourceUrl: {
		type: String,
		trim: true,
		required: true,
		unique: true
	},
	shortenUrl: {
		type: String,
		trim: true,
		required: true,
		unique: true
	},
	companyName: {
		type: String,
		trim: true,
		default: 'EpicMusicVN'
	}
});

mongoose.model('UrlSale', UrlSaleSchema);
