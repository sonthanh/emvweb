'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Role Schema
 */
var RoleSchema = new Schema({
	role: {
		type: String,
		required: true,
		index: true
	},
	value: {
		type: Number,
		required: true,
		index: true
	},
	netValue: {
		type: Number,
		required: true,
		index: true,
		'default': 0
	},
	trackId: {
		type: Schema.ObjectId,
		index: true
	},
	commission: Number,
	netCommission: Number,
	ispaid: {
		type: Boolean,
		'default': false
	}
}, { _id: false });

/**
 * AllCommission Schema
 */
var AllCommissionSchema = new Schema({
	userId: {
		type: Schema.ObjectId,
		required: true,
		ref: 'User',
		index: true
	},
	detail: [RoleSchema],
	totalValue: {
		type: Number,
		required: true,
		index: true
	}
}, { _id: false });

/**
 * Sale Schema
 */
var YoutubeSchema = new Schema({
	videoId: { type: String, required: '{PATH} is required!', trim: true },
	track_name: { type: String, required: '{PATH} is required!', trim: true },
	channelId: { type: String, required: '{PATH} is required!', trim: true },
	album_name: { type: String, required: '{PATH} is required!', trim: true },
	profit: { type: Number, required: '{PATH} is required!' },
	original_profit: { type: Number, required: '{PATH} is required!' },
	original_commission: { type: Number, required: '{PATH} is required!' },
	currency: { type: String, 'default': '€' },
	import_date: { type: Date, required: '{PATH} is required!' },
	isDeleted: { type: Boolean, 'default': false },
	allCommissions: [AllCommissionSchema]
});

mongoose.model('YoutubeSale', YoutubeSchema);
