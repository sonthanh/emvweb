'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Sale Schema
 */
var InvitationSchema = new Schema({
	user_id: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	invite_email: {
		type: String,
		trim: true,
		required: true,
		match: [/.+\@.+\..+/, 'Please fill a valid email address']
	},
	status: {
		type: String,
		'default': 'P',
		'enum': ['P', 'A']
	}
});

mongoose.model('Invitation', InvitationSchema);
