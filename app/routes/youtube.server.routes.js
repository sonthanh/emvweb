'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var youtube = require('../../app/controllers/youtube.server.controller');

	// Sales Routes
	app.route('/api/youtube/report')
		.post(users.hasAuthorization(['admin']), youtube.importReport)
		.get(users.hasAuthorization(['admin']), youtube.list);

	app.route('/api/youtube/sale-by-channel')
		.get(youtube.getSaleByChannel);

	app.route('/api/channelvideoshareinfo')
		.get(users.requiresLogin, youtube.channelVideoShareInfo);

	app.route('/api/youtube-commission-byGroup')
		.get(users.requiresLogin, youtube.listCommissionGroup);

	app.route('/api/youtube-commission-byVideo')
		.get(users.requiresLogin, youtube.listCommissionGroupByVideo);

	app.route('/api/youtube-commission-summary')
    	.get(users.requiresLogin, youtube.getCommissionSummary);

    app.route('/api/youtube-commission-chart')
    	.get(users.requiresLogin, youtube.getChartSummarizedByMonth);

    app.route('/api/youtube-user-earning')
    	.get(users.requiresLogin, youtube.getUserEarningByMonth);

	app.route('/api/youtube/allUserClip')
		.get(users.requiresLogin, youtube.getAllUserClip);

	app.route('/api/youtube/search-video')
		.get(users.requiresLogin, youtube.searchVideo);
};
