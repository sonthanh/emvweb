'use strict';

module.exports = function(app) {
    var trackShare = require('../../app/controllers/track-share.server.controller');
    var users = require('../../app/controllers/users.server.controller');

    // Track Share Routes
    app.route('/api/track-shares')
        .get(users.requiresLogin, trackShare.setTypeShare, trackShare.getTrackShares);

    app.route('/api/track-shares/add')
        .post(users.requiresLogin, users.hasAdmin, trackShare.setTypeShare, trackShare.addTrackShares);

    app.route('/api/track-shares/add-collaborator')
        .post(users.requiresLogin, trackShare.setTypeShare, trackShare.addCollaboratorTrackShares);

    app.route('/api/track-shares/edit')
        .post(users.requiresLogin,trackShare.setTypeShare,  trackShare.editTrackShares);

    app.route('/api/track-shares/delete')
        .post(users.requiresLogin, users.hasAdmin, trackShare.setTypeShare, trackShare.deleteTrackShares);

    app.route('/api/track-shares/delete-collaborator')
        .post(users.requiresLogin, trackShare.setTypeShare, trackShare.deleteCollaboratorTrackShares);

    app.route('/api/track-shares/delete-multiple')
        .post(users.requiresLogin, users.hasAdmin, trackShare.setTypeShare, trackShare.deleteMultipleShares);

    // Album User Routes
    app.route('/api/allAlbumUsers')
        .get(users.requiresLogin, trackShare.setTypeShare, trackShare.getAllAlbumUsers);

    // Album Info Routes
    app.route('/api/albuminfo')
        .get(users.requiresLogin, trackShare.setTypeShare, trackShare.getAlbumInfo);

    // User Info Routes
    app.route('/api/allusers')
        .get(users.requiresLogin, trackShare.getAllUser);

    // Get all collaborator of user
    app.route('/api/allcollaborators')
        .get(users.requiresLogin, trackShare.setTypeShare, trackShare.allCollaborators);

    // Invite collaborator
    app.route('/api/track-shares/invite-collaborator')
        .post(users.requiresLogin, trackShare.setTypeShare, trackShare.inviteCollaborator);

};
