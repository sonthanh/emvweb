/**
 * Created by sonthanh on 4/6/15.
 */
'use strict';

module.exports = function(app){
    // Music source Routes
    var users = require('../../app/controllers/users.server.controller');
    var musicsources = require('../../app/controllers/musicsource/musicsource.submit.server.controller');


    app.route('/musicsource/submit')
        .get(function(req, res){
            res.json([]);
        });
};
