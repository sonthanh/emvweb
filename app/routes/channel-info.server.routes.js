'use strict';

module.exports = function(app) {
    var channel = require('../../app/controllers/channel-info.server.controller');
    var users = require('../../app/controllers/users.server.controller');

    // Channel Info Routes
    app.route('/api/channelinfo')
        .get(users.requiresLogin, channel.getChannelInfo);

    app.route('/api/youtube/auth')
        .get(users.hasAuthorization(['admin', 'label']), channel.getYouTubeChannelAuth);

    app.route('/api/youtube/channels')
        .get(users.requiresLogin, channel.listChannels);

    app.route('/auth/youtube/callback')
        .get(channel.getImportYouTubeChannel);
};
