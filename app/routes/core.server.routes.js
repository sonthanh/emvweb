'use strict';

module.exports = function(app) {
	// Root routing
	var core = require('../../app/controllers/core.server.controller');
	var saleUrl = require('../../app/controllers/sale-url.server.controller');
	var reference = require('../../app/models/reference.server.model');
	var users = require('../../app/controllers/users.server.controller');
	
	app.route('/api/reference/albumLabel')
		.get(users.requiresLogin, function(req, res) {
			res.send(reference.albumLabel);
		});

	app.route('/api/bitly-auth')
		.post(users.requiresLogin, users.hasAdmin, saleUrl.getUpdateSaleURL);

	app.route('/api/sale-url')
		.get(users.requiresLogin, saleUrl.listSaleUrl);

	app.route('/api/sale-url/autosuggest-company')
    	.get(users.requiresLogin, saleUrl.suggestCompany);

	app.route('/api/sale-url/delete')
		.post(users.requiresLogin, users.hasAdmin, saleUrl.deleteSaleURL);
	
	app.route('/acc/*').get(core.index);
	app.route('/').get(function(req, res) {return res.redirect('/acc')});
};
