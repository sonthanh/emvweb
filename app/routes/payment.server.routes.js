'use strict';

module.exports = function(app) {
	var payment = require('../../app/controllers/payment.server.controller');
	var users = require('../../app/controllers/users.server.controller');

	// Albums Routes
	app.route('/api/payment')
		.post(payment.createSalePayment);

	app.route('/api/youtube-payment')
    	.post(payment.createYouTubePayment);
};
