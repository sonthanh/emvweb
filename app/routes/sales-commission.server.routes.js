'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var sales = require('../controllers/sales-commission.server.controller.js');
	var trackShare = require('../../app/controllers/track-share.server.controller');
	
	app.route('/api/sales/report')
		.post(users.hasAuthorization(['admin', 'label']), trackShare.setTypeShare, sales.importReport);

	app.route('/api/sales-commission')
		.get(users.requiresLogin, sales.listSalesCommission);

	app.route('/api/sales-commission-byTrack')
		.get(users.requiresLogin, sales.listCommissionGroupByTrack);

	app.route('/api/sales-commission-byArtist')
		.get(users.requiresLogin, sales.listCommissionGroupByArtist);

	app.route('/api/sales-commission-byGroup')
		.get(users.requiresLogin, sales.listCommissionByGroup);

	app.route('/api/sales-commission-summary')
		.get(users.requiresLogin, sales.getCommissionSummary);

	app.route('/api/sales-commission-chart')
		.get(users.requiresLogin, sales.getChartSummarizedByMonth);

	app.route('/api/sales-user-earning')
		.get(users.requiresLogin, sales.getUserEarningByMonth);

	app.route('/api/albumtrackshareinfo')
		.get(users.requiresLogin, sales.albumTrackShareInfo);

};
