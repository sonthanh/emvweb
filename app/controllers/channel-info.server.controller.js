'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    _ = require('lodash'),
    Q = require('q'),
    promise = require('bluebird'),
    ChannelInfo = mongoose.model('ChannelInfo'),
    config = require('../../config/config'),
    TrackShareUtil = require('./utils/share.util.js'),
    validator = require('validator');

var google = require('googleapis');
var youtube = google.youtube('v3');
var OAuth2 = google.auth.OAuth2;

var oauth2Client = new OAuth2(config.youtube.clientID, config.youtube.clientSecret, config.youtube.callbackURL);

exports.getChannelInfo = function (req, res) {
	var cond = {};
	if (!_.intersection(req.user.roles, ['admin']).length) {
		cond = { 'trackList.tags': req.user.saleId };
	}

	ChannelInfo.find(cond).sort({ name: 1 }).exec().then(function (allChannel) {
		res.jsonp({ data: allChannel });
	});
};

exports.listChannels = function (req, res) {
	ChannelInfo.find({}, 'channelId name', function (err, channels) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(channels);
		}
	});
};

var videoList = [];
var pageList = 1;

exports.getYouTubeChannelAuth = function (req, res) {
	videoList = [];
	pageList = 1;
	var scopes = ['https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/youtube.force-ssl', 'https://www.googleapis.com/auth/youtube.readonly'];

	var url = oauth2Client.generateAuthUrl({
		scope: scopes
	});

	res.redirect(url);
};

function saveVideoDateTags(videos, channelId) {
	var resolver = promise.pending();
	var cnt = 0;
	for (var i in videos) {
		ChannelInfo.update({ channelId: channelId, 'trackList.trackId': videos[i].trackId }, { $set: {
				'trackList.$.tags': videos[i].tags,
				'trackList.$.publishDate': videos[i].publishDate
			} }, function (err) {
			if (err) {
				resolver.reject(err);
			}

			cnt++;
			if (cnt === videos.length) {
				resolver.resolve(true);
			}
		});
	}

	return resolver.promise;
}

function getYouTubeData(video_list, oauth2Client) {
	var resolver = promise.pending();
	var cnt = 0,
	    allTags = [];

	for (var iii in video_list) {
		youtube.videos.list({
			part: 'id,snippet',
			id: video_list[iii],
			fields: 'items(id,snippet)',
			auth: oauth2Client
		}, function (err, response) {

			if (err) {
				resolver.reject(err);
			}

			for (var ii = 0; ii < response.items.length; ii++) {
				var tags = response.items[ii].snippet.tags;
				if (tags) {
					for (var i = tags.length; i >= 0; i--) {
						if (!validator.isEmail(tags[i])) {
							tags.splice(i, 1);
						}
					}
					if (tags.length > 0) {
						allTags.push({
							trackId: response.items[ii].id,
							tags: tags,
							publishDate: response.items[ii].snippet.publishedAt
						});
					}
				}
			}

			cnt++;
			if (cnt === video_list.length) {
				resolver.resolve(allTags);
			}
		});
	}

	return resolver.promise;
}

function getYouTubeChannelInfo(oauth2Client) {
	var resolver = promise.pending();
	youtube.channels.list({
		part: 'snippet',
		mine: 'true',
		fields: 'items(id,snippet)',
		auth: oauth2Client
	}, function (err, response) {
		if (err || response.length === 0) {
			resolver.reject(false);
		}

		response = {
			channelId: response.items[0].id,
			name: response.items[0].snippet.title
		};
		resolver.resolve(response);
	});

	return resolver.promise;
}

function saveChannelInfo(channel) {
	var resolver = promise.pending();

	ChannelInfo.update({ _id: channel.channelId, channelId: channel.channelId }, channel, { upsert: true }, function (err) {

		if (err) {
			resolver.reject(false);
		}

		resolver.resolve(true);
	});

	return resolver.promise;
}

function getChannelVideos(oauth2Client, nextPageToken, callback) {

	var searchParams = {
		part: 'snippet',
		forMine: 'true',
		maxResults: 50,
		type: 'video',
		fields: 'items(id,snippet),nextPageToken',
		auth: oauth2Client
	};

	if (nextPageToken) {
		searchParams.pageToken = nextPageToken;
	}

	youtube.search.list(searchParams, function (err, response) {
		for (var i in response.items) {
			videoList.push({
				trackId: response.items[i].id.videoId,
				trackName: response.items[i].snippet.title
			});
		}

		if (!response.nextPageToken) {
			callback(videoList);
		} else {
			console.log('Page: ' + pageList);
			pageList++;
			getChannelVideos(oauth2Client, response.nextPageToken, callback);
		}
	});
}

function updateChannelVideo(channelId, videos) {
	var resolver = promise.pending();

	ChannelInfo.update({ channelId: channelId }, { $set: { trackList: videos } }, function (err) {

		if (err) {
			resolver.reject(false);
		}

		resolver.resolve(true);
	});

	return resolver.promise;
}

exports.getImportYouTubeChannel = function (req, res) {
	var redirect = '/acc/youtube/view-report?import-status=';
	var code = 500;
	oauth2Client.getToken(req.query.code, function (err, tokens) {
		if (err) {
			res.redirect(redirect + code);
		}
		oauth2Client.setCredentials(tokens);

		getYouTubeChannelInfo(oauth2Client).then(function (channel, err) {
			if (err) {
				res.redirect(redirect + code);
			}
			saveChannelInfo(channel).then(function (sts, err) {
				if (err) {
					res.redirect(redirect + code);
				}
				getChannelVideos(oauth2Client, null, function (allVideos, err) {
					if (err) {
						res.redirect(redirect + code);
					}
					updateChannelVideo(channel.channelId, allVideos).then(function (sts, err) {
						if (err) {
							res.redirect(redirect + code);
						}

						var trackId = '';
						var video_list = [];
						for (var i in allVideos) {
							if (trackId.length > 0) {
								trackId += ',';
							}
							trackId += allVideos[i].trackId;
							if (i % 50 === 1 || i === allVideos.length - 1) {
								video_list.push(trackId);
								trackId = '';
							}
						}

						getYouTubeData(video_list, oauth2Client).then(function (videos, err) {
							if (err) {
								res.redirect(redirect + code);
							}

							saveVideoDateTags(videos, channel.channelId).then(function (response, err) {
								if (err) {
									res.redirect(redirect + code);
								}

								code = 200;
								return res.redirect(redirect + code);

								/*
        allVideos = allVideos.map((item) => {
        	return {trackId: item.trackId};
        });
        var input = {album: channel.channelId, addTracks: allVideos,userId: req.user._id, role:'label', commission: 100};
        TrackShareUtil.setTypeShare('youtube');
        TrackShareUtil.insertMultipleShares(input)
        	.then(function() {
        		code = 200;
        		return res.redirect(redirect+code);
        	}).catch(function(err) {
        	return res.redirect(redirect+code);
        });
        */
							});
						});
					});
				});
			});
		});
	});
};

exports.getAllChannelIDNames = function () {
	var deferred = Q.defer();
	ChannelInfo.find().select({ channelId: 1, name: 1 }).exec(function (err, channels) {
		if (err) {
			deferred.reject(false);
		}
		var channelMap = {};
		channels.forEach(function (channel) {
			channelMap[channel.channelId] = channel.name;
		});
		deferred.resolve(channelMap);
	});
	return deferred.promise;
};

exports.getAllChannelInfo = function () {
	var deferred = Q.defer();
	ChannelInfo.find().exec(function (err, channels) {
		if (err) {
			deferred.reject(false);
		}
		var channelMap = {};
		channels.forEach(function (channel) {
			if (!channelMap[channel.channelId]) {
				channelMap[channel.channelId] = {};
			}
			channel.trackList.forEach(function (video) {
				channelMap[channel.channelId][video.trackId] = video.trackName;
			});
		});

		deferred.resolve(channelMap);
	});
	return deferred.promise;
};

exports.getChannelGroupByVideos = function () {
	var deferred = Q.defer();
	ChannelInfo.find().exec(function (err, channels) {
		if (err) {
			deferred.reject(false);
		}
		var channelMap = {};
		channels.forEach(function (channel) {
			channel.trackList.forEach(function (video) {
				channelMap[video.trackId] = {
					channelId: channel.channelId,
					name: channel.name
				};
			});
		});

		deferred.resolve(channelMap);
	});
	return deferred.promise;
};

exports.getChannelVideos = function (channelId) {
	var deferred = Q.defer();

	ChannelInfo.findOne({ channelId: channelId }).select({ trackList: 1, channelId: 1, name: 1 }).exec(function (err, channels) {
		if (err) {
			deferred.reject(false);
		}

		deferred.resolve(channels);
	});

	return deferred.promise;
};
