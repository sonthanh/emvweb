'use strict';

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId;

exports.index = function (req, res) {
  if (process.env.isOnProcessing) {
    res.render('unavailable');
  } else {
    res.render('index', {
      user: req.user || null,
      request: req
    });
  }
};
