'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    errorHandler = require('./errors.server.controller'),
    _ = require('lodash'),
    csv = require('fast-csv'),
    promise = require('bluebird'),
    ChannelInfoCtrl = require('./channel-info.server.controller.js'),
    YoutubeSale = mongoose.model('YoutubeSale'),
    VideoShare = mongoose.model('VideoShare'),
    videoShareCtrl = require('./video-share.server.controller.js'),
    userCtrl = require('./users.server.controller.js'),
    VideoShareUtil = require('./utils/videoshare/videoshare.util'),
    ChannelInfo = mongoose.model('ChannelInfo'),
    validator = require('validator'),
    Q = require('q'),
    underscore = require('underscore')._;

var User = mongoose.model('User');
var Core = require("./services/core.js");

function saveYouTubeSaleData(saleRecords) {
    var resolver = promise.pending();
    var cnt = 0;
    for (var i in saleRecords) {
        var record = saleRecords[i];
        YoutubeSale.update({ videoId: record.videoId, import_date: record.import_date }, record, { upsert: true }, function (err) {

            if (err) {
                console.log(err);
                resolver.reject(false);
            }

            cnt++;

            if (cnt === saleRecords.length) {
                resolver.resolve(true);
            }
        });
    }

    return resolver.promise;
}

function getFileSize(path) {
    var resolver = promise.pending();
    var saleRecords = [];
    csv.fromPath(path, { encoding: 'utf8', headers: true }).on('data', function (data) {
        saleRecords.push({});
    }).on('end', function () {
        resolver.resolve(saleRecords.length);
    });

    return resolver.promise;
}

/**
 * Sale import report
 */
function groupArrIntoChunks(arr, len) {
    var chunks = [],
        i = 0,
        n = arr.length;
    while (i < n) {
        chunks.push(arr.slice(i, i += len));
    }
    return chunks;
}

var updateCreateProfitDefered = Q.defer();
function updateCreateProfit(index, totalSaleRecords, allTrackShare) {
    global.gc();
    if (index === totalSaleRecords.length) {
        return updateCreateProfitDefered.resolve(true);
    }

    VideoShareUtil.updateProfit(allTrackShare, totalSaleRecords[index]).then(function () {
        console.log('Start saving report to db...');
        console.log("Process Record: " + index);
        saveYouTubeSaleData(totalSaleRecords[index]).then(function (sts) {
            return updateCreateProfit(index + 1, totalSaleRecords, allTrackShare);
        })['catch'](function (err) {
            delete process.env.isOnProcessing;
        });
    });

    return updateCreateProfitDefered.promise;
}

exports.importReport = function (req, res) {
    process.env.isOnProcessing = true;
    var import_date = Date.parse(req.headers['import-date']);
    import_date = new Date(import_date);
    import_date.setDate(25);
    YoutubeSale.remove({ $where: 'return this.import_date.getMonth() ===' + import_date.getMonth() }).then(function () {
        var file = req.files.file;
        if (!file) {
            return res.status(400).send({ message: 'File Not Exist' });
        }

        var saleRecords = [];
        getFileSize(file.path).then(function (length) {
            csv.fromPath(file.path, { encoding: 'utf8', headers: true }).on('record', function (data) {
                saleRecords.push({
                    videoId: data['Video ID'].trim(),
                    track_name: data['Video'],
                    original_commission: parseFloat(req.headers['original-commission']),
                    original_profit: parseFloat(data['Total estimated earnings (USD)']),
                    currency: '$',
                    import_date: import_date
                });

                if (saleRecords.length === length) {
                    var channelId = '';
                    var album_name = '';
                    if (saleRecords.length > 0) {
                        // update the share profit if we have VideoShare
                        ChannelInfoCtrl.getChannelGroupByVideos().then(function (channelMap) {
                            saleRecords.forEach(function (row) {
                                row.profit = row.original_commission * row.original_profit / 100;
                                if (channelMap[row.videoId]) {
                                    row.channelId = channelMap[row.videoId].channelId;
                                    row.album_name = channelMap[row.videoId].name;
                                    channelId = channelMap[row.videoId].channelId;
                                    album_name = channelMap[row.videoId].name;
                                } else {
                                    row.channelId = channelId;
                                    row.album_name = album_name;
                                    row.isDeleted = true;
                                }
                            });
                            res.status(200).send();
                            VideoShare.find().exec().then(function (allVideoShare) {
                                updateCreateProfit(0, groupArrIntoChunks(saleRecords, 1000), allVideoShare).then(function () {
                                    console.log('DONE!!!!!!!!!');
                                    delete process.env.isOnProcessing;
                                })['catch'](function (err) {
                                    console.log(err);
                                    delete process.env.isOnProcessing;
                                });
                            });
                        })['catch'](function (err) {
                            console.log(err);
                            delete process.env.isOnProcessing;
                        });
                    } else {
                        console.log(err);
                        delete process.env.isOnProcessing;
                    }
                }
            });
        });
    });
};

exports.list = function (req, res) {
    YoutubeSale.find().sort('import_date').exec(function (err, videos) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(videos);
        }
    });
};

function getSaleByChannel() {
    var project = {
        channelId: 1,
        videoId: 1,
        channelName: 1,
        profit: 1,
        currency: 1
    };

    var group = {};
    group._id = {
        channelId: '$channelId',
        channelName: '$album_name',
        currency: '$currency'
    };
    group.profit = { $sum: '$profit' };
    group.video = { $sum: 1 };

    return promise.cast(YoutubeSale.aggregate({ $project: project }, { $group: group }).exec());
}

function getTotalChannelVideo(channelId, idx) {
    var resolver = promise.pending();

    ChannelInfo.find({ channelId: channelId }).select({ videoList: 1 }).exec(function (err, videoList) {

        if (err) {
            resolver.reject(false);
        }

        resolver.resolve({ count: videoList[0].videoList.length, idx: idx });
    });

    return resolver.promise;
}

exports.getSaleByChannel = function (req, res) {
    getSaleByChannel().then(function (data, err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        var cnt = data.length;
        for (var i in data) {
            data[i].channelName = data[i]._id.channelName;
            data[i].currency = data[i]._id.currency;
            getTotalChannelVideo(data[i]._id.channelId, i).then(function (response) {
                data[response.idx].video = response.count;
                delete data[response.idx]._id.channelId;
                cnt--;
                if (cnt === 0) {
                    return res.status(200).send({
                        data: data
                    });
                }
            });
        }
    });
};

exports.channelVideoShareInfo = function (req, res) {
    userCtrl.getAllUserIDNames().then(function (userMap) {
        videoShareCtrl.getAllCollaborators(req.user).then(function (channelGroups) {
            channelGroups.forEach(function (channel) {
                channel.channelId = channel._id;
                channel.users = [];
                var userArrs = {};
                channel.userRoles.forEach(function (userRole) {
                    if (!userArrs[userRole.user]) {
                        userArrs[userRole.user] = {};
                    }
                    userArrs[userRole.user][userRole.role] = 1;
                });
                for (var userId in userArrs) {
                    var roleObjs = userArrs[userId];
                    var roleArrs = [];
                    for (var role in roleObjs) {
                        roleArrs.push({ role: role });
                    }
                    channel.users.push({
                        userId: userId,
                        userName: userMap[userId],
                        roles: roleArrs
                    });
                }

                delete channel._id;
                delete channel.userRoles;
            });

            res.jsonp({ data: channelGroups });
        });
    });
};

var buildRegex = function buildRegex(pattern, listSchema) {
    var regex = new RegExp(pattern, 'i');
    return listSchema.map(function (schema) {
        var newObj = {};
        newObj[schema] = regex;
        return newObj;
    });
};

var queryCondition = function queryCondition(albumName, userId, role, searchText, startDate, endDate) {
    var cond = {};
    if (albumName !== '') {
        cond.album_name = albumName;
    }
    if (searchText) {
        cond.$or = buildRegex(searchText.toString().trim(), ['album_name', 'track_name']);
    }
    if (startDate && endDate) {
        startDate = new Date(parseInt(startDate));
        endDate = new Date(parseInt(endDate));
        if (startDate.getDate() > 3) {
            startDate.setDate(3);
            startDate.setMonth(startDate.getMonth() + 1);
        }
        if (endDate.getMonth === startDate.getMonth) {
            var int_d = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 1);
            endDate = new Date(int_d - 1);
        }
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        endDate.setHours(23);
        endDate.setMinutes(59);
        endDate.setSeconds(59);
        cond.import_date = { $gte: startDate, $lt: endDate };
    }
    cond.allCommissions = { $not: { $size: 0 } };
    if (userId !== '' && role !== '') {
        // match specific user and specific role
        cond.allCommissions.$elemMatch = { userId: new ObjectId(userId), 'detail.role': role };
    } else if (userId !== '' && role === '') {
        // match specific user and All role
        cond.allCommissions.$elemMatch = { userId: new ObjectId(userId) };
    } else if (userId === '' && role !== '') {
        // match All user and specific role
        cond.allCommissions.$elemMatch = { 'detail.role': role };
    }

    return cond;
};
exports.queryCondition = queryCondition;

var aggregateArr = function aggregateArr(queryCond, user) {
    // This is Based Aggregation Framework

    var deferred = Q.defer();

    var project1 = {
        ID: '$_id',
        album_name: 1,
        track_name: 1,
        profit: 1,
        import_date: 1,
        allCommissions: 1
    };

    var project2 = underscore.extend({}, project1);
    project2.userId = '$allCommissions.userId';
    project2.totalProfit = '$allCommissions.totalValue';
    project2.detail = '$allCommissions.detail';

    var queryCond2 = {};
    if (queryCond.allCommissions.$elemMatch) {
        if (queryCond.allCommissions.$elemMatch.userId) {
            queryCond2.userId = queryCond.allCommissions.$elemMatch.userId;
        }

        if (queryCond.allCommissions.$elemMatch.detail) {
            queryCond2.detail.role = queryCond.allCommissions.$elemMatch.detail.role;
        }
    }

    var project3 = underscore.extend({}, project2);
    delete project3.detail;
    delete project3.allCommissions;
    project3.trackId = '$detail.trackId';
    project3.role = '$detail.role';
    project3.orgProfit = '$detail.value';
    project3.netProfit = '$detail.netValue';
    project3.commission = '$detail.commission';
    project3.ispaid = '$detail.ispaid';

    videoShareCtrl.getYouTubeAccessCond(user).then(function (cond, err) {

        if (err) {
            deferred.reject(false);
        }

        deferred.resolve([{ $match: queryCond }, { $project: project1 }, { $unwind: '$allCommissions' }, { $project: project2 }, { $match: queryCond2 }, { $unwind: '$detail' }, { $project: project3 }, { $match: cond }]);
    });

    return deferred.promise;
};

var aggregateGroupCommission = function aggregateGroupCommission(queryCond, groupName, role, user) {
    // This is extended from Based Framework
    var deferred = Q.defer();

    aggregateArr(queryCond, user).then(function (aggregate) {
        var match = { $match: {} };
        if (role !== '') {
            match.$match.role = role;
        }

        var project = { $project: {
                ID: 1,
                role: 1,
                profit: { $divide: [{ $multiply: ['$orgProfit', 100] }, '$commission'] },
                netProfit: 1
            } };
        project.$project[groupName] = 1;

        var group = { $group: {
                _id: { ID: '$ID', role: '$role' },
                role: { $first: '$role' },
                profit: { $sum: '$profit' },
                commission: { $sum: '$netProfit' }
            } };
        group.$group._id[groupName] = '$' + groupName;

        var group1 = { $group: {
                _id: { role: '$role' },
                role: { $first: '$role' },
                profit: { $sum: '$profit' },
                commission: { $sum: '$commission' }
            } };
        group1.$group._id[groupName] = '$_id.' + groupName;
        group1.$group.name = { $first: '$_id.' + groupName };

        aggregate.push(match);
        aggregate.push(project);
        aggregate.push(group);
        aggregate.push(group1);

        YoutubeSale.aggregate(aggregate).exec(function (err, data) {
            if (err) {
                deferred.reject(false);
            }

            deferred.resolve(data);
        });
    });

    return deferred.promise;
};

exports.listCommissionGroup = function (req, res) {
    var channelName = req.query.channelName;
    var userId = req.query.userId;
    var role = req.query.role;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;

    var sortingObj = function sortingObj(a, b) {
        if (parseInt(req.query.isAscending) > 0) {
            if (a[req.query.sort] >= b[req.query.sort]) return -1;
            return 1;
        }
        if (a[req.query.sort] <= b[req.query.sort]) return -1;
        return 1;
    };
    var searchText = req.query.searchText;

    var page = req.query.page;
    var numPerPage = req.query.numPerPage;
    var skip = (page - 1) * numPerPage;

    // build Mongo query
    var cond = queryCondition(channelName, userId, role, searchText, startDate, endDate);

    aggregateGroupCommission(cond, req.query.groupName, role, req.user).then(function (data) {
        data.sort(sortingObj);
        res.jsonp({ data: data.slice(skip, parseInt(skip) + parseInt(numPerPage)), total: data.length });
    });
};

var aggregateVideoCommission = function aggregateVideoCommission(queryCond, role, user) {
    // This is extended from Based Framework
    var deferred = Q.defer();

    aggregateArr(queryCond, user).then(function (aggregate) {
        var match = { $match: {} };
        if (role !== '') {
            match.$match.role = role;
        }

        var group = { $group: {
                _id: { ID: '$ID', videoName: '$track_name' },
                channelName: { $first: '$album_name' },
                profit: { $first: '$profit' },
                commission: { $sum: '$netProfit' }
            } };

        var group1 = { $group: {
                _id: { videoName: '$_id.videoName' },
                videoName: { $first: '$_id.videoName' },
                channelName: { $first: '$channelName' },
                profit: { $sum: '$profit' },
                commission: { $sum: '$commission' }
            } };
        aggregate.push(match);
        aggregate.push(group);
        aggregate.push(group1);

        YoutubeSale.aggregate(aggregate).exec(function (err, data) {
            if (err) {
                deferred.reject(false);
            }

            deferred.resolve(data);
        });
    });

    return deferred.promise;
};

exports.listCommissionGroupByVideo = function (req, res) {
    //console.log(req.query);
    var channelName = req.query.channelName;
    var userId = req.query.userId;
    var role = req.query.role;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;
    var type = req.query.type;

    var sortingObj = function sortingObj(a, b) {
        if (parseInt(req.query.isAscending) > 0) {
            if (a[req.query.sort] >= b[req.query.sort]) return -1;
            return 1;
        }
        if (a[req.query.sort] <= b[req.query.sort]) return -1;
        return 1;
    };
    var searchText = req.query.searchText;

    var page = req.query.page;
    var numPerPage = req.query.numPerPage;
    var skip = (page - 1) * numPerPage;

    // build Mongo query
    var cond = queryCondition(channelName, userId, role, searchText, startDate, endDate);
    if (type === 'tabDeleteVideo') {
        cond.isDeleted = true;
        delete cond.allCommissions;
        YoutubeSale.find(cond).select({ album_name: 1, track_name: 1, profit: 1 }).then(function (data) {
            var response = [];
            data.forEach(function (row) {
                response.push({
                    commission: row.profit,
                    videoName: row.track_name,
                    channelName: row.album_name,
                    profit: row.profit,
                    _id: row._id
                });
            });
            response.sort(sortingObj);

            res.jsonp({ data: response.slice(skip, parseInt(skip) + parseInt(numPerPage)), total: response.length });
        });
    } else {
        aggregateVideoCommission(cond, role, req.user).then(function (data) {
            data.sort(sortingObj);
            res.jsonp({ data: data.slice(skip, parseInt(skip) + parseInt(numPerPage)), total: data.length });
        });
    }
};

exports.getCommissionSummary = function (req, res) {
    var albumName = req.query.channelName;
    var userId = req.query.userId;
    var role = req.query.role;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;

    var cond = queryCondition(albumName, userId, role, undefined, startDate, endDate);
    var newCnd = underscore.extend({}, cond);
    aggregateArr(cond, req.user).then(function (aggregate) {
        var group = { $group: {
                _id: { ID: '$ID', ispaid: '$ispaid' },
                commission: { $sum: '$netProfit' },
                ispaid: { $first: '$ispaid' }
            } };

        aggregate.push(group);
        newCnd.isDeleted = true;
        delete newCnd.allCommissions;
        YoutubeSale.find(newCnd).select({ profit: 1 }).then(function (deletedVideos) {
            var removedProfit = 0;
            deletedVideos.forEach(function (row) {
                removedProfit += row.profit;
            });
            YoutubeSale.aggregate(aggregate).exec(function (err, response) {
                var data = {
                    commission: 0,
                    paid: 0,
                    balance: 0,
                    quantity: 0
                };
                if (req.user.roles.indexOf('admin') >= 0) {
                    data.removedProfit = removedProfit;
                    data.removedQuantity = deletedVideos.length;
                }
                response.forEach(function (value) {
                    data.quantity += 1;
                    data.commission += value.commission;
                    if (value._id.ispaid) {
                        data.paid += value.commission;
                    } else {
                        data.balance += value.commission;
                    }
                });

                res.jsonp({ data: data });
            });
        });
    });
};

exports.getChartSummarizedByMonth = function (req, res) {
    var albumName = req.query.channelName;
    var userId = req.query.userId;
    var role = req.query.role;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;

    var cond = queryCondition(albumName, userId, role, undefined, startDate, endDate);
    aggregateArr(cond, req.user).then(function (aggregate) {
        var group = { $group: {
                _id: { sale_date: '$import_date' },
                commission: { $sum: '$netProfit' }
            } };
        aggregate.push(group);

        YoutubeSale.aggregate(aggregate).exec(function (err, data) {
            data.sort(function (a, b) {
                if (a._id.sale_date >= b._id.sale_date) {
                    return 1;
                }
                return -1;
            });
            res.jsonp({ data: data });
        });
    });
};

exports.getUserEarningByMonth = function (req, res) {
    var albumName = req.query.albumName;
    var userId = req.query.userId;
    var role = req.query.role;

    var sortingObj = function sortingObj(a, b) {
        if (parseInt(req.query.isAscending) > 0) {
            if (a[req.query.sort] >= b[req.query.sort]) return -1;
            return 1;
        }
        if (a[req.query.sort] <= b[req.query.sort]) return -1;
        return 1;
    };
    var searchText = req.query.searchText;

    var page = req.query.page;
    var numPerPage = req.query.numPerPage;
    var skip = (page - 1) * numPerPage;

    var cond = queryCondition(albumName, userId, role, searchText);
    aggregateArr(cond, req.user).then(function (aggregate) {
        var match = { $match: {} };
        if (role !== '') {
            match.$match.role = role;
        }

        var group = { $group: {
                _id: { sale_date: '$import_date', album_name: '$album_name', user: '$userId', role: '$role', ispaid: '$ispaid' },
                sale_date: { $first: '$import_date' },
                album_name: { $first: '$album_name' },
                user_id: { $first: '$userId' },
                role: { $first: '$role' },
                status: { $first: '$ispaid' },
                commission: { $sum: '$netProfit' }
            } };
        aggregate.push(match);
        aggregate.push(group);

        YoutubeSale.aggregate(aggregate).exec(function (err, data) {
            if (err) {
                return res.status(400).send();
            } else {
                userCtrl.getAllUserIDNames().then(function (userMap) {
                    var totalEarning = 0,
                        totalUnpaid = 0;
                    data.forEach(function (item) {
                        item.user_name = userMap[item.user_id];
                        totalEarning += item.commission;
                        if (!item.status) {
                            totalUnpaid += item.commission;
                        }
                        delete item._id;
                    });

                    var dateRangeSel = [];
                    var isInclude = {};
                    data.forEach(function (item) {
                        if (!isInclude[item.sale_date]) {
                            dateRangeSel.push({
                                text: item.sale_date,
                                value: item.sale_date.getTime()
                            });
                        }
                        isInclude[item.sale_date] = true;
                    });

                    dateRangeSel.sort(function (a, b) {
                        if (a.value >= b.value) {
                            return 1;
                        }
                        return -1;
                    });

                    var startDate = parseInt(req.query.startDate);
                    var endDate = parseInt(req.query.endDate);
                    data = data.filter(function (item) {
                        var rs = true;
                        if (startDate) {
                            rs = rs && item.sale_date.getTime() >= startDate;
                        }
                        if (endDate) {
                            rs = rs && item.sale_date.getTime() <= endDate;
                        }
                        return rs;
                    });

                    data.sort(sortingObj);
                    res.jsonp({
                        data: data.slice(skip, parseInt(skip) + parseInt(numPerPage)),
                        dateRangeSel: dateRangeSel,
                        total: data.length,
                        totalEarning: totalEarning,
                        totalUnpaid: totalUnpaid
                    });
                });
            }
        });
    });
};

var getUserClipMapping = function getUserClipMapping(user, isAdmin) {
    var allUserPromise = Q.defer();
    if (isAdmin) {
        User.find({}).exec().then(function (usersDB) {
            return allUserPromise.resolve(usersDB);
        }, function (err) {
            return allUserPromise.reject(err);
        });
    } else {
        allUserPromise.resolve([user]);
    }
    var deferred = Q.defer();
    allUserPromise.promise.then(function (users) {
        ChannelInfo.find({}).exec().then(function (allChannel) {
            var queryVideoShare = users.map(function (user) {
                return { userId: user._id };
            });
            VideoShare.find({ $or: queryVideoShare }).exec().then(function (allUserClipShare) {
                var mapping = [];
                users.forEach(function (user) {
                    var userClipShare = allUserClipShare.filter(function (share) {
                        return Core.compareObjId(share.userId, user._id);
                    });
                    var listVisibleChannel = [];
                    allChannel.forEach(function (channel) {
                        var visibleClips = channel.trackList.filter(function (track) {
                            var checkingTag = track.tags.indexOf(user.email) !== -1;
                            var clipOfUser = userClipShare.filter(function (share) {
                                return share.albumId === channel._id && share.trackId === track.trackId;
                            });
                            return checkingTag || clipOfUser.length > 0;
                        });
                        if (visibleClips.length > 0) {
                            listVisibleChannel.push({
                                _id: channel._id,
                                channelId: channel.channelId,
                                name: channel.name,
                                trackList: visibleClips
                            });
                        }
                    });

                    if (listVisibleChannel.length > 0) {
                        mapping.push({
                            _id: user._id,
                            displayName: user.displayName,
                            albums: listVisibleChannel
                        });
                    }
                });
                deferred.resolve(mapping);
            });
        });
    }, function (err) {
        return deferred.reject(err);
    });
    return deferred.promise;
};

exports.getAllTaggedVideo = function (user, channelId) {
    var deferred = Q.defer();
    getUserClipMapping({}, true).then(function (mapping) {
        var userResult = mapping.filter(function (item) {
            return Core.compareObjId(item._id, user._id);
        });
        if (userResult.length === 0) {
            deferred.resolve([]);return;
        }

        var videoResult = userResult[0].albums.filter(function (item) {
            return Core.compareObjId(item.channelId, channelId);
        });
        if (videoResult.length === 0) {
            deferred.resolve([]);return;
        }

        deferred.resolve(videoResult[0].trackList);
    });
    return deferred.promise;
};

exports.getAllUserClip = function (req, res) {
    var thisUser = req.user;
    var isAdmin = thisUser.roles.indexOf('admin') >= 0;
    getUserClipMapping(thisUser, isAdmin).then(function (mapping) {
        return res.jsonp({ data: mapping });
    });
};

exports.searchVideo = function (req, res) {
    var search = req.query.search;
    var channelId = req.query.channelId;
    var limit = parseInt(req.query.limit);
    if (isNaN(limit)) {
        limit = 5;
    }
    //channelId = 'UCrNRiQqbEGUtV17i4aiwGmg';
    var escapeRegExp = function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    };
    var matchAllRegex = function matchAllRegex(listRegex, matchingString) {
        return listRegex.every(function (regex) {
            return matchingString.match(regex);
        });
    };
    ChannelInfoCtrl.getChannelVideos(channelId).then(function (channel) {
        if (!channel) {
            res.jsonp({ 'ret_code': 2 });
            return;
        }
        var filterList = channel.trackList.filter(function (track) {
            if (search && search !== '') {
                var multiRegex = search.split(/\s+/).map(function (word) {
                    return new RegExp(escapeRegExp(word), 'i');
                });
                return matchAllRegex(multiRegex, track.trackName);
            } else {
                return true;
            }
        });
        filterList = filterList.slice(0, limit);
        res.jsonp({ 'ret_code': 0, 'data': filterList });
    }, function (err) {
        res.jsonp({ 'ret_code': 1 });
    });
};
