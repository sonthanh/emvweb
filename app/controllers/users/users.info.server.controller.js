'use strict';

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Q = require('q');

exports.getAllUserIDNames = function getAllUserIDNames() {
	var deferred = Q.defer();
	User.find().select({ displayName: 1 }).exec(function (err, users) {
		if (err) {
			deferred.reject(false);
		}
		var userMap = {};
		users.forEach(function (user) {
			userMap[user._id] = user.displayName;
		});
		deferred.resolve(userMap);
	});
	return deferred.promise;
};
