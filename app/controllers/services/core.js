'use strict';

var Q = require('q');
var extend = require('util')._extend;
var underscore = require('underscore')._;

/**********************************************************************************************************************
 *
 * UTILITY FUNCTION
 *
 **********************************************************************************************************************/

function shallowCopy(json) {
    return extend({}, json);
}

function runSequenceRecursive(listObj, action, aggregateResult, defer) {
    if (listObj.length > 0) {
        var head = listObj[0];
        var tail = underscore.rest(listObj);
        action(head).then(function (result) {
            aggregateResult.push(result);
            runSequenceRecursive(tail, action, aggregateResult, defer);
        }, function (err) {
            defer.reject({ 'error obj': head, 'error message': JSON.stringify(err) });
        });
    } else {
        defer.resolve(aggregateResult);
    }
}

function runSequence(listObj, action) {
    var defer = Q.defer();
    runSequenceRecursive(listObj, action, [], defer);
    return defer.promise;
}

function genericErrHandler(promise, jsonMsg, reject, tagMsg) {
    return function (error) {
        var errMsg = tagMsg + ' ==>\n' + error.stack;
        console.log(errMsg);
        if (reject === 'reject') {
            promise.reject({ error: error.stack });
        } else {
            promise.resolve(jsonMsg);
        }
    };
}

function validateCommission(commission) {
    var value = parseInt(commission);
    return !isNaN(value) && value >= 0 && value <= 100;
}

function getObjId(obj) {
    if (typeof obj === 'undefined') {
        return obj;
    } else if (typeof obj._id === 'undefined') {
        return obj;
    } else {
        return obj._id;
    }
}

function flattenArray(childRemoval) {
    return childRemoval.reduce(function (a, b) {
        return a.concat(b);
    }, []);
}

function extractField(obj, path) {
    return path.split(".").reduce(function (o, p) {
        return o && o[p];
    }, obj);
}

function getUndefined(obj, listPath) {
    return listPath.filter(function (path) {
        var value = extractField(obj, path);
        return typeof value === 'undefined';
    });
}

function getTotalUndefined(listObj, schema) {
    var totalUndefined = listObj.map(function (obj) {
        return getUndefined(obj, schema);
    });
    return flattenArray(totalUndefined);
}

function compareObjId(o1, o2) {
    if (typeof o1 === 'undefined' || typeof o2 === 'undefined') {
        return false;
    }
    var obj1 = getObjId(o1);
    var obj2 = getObjId(o2);
    var str1 = obj1.str || obj1.toString();
    var str2 = obj2.str || obj2.toString();
    return str1 === str2;
}

function getTrackName(trackId, trackList) {
    var foundTrack = trackList.filter(function (track) {
        return compareObjId(track.trackId, trackId);
    });
    return foundTrack[0] && foundTrack[0].trackName || '';
}
/*
function getVideoName(videoId, videoList) {
    var foundVideo = videoList.filter(function (video) {
        return video.videoId === videoId;
    });
    return (foundVideo[0] && foundVideo[0].videoName) || '';
}
*/

function wrapperQPromise(p, mapData) {
    var deferred = Q.defer();
    var successHandler = function successHandler(data) {
        return deferred.resolve(data);
    };
    var rejectHandler = function rejectHandler(err) {
        return deferred.reject(err);
    };
    p.then(successHandler, rejectHandler);
    return deferred.promise;
}

function orQueryDB(DB, arrQuery, populatePath) {
    if (arrQuery.length === 0) {
        var deferred = Q.defer();
        deferred.resolve([]);
        return deferred.promise;
    } else {
        if (typeof populatePath === 'undefined') {
            return wrapperQPromise(DB.find({ $or: arrQuery }).exec());
        } else {
            return wrapperQPromise(DB.find({ $or: arrQuery }).populate(populatePath).exec());
        }
    }
}

exports.shallowCopy = shallowCopy;
exports.runSequence = runSequence;
exports.genericErrHandler = genericErrHandler;
exports.validateCommission = validateCommission;

exports.getObjId = getObjId;
exports.flattenArray = flattenArray;
exports.compareObjId = compareObjId;
exports.getTotalUndefined = getTotalUndefined;
exports.getTrackName = getTrackName;
//exports.getVideoName = getVideoName;
exports.wrapperQPromise = wrapperQPromise;
exports.orQueryDB = orQueryDB;
