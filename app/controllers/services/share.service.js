'use strict';

var mongoose = require('mongoose');
var typeShare = 'track';
var ShareDB = mongoose.model('TrackShare');
var CommissionDB = mongoose.model('SaleCommission');
var ShareInfoDB = mongoose.model('AlbumInfo');
var Q = require('q');
var Core = require('./core.js');
var YoutubeController = require('../youtube.server.controller.js');

/**********************************************************************************************************************
 *
 * API FUNCTION
 *
 **********************************************************************************************************************/

function setTypeShare(type) {
    typeShare = type;
    if (typeShare === 'track') {
        ShareDB = mongoose.model('TrackShare');
        CommissionDB = mongoose.model('SaleCommission');
        ShareInfoDB = mongoose.model('AlbumInfo');
    } else if (typeShare === 'youtube') {
        ShareDB = mongoose.model('VideoShare');
        CommissionDB = mongoose.model('YoutubeSale');
        ShareInfoDB = mongoose.model('ChannelInfo');
    }
    console.log('[Share.Service] Set typeShare to: ' + typeShare);
}

function findAllShareInfo() {
    return Core.wrapperQPromise(ShareInfoDB.find({}).sort({ name: 1 }).exec());
}

function aggregateShare(query) {
    return ShareDB.aggregate(query).exec();
}

function findShares(input) {
    var album = input.album;
    var addTracks = input.addTracks;
    var userId = input.userId;
    var role = input.role;

    var query = addTracks.map(function (track) {
        return {
            trackId: track.trackId,
            albumId: Core.getObjId(album),
            userId: Core.getObjId(userId),
            role: role
        };
    });
    console.log('[Share.Service] Finding share with query: ' + JSON.stringify(query));
    return Core.orQueryDB(ShareDB, query);
}

function populateShareInfo(sharePromise) {
    return sharePromise;
    /*
    if (typeShare === 'youtube') {
        // because albumId field in Youtube Share is not reference to _id of ChannelInfo
        // we need to populate manually to link with field channelId in ChannelInfo
        var deferred = Q.defer();
        var handleError = err => deferred.reject(err);
        var catchError = Core.genericErrHandler(deferred, {}, 'reject', 'ERROR in populateChannel()');
        sharePromise.then(function (listShare) {
            console.log('[Share.Service] Populate shareInfo with length: ' + listShare.length);
            var queryChannelInfo = listShare.map(share => ({channelId: share.albumId}));
            Core.orQueryDB(ShareInfoDB, queryChannelInfo).then(function (listChannel) {
                var shareWithPopulate = listShare.map(share => {
                    // find the correct channel with its videoId
                    var channel = listChannel.filter(channel => channel.channelId === share.albumId)[0];
                    channel._id = channel.channelId;
                    channel.trackList = channel.trackList.filter(video => video.trackId === share.trackId);
                    share.albumId = channel;
                    return share;
                });
                deferred.resolve(shareWithPopulate);
            }, handleError).catch(catchError);
        }, handleError).catch(catchError);
        return deferred.promise;
    } else {
        return sharePromise;
    }
    */
}

function findSharesByArray(arrayShares) {
    var populatePath = [{ path: 'albumId' }];
    var query = arrayShares.map(function (share) {
        return {
            trackId: share.trackId,
            albumId: Core.getObjId(share.albumId),
            userId: Core.getObjId(share.userId),
            role: share.role
        };
    });
    console.log('[Share.Service] Finding share by array with query: ' + JSON.stringify(query));
    return populateShareInfo(Core.orQueryDB(ShareDB, query, populatePath));
}

function findOneShare(shareInfo) {
    var clone = JSON.parse(JSON.stringify(shareInfo));
    if (clone.albumId) {
        clone.albumId.trackList = [];
    } else if (clone.album) {
        clone.album.trackList = [];
    }
    console.log('[Share.Service] call findOneShare() with param: ' + JSON.stringify(clone));
    var populatePath = [{ path: 'albumId' }];
    var albumId;
    if (typeof shareInfo.album !== 'undefined') {
        albumId = Core.getObjId(shareInfo.album);
    } else {
        albumId = Core.getObjId(shareInfo.albumId);
    }
    var query = {
        trackId: shareInfo.trackId,
        albumId: albumId,
        userId: Core.getObjId(shareInfo.userId),
        role: shareInfo.role
    };
    return populateShareInfo(Core.wrapperQPromise(ShareDB.findOne(query).populate(populatePath).exec()));
}

function findShareByAlbum(albumId) {
    var query = {
        albumId: Core.getObjId(albumId)
    };
    return populateShareInfo(Core.wrapperQPromise(ShareDB.find(query).select({ trackId: 1 }).exec()));
}

function findSharesWithPath(query, populatePath) {
    var sort = { 'albumId': 1 };
    populatePath.push({ path: 'albumId' });
    return populateShareInfo(Core.wrapperQPromise(ShareDB.find(query).populate(populatePath).sort(sort).exec()));
}

function copyShareToQuery(share) {
    return {
        trackId: share.trackId,
        albumId: Core.getObjId(share.albumId)
    };
}

function copyShareToClient(share) {
    var album = JSON.parse(JSON.stringify(share.albumId));
    album.trackList = album.trackList.filter(function (video) {
        return video.trackId === share.trackId;
    });
    var copiedShare = {
        albumId: album,
        trackId: share.trackId,
        albumName: album.name,
        trackName: Core.getTrackName(share.trackId, share.albumId.trackList)
    };
    //console.log('[Share.Service] Copying share to client ' + JSON.stringify(copiedShare));
    return copiedShare;
}

function insertMultipleShares(input) {
    var album = input.album;
    var addTracks = input.addTracks;
    var userId = input.userId;
    var role = input.role;
    var commission = input.commission;

    var createDB = function createDB(album, addTracks, userId, role, commission) {
        var newShares = addTracks.map(function (track) {
            return {
                trackId: track.trackId,
                albumId: Core.getObjId(album),
                userId: Core.getObjId(userId),
                role: role,
                commission: commission,
                realCommission: commission,
                netCommission: commission,
                payee: [],
                shareWith: []
            };
        });
        if (typeShare === 'track') {
            // update contributor of this track in AlbumInfo
            ShareInfoDB.findOne({ _id: Core.getObjId(album) }).exec().then(function (albumInfo) {
                addTracks.forEach(function (addTrack) {
                    // locate trackId
                    var foundTrack = albumInfo.trackList.filter(function (track) {
                        return Core.compareObjId(track.trackId, addTrack.trackId);
                    })[0];
                    var contributor = {
                        userId: Core.getObjId(userId),
                        role: role
                    };
                    if (typeof foundTrack.contributor === 'undefined') {
                        foundTrack.contributor = [contributor];
                    } else {
                        // check if this contributor exists or not
                        var checkExists = foundTrack.contributor.filter(function (userRole) {
                            return Core.compareObjId(userRole.userId, userId) && userRole.role === role;
                        });
                        if (checkExists.length === 0) {
                            foundTrack.contributor.push(contributor);
                        }
                    }
                });
                albumInfo.save(function (err) {
                    if (err) {
                        console.log('ERROR in updating contributor of AlbumInfo: ' + JSON.stringify(err.stack));return;
                    }
                    //console.log('DONE updating contributor');
                });
            });
        }
        return Core.wrapperQPromise(ShareDB.create(newShares));
    };

    if (typeShare === 'youtube' && addTracks.length === 1 && addTracks[0].trackId === 'All') {
        var deferred = Q.defer();
        YoutubeController.getAllTaggedVideo(userId, album.channelId).then(function (taggedTrackList) {
            if (taggedTrackList.length === 0) {
                deferred.reject({});
            } else {
                var success = function success(data) {
                    return deferred.resolve(data);
                };
                var error = function error(err) {
                    return deferred.reject(err);
                };
                createDB(album, taggedTrackList, userId, role, commission).then(success, error);
            }
        });
        return deferred.promise;
    } else {
        return createDB(album, addTracks, userId, role, commission);
    }
}

function insertOneShare(share) {
    var newShare = {
        trackId: share.trackId,
        albumId: Core.getObjId(share.albumId),
        userId: Core.getObjId(share.userId),
        role: share.role,
        commission: share.commission,
        netCommission: share.netCommission,
        realCommission: share.realCommission,
        payee: share.payee,
        directPayee: share.directPayee
    };
    return Core.wrapperQPromise(ShareDB.create(newShare));
}

/**
 * if not valid return list of undefined field, otherwise return empty array
 * @param listShare
 * @returns {*}
 */
function validateShares(listShare) {
    var schema = ['trackId', 'albumId', 'albumId._id', 'albumId.trackList', 'albumId.name', 'userId', 'role', 'realCommission', 'netCommission'];
    return Core.getTotalUndefined(listShare, schema);
}

/**
 *
 * @param listShare
 * @returns {*}
 */
function updateName(listShare) {
    return listShare.map(function (share) {
        share.trackName = Core.getTrackName(share.trackId, share.albumId.trackList || []);
        share.albumName = share.albumId.name || '';
        return share;
    });
}

/**
 *
 * @param listShare
 * @returns {*|promise}
 */
function findAllRelevantCommission(listShare) {
    // get all necessary query to fetch all data at once
    var query = listShare.map(function (share) {
        return {
            track_name: { $in: [share.trackName, 'N/A'] },
            album_name: share.albumName
        };
    });
    return Core.orQueryDB(CommissionDB, query);
}

function isShareRelevantToSale(share, sale) {
    if (typeShare === 'track') {
        var sameAlbum = share.albumName === sale.album_name;
        var sameTrack = share.trackName === sale.track_name;
        return sameAlbum && (sameTrack || sale.track_name === 'N/A');
    } else {
        var sameChannel = share.albumName === sale.album_name;
        var sameVideo = share.trackName === sale.track_name;
        return sameChannel && sameVideo;
    }
}

function checkIndividualSale(sale) {
    return typeShare === 'track' && sale.track_name === 'N/A';
}

function updateRealProfit(share, sale, userProfit) {
    var realProfit = sale.profit;
    if (checkIndividualSale(sale)) {
        // update sale for whole album
        realProfit = sale.profit / share.albumId.trackList.length;
        userProfit.trackId = share.trackId;
    }
    return realProfit;
}

function checkRoleInUpdateCommission(roleValue, share, sale) {
    if (checkIndividualSale(sale)) {
        return roleValue.role === share.role && Core.compareObjId(roleValue.trackId, share.trackId);
    } else {
        return roleValue.role === share.role;
    }
}

function checkRoleInRemoveCommission(roleValue, share, sale, removeRole) {
    if (checkIndividualSale(sale)) {
        return roleValue.role === removeRole && Core.compareObjId(roleValue.trackId, share.trackId);
    } else {
        return roleValue.role === removeRole;
    }
}

function logUpdateCommission(id, sale) {
    console.log(id + ' ~~> Updating this sale: (' + sale.album_name + ', ' + sale.track_name + ') with quantity=' + sale.quantity + ' and profit=' + sale.profit);
}

function logRemoveCommission(id, sale, removeUser, removeRole) {
    console.log(id + ' ~~> Removing (' + removeUser + '/' + removeRole + ') of this sale: (' + sale.album_name + ', ' + sale.track_name + ') with quantity=' + sale.quantity + ' and profit=' + sale.profit);
}

function tranformInputToUpdateCommission(input) {
    var album = input.album;
    var addTracks = input.addTracks;
    var userId = input.userId;
    var role = input.role;
    var commission = input.commission;

    return addTracks.map(function (track) {
        return { trackId: track.trackId, albumId: album, userId: Core.getObjId(userId), role: role,
            commission: commission, realCommission: commission, netCommission: commission };
    });
}

function combineParentWithColla(listCollaborator, listParent) {
    var allDetailCollaborator = [];
    for (var i = 0; i < listParent.length; i++) {
        for (var j = 0; j < listCollaborator.length; j++) {
            var parent = listParent[i];
            var collaborator = listCollaborator[j];
            allDetailCollaborator.push({
                albumId: parent.albumId,
                trackId: parent.trackId,
                parentUser: Core.getObjId(parent.userId),
                parentRole: parent.role,
                userId: Core.getObjId(collaborator.userId),
                role: collaborator.role,
                commission: collaborator.commission
            });
        }
    }
    return allDetailCollaborator;
}

function findParent(share, listParentShare) {
    var trackId = share.trackId;
    var albumId = share.albumId;
    var parentUser = share.parentUser;
    var parentRole = share.parentRole;

    var filter = listParentShare.filter(function (parent) {
        return Core.compareObjId(trackId, parent.trackId) && Core.compareObjId(albumId, Core.getObjId(parent.albumId)) && Core.compareObjId(parentUser, parent.userId) && parentRole === parent.role;
    });
    return filter[0];
}

function shareInfo2Str(shareInfo) {
    var trackId = shareInfo.trackId;
    var albumId = shareInfo.albumId;
    var userId = shareInfo.userId;
    var role = shareInfo.role;

    if (typeof shareInfo.album !== 'undefined') {
        albumId = Core.getObjId(shareInfo.album);
    } else {
        albumId = Core.getObjId(shareInfo.albumId);
    }
    return '(' + trackId + ', ' + albumId + ', ' + Core.getObjId(userId) + ', ' + role + ')';
}

function findCollaborator(allShares, share, collaborator) {
    var trackId = share.trackId;
    var albumId = share.albumId;
    var userId = collaborator.userId;
    var role = collaborator.role;

    var filter = allShares.filter(function (search) {
        return Core.compareObjId(search.trackId, trackId) && Core.compareObjId(Core.getObjId(search.albumId), Core.getObjId(albumId)) && Core.compareObjId(Core.getObjId(search.userId), Core.getObjId(userId)) && search.role === role;
    });
    return filter[0];
}

function updateCommission(query, updateOption, callback) {
    return CommissionDB.update(query, updateOption, callback);
}

exports.setTypeShare = setTypeShare;
exports.findShares = findShares;
exports.insertMultipleShares = insertMultipleShares;
exports.validateShares = validateShares;
exports.updateName = updateName;
exports.findAllRelevantCommission = findAllRelevantCommission;
exports.isShareRelevantToSale = isShareRelevantToSale;
exports.updateRealProfit = updateRealProfit;
exports.checkRoleInUpdateCommission = checkRoleInUpdateCommission;
exports.checkRoleInRemoveCommission = checkRoleInRemoveCommission;
exports.logUpdateCommission = logUpdateCommission;
exports.logRemoveCommission = logRemoveCommission;
exports.tranformInputToUpdateCommission = tranformInputToUpdateCommission;
exports.findSharesByArray = findSharesByArray;
exports.combineParentWithColla = combineParentWithColla;
exports.findParent = findParent;
exports.findOneShare = findOneShare;
exports.shareInfo2Str = shareInfo2Str;
exports.insertOneShare = insertOneShare;

exports.findSharesWithPath = findSharesWithPath;

exports.copyShareToQuery = copyShareToQuery;
exports.copyShareToClient = copyShareToClient;

exports.findCollaborator = findCollaborator;
exports.findAllShareInfo = findAllShareInfo;
exports.aggregateShare = aggregateShare;

exports.updateCommission = updateCommission;
exports.findShareByAlbum = findShareByAlbum;
