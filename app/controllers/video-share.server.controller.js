'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    users = require('./users.server.controller'),
    ObjectId = mongoose.Types.ObjectId,
    VideoShare = mongoose.model('VideoShare'),
    userCtrl = require('./users.server.controller.js'),
    Q = require('q'),
    underscore = require('underscore')._,
    VideoShareUtil = require('./utils/videoshare/videoshare.util'),
    channelInfoCtrl = require('./channel-info.server.controller.js'),
    _ = require('lodash');

var getObjId = VideoShareUtil.getObjId;
var compareObjId = VideoShareUtil.compareObjId;

exports.getAllUser = function (req, res) {
    var cond = {};
    if (!_.intersection(req.user.roles, ['admin']).length) {
        cond = { '_id': new ObjectId(req.user._id) };
    }
    User.find(cond).select({ _id: 1, displayName: 1 }).sort({ displayName: 1 }).exec().then(function (allusers) {
        res.jsonp({
            data: allusers
        });
    });
};

function getAccessCollaboratorCond(user, isDetail) {
    var deferred = Q.defer();
    if (userCtrl.isAdmin(user)) {
        deferred.resolve({});
    } else {
        var match = { $match: { userId: user._id } };
        var unwind = { $unwind: '$shareWith' };
        var group = { $group: {
                _id: { userId: '$shareWith.userId', role: '$shareWith.role' }
            } };
        if (isDetail) {
            group.$group._id.videoId = '$trackId';
            group.$group._id.channelId = '$albumId';
        }
        VideoShare.aggregate([match, unwind, group]).exec().then(function (users) {
            var cond = [{ userId: user._id }];
            users.forEach(function (item) {
                var row = {
                    userId: item._id.userId,
                    role: item._id.role
                };

                if (isDetail) {
                    row.channelId = item._id.channelId;
                    row.videoId = item._id.videoId;
                }

                cond.push(row);
            });

            deferred.resolve({ $or: cond });
        });
    }
    return deferred.promise;
}

/*
Update the field shareWith
VideoShare
    .find()
    .populate({ path: 'directPayee.userId', select: 'displayName'})
    .exec().then(function (allShare) {
        console.log('length = ' + allShare.length);
        allShare.forEach(function (share) {
            if (share.directPayee && share.directPayee.userId) {
                var query = {videoId: share.videoId, channelId: share.channelId,
                    userId: share.directPayee.userId._id, role: share.directPayee.role};
                VideoShare.findOne(query, function (err, parentShare) {
                    if (err) { console.log(err); return;}

                    var newData = {role: share.role, userId: share.userId};
                    if (parentShare.shareWith) {
                        parentShare.shareWith.push(newData);
                    } else {
                        parentShare.set('shareWith', [newData]);
                    }
                    parentShare.save(function (err) {
                        if (err) { console.log(err); return; }
                    });
                });
            }
        });
        res.jsonp({data: 1})
    });
*/

exports.getVideoShares = function (req, res) {

    var userId = req.query.userId;
    var channelId = req.query.channelId;

    getAccessCollaboratorCond(req.user, true).then(function (match) {
        if (!userCtrl.isAdmin(req.user)) {
            for (var i = match.$or.length - 1; i >= 0; i--) {
                var item = match.$or[i];
                if (!item.userId.equals(new ObjectId(userId))) {
                    match.$or.splice(i, 1);
                } else {

                    if (!item.channelId) {
                        item.channelId = new ObjectId(channelId);
                    }

                    if (!item.channelId.equals(new ObjectId(channelId))) {
                        match.$or.splice(i, 1);
                    }
                }
            }
        } else {
            match.userId = userId;
            match.channelId = channelId;
        }
        channelInfoCtrl.getChannelVideos(channelId).then(function (response) {
            VideoShare.find(match).populate([{ path: 'userId', select: 'displayName' }, { path: 'shareWith.userId', select: 'displayName' }, { path: 'payee.userId', select: 'displayName' }]).sort({ 'channelId': 1 }).exec().then(function (videoShares) {
                //console.log('1. Fetch all relating video share with query ' + JSON.stringify(match) + ' with length = ' + videoShares.length);

                var buildQuery = [];
                if (userId) {
                    // find video share of specific user
                    // build query to get the commission of every Collaborator in field shareWith
                    videoShares.forEach(function (share) {
                        if (share.shareWith) {
                            share.shareWith.forEach(function (collaborator) {
                                buildQuery.push({
                                    videoId: share.videoId,
                                    channelId: response.channelId.channelId,
                                    userId: collaborator.userId._id,
                                    role: collaborator.role
                                });
                            });
                        }
                    });
                }

                var collaboratorPromise;
                console.log('2. Prepare query to get info of all collaborator with length = ' + buildQuery.length);
                if (buildQuery.length > 0) {
                    collaboratorPromise = VideoShare.find({ $or: buildQuery }).populate([{ path: 'userId', select: 'displayName' }]).exec();
                } else {
                    var deferred = Q.defer();
                    deferred.resolve(videoShares);
                    collaboratorPromise = deferred.promise;
                }

                collaboratorPromise.then(function (foundVideoShares) {
                    var newData = videoShares.map(function (originalShare) {
                        // convert to JS Object
                        var share = originalShare.toObject();
                        share.channelId = response;

                        // get the name of videoId
                        var videoFound = share.channelId.videoList.filter(function (video) {
                            return video.videoId === share.videoId;
                        });
                        share.videoName = videoFound[0] && videoFound[0].videoName || '';

                        // create commission for every Collaborator if have any
                        if (share.shareWith) {
                            share.shareWith = share.shareWith.map(function (collaborator) {
                                // find commission of this collaborator in array `foundVideoShares`
                                var foundCollaborator = foundVideoShares.filter(function (search) {
                                    return compareObjId(search.videoId, share.videoId) && compareObjId(search.channelId._id, share.channelId._id) && compareObjId(search.userId._id, collaborator.userId._id) && search.role === collaborator.role;
                                });
                                var foundCommission = foundCollaborator[0] && foundCollaborator[0].commission || 0;

                                return {
                                    userId: collaborator.userId._id, role: collaborator.role,
                                    commission: foundCommission, userName: collaborator.userId.displayName
                                };
                            });
                        }
                        return share;
                    });

                    var result = underscore.groupBy(newData, function (elem) {
                        return elem.channelId.channelName;
                    });
                    var listChannel = {};

                    console.log(result[0]);
                    for (var channelName in result) {
                        var listVideo = {};
                        //var sorted = result[channelName].sort(function (e1, e2) { return e1.videoName.localeCompare(e2.videoName); });
                        var groupByVideo = underscore.groupBy(result[channelName], function (elem) {
                            return elem.videoName;
                        });
                        for (var videoName in groupByVideo) {
                            var listUser = {};
                            var groupByUser = underscore.groupBy(groupByVideo[videoName], function (elem) {
                                return elem.userId.displayName;
                            });
                            for (var userName in groupByUser) {
                                var newShares = groupByUser[userName].map(function (share) {
                                    return { channelId: share.channelId,
                                        videoId: share.videoId,
                                        userId: share.userId,
                                        role: share.role,
                                        commission: share.commission,
                                        shareWith: share.shareWith };
                                });
                                listUser[userName] = newShares;
                            }
                            listVideo[videoName] = listUser;
                        }
                        listChannel[channelName] = listVideo;
                    }

                    res.jsonp({ data: listChannel });
                });
            });
        });
    });
};

exports.getAllChannelUsers = function (req, res) {

    getAccessCollaboratorCond(req.user).then(function (match) {

        match = { $match: match };
        var project = { $project: { channelId: 1, userId: 1 } };
        var group = { $group: {
                _id: { userId: '$userId' },
                channels: { $addToSet: { _id: '$channelId' } }
            } };

        userCtrl.getAllUserIDNames().then(function (userMap) {
            channelInfoCtrl.getAllChannelIDNames().then(function (channelMap) {
                VideoShare.aggregate([match, project, group]).exec().then(function (data) {
                    data.forEach(function (user) {
                        user._id = user._id.userId;
                        user.displayName = userMap[user._id];
                        user.channels.forEach(function (channel) {
                            channel.name = channelMap[channel._id];
                        });
                    });

                    res.jsonp({ data: data });
                });
            });
        });
    });
};

/**
 * Handle Add the main video share request
 * @param req
 * @param res
 */
exports.addVideoShares = function (req, res) {
    /* POST Data:
     {}
     */
    console.log('[addVideoShares] Receive data from client: ');
    VideoShareUtil.addVideoShares(req.body.channelId, req.body.addVideos || [], req.body.userId, req.body.role, req.body.commission).then(function (msg) {
        res.jsonp(msg);
    });
};

/**
 * Handle delete request
 * @param req
 * @param res
 */
exports.deleteVideoShares = function (req, res) {
    console.log('[deleteVideoShares] Receive data from client: ');
    //console.log(req.body);
    var videoShare = req.body;
    console.log(videoShare.channelId);
    VideoShareUtil.removeVideoShares(videoShare.videoId, videoShare.videoName, videoShare.channelId.channelId, videoShare.channelId.channelName, getObjId(videoShare.userId), videoShare.role).then(function (msg) {
        res.jsonp(msg);
    });
};

function getChannelsAccess(user, isVideo) {
    var deferred = Q.defer();

    var project = { $project: { albumId: 1, userId: 1, role: 1 } };

    if (userCtrl.isAdmin(user)) {
        deferred.resolve(project);
    } else {
        var aggArrs = [];
        var match = { $match: { userId: user._id } };
        var group = { $group: {
                _id: '$albumId',
                userRoles: { $addToSet: { user: '$userId', role: '$role' } }
            } };
        if (isVideo) {
            project.$project.trackId = 1;
            group.$group.userRoles.$addToSet.videoId = '$trackId';
        }
        aggArrs.push(match);
        aggArrs.push(project);
        aggArrs.push(group);

        VideoShare.aggregate(aggArrs).exec(function (err, data) {
            if (err) {
                deferred.reject(false);
            }
            var channelMap = {};
            data.forEach(function (item) {
                channelMap[item._id] = item.userRoles;
            });

            deferred.resolve(channelMap);
        });
    }
    return deferred.promise;
}

/**
 * Get all user's collaborators
 */

function getAllCollaborators(user, isVideo) {
    var deferred = Q.defer();
    getChannelsAccess(user, isVideo).then(function (response) {
        var aggArrs = [];
        if (userCtrl.isAdmin(user)) {
            aggArrs.push(response);
        } else {
            var match = { $match: {
                    userId: user._id,
                    shareWith: { $not: { $size: 0 } }
                } };

            var project1 = { $project: { albumId: 1, shareWith: 1 } };
            var unwind = { $unwind: '$shareWith' };
            var project2 = { $project: {
                    channelId: '$albumId',
                    userId: '$shareWith.userId',
                    role: '$shareWith.role'
                } };

            if (isVideo) {
                project1.$project.videoId = 1;
                project2.$project.videoId = '$trackId';
            }

            aggArrs.push(match);
            aggArrs.push(project1);
            aggArrs.push(unwind);
            aggArrs.push(project2);
        }

        var group = { $group: {
                _id: '$albumId',
                userRoles: { $addToSet: { user: '$userId', role: '$role' } }
            } };

        if (isVideo) {
            group.$group.userRoles.$addToSet.videoId = '$trackId';
        }

        aggArrs.push(group);

        channelInfoCtrl.getAllChannelIDNames().then(function (channels, err) {
            if (err) {
                deferred.reject(false);
            }

            VideoShare.aggregate(aggArrs).exec(function (err, data) {
                if (err) {
                    deferred.reject(false);
                }

                data.forEach(function (item) {
                    item.channelName = channels[item._id];
                    if (!userCtrl.isAdmin(user)) {
                        item.userRoles = item.userRoles.concat.apply(item.userRoles, response[item._id]);
                        delete response[item._id];
                    }
                });

                if (!userCtrl.isAdmin(user)) {
                    for (var key in response) {
                        data.push({
                            _id: key,
                            channelName: channels[key],
                            userRoles: response[key]
                        });
                    }
                }

                deferred.resolve(data);
            });
        });
    });

    return deferred.promise;
}

exports.getAllCollaborators = getAllCollaborators;

exports.getYouTubeAccessCond = function (user) {
    var deferred = Q.defer();
    if (userCtrl.isAdmin(user)) {
        deferred.resolve({});
    } else {
        channelInfoCtrl.getAllChannelInfo().then(function (channelMap, err) {
            if (err) {
                deferred.reject(false);
            }
            getAllCollaborators(user, true).then(function (data, err) {
                if (err) {
                    deferred.reject(false);
                }

                var arrCond = [];
                data.forEach(function (channel) {
                    channel.userRoles.forEach(function (item) {
                        arrCond.push({
                            album_name: channel.channelName,
                            $or: [{ videoId: item.videoId }, { track_name: channelMap[channel._id][item.videoId] }],
                            userId: item.user,
                            role: item.role
                        });
                    });
                });

                if (arrCond.length === 0) {
                    deferred.reject(false);
                }
                arrCond = { $or: arrCond };

                deferred.resolve(arrCond);
            });
        });
    }
    return deferred.promise;
};
