'use strict';

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
    request = require('request'),
    url = require('url'),
    underscore = require('underscore')._,
    config = require('../../config/config'),
    UrlSale = mongoose.model('UrlSale'),
    ObjectId = mongoose.Types.ObjectId;

var apiUrl = 'https://api-ssl.bitly.com';
var partner = {
    amazon: 'tag=epi0b-20',
    itunes: 'at=10lpfL'
};
var apiKey = 'R_922d784e11179f4f27e473d034e1d06f';

exports.getUpdateSaleURL = function (req, res) {
    var sourceUrl = req.body.sourceUrl;
    var companyName = req.body.companyName;
    if(! companyName || companyName.length === 0) {
        companyName = 'EpicMusicVn';
    }
    var resCode = 200;
    var resMessage = '';
    var redirect = '/core/url-sale?redirectState=urlSale&code=';

    var partnerCode = null;
    Object.keys(partner).forEach(function(key) {
        if(sourceUrl.toLowerCase().indexOf(key.toLowerCase() + '.') >= 0 &&
            sourceUrl.toLowerCase().indexOf(partner[key].toLowerCase()) < 0) {
            partnerCode = partner[key];
        }
    });

    var longUrl = sourceUrl;
    if(partnerCode !== null) {
        if(longUrl.indexOf('?') < 0) {
            longUrl = longUrl + '?';
        } else {
            longUrl = longUrl + '&';
        }
        longUrl = encodeURIComponent(longUrl + partnerCode);
    }

    request.get(apiUrl + '/v3/shorten?login=sonthanh&apiKey=' + apiKey + '&longUrl=' + longUrl, function(err, response, body) {
        if(err) {
            resCode = 500;
            resMessage = err.toString();
            return res.redirect(redirect + resCode + '&message=' + resMessage);
        }

        var payload = {
            userId: new ObjectId(req.user._id),
            sourceUrl: sourceUrl,
            shortenUrl: JSON.parse(body).data.url,
            companyName: companyName
        };

        UrlSale.update({ sourceUrl: payload.sourceUrl }, payload, { upsert: true }, function (err) {
            resMessage = 'URL has been shortened';
            if(err) {
                resCode = 500;
                resMessage = err.toString();
            }
            return res.redirect(redirect + resCode + '&message=' + resMessage);
        });
    });
};

exports.listSaleUrl = function (req, res) {

    var sortingObj = function sortingObj(a, b) {
        if (parseInt(req.query.isAscending) > 0) {
            if (a[req.query.sort] >= b[req.query.sort]) return -1;
            return 1;
        }
        if (a[req.query.sort] <= b[req.query.sort]) return -1;
        return 1;
    };
    var searchText = req.query.searchText;
    var cond = {};

    if(searchText && searchText.length) {
        cond = {$or: [{sourceUrl: new RegExp(searchText, 'i')}, {companyName: new RegExp(searchText, 'i')}]};
    }

    var page = req.query.page;
    var numPerPage = req.query.numPerPage;
    var skip = (page - 1) * numPerPage;

    UrlSale.find(cond).select({sourceUrl: 1, shortenUrl: 1, companyName: 1}).exec().then(function (data) {
        data.sort(sortingObj);
        var data = {
            data: data.slice(skip, parseInt(skip) + parseInt(numPerPage)),
            total: data.length
        };

        return res.jsonp(data);
    });

};

exports.deleteSaleURL = function(req, res) {
    UrlSale.find({_id: new ObjectId(req.body.urlId) }).remove().exec(function() {
        return res.jsonp({});
    });
};

exports.suggestCompany = function(req, res) {
    UrlSale.find({}).distinct('companyName', function (err, data) {
        return res.jsonp(data);
    });
};
