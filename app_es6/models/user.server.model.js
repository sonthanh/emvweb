'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	crypto = require('crypto');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
	return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * A Validation function for local strategy password
 */
var validateLocalStrategyPassword = function(password) {
	return (this.provider !== 'local' || (password && password.length > 6));
};

/**
 * User Schema
 */
var UserSchema = new Schema({
	firstName: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Please fill in your first name']
	},
	lastName: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Please fill in your last name']
	},
	displayName: {
		type: String,
		trim: true
	},
	email: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Please fill in your email'],
		match: [/.+\@.+\..+/, 'Please fill a valid email address']
	},
	username: {
		type: String,
		unique: 'Username already exists',
		required: 'Please fill in a username',
		trim: true
	},
	saleId: {
		type: String,
		unique: true,
		required: true,
		trim: true
	},
	payment: {
		channel: {
			type: String,
			enum: ['paypal']
		},
		point: Number,
		account: String,
		email: String
	},
	password: {
		type: String,
		default: '',
		validate: [validateLocalStrategyPassword, 'Password should be longer']
	},
	salt: {
		type: String
	},
	provider: {
		type: String,
		required: 'Provider is required'
	},
	providerData: {},
	additionalProvidersData: {},
	roles: {
		type: [{
			type: String,
			enum: ['user', 'admin', 'label']
		}],
		default: ['user']
	},
	updated: {
		type: Date
	},
	created: {
		type: Date,
		default: Date.now
	},
	/* For reset password */
	resetPasswordToken: {
		type: String
	},
	resetPasswordExpires: {
		type: Date
	}
});

/**
 * Hook a pre save method to hash the password
 */
UserSchema.pre('save', function(next) {
	if (this.password && this.password.length > 6) {
		this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
		this.password = this.hashPassword(this.password);
	}

	next();
});

/**
 * Create instance method for hashing a password
 */
UserSchema.methods.hashPassword = function(password) {
	if (this.salt && password) {
		return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
	} else {
		return password;
	}
};

/**
 * Create instance method for authenticating user
 */
UserSchema.methods.authenticate = function(password) {
	return this.password === this.hashPassword(password);
};

/**
 * Find possible not used username
 */
UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
	var _this = this;
	var possibleUsername = username + (suffix || '');

	_this.findOne({
		username: possibleUsername
	}, function(err, user) {
		if (!err) {
			if (!user) {
				callback(possibleUsername);
			} else {
				return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
			}
		} else {
			callback(null);
		}
	});
};

var User = mongoose.model('User', UserSchema);

User.find()
	.exec(function(err, users) {
		if (! err && users.length === 0) {
			users = [{
				firstName: 'Son Thanh',
				lastName: 'Do',
				displayName: 'Son Thanh Do',
				email: 'sonthanhdo2004@gmail.com',
				username: 'sonthanhdo2004',
				password: '12345678',
				saleId: 'sonthanhdo2004',
				roles: ['user', 'admin'],
				provider: 'google'
			}, {
				firstName: 'Steven',
				lastName: 'Tran',
				displayName: 'Steven Tran',
				email: 'tranquangtrung1989@gmail.com',
				username: 'tranquangtrung1989',
				password: '12345678',
				saleId: 'tranquangtrung1989',
				roles: ['user', 'admin'],
				provider: 'google'
			}, {
               	firstName: 'EpicMusicVn',
               	lastName: 'Label',
               	displayName: 'EpicMusicVn Label',
               	email: 'epicmusicvn4@gmail.com',
               	username: 'epicmusicvn4@gmail.com',
               	password: 'welcome123',
               	saleId: 'EpicMusicVn',
               	roles: ['user', 'label'],
               	provider: 'google'
             }];

			for(var i in users) {
				users[i] = new User(users[i]);
				users[i].save();
			}
		}
	});
