'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
* Video Schema
*/
var VideoSchema = new Schema({
    trackId: {
        type: String,
        required: true,
        trim: true
    },
    trackName: {
        type: String,
        required: true,
        index: true,
        trim: true
    },
    tags: [{type: String, default: null}],
    publishDate: {type: Date}
}, { _id: false });

/**
 * Channel Info Schema
 */
var ChannelInfoSchema = new Schema({
    _id: {
        type: String,
        unique: true,
        required: true,
        index: true,
        trim: true
    },
    channelId: {
        type: String,
        unique: true,
        required: true,
        index: true,
        trim: true
    },
    name: {
        type: String,
        unique: true,
        required: true,
        index: true,
        trim: true
    },
    trackList: [VideoSchema]
});

mongoose.model('ChannelInfo', ChannelInfoSchema);
