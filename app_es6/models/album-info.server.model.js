'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PayeeSchema = new Schema({
    userId: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    role: String
}, { _id: false });

/**
* Track Schema
*/
var TrackSchema = new Schema({
    trackId: {
        type: Schema.ObjectId,
        required: true
    },
    trackName: {
        type: String,
        required: true,
        index: true
    },
    contributor: [PayeeSchema]
}, { _id: false });

/**
 * Album Info Schema
 */
var AlbumInfoSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true,
        index: true
    },
    trackList: [TrackSchema]
});

mongoose.model('AlbumInfo', AlbumInfoSchema);
