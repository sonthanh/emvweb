'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Payee Schema
 */
var PayeeSchema = new Schema({
    userId: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    role: String
}, { _id: false });

/**
 * VideoShare Schema
 */
var VideoShareSchema = new Schema({
    trackId: {
        type: String,
        required: true,
        index: true,
        trim: true
    },
    albumId: {
        type: String,
        ref: 'ChannelInfo',
        required: true,
        index: true,
        trim: true
    },
    userId: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true,
        index: true
    },
    role: String,
    commission: Number, // <==== relative to parent
    realCommission: Number, // <==== absolute value from root
    netCommission: Number, // <==== actually earned value
    directPayee: {
        userId: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        role: String
    },
    payee: [PayeeSchema],
    shareWith: [PayeeSchema]
});

PayeeSchema.index({userId: 1, role: 1}, { sparse: true });
VideoShareSchema.index({ trackId: 1, albumId: 1, userId: 1, role: 1}, { unique: true });
mongoose.model('VideoShare', VideoShareSchema);
