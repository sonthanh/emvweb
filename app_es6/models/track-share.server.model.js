'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Payee Schema
 */
var PayeeSchema = new Schema({
    userId: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    role: String
}, { _id: false });

/**
 * TrackShare Schema
 */
var TrachShareSchema = new Schema({
    trackId: {
        type: Schema.ObjectId,
        required: true,
        index: true
    },
    albumId: {
        type: Schema.ObjectId,
        ref: 'AlbumInfo',
        index: true
    },
    userId: {
        type: Schema.ObjectId,
        ref: 'User',
        index: true
    },
    role: String,
    commission: Number, // <==== relative to parent
    realCommission: Number, // <==== absolute value from root
    netCommission: Number, // <==== actually earned value
    directPayee: {
        userId: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        role: String
    },
    payee: [PayeeSchema],
    shareWith: [PayeeSchema]
});

PayeeSchema.index({userId: 1, role: 1}, { sparse: true });
TrachShareSchema.index({ trackId: 1, albumId: 1, userId: 1, role: 1}, { unique: true });
mongoose.model('TrackShare', TrachShareSchema);
