'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	mongoose = require('mongoose'),
	ObjectId = mongoose.Types.ObjectId,
	User = mongoose.model('User');

/**
 * User middleware
 */
exports.userByID = function(req, res, next, id) {
	req.me = req.user;
	if(! ObjectId.isValid(id)) {
		id = new ObjectId(id);
	}
	User.findById(id).populate('user', 'displayName').exec(function(err, user) {
		if (err) return next(err);
		if (! user) return next(new Error('Failed to load User ' + id));
		req.user = user;
		next();
	});
};

/**
 * Require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
	if (!req.isAuthenticated()) {
		return res.status(401).send({
			message: 'User is not logged in'
		});
	}

	next();
};

/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function(roles) {
	var _this = this;

	return function(req, res, next) {
		_this.requiresLogin(req, res, function() {
			if (_.intersection(req.user.roles, roles).length) {
				return next();
			} else {
				return res.status(403).send({
					message: 'User is not authorized'
				});
			}
		});
	};
};

/**
 * Require admin access middleware
 */
exports.hasAdmin = function (req, res, next) {
	if (req.user.roles.indexOf('admin') === -1) {
		return res.status(401).send({
			message: 'User is not an admin'
		});
	}

	next();
};

/**
 * Check if a user is admin
 */
exports.isAdmin = function(user) {
	var isAdmin = true;
	if(! user.roles || user.roles.indexOf('admin') === -1) {
		isAdmin = false;
	}

	return isAdmin;
};
