'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors.server.controller.js'),
	mongoose = require('mongoose'),
	passport = require('passport'),
	User = mongoose.model('User'),
	async = require('async'),
	config = require('../../../config/config'),
	nodemailer = require('nodemailer'),
	Invitation = mongoose.model('Invitation'),
	ObjectId = mongoose.Types.ObjectId;

var smtpTransport = nodemailer.createTransport(config.mailer.options);

/**
 * Update user details
 */
exports.update = function(req, res) {
	// Init Variables
	var user = req.user;
	
	var isMe = false;
	if (req.user._id.equals(req.me._id)) {
		isMe = true;
	} else if (! _.intersection(req.me.roles, ['admin']).length){
		return res.status(400).send({
			message: 'Forbidden'
		});
	}

	// For security measurement we remove the roles from the req.body object
	req.body.roles = req.body.roles.toString().split(',');

	if (user) {
		// Merge existing user
		user = _.extend(user, req.body);
		user.updated = Date.now();
		user.displayName = user.firstName + ' ' + user.lastName;
		if(! user.saleId) {
			user.saleId = user.displayName;
		}

		user.save(function(err) {

			user.password = undefined;
			user.salt = undefined;
			user.provider = undefined;
			user.providerData = undefined;

			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else if(isMe) {
				req.login(user, function(err) {
					if (err) {
						res.status(400).send(err);
					} else {
						res.json(user);
					}
				});
			} else {
				res.json(user);
			}
		});
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};

/**
 * List All User
 */

exports.list = function(req, res) {
	var sortingObj = function(a, b){
		if(parseInt(req.query.isAscending) > 0) {
			if (a[req.query.sort] >= b[req.query.sort])
				return -1;
			return 1;
		}
		if (a[req.query.sort] <= b[req.query.sort])
			return -1;
		return 1;
	};
	var searchText = req.query.searchText;

	var cond = {};
	if(searchText && searchText.length > 0) {
		cond.displayName = new RegExp(searchText, 'i');
	}console.log(sortingObj);

	var page = req.query.page;
	var numPerPage = req.query.numPerPage;
	var skip = (page - 1) * numPerPage;

	User.find(cond).sort('displayName')
		.select({_id: 1, displayName: 1, email: 1, roles: 1})
		.exec(function(err, users) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				users.sort(sortingObj);
				res.jsonp({data: users.slice(skip, parseInt(skip) + parseInt(numPerPage)), total: users.length});
			}
	});
};

/**
 * Create a new User
 */

exports.create = function(req, res) {
	var user = new User(req.body);
	user.provider = 'local';
	user.displayName = user.firstName + ' ' + user.lastName;

	// Then save the user
	user.save(function(err) {
		if (err) {console.log(err);
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;
			res.json(user);
		}
	});
};

/**
 * Delete an user
 */
exports.delete = function(req, res) {
	var user = req.user;
	
	if (! user._id.equals(req.me._id) && _.intersection(req.me.roles, ['admin']).length) {
		user.remove(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(user);
			}
		});

		var cond = {
			shares: {
				$elemMatch: {
					user: {_id: new ObjectId(user._id)}
				}
			}
		};

		//Album.find(cond)
		//	.exec(function (err, albums) {
		//		for(var i = 0; i < albums.length; i++) {
		//			for(var j = 0; j < albums[i].shares.length; j++) {
		//				if(albums[i].shares[j].user.equals(user._id)) {
		//					albums[i].shares.splice(j, 1);
		//				}
		//			}
		//			albums[i].save();
		//		}
		//	});
	} else {
		return res.status(400).send({
			message: 'Forbidden'
		});
	}
};

exports.get = function(req, res) {
	if(_.intersection(req.me.roles, ['admin']).length) {
		var user = req.user;
		user.password = undefined;
		user.salt = undefined;
		user.provider = undefined;
		user.providerData = undefined;
		res.jsonp(user);
	} else {
		return res.status(400).send({
			message: 'Forbidden'
		});
	}
};

exports.inviteEmail = function(req, res, next) {
	async.waterfall([
		// Generate random token
		function(done) {
			var email = req.body.email,
				userId = new ObjectId(req.user._id);
			Invitation.findOne({
				user_id: userId,
				invite_email: email,
				status: 'P'
			}).exec(function (err, data) {
				if(! data || data.length === 0) {
					var invitation = new Invitation({
						user_id: userId,
						invite_email: email,
						status: 'P'
					});
					invitation.save(function(err, user) {
						done(err, email, user._id);
					});
				} else {
					done(err, email, data._id);
				}
			});
		},
		function(email, token, done) {
			res.render('templates/invite-user-email', {
				name: email.substring(0, email.indexOf('@')),
				appName: config.app.title,
				url: 'http://' + req.headers.host + '/acc/signup?token=' + token + '&email=' + email
			}, function(err, emailHTML) {
				done(err, emailHTML, email);
			});
		},
		function(emailHTML, email, done) {
			var mailOptions = {
				to: email,
				from: config.mailer.from,
				subject: 'Member Invitation',
				html: emailHTML
			};
			smtpTransport.sendMail(mailOptions, function(err) {
				if (!err) {
					res.send({
						message: 'Email has been successfully sent to ' + email + '.'
					});
				} else {
					return res.status(400).send({
						message: 'Failure sending email'
					});
				}
				done(err);
			});
		}],
		function(err) {
			if (err) return next(err);
		}
	);
};

/**
 * Reset invitation email token
 */
exports.validateInviteToken = function(req, res) {
	Invitation.findOne({
		_id: new ObjectId(req.params.token),
		invite_email: req.query.email,
		status: 'P'
	}, function(err, token) {
		if(err || !token || token.length === 0) {
			return res.status(400).send();
		} else {
			res.status(200).send();
		}
	});
};
