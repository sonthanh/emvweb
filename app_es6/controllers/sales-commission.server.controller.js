'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    _ = require('lodash'),
    csv = require('fast-csv'),
    csvStr = require('csv-string'),
    promise = require('bluebird'),
    ObjectId = mongoose.Types.ObjectId,
    AlbumInfo = mongoose.model('AlbumInfo'),
    TrackShare = mongoose.model('TrackShare'),
    Q = require('q'),
    underscore = require('underscore')._,
    SaleCommission = mongoose.model('SaleCommission'),
    TrackShareUtil = require('./utils/share.util.js'),
    trackShareCtrl = require('./track-share.server.controller.js'),
    albumInfoCtrl = require('./album-info.server.controller.js'),
    userCtrl = require('./users.server.controller');

/**
 * Sale authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    next();
};

function getSaleDates() {
    return promise.cast(SaleCommission.find()
        .distinct('sale_date')
        .exec()
    );
}

function findOneAndUpdateAlbum(albumName, trackList) {
  var deferred = Q.defer();
  AlbumInfo.findOne({name: albumName}, function (err, album) {
    if (err) {
      deferred.reject(err);
    }

    var existingTracks = [];
    var trackListWithObjId = [];
    if(album) {
      existingTracks = album.trackList.map(function(row) {
        return row.trackName;
      });
      trackListWithObjId = album.trackList;
    }
    trackList = trackList.filter(function(row) {
      return existingTracks.indexOf(row) === -1;
    });


    trackListWithObjId = trackListWithObjId.concat(trackList.map(function (name) {
        return {trackId: new ObjectId(), trackName: name};
    }));

    var newAlbum = {
      name: albumName,
      trackList: trackListWithObjId
    };

    AlbumInfo.findOneAndUpdate({name: albumName}, newAlbum, {upsert: true}, function (err) {
      if (err) {
        console.log('Error in saving album: ' + albumName);
        return deferred.reject(err);
      }
      return deferred.resolve(true);
    });
  });
  return deferred.promise;
}

function getAlbumTrackShareInfo(user) {
    var deferred = Q.defer();
    userCtrl.getAllUserIDNames().then((userMap) => {
        trackShareCtrl.getAllCollaborators(user).then((albumGroups) => {
            albumGroups.forEach(function(album){
            album.albumId = album._id;
            album.users = [];

            var userArrs = {};
            album.userRoles.forEach(function(userRole) {
                if(! userArrs[userRole.user]) {
                    userArrs[userRole.user] = {};
                }
                userArrs[userRole.user][userRole.role] = 1;
            });
            for(var userId in userArrs) {
                var roleObjs = userArrs[userId];
                var roleArrs = [];
                for(var role in roleObjs) {
                    roleArrs.push({role: role});
                }
                album.users.push({
                    userId: userId,
                    userName: userMap[userId],
                    roles: roleArrs
                });
            }

            delete album._id;
            delete album.userRoles;
        });

        deferred.resolve(albumGroups);
    }).catch ((err) =>{ deferred.reject(err);});
    }).catch ((err) =>{ deferred.reject(err);});
    return deferred.promise;
};

function buildAlbumInfo(saleRecords) {
    var deferred = Q.defer();
    console.log('#Try to build AlbumInfo from saleRecords with length = ' + saleRecords.length);
    var groupByAlbum = underscore.groupBy(saleRecords, function(sale) { return sale.album_name; });
    var albums = [];
    var processArr = [];
    for (var albumName in groupByAlbum) {
        //console.log("Updating album: " + albumName);
        var trackList = underscore.uniq(groupByAlbum[albumName].map(function (sale) {return sale.track_name;}));
        trackList = trackList.filter(function (name) {return name !== 'N/A';});
        albums.push(albumName);
        processArr.push(findOneAndUpdateAlbum(albumName, trackList));
    }
    Q.all(processArr).then(function() {
      return deferred.resolve(albums);
    });

    return deferred.promise;
}

function setDefaultTrackShares (albums, userId) {
    var deferred = Q.defer();
    if(albums.length > 0) {
        var count = albums.length;
        albums.forEach(album => {
            albumInfoCtrl.getAlbumByName(album)
            .then(albumTracks => {
                var albumIds = albumTracks.map(aT => aT._id);
                if(albumIds.length > 0) {
                    albumIds = albumIds[0];
                    trackShareCtrl.getTrackSharesByAlbum(albumIds)
                    .then(tracks => {
                        tracks = tracks.map(track => track.trackId.toString());
                        albumTracks = albumTracks[0].trackList.filter(track =>{
                            return tracks.indexOf(track.trackId.toString()) < 0;
                        });
                        var input = {album:albumIds,addTracks:albumTracks,userId:userId,role:'label',commission:100};
                        TrackShareUtil.insertMultipleShares(input)
                        .then(function() {
                            count--;
                            if(count ===0) {
                                deferred.resolve(true);
                            }
                        }).catch(function(err) {
                            console.log(err);
                            deferred.resolve(false);
                        });
                    });
                } else {
                    console.log('invalid albumIds');
                    deferred.resolve(false);
                }
            });
        });
    } else {
        console.log('Empty album info !!!');
        deferred.resolve(false);
    }
    return deferred.promise;
}

function groupArrIntoChunks (arr, len) {
  var chunks = [],
      i = 0,
      n = arr.length;
  while (i < n) {
    chunks.push(arr.slice(i, i += len));
  }
  return chunks;
}

var updateCreateProfitDefered = Q.defer();
function updateCreateProfit(index, totalSaleRecords, allTrackShare) {
  global.gc();
  if(index === totalSaleRecords.length) {
    return updateCreateProfitDefered.resolve(true);
  }
  TrackShareUtil.updateProfit(allTrackShare, totalSaleRecords[index]).then(function () {
      console.log('Start saving report to db...');
      console.log("Process Record: " + index);
      SaleCommission.create(totalSaleRecords[index], function(err) {
        return updateCreateProfit(index + 1, totalSaleRecords, allTrackShare);
      });
  });

  return updateCreateProfitDefered.promise;
}

function processImportedData(totalSaleRecords, userId) {
    var deferred = Q.defer();
    buildAlbumInfo(totalSaleRecords || [])
    .then(function(albums) {
        setDefaultTrackShares (albums, userId).then(function(status) {
            // update the share profit if we have TrackShare
            if(status) {
                TrackShare.find().populate([{path: 'albumId'}]).exec().then(function (allTrackShare) {
                    updateCreateProfit(0, groupArrIntoChunks (totalSaleRecords, 1000), allTrackShare).then(function() {
                        return deferred.resolve(true);
                    });
                });
            } else {
              return deferred.resolve(true);
            }
        });
    });
    return deferred.promise;
}

/**
 * Sale import report
 */
exports.importReport = function (req, res) {
    process.env.isOnProcessing = true;
    var sateDates = [];

    getSaleDates().then(function(dates, err) {
        for(var i in dates) {
            sateDates[dates[i].getYear() + '.' +dates[i].getMonth()] = true;
        }

        var file = req.files.file;
        var isLabel = req.user.roles.indexOf('label') >= 0 ? true: false;
        if (!file) {
            delete process.env.isOnProcessing;
            return res.status(400).send({message: 'File Not Exist'});
        }

        if (! isLabel) {
            delete process.env.isOnProcessing;
            return res.status(400).send({message: 'Only Label Can Import Data!!!'});
        }
        res.status(200).send();

        var isBody = false;
        var saleRecords = [];
        var userId = req.user._id;
        csv.fromPath(file.path, {encoding: 'utf8', quote: null}).on('data', function (data) {
            if (isBody) {
                var row = csvStr.parse(data)[0];
                //console.log(row);
                if (row) {
                    if(row[15] === '') {
                        row[15] = 'EUR';
                    }
                    row[18] = new Date(Date.parse(row[18]));
                    row[18].setDate(17);
                    row[18].setDate(row[18].getDate() - 120);
                    row[18].setDate(25);
                    if(! sateDates[row[18].getYear() + '.' +row[18].getMonth()]) {
                        saleRecords.push({
                            album_artist: row[0],
                            track_artist: row[1],
                            label: row[2],
                            album_name: row[3],
                            track_name: row[6],
                            partner: row[10],
                            dms: row[20],
                            type: row[9],
                            quantity: row[11],
                            unit: row[12],
                            profit: row[14],
                            currency: row[15],
                            country: row[16],
                            sale_date: row[18],
                            import_date: new Date(Date.parse(row[17]))
                        });
                    }
                }
            }
            isBody = true;
        }).on('end', function () {
            processImportedData(saleRecords, userId)
            .then((response, err) => {
                console.log('DONE!!!!!!!!!');
                delete process.env.isOnProcessing;
            });
        });
    });
};

var buildRegex = function (pattern, listSchema) {
    var regex = new RegExp(pattern, 'i');
    return listSchema.map(function (schema) {
        var newObj = {};
        newObj[schema] = regex;
        return newObj;
    });
};

var queryCondition = function (albumName, userId, role, searchText, startDate, endDate) {
    var cond = {};
    if (albumName !== '') {
        cond.album_name = albumName;
    }
    if (searchText) {
        cond.$or = buildRegex(searchText.toString().trim(), ['album_name', 'track_name', 'partner']);
    }
    if(startDate && endDate) {
        startDate = new Date(parseInt(startDate));
        endDate = new Date(parseInt(endDate));
        if(startDate.getDate() > 3) {
            startDate.setDate(3);
            startDate.setMonth(startDate.getMonth() + 1);
        }
        cond.sale_date = {$gte: startDate, $lt: endDate};
    }
    cond.allCommissions = {$not: {$size: 0}};
    if (userId !== '' && role !== '') {
        // match specific user and specific role
        cond.allCommissions.$elemMatch = {userId: new ObjectId(userId), 'detail.role': role};
    } else if (userId !== '' && role === '') {
        // match specific user and All role
        cond.allCommissions.$elemMatch = {userId: new ObjectId(userId)};
    } else if (userId === '' && role !== '') {
        // match All user and specific role
        cond.allCommissions.$elemMatch = {'detail.role': role};
    }

    return cond;
};
exports.queryCondition = queryCondition;

var aggregateArr = function(queryCond, user) {// This is Based Aggregation Framework

    var deferred = Q.defer();

    var project1 = {
        ID: '$_id',
        album_name: 1,
        track_artist: 1,
        label: 1,
        track_name: 1,
        partner: 1,
        type: 1,
        quantity: 1,
        profit: 1,
        sale_date: 1,
        allCommissions: 1
    };

    var project2 = underscore.extend({}, project1);
    project2.userId = '$allCommissions.userId';
    project2.totalProfit = '$allCommissions.totalValue';
    project2.detail =  '$allCommissions.detail';

    var queryCond2 = {};
    if(queryCond.allCommissions.$elemMatch) {
        if(queryCond.allCommissions.$elemMatch.userId) {
            queryCond2.userId = queryCond.allCommissions.$elemMatch.userId;
        }

        if(queryCond.allCommissions.$elemMatch.detail) {
            queryCond2.detail.role = queryCond.allCommissions.$elemMatch.detail.role;
        }
    }

    var project3 = underscore.extend({}, project2);
    delete project3.detail;
    delete project3.allCommissions;
    project3.trackId = '$detail.trackId';
    project3.role = '$detail.role';
    project3.netProfit = '$detail.netValue';
    project3.profit = {$divide: [{$multiply:['$detail.value', 100]}, '$detail.commission']};
    project3.commission = '$detail.commission';
    project3.ispaid = '$detail.ispaid';

    trackShareCtrl.getSaleAccessCond(user).then((cond) => {
        deferred.resolve([{$match: queryCond},
            {$project: project1},
            {$unwind: '$allCommissions'},
            {$project: project2},
            {$match: queryCond2},
            {$unwind: '$detail'},
            {$project: project3},
            {$match: cond}]);
    }).catch ((err) =>{ deferred.reject(err);});

    return deferred.promise;
};

exports.getChartSummarizedByMonth = function(req, res) {
    var albumName = req.query.albumName;
    var userId = req.query.userId;
    var role = req.query.role;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;

    var cond = queryCondition(albumName, userId, role, undefined, startDate, endDate);
    aggregateArr(cond, req.user).then((aggregate) => {
        var group = {$group:{
            _id: {sale_date: '$sale_date'},
            commission: {$sum: '$netProfit'}
        }};
        aggregate.push(group);

        SaleCommission.aggregate(aggregate).exec().then((data) => {
            data.sort(function(a, b) {
                if(a._id.sale_date >= b._id.sale_date) {
                    return 1;
                }
                return -1;
            });
            res.jsonp({data: data});
        }).catch ((err) => {
            res.status(500).send({message: err.toString()});
        });
    }).catch ((err) => {
        res.status(500).send({message: err.toString()});
    });
};

exports.getCommissionSummary = function(req, res) {
    var albumName = req.query.albumName;
    var userId = req.query.userId;
    var role = req.query.role;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;

    var cond = queryCondition(albumName, userId, role, undefined, startDate, endDate);
    aggregateArr(cond, req.user).then((aggregate) => {
        var project = {$project: {
          ID: 1,
          ispaid: 1,
          type: 1,
          quantity: 1,
          netProfit: 1
        }};

        var group = {$group:{
            _id: {ID: '$ID', ispaid: '$ispaid'},
            type: {$first: '$type'},
            idQuantity: {$addToSet: {quantity: '$quantity', ID: '$ID'}},
            commission: {$sum: '$netProfit'},
            ispaid: {$first: '$ispaid'}
        }};
        aggregate.push(project);
        aggregate.push(group);

        SaleCommission.aggregate(aggregate).exec().then((response) => {
            var data = {
                commission: 0,
                paid: 0,
                balance: 0,
                DA: 0,
                DT: 0,
                S: 0
            };
            var mapId = {};
            response.forEach(function(value){
                value.idQuantity.forEach(function(item) {
                    if(! mapId[item.ID]) {
                        mapId[item.ID] = true;
                        data[value.type] += item.quantity;
                    }
                });

                if(! isNaN(value.commission)) {
                    data.commission += value.commission;
                    if(value._id.ispaid) {
                        data.paid += value.commission;
                    } else {
                        data.balance += value.commission;
                    }
                }
            });

            res.jsonp({data: data});
        }).catch ((err) => {
            res.status(500).send({message: err.toString()});
        });
    }).catch ((err) => {
        res.status(500).send({message: err.toString()});
    });
};

var aggregateArtistCommission = function(queryCond, role, user) {// This is extended from Based Framework
    var deferred = Q.defer();

    aggregateArr(queryCond, user).then((aggregate) => {
        var match = {$match: {}};
        if(role !== '') {
            match.$match.role = role;
        }

        var project = {$project:{
            ID: 1,
            userId: 1,
            type: 1,
            quantity: 1,
            role: 1,
            profit: 1,
            netProfit: 1
        }};

        var group = {$group:{
            _id: {ID: '$ID', role: '$role', type: '$type', userId: '$userId'},
            userId: {$first: '$userId'},
            role: {$first: '$role'},
            type: {$first: '$type'},
            quantity: {$first: '$quantity'},
            profit: {$sum: '$profit'},
            commission: {$sum: '$netProfit'}
        }};

        var group1 = {$group:{
            _id: {type: '$type', role: '$role', userId: '$userId'},
            userId: {$first: '$userId'},
            role: {$first: '$role'},
            type: {$first: '$type'},
            quantity: {$sum: '$quantity'},
            profit: {$sum: '$profit'},
            commission: {$sum: '$commission'}
        }};

        var group2 = {$group:{
            _id: {role: '$role', userId: '$userId'},
            name: {$first: '$userId'},
            role: {$first: '$role'},
            group: {$addToSet: {
                role: '$role',
                type: '$type', quantity: '$quantity',
                profit: '$profit', commission: '$commission'
            }}
        }};

        aggregate.push(match);
        aggregate.push(project);
        aggregate.push(group);
        aggregate.push(group1);
        aggregate.push(group2);

        SaleCommission.aggregate(aggregate).exec().then((data) => {
            deferred.resolve(data);
        }).catch ((err) =>{ deferred.reject(err);});
    }).catch ((err) =>{ deferred.reject(err);});

    return deferred.promise;
};

exports.listCommissionGroupByArtist = function(req, res) {
    var albumName = req.query.albumName;
    var userId = req.query.userId;
    var role = req.query.role;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;

    var sortingObj = function(a, b){
        if(parseInt(req.query.isAscending) > 0) {
            if (a[req.query.sort] >= b[req.query.sort])
                return -1;
            return 1;
        }
        if (a[req.query.sort] <= b[req.query.sort])
            return -1;
        return 1;
    };
    var searchText = req.query.searchText;

    var page = req.query.page;
    var numPerPage = req.query.numPerPage;
    var skip = (page - 1) * numPerPage;

    // build Mongo query
    var cond = queryCondition(albumName, userId, role, searchText, startDate, endDate);

    aggregateArtistCommission(cond, role, req.user).then((response) => {
        var data = [];
        response.forEach(function(value) {
            var row = {
                name: value.name,
                role: value.role,
                da_quantity: 0,
                da_profit: 0,
                dt_quantity: 0,
                dt_profit: 0,
                s_quantity: 0,
                s_profit: 0,
                profit: 0,
                commission: 0
            };

            value.group.forEach(function (item) {
                row[(item.type + '_quantity').toLowerCase()] = item.quantity;
                row[(item.type + '_profit').toLowerCase()] = item.profit;
                row.profit += item.profit;
                row.commission += item.commission;
            });

            data.push(row);
        });

        data.sort(sortingObj);
        res.jsonp({data: data.slice(skip, parseInt(skip) + parseInt(numPerPage)), total: data.length});
    }).catch ((err) => {
        res.status(500).send({message: err.toString()});
    });
};

var aggregateGroupCommission = function(queryCond, groupName, role, user) {
    var deferred = Q.defer();

    aggregateArr(queryCond, user).then((aggregate) => {
        var match = {$match: {}};
        if(role !== '') {
            match.$match.role = role;
        }

        var group = {$group:{
            _id: {ID: '$ID'},
            type: {$first: '$type'},
            quantity: {$first: '$quantity'},
            profit: {$first: '$profit'},
            commission: {$sum: '$netProfit'}
        }};
        group.$group._id[groupName] = '$' + groupName;


        var group1 = {$group:{
            _id: {type: '$type'},
            type: {$first: '$type'},
            quantity: {$sum: '$quantity'},
            profit: {$sum: '$profit'},
            commission: {$sum: '$commission'}
        }};
        group1.$group._id[groupName] = '$_id.' + groupName;
        group1.$group[groupName] = {$first: '$_id.' + groupName};

        var group2 = {$group:{
            _id: {},
            group: {$addToSet: {
                type: '$type', quantity: '$quantity',
                profit: '$profit', commission: '$commission'
            }}
        }};
        group2.$group._id[groupName] = '$_id.' + groupName;
        group2.$group.name = {$first: '$_id.' + groupName};

        aggregate.push(match);
        aggregate.push(group);
        aggregate.push(group1);
        aggregate.push(group2);

        SaleCommission.aggregate(aggregate).exec().then((data) => {
            deferred.resolve(data);
        }).catch ((err) =>{ deferred.reject(err);});
    }).catch ((err) =>{ deferred.reject(err);});

    return deferred.promise;
};

exports.listCommissionByGroup = function(req, res) {
//console.log(req.query);
    var albumName = req.query.albumName;
    var userId = req.query.userId;
    var role = req.query.role;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;

    var sortingObj = function(a, b){
        if(parseInt(req.query.isAscending) > 0) {
            if (a[req.query.sort] >= b[req.query.sort])
                return -1;
            return 1;
        }
        if (a[req.query.sort] <= b[req.query.sort])
            return -1;
        return 1;
    };
    var searchText = req.query.searchText;

    var page = req.query.page;
    var numPerPage = req.query.numPerPage;
    var skip = (page - 1) * numPerPage;

    // build Mongo query
    var cond = queryCondition(albumName, userId, role, searchText, startDate, endDate);
    aggregateGroupCommission(cond, req.query.groupName, role, req.user).then((response) => {
        var data = [];
        response.forEach(function(value) {
            var row = {
                name: value.name,
                da_quantity: 0,
                da_profit: 0,
                dt_quantity: 0,
                dt_profit: 0,
                s_quantity: 0,
                s_profit: 0,
                profit: 0,
                commission: 0
            };

            value.group.forEach(function (item) {
                row[(item.type + '_quantity').toLowerCase()] = item.quantity;
                row[(item.type + '_profit').toLowerCase()] = item.profit;
                row.profit += item.profit;
                row.commission += item.commission;
            });

            data.push(row);
        });

        data.sort(sortingObj);
        res.jsonp({data: data.slice(skip, parseInt(skip) + parseInt(numPerPage)), total: data.length});
    }).catch ((err) => {
        res.status(500).send({message: err.toString()});
    });
};

var aggregateTrackCommission = function(queryCond, role, user) {// This is extended from Based Framework
    var deferred = Q.defer();

    aggregateArr(queryCond, user).then((aggregate) => {
        var match = {$match: {}};
        if(role !== '') {
            match.$match.role = role;
        }

        var group = {$group:{
            _id: {ID: '$ID', track_name: '$track_name'},
            album_name: {$first: '$album_name'},
            type: {$first: '$type'},
            quantity: {$first: '$quantity'},
            profit: {$first: '$profit'},
            commission: {$sum: '$netProfit'}
        }};

        var group1 = {$group:{
            _id: {track_name: '$_id.track_name', type: '$type'},
            track_name: {$first: '$_id.track_name'},
            type: {$first: '$type'},
            album_name: {$first: '$album_name'},
            quantity: {$sum: '$quantity'},
            profit: {$sum: '$profit'},
            commission: {$sum: '$commission'}
        }};
        aggregate.push(match);
        aggregate.push(group);
        aggregate.push(group1);

        SaleCommission.aggregate(aggregate).exec().then((data) => {
            deferred.resolve(data);
        }).catch ((err) =>{ deferred.reject(err);});
    }).catch ((err) =>{ deferred.reject(err);});

    return deferred.promise;
};

exports.listCommissionGroupByTrack = function(req, res) {
    //console.log(req.query);
    var albumName = req.query.albumName;
    var userId = req.query.userId;
    var role = req.query.role;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;

    var sortingObj = function(a, b){
        if(parseInt(req.query.isAscending) > 0) {
            if (a[req.query.sort] >= b[req.query.sort])
                return -1;
            return 1;
        }
        if (a[req.query.sort] <= b[req.query.sort])
            return -1;
        return 1;
    };
    var searchText = req.query.searchText;

    var page = req.query.page;
    var numPerPage = req.query.numPerPage;
    var skip = (page - 1) * numPerPage;

    // build Mongo query
    var cond = queryCondition(albumName, userId, role, searchText, startDate, endDate);
    aggregateTrackCommission(cond, role, req.user).then((data) => {
        data.sort(sortingObj);
        res.jsonp({data: data.slice(skip, parseInt(skip) + parseInt(numPerPage)), total: data.length});
    }).catch ((err) => {
        res.status(500).send({message: err.toString()});
    });
};

var filterAllCommission = function (saleItem, filterUsers) {
    // only return AllCommission belong to filterUsers
    return saleItem.allCommissions.filter(function (commission) {
        return filterUsers[commission.userId.toString()];
    });
};

exports.listSalesCommission = function (req, res) {
    //console.log(req.query);
    var albumName = req.query.albumName;
    var userId = req.query.userId;
    var role = req.query.role;
    var sortingObj = {};
    sortingObj[req.query.sort] = req.query.isAscending;
    var searchText = req.query.searchText;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;

    var page = req.query.page;
    var numPerPage = req.query.numPerPage;
    var skip = (page - 1) * numPerPage;

    getAlbumTrackShareInfo(req.user).then((albumTrackShare) => {
        let filterUsers = {};
        var cond = {$or: []};
        albumTrackShare.forEach((album) => {
            album.users.forEach((user) => {
                filterUsers[user.userId] = true;
                user.roles.forEach((role) => {
                    cond.$or.push({albumName: album.albumName, userId: user.userId, role: role.role});
                });
            });
        });

        cond.$or = cond.$or.filter((item) => {
            if (albumName === '') return true;
            if (albumName !== item.albumName) return false;
            if (userId === '') return true;
            if (userId !== item.userId) return false;
            if (role === '') return true;
            if (role !== item.role) return false;
            return true;
        }).map((item) => {
           return queryCondition(item.albumName, item.userId, item.role, searchText, startDate, endDate);
        });

        var p1 = SaleCommission.find(cond).count().exec();
        var p2 = SaleCommission.find(cond).skip(skip).limit(numPerPage).sort(sortingObj).lean().exec();
        Q.all([p1, p2]).then((data) => {
            var totalSale = data[0];
            var sales = data[1];
            var userObj = {};
            userObj[userId] = true;
            filterUsers = userId !== '' ? userObj : filterUsers;
            var preprocess = sales.map((saleItem) => {
                saleItem.unit = saleItem.unit * 0.9;
                saleItem.allCommissions = filterAllCommission(saleItem, filterUsers);
                return saleItem;
            }).filter((saleItem) => {return saleItem.allCommissions.length > 0;});

            var preprocessSaleItems = preprocess.map((saleItem) => {
                var cms = saleItem.allCommissions[0];
                if (role === '') {
                    saleItem.commission = cms.totalValue;
                } else {
                    var filterDetail = cms.detail.filter((elem) => {
                        return elem.role === role;
                    });
                    if (filterDetail.length > 0) {
                        var detail = filterDetail[0];
                        saleItem['commission'] = detail.value;
                    } else {
                        saleItem = null;
                    }
                }
                return saleItem;
            }).filter((item) => item);
            res.jsonp({data: preprocessSaleItems, total: totalSale});
        }).catch((err) => {
            console.log(err);
            res.status(400).send({message: err.toString()});
        });
    }).catch((err) => {
        console.log(err);
        res.status(501).send({message: err.toString()});
    });
};

exports.albumTrackShareInfo = function(req, res) {
    getAlbumTrackShareInfo(req.user).then((albumGroups) => {
        res.jsonp({data: albumGroups});
    }).catch ((err) => {
        res.status(500).send({message: err.toString()});
    });
};

exports.getUserEarningByMonth = function(req, res) {
    var albumName = req.query.albumName;
    var userId = req.query.userId;
    var role = req.query.role;

    var sortingObj = function(a, b){
        if(parseInt(req.query.isAscending) > 0) {
            if (a[req.query.sort] >= b[req.query.sort])
                return -1;
            return 1;
        }
        if (a[req.query.sort] <= b[req.query.sort])
            return -1;
        return 1;
    };
    var searchText = req.query.searchText;

    var page = req.query.page;
    var numPerPage = req.query.numPerPage;
    var skip = (page - 1) * numPerPage;

    var cond = queryCondition(albumName, userId, role, searchText);
    aggregateArr(cond, req.user).then((aggregate) => {
        var match = {$match: {}};
        if(role !== '') {
            match.$match.role = role;
        }

        var group = {$group:{
            _id: {sale_date: '$sale_date', album_name: '$album_name', user: '$userId', role: '$role', ispaid: '$ispaid'},
            sale_date: {$first: '$sale_date'},
            album_name: {$first: '$album_name'},
            user_id: {$first: '$userId'},
            role: {$first: '$role'},
            status: {$first: '$ispaid'},
            commission: {$sum: '$netProfit'}
        }};
        aggregate.push(match);
        aggregate.push(group);

        SaleCommission.aggregate(aggregate).exec().then((data) => {
                userCtrl.getAllUserIDNames().then(function(userMap) {
                    var totalEarning = 0, totalUnpaid = 0;
                    data.forEach(function (item) {
                        item.user_name = userMap[item.user_id];
                        totalEarning += item.commission;
                        if (!item.status) {
                            totalUnpaid += item.commission;
                        }
                        delete item._id;
                    });

                    var dateRangeSel = [];
                    var isInclude = {};
                    data.forEach(function(item) {
                        if(! isInclude[item.sale_date]) {
                            dateRangeSel.push({
                                text: item.sale_date,
                                value: item.sale_date.getTime()
                            });
                        }
                        isInclude[item.sale_date] = true;
                    });

                    dateRangeSel.sort(function(a, b) {
                        if(a.value >= b.value) {
                            return 1;
                        }
                        return -1;
                    });

                    var startDate = parseInt(req.query.startDate);
                    var endDate = parseInt(req.query.endDate);
                    data = data.filter(function(item) {
                        var rs = true;
                        if(startDate) {
                            rs = rs && (item.sale_date.getTime() >= startDate);
                        }
                        if(endDate) {
                            rs = rs && (item.sale_date.getTime() <= endDate);
                        }
                        return rs;
                    });

                    data.sort(sortingObj);
                    res.jsonp({
                        data: data.slice(skip, parseInt(skip) + parseInt(numPerPage)),
                        dateRangeSel: dateRangeSel,
                        total: data.length,
                        totalEarning: totalEarning,
                        totalUnpaid: totalUnpaid
                    });
                });
        }).catch ((err) => {
            res.status(500).send({message: err.toString()});
        });
    }).catch ((err) => {
        res.status(500).send({message: err.toString()});
    });
};
