'use strict';

var mongoose = require('mongoose'),
    //TrackShare = mongoose.model('TrackShare'),
    Q = require('q'),
    underscore = require('underscore')._;
    //SaleCommission = mongoose.model('SaleCommission');

var ShareService = require("../services/share.service.js");
var Core = require("../services/core.js");

/**
 * Update user profit in field `allCommissions`
 * @param listTrackShare list of json {trackId, albumId: {_id, trackList, name}, userId, role, realCommission, netCommission}
 * @param saleCommissions
 */
function updateProfit(listTrackShare, saleCommissions) {
    var deferred = Q.defer();

    var totalUndefined = ShareService.validateShares(listTrackShare);
    if (totalUndefined.length > 0) {
        deferred.resolve({ret_code: 2, msg: 'Wrong format of input with undefined fields: ' + JSON.stringify(totalUndefined)});
        return;
    }

    var updatedTrackShares = ShareService.updateName(listTrackShare);

    var salesPromise;
    if (typeof saleCommissions === 'undefined') {
        // get all necessary query to fetch all data at once
        console.log('[updateProfit] Fetch all sale data relating with updated commission - query length = ' + updatedTrackShares.length);
        salesPromise = ShareService.findAllRelevantCommission(updatedTrackShares);
    } else {
        var deferred2 = Q.defer();
        deferred2.resolve(saleCommissions);
        console.log('[updateProfit] Get the sales in memory with length = ' + saleCommissions.length);
        salesPromise = deferred2.promise;
    }

    var catchError = Core.genericErrHandler(deferred, {ret_code: 3, msg: 'Exception in updating profit'}, 'resolve', 'ERROR in updateProfit()');
    salesPromise.then(function (updateSales) {
        console.log('Update all related sale with length = ' + updateSales.length);
        var count = 0;
        var updatingSalePromises = updateSales.map(function (sale) {
            sale.allCommissions = sale.allCommissions || [];
            updatedTrackShares.forEach(function (relatedShare) {
                if (ShareService.isShareRelevantToSale(relatedShare, sale)) {

                    count += 1;
                    //ShareService.logUpdateCommission(count, sale);

                    var newUserProfit = {};
                    var realProfit = ShareService.updateRealProfit(relatedShare, sale, newUserProfit);
                    newUserProfit.role = relatedShare.role;
                    newUserProfit.value = realProfit * relatedShare.realCommission / 100;
                    newUserProfit.commission = relatedShare.realCommission;
                    newUserProfit.netValue = realProfit * relatedShare.netCommission / 100;
                    newUserProfit.netCommission = relatedShare.netCommission;

                    // locate the user to update
                    var foundUser = sale.allCommissions.filter(userCommission => Core.compareObjId(userCommission.userId, relatedShare.userId));
                    if (foundUser.length === 0) {
                        // this user doesn't exist, push the new data
                        sale.allCommissions.push({
                            userId: relatedShare.userId,
                            totalValue: newUserProfit.netValue,
                            detail: [newUserProfit]
                        });
                    } else {
                        // locate the role to update
                        var foundRole = foundUser[0].detail.filter(roleValue => ShareService.checkRoleInUpdateCommission(roleValue, relatedShare, sale));
                        if (foundRole.length === 0) {
                            // this role doesn't exist, push the new one
                            foundUser[0].detail.push(newUserProfit);
                            foundUser[0].totalValue += newUserProfit.netValue;
                        } else {
                            // update the value and also totalValue (recalculate the sum)
                            foundRole[0].value = newUserProfit.value;
                            foundRole[0].commission = relatedShare.realCommission;
                            foundRole[0].netValue = newUserProfit.netValue;
                            foundRole[0].netCommission = relatedShare.netCommission;
                            foundUser[0].totalValue = foundUser[0].detail.reduce(function (prev, current) {
                                return current.netValue + prev;
                            }, 0);
                        }
                    }
                }
            });

            var oneSalePromise = Q.defer();
            if (typeof saleCommissions === 'undefined') {
                // finish the update process, persist data to database
                var callBack = function (index) {
                    var handleErr = function (saveErr, savedData) {
                        if (saveErr) {
                            console.log(index + ' ~~> Cannot persist this sale');
                            console.log(saveErr);
                            oneSalePromise.resolve({ret_code: 3, msg: 'Mongo exception: ' + JSON.stringify(saveErr)});
                            return;
                        }
                        //console.log(index + ' ~~> Persist Done with this data: ' + JSON.stringify(savedData.allCommissions));
                        oneSalePromise.resolve({ret_code: 0});
                    };
                    return handleErr;
                };
                sale.save(callBack(count));
            } else {
                oneSalePromise.resolve({ret_code: 0});
            }
            return oneSalePromise.promise;
        });
        Q.all(updatingSalePromises).then(function (listResult) {
            var err = listResult.filter(function (res) {
                return res.ret_code !== 0;
            });
            if (err.length !== 0) {
                console.log('[updateProfit] There are ' + err.length + ' errors');
                deferred.resolve({
                    ret_code: 3,
                    msg: 'there are ' + err.length + ' errors, detail:\n' + JSON.stringify(err)
                });
            } else {
                console.log('[updateProfit] Total updated with ' + count + ' !!!');
                deferred.resolve({ret_code: 0, msg: 'total updated with' + count});
            }
        }).catch(catchError);
    }).catch(catchError);
    return deferred.promise;
}

/**
 *
 * @param shareInfo
 * @param newCommission
 * @param newRealParentCommission
 * @returns {*|promise}
 */
function editShareRecursive(shareInfo, newCommission, newRealParentCommission) {
    var debugInfo = ShareService.shareInfo2Str(shareInfo);
    console.log('[editShareRecursive] ' + debugInfo + ' ~~~~> ' + '(' + newCommission + ', ' + newRealParentCommission + ')');
    var deferred = Q.defer();
    var catchError = Core.genericErrHandler(deferred, [], 'resolve', 'ERROR in editShareRecursive()');
    ShareService.findOneShare(shareInfo).then(function (shareFromDB) {
        if (!shareFromDB) {
            console.log('[editShareRecursive] ~~~~~> NOT FOUND');
            deferred.resolve([]);
            return;
        }
        var oldCommission = shareFromDB.commission;
        var oldRealCommission = shareFromDB.realCommission;
        var oldRawNetCommission = shareFromDB.netCommission / oldRealCommission * 100;
        var realParentCommission = newRealParentCommission || oldRealCommission / oldCommission * 100;
        if (typeof newCommission === 'undefined') {
            shareFromDB.commission = oldCommission;
        } else {
            shareFromDB.commission = newCommission;
        }

        shareFromDB.realCommission = realParentCommission * shareFromDB.commission / 100;
        shareFromDB.netCommission = oldRawNetCommission * shareFromDB.realCommission / 100;
        console.log('Result: (' + shareFromDB.commission + ' - ' + shareFromDB.realCommission + ' - ' + shareFromDB.netCommission + ')');

        var arrPromise = shareFromDB.shareWith.map(function (childShare) {
            shareInfo.userId = childShare.userId;
            shareInfo.role = childShare.role;
            return editShareRecursive(shareInfo, undefined, shareFromDB.realCommission);
        });

        if (arrPromise.length === 0) {
            var deferred2 = Q.defer();
            deferred2.resolve([]);
            arrPromise = [deferred2.promise];
        }

        Q.all(arrPromise).then(function (listResult) {
            var allEditedTrack = Core.flattenArray(listResult);
            allEditedTrack.push(shareFromDB.toObject());

            shareFromDB.save(function (saveErr) {
                if (saveErr) {
                    console.log('[editShareRecursive] cannot save this share ' + debugInfo);
                    deferred.resolve([]);
                    return;
                }
                deferred.resolve(allEditedTrack);
            });
        }).catch(catchError);
    }).catch(catchError);
    return deferred.promise;
}

/**
 *
 * @param clientTrackShare
 * @returns {*|promise}
 */
function editOneTrackShare(clientTrackShare) {
    var deferred = Q.defer();
    var newCommission = clientTrackShare.commission;
    if (!Core.validateCommission(newCommission)) {
        deferred.resolve({'ret_code': 2, 'msg': 'invalid commission'});
        return deferred.promise;
    }

    var catchError = Core.genericErrHandler(deferred, {'ret_code': 4, 'msg': 'Exception in editOneTrackShare'}, 'resolve', 'ERROR in editOneTrackShare()');
    ShareService.findOneShare(clientTrackShare).then(function (modifiedShare) {
        if (!modifiedShare) {
            deferred.resolve({'ret_code': 3, 'msg': 'edit share not found'});
            return;
        }

        // find parent of this share if possible
        var parentPromise = Q.defer();
        var parent = modifiedShare.directPayee;
        if (parent.userId && parent.role) {
            var cloneShare = Core.shallowCopy(clientTrackShare);
            cloneShare.userId = parent.userId;
            cloneShare.role = parent.role;
            ShareService.findOneShare(cloneShare).then(function (parentShare) {
                if (!parentShare) {
                    // something wrong here, exists parent id but not found, reset share and save back to db
                    modifiedShare.directPayee = {};
                    modifiedShare.save(function () {
                        parentPromise.resolve(undefined);
                    });
                    return;
                }
                var oldCommission = modifiedShare.commission;
                var oldNetParentCommission = parentShare.netCommission / parentShare.realCommission * 100;
                var newNetParentCommission = oldNetParentCommission - (newCommission - oldCommission);
                parentShare.netCommission = newNetParentCommission * parentShare.realCommission / 100;
                parentShare.save(function (err, savedParent) {
                    if (err) {parentPromise.resolve(undefined);}
                    parentPromise.resolve(savedParent);
                });
            }).catch(catchError);
        } else {
            parentPromise.resolve(undefined);
        }

        parentPromise.promise.then(function (parentShare) {
            editShareRecursive(clientTrackShare, newCommission, undefined)
                .then(function (allEditedTrack) {
                    if (allEditedTrack.length === 0) {
                        deferred.resolve({'ret_code': 1, 'msg': 'cannot save after modifying trackShare'});
                    } else {
                        // update the profit
                        if (parentShare) {
                            allEditedTrack.push(parentShare);
                        }

                        updateProfit(allEditedTrack, undefined);
                        deferred.resolve({'ret_code': 0, 'msg': 'Successfully edit ' + allEditedTrack.length + ' trackShare '});
                    }
                }).catch(catchError);
        });
    }).catch(catchError);
    return deferred.promise;
}

/**
 * Update the share in collection TrackShare
 * @param share
 * @param listParentShare
 * @returns {*|promise}
 */
function updateTrackShare(share, listParentShare) {
    var {commission, parentUser, parentRole} = share;
    var deferred = Q.defer();

    // find the parent share in `listParentShare`
    var parentShare = ShareService.findParent(share, listParentShare);
    var parentPayee = (parentShare && parentShare.payee) || [];
    var newPayee = parentPayee.concat({userId: parentUser, role: parentRole});
    var parentRealCommission = (parentShare && parentShare.realCommission) || 100;
    var realCommission = commission * parentRealCommission / 100;

    var cloneShareInfo = Core.shallowCopy(share);
    var debugInfo = ShareService.shareInfo2Str(cloneShareInfo);
    var errorHandler = Core.genericErrHandler(deferred, {}, 'reject', 'ERROR in updateTrackShare()');
    editShareRecursive(cloneShareInfo, commission, undefined)
        .then(function (allEditedTrack) {
            if (allEditedTrack.length === 0) {
                // track share doesn't exists
                console.log('[updateTrackShare] Create new share ' + debugInfo + ' with (' + commission + ', ' + realCommission + ')');

                share.netCommission = realCommission;
                share.realCommission = realCommission;
                share.payee = newPayee;
                share.directPayee = {userId: parentUser, role: parentRole};
                share.albumId.trackList = parentShare.albumId.trackList;


                var successHandler = function () {deferred.resolve([share]);};
                ShareService.insertOneShare(share).then(successHandler, errorHandler);
            } else {
                console.log('[updateTrackShare] Edit old share ' + debugInfo + ' with (' + commission + ', ' + realCommission + ')');
                deferred.resolve(allEditedTrack);
            }
        }).catch(errorHandler);
    return deferred.promise;
}

/**
 * Remove user profit in field `allCommissions`
 * @param listTrackShare list of json {trackName, trackId, albumName, removalUsers: [{userId, role}]}
 */
function removeProfit(listTrackShare) {
    if (listTrackShare.length === 0) {
        console.log('[removeProfit] NO REMOVAL because input is empty');
        return;
    }
    console.log('[removeProfit] Fetch all relevant sale data - query length = ' + listTrackShare.length);

    var updatedTrackShares = ShareService.updateName(listTrackShare);

    var deferred = Q.defer();
    var catchError = Core.genericErrHandler(deferred, {}, 'reject', 'ERROR in removeProfit()');
    ShareService.findAllRelevantCommission(updatedTrackShares).then(function (updateSales) {
        console.log('Return from database: all related sale with length = ' + updateSales.length);
        var count = 0;
        updateSales.forEach(function (sale) {
            listTrackShare.forEach(function (relatedShare) {
                if (ShareService.isShareRelevantToSale(relatedShare, sale)) {
                    relatedShare.removalUsers.forEach(function (removal) {
                        var removeUser = removal.userId;
                        var removeRole = removal.role;
                        count += 1;
                        ShareService.logRemoveCommission(count, sale, removeUser, removeRole);

                        // locate the user to update
                        var indexUser = -1;
                        for (var i = 0; i <  sale.allCommissions.length; i++) {
                            if (Core.compareObjId(sale.allCommissions[i].userId, removeUser)) {
                                indexUser = i;
                                break;
                            }
                        }
                        if (indexUser === -1) {
                            console.log('[removeProfit] NO REMOVAL because user not found');
                            return;
                        }

                        var userProfit = sale.allCommissions[indexUser];
                        // locate which role to remove
                        var indexRole = -1;
                        for (var j = userProfit.detail.length - 1; j >= 0; j--) {
                            var listRole = userProfit.detail[j];
                            if (ShareService.checkRoleInRemoveCommission(listRole, relatedShare, sale, removeRole)) {
                                indexRole = j;
                                break;
                            }
                        }
                        if (indexRole === -1) {
                            console.log('[removeProfit] NO REMOVAL because role not found');
                            return;
                        }

                        userProfit.detail.splice(indexRole, 1);

                        if (userProfit.detail.length === 0) {
                            // after remove Role if it is empty, remove this user as well
                            sale.allCommissions.splice(indexUser, 1);
                        } else {
                            // update the totalValue
                            userProfit.totalValue = userProfit.detail.reduce(function (prev, current) {
                                return current.netValue + prev;
                            }, 0);
                        }
                    });
                }
            });

            // finish the update process, persist data to database
            var callBack = function (index) {
                var handleErr = function (saveErr) {
                    if (saveErr) {
                        console.log(index + ' ~~> Cannot update this sale with allCommissions = ' + JSON.stringify(sale.allCommissions));
                        console.log(saveErr);
                    }
                    //console.log(index + ' ~~> Update this sale with allCommissions = ' + JSON.stringify(sale.allCommissions));
                };
                return handleErr;
            };
            ShareService.updateCommission(
                {_id: sale._id},
                {$set: {allCommissions: sale.allCommissions}},
                callBack(count));
        });
        console.log('[removeProfit] Total removed = ' + count + '!!!');
    }).catch(catchError);
}

/**
 * Recursively remove the share in field `shareWith`
 * @param shareInfo
 * @returns {*|promise}
 */
function removeRecursiveChildrenShare(shareInfo) {
    var {userId, role} = shareInfo;
    console.log('[removeRecursiveChildrenShare] user = ' + userId + ' with role = ' + role);
    var deferred = Q.defer();
    var catchError = Core.genericErrHandler(deferred, {'ret_code': 2, 'msg': 'Exception in removeRecursiveChildrenShare'}, 'resolve', 'ERROR in removeRecursiveChildrenShare()');
    ShareService.findOneShare(shareInfo).then(function (shareFromDB) {
        if (!shareFromDB) {
            deferred.resolve({'ret_code': 0, 'msg': 'track share not exists'});
            return;
        }
        // remove all child (user, role) in shareWith
        var arrPromise = shareFromDB.shareWith.map(function (childShare) {
            var cloneShare = Core.shallowCopy(shareInfo);
            cloneShare.userId = childShare.userId;
            cloneShare.role = childShare.role;
            return removeRecursiveChildrenShare(cloneShare);
        });

        if (arrPromise.length === 0) {
            var deferred2 = Q.defer();
            deferred2.resolve({ret_code: 0, 'msg': 'empty shareWith', removalUsers: []});
            arrPromise = [deferred2.promise];
        }

        Q.all(arrPromise).then(function (listResult) {
            var isOkAll = listResult.every(function (result) {
                return result.ret_code === 0;
            });
            if (!isOkAll) {
                console.log('something wrong in recursive removal:\n' + JSON.stringify(listResult));
            }

            var childRemoval = Core.flattenArray(listResult.map(function (result) {
                return result.removalUsers;
            }));
            childRemoval.push({userId: userId, role: role});

            shareFromDB.remove(function (err) {
                if (err) {
                    console.log('cannot remove this share');
                    deferred.resolve({'ret_code': 1, 'msg': JSON.stringify(err)});
                    return;
                }
                deferred.resolve({'ret_code': 0, 'msg': 'Successfully remove trackShare ', removalUsers: childRemoval});
            });
        }).catch(catchError);
    }).catch(catchError);
    return deferred.promise;
}

/**
 *
 * @param shareFromDB
 * @returns {*|promise}
 */
function removeOneShare(shareFromDB) {
    var deferred = Q.defer();
    var userId = shareFromDB.userId;
    var role = shareFromDB.role;

    var catchError = Core.genericErrHandler(deferred, {'ret_code': 2, 'msg': 'Exception in removeOneShare'}, 'resolve', 'ERROR in removeOneShare()');

    // need to remove this (user, role) in shareWith of parent (directPayee)
    var parentPromise = Q.defer();
    if (shareFromDB.directPayee && shareFromDB.directPayee.userId) {
        var parent = shareFromDB.directPayee;

        // if user has parent, need to update netCommission of parent and netValue
        var cloneShare = Core.shallowCopy(shareFromDB.toObject());
        cloneShare.userId = parent.userId;
        cloneShare.role = parent.role;
        ShareService.findOneShare(cloneShare).then(function (parentShare) {
            if (!parentShare) {
                console.log('[removeOneShare] ERROR ~~~> cannot find parent share');
                parentPromise.resolve(true);
                return;
            }
            // update shareWith of parent
            parentShare.shareWith = parentShare.shareWith.filter(function (child) {
                return !(Core.compareObjId(child.userId, userId) && child.role === role);
            });
            // update netCommision of parent
            parentShare.netCommission = parentShare.netCommission + shareFromDB.realCommission;
            parentShare.save(function (err) {
                if (err) {
                    console.log('[removeOneShare] ERROR ~~~> cannot save parent back to db');
                    parentPromise.resolve(true);
                    return;
                }

                // update netValue of parent
                updateProfit([parentShare], undefined).then(function (result) {
                    if (result.ret_code !== 0) {
                        console.log('[removeOneShare] ERROR in updating profit of parentShare:\n' + JSON.stringify(result));
                    }
                    parentPromise.resolve(true);
                });
            });
        }).catch(catchError);
    } else {
        parentPromise.resolve(true);
    }

    parentPromise.promise.then(function () {
        // remove all child (user, role) in shareWith
        var arrPromise = shareFromDB.shareWith.map(function (childShare) {
            var cloneShareInfo = Core.shallowCopy(shareFromDB.toObject());
            cloneShareInfo.userId = childShare.userId;
            cloneShareInfo.role = childShare.role;
            return removeRecursiveChildrenShare(cloneShareInfo);
        });
        if (arrPromise.length === 0) {
            var deferred2 = Q.defer();
            deferred2.resolve({ret_code: 0, 'msg': 'empty shareWith', removalUsers: []});
            arrPromise = [deferred2.promise];
        }

        Q.all(arrPromise).then(function (listResult) {
            var isOkAll = listResult.every(function (result) {
                return result.ret_code === 0;
            });
            if (!isOkAll) {
                console.log('something wrong in recursive removal:\n' + JSON.stringify(listResult));
            }

            var childRemoval = Core.flattenArray(listResult.map(function (result) {
                return (result.removalUsers || []);
            }));
            childRemoval.push({userId: userId, role: role});


            var shareObj = Core.shallowCopy(shareFromDB.toObject());
            shareObj.removalUsers = childRemoval;

            shareFromDB.remove(function (err) {
                if (err) {
                    console.log('cannot remove this share');
                    deferred.resolve({'ret_code': 1, 'msg': JSON.stringify(err), removedShare: []});
                    return;
                }
                deferred.resolve({'ret_code': 0, 'msg': 'Successfully remove trackShare ', removedShare: [shareObj]});
            });
        }).catch(catchError);
    }).catch(catchError);

    return deferred.promise;
}

/* removalTrackShares
[ { albumId:
      { _id: '',
        name: '',
        trackList: [ {trackName: '', trackId: ''},... ] },
    trackId: '',
    userId: { _id: '', displayName: '' },
    role: '',
    albumName: '',
    trackName: '',
}]
*/
/**
 *
 * @param removalTrackShares
 * @returns {*|promise}
 */
function removeMultiple(removalTrackShares) {
    var deferred = Q.defer();
    var catchError = Core.genericErrHandler(deferred, {'ret_code': 2, 'msg': 'Exception in removeMultiple'}, 'resolve', 'ERROR in removeMultiple()');
    if (removalTrackShares.length === 0) {
        console.log('[removeMultiple] NO REMOVAL because input is empty');
        deferred.resolve({'ret_code': 1, 'msg': 'NO REMOVAL because input is empty'});
        return deferred.promise;
    }
    ShareService.findSharesByArray(removalTrackShares).then(function (listShareFromDB) {
        var arrPromise = Core.runSequence(listShareFromDB, removeOneShare);

        Q.all(arrPromise).then(function (listResult) {
            var allRemovedShares = Core.flattenArray(listResult.map(result => (result.removedShare || []) ));

            // then remove the share profit in SaleCommission
            removeProfit(allRemovedShares);
            deferred.resolve({'ret_code': 0, 'msg': 'Successfully remove trackShare '});
        }).catch(catchError);
    });
    return deferred.promise;
}

/**
 *
 * @param input
 * @returns {*|promise}
 */
//(album, addedTrackList, user, role, newCommission)
function addTrackShares(input) {
    var deferred = Q.defer();
    if (!Core.validateCommission(input.commission)) {
        deferred.resolve({ret_code: 3, msg: 'invalid commission'});
        return deferred.promise;
    }

    var catchError = Core.genericErrHandler(deferred, {'ret_code': 2, 'msg': 'Exception in addTrackShares'}, 'resolve', 'ERROR in addTrackShares()');

    // don't add if the share exists (trackId, albumId, userId, role) ~~> primary key
    ShareService.findShares(input).then(data => {
        if (data.length === 0) {
            var errorHandler = Core.genericErrHandler(deferred, {ret_code: 2, msg: 'saving error'}, 'resolve', 'ERROR in addTrackShares()');
            var successHandler = function () {
                // successfully update TrackShare
                // run the background process to update all the relevant sale in SaleCommissions
                var updateProfitPromise = updateProfit(ShareService.tranformInputToUpdateCommission(input), undefined);
                deferred.resolve({
                    ret_code: 0,
                    msg: 'adding successfully (main) track share with length = ' + arguments.length,
                    updateProfitPromise: updateProfitPromise
                });
            };

            ShareService.insertMultipleShares(input).then(successHandler, errorHandler).catch(catchError);
        } else {
            deferred.resolve({ret_code: 1, msg: 'duplicate data'});
        }
    }).catch(catchError);
    return deferred.promise;
}

/**
 *
 * @param listCollaborator
 * @param listParent
 * @returns {*|promise}
 */
function addCollaboratorTrackShares(listCollaborator, listParent) {
    var deferred = Q.defer();
    var invalid = listCollaborator.filter(function (collaShare) {
        var valid = !Core.validateCommission(collaShare.commission);
        collaShare.commission = parseInt(collaShare.commission);
        return valid;
    });
    if (invalid.length > 0) {
        deferred.resolve({ret_code: 3, msg: 'invalid commission'});
        return deferred.promise;
    }
    var total = listCollaborator.reduce(function (total, collaShare) {return total + collaShare.commission;}, 0);
    if (total <= 0 || total > 100) {
        deferred.resolve({ret_code: 4, msg: 'Total commission of all Collaborator must be from 0 to 100'});
        return deferred.promise;
    }

    var catchError = Core.genericErrHandler(deferred, {'ret_code': 1, 'msg': 'Exception in addCollaboratorTrackShares'}, 'resolve', 'ERROR in addCollaboratorTrackShares()');

    ShareService.findSharesByArray(listParent).then(function (listParentShare) {
        // calculate netCommission of all parent = 100 - sum_commission(listCollaborator)
        var netCommission = 100 - listCollaborator.reduce(function (total, collaShare) {return total + collaShare.commission;}, 0);
        console.log('netCommission = ' + netCommission);
        // update shareWith in all parent
        listParentShare.forEach(function (parent) {
            parent.netCommission = netCommission * parent.realCommission / 100;
            parent.shareWith = listCollaborator.map(function (collaShare) {
                return {userId: Core.getObjId(collaShare.userId), role: collaShare.role};
            });
            parent.save(function (err) {
                if (err) { console.log('ERROR in updating shareWith: ' + JSON.stringify(err.stack)); }
            });
        });

        // store all track share
        var arrPromises = ShareService.combineParentWithColla(listCollaborator, listParent).map(function (newShare) {
            return updateTrackShare(newShare, listParentShare);
        });

        var successHandler = data => {
            var flattenShares = Core.flattenArray(data);
            console.log('Successfully saving ' + flattenShares.length + ' trackShares');
            // 3. update the profit
            var updateProfitPromise = updateProfit(flattenShares.concat(listParentShare), undefined);
            deferred.resolve({
                ret_code: 0,
                msg: 'Successfully  saving ' + flattenShares.length + ' trackShares',
                updateProfitPromise: updateProfitPromise
            });
        };
        var errorHandler = Core.genericErrHandler(deferred, {ret_code: 2}, 'resolve', 'ERROR in addCollaboratorTrackShares()');
        Q.all(arrPromises).then(successHandler, errorHandler).catch(catchError);
    }).catch(catchError);
    return deferred.promise;
}

/**
 *
 * @param query
 * @param userId
 * @param isAdmin
 * @returns {*|promise}
 */
function getAllShares(query, userId, isAdmin) {
    var deferred = Q.defer();
    var catchError = Core.genericErrHandler(deferred, {'ret_code': 1, 'msg': 'Exception in getAllShares'}, 'resolve', 'ERROR in getAllShares()');

    var populatePath = [
        {path: 'userId', select: 'displayName'},
        {path: 'shareWith.userId', select: 'displayName'},
        {path: 'directPayee.userId', select: 'displayName'}];
    ShareService.findSharesWithPath(query, populatePath).then(function (trackShares) {
        console.log('1. Fetch all relating track share with query ' + JSON.stringify(query) + ' with length = ' + trackShares.length);

        var buildQuery = [];
        if (userId) {
            // find track share of specific user
            // build query to get the commission of every Collaborator in field shareWith
            trackShares.forEach(function (share) {
                if (share.shareWith) {
                    share.shareWith.forEach(function (collaborator) {
                        var metaInfo = ShareService.copyShareToQuery(share);
                        metaInfo.userId = Core.getObjId(collaborator.userId);
                        metaInfo.role = collaborator.role;
                        buildQuery.push(metaInfo);
                    });
                }
            });
        }

        var collaboratorPromise;
        console.log('2. Prepare query to get info of all collaborator with length = ' + buildQuery.length);
        if (buildQuery.length > 0) {
            collaboratorPromise = ShareService.findSharesWithPath({$or: buildQuery}, [{path: 'userId', select: 'displayName'}]);
        } else {
            var d = Q.defer();
            d.resolve(trackShares);
            collaboratorPromise = d.promise;
        }

        collaboratorPromise.then(function (foundTrackShares) {
            var allTrackShare = trackShares.map(function (originalShare) {
                // Data structure return to client (if trackShare)
                var jsonStructure = {
                    albumId: {
                        _id: '',
                        name: '',
                        trackList: ['']
                    },
                    trackId: '',
                    userId: {_id: '', displayName: ''},
                    role: '',
                    commission: 100,
                    shareWith: [{
                        userId: '',
                        userName: '',
                        role: '',
                        commission: ''
                    }],
                    albumName: '',
                    trackName: '',
                    userName: ''
                };

                var share = ShareService.copyShareToClient(originalShare);
                share.userId = originalShare.userId;
                share.userName = originalShare.userId.displayName;
                share.role = originalShare.role;
                share.commission = originalShare.commission;

                // create commission for every Collaborator if have any
                if (originalShare.shareWith) {
                    share.shareWith = originalShare.shareWith.map(function (collaborator) {
                        // find commission of this collaborator in array `foundTrackShares`
                        var foundColla = ShareService.findCollaborator(foundTrackShares, originalShare, collaborator);
                        var collaCommission = (foundColla && foundColla.commission) || 0;
                        var collaRealCommission = (foundColla && foundColla.realCommission) || 0;
                        var collaNetCommission = (foundColla && foundColla.netCommission) || 0;

                        var newChild = {
                            userId: Core.getObjId(collaborator.userId),
                            userName: collaborator.userId.displayName,
                            role: collaborator.role,
                            commission: collaCommission
                        };
                        newChild.debugShare = '';
                        if (isAdmin) {
                            newChild.realCommission = collaRealCommission;
                            newChild.netCommission = collaNetCommission;
                            newChild.debugShare = 'realCommission = ' + newChild.realCommission + ', netCommission = ' + newChild.netCommission;
                        }
                        return newChild;
                    });
                }

                share.debugShare = '';
                if (isAdmin) {
                    share.directPayee = originalShare.directPayee;
                    share.realCommission = originalShare.realCommission;
                    share.netCommission = originalShare.netCommission;
                    var parent = 'No parent';
                    if (share.directPayee && share.directPayee.userId) {
                        parent = 'Parent (' + share.directPayee.userId.displayName + ', ' + share.directPayee.role + ')';
                    }
                    share.debugShare = parent + ', realCommission = ' + share.realCommission + ', netCommission = ' + share.netCommission;
                }
                return share;
            });
            global.gc();
            deferred.resolve(allTrackShare);
        }).catch(catchError);
    }).catch(catchError);
    return deferred.promise;
}

exports.setTypeShare = function (type) {
    ShareService.setTypeShare(type);
};

exports.addTrackShares = addTrackShares;
exports.addCollaboratorTrackShares = addCollaboratorTrackShares;
exports.editOneTrackShare = editOneTrackShare;

exports.removeMultiple = removeMultiple;
exports.updateProfit = updateProfit;

exports.getAllShares = getAllShares;

exports.getAllShareInfo = ShareService.findAllShareInfo;
exports.aggregateShare = ShareService.aggregateShare;
exports.getTrackSharesByAlbum = ShareService.findShareByAlbum;
exports.insertMultipleShares = ShareService.insertMultipleShares;
