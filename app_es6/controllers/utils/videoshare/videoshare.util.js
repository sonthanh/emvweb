'use strict';

var mongoose = require('mongoose'),
    Q = require('q'),
    VideoShare = mongoose.model('VideoShare'),
    YoutubeSale = mongoose.model('YoutubeSale');

var validateCommission = function (commission) {
    var value = parseInt(commission);
    return !isNaN(value) && value >= 0 && value <= 100;
};

var flattenArray = function (childRemoval) {
    return childRemoval.reduce(function (a, b) {
        return a.concat(b);
    }, []);
};

var getObjId = function (obj) {
    if (typeof obj._id === 'undefined') {
        return obj;
    } else {
        return obj._id;
    }
};

var compareObjId = function (o1, o2) {
    if (typeof o1 === 'undefined' || typeof o2 === 'undefined') { return false; }
    var obj1 = getObjId(o1);
    var obj2 = getObjId(o2);
    var str1 = obj1.str || obj1.toString();
    var str2 = obj2.str || obj2.toString();
    return str1 === str2;
};

/**
 * Update user profit in field `allCommissions`
 * @param updatedVideoShares list of json {videoId, channelId: {_id, videoList, name}, userId, role, realCommission, netCommission}
 * @param listUpdateSale
 */
var updateProfit = function (updatedVideoShares, listUpdateSale) {
    var salesPromise;
    if (typeof listUpdateSale === 'undefined') {
        // get all necessary query to fetch all data at once
        var buildQuery = updatedVideoShares.map(function (share) {
            return {'videoId': {$in: [share.videoId]}};
        });
        if (buildQuery.length === 0) {
            console.log('[updateProfit] NO UPDATE because input is empty');
            return;
        }
        console.log('[updateProfit] Fetch all sale data relating with updated commission - query length = ' + buildQuery.length);
        salesPromise = YoutubeSale.find({$or: buildQuery}).exec();
    } else {
        var deferred2 = Q.defer();
        deferred2.resolve(listUpdateSale);
        console.log('[updateProfit] Get the sales in memory with length = ' + listUpdateSale.length);
        salesPromise = deferred2.promise;
    }
    var deferred = Q.defer();
    salesPromise.then(function (updateSales) {
        console.log('Update all related sale with length = ' + updateSales.length);
        var count = 0;
        try {
            updateSales.forEach(function (sale) {
                sale.allCommissions = sale.allCommissions || [];
                updatedVideoShares.forEach(function (relatedShare) {
                    if (relatedShare.albumId === sale.channelId && relatedShare.trackId === sale.videoId) {

                        count += 1;
                        console.log(count + ' ~~> Updating this sale: (' + sale.album_name + ', ' + sale.track_name +
                            ') with profit=' + sale.profit);

                        var realProfit = sale.profit;
                        var newUserProfit = {};
                        newUserProfit.role = relatedShare.role;
                        newUserProfit.value = realProfit * relatedShare.realCommission / 100;
                        newUserProfit.commission = relatedShare.realCommission;
                        newUserProfit.netValue = realProfit * relatedShare.netCommission / 100;
                        newUserProfit.netCommission = relatedShare.netCommission;

                        // locate the user to update
                        var foundUser = sale.allCommissions.filter(function (userCommission) {
                            return compareObjId(userCommission.userId, relatedShare.userId);
                        });
                        if (foundUser.length === 0) {
                            // this user doesn't exist, push the new data
                            sale.allCommissions.push({
                                userId: relatedShare.userId,
                                totalValue: newUserProfit.netValue,
                                detail: [newUserProfit]
                            });
                        } else {
                            // locate the role to update
                            var foundRole = foundUser[0].detail.filter(function (roleValue) {
                                return roleValue.role === relatedShare.role;
                            });
                            if (foundRole.length === 0) {
                                // this role doesn't exist, push the new one
                                foundUser[0].detail.push(newUserProfit);
                                foundUser[0].totalValue += newUserProfit.netValue;
                            } else {
                                // update the value and also totalValue (recalculate the sum)
                                foundRole[0].value = newUserProfit.value;
                                foundRole[0].commission = relatedShare.realCommission;
                                foundRole[0].netValue = newUserProfit.netValue;
                                foundRole[0].netCommission = relatedShare.netCommission;
                                foundUser[0].totalValue = foundUser[0].detail.reduce(function (prev, current) {
                                    return current.netValue + prev;
                                }, 0);
                            }
                        }
                    }
                });

                if (typeof listUpdateSale === 'undefined') {
                    // finish the update process, persist data to database
                    var callBack = function (index) {
                        var handleErr = function (saveErr) {
                            if (saveErr) {
                                console.log(index + ' ~~> Cannot persist this sale');
                                console.log(saveErr);
                            }
                            //console.log(index + ' ~~> Persist Done');
                        };
                        return handleErr;
                    };
                    sale.save(callBack(count));
                }
            });
            global.gc();
            console.log('[updateProfit] Total updated with ' + count + ' !!!');
            deferred.resolve({ret_code: 0, msg: 'total updated with' + count});
        } catch (error) {
            console.log(count + ' ~~> Exception with stacktrace:');
            console.error(error.stack);
        }
    });
    return deferred.promise;
};

var addVideoShares = function (channel, addedVideoList, user, role, newCommission) {
    var deferred = Q.defer();
    if (!validateCommission(newCommission)) {
        deferred.resolve({ret_code: 3, msg: 'invalid commission'});
        return;
    }

    var checkExists = addedVideoList.map(function (video) {
        return {videoId: video.videoId, channelId: channel, userId: user, role: role};
    });

    // don't add if the share exists (videoId, channelId, userId, role) ~~> primary key
    VideoShare.find({$or: checkExists}).exec().then(function (data) {
        if (data.length === 0) {
            var insertDocs = addedVideoList.map(function (video) {
                return {videoId: video.videoId, channelId: channel, userId: user, role: role,
                    commission: newCommission, realCommission: newCommission, netCommission: newCommission, payee: [], shareWith: []};
            });

            VideoShare.create(insertDocs, function (err) {
                if (err) {console.log(err);
                    deferred.resolve({ret_code: 2, msg: 'saving error'});
                    return;
                }

                // successfully update VideoShare
                // run the background process to update all the relevant sale in YoutubeSales
                var updateShares = addedVideoList.map(function (video) {
                    return {videoId:  video.videoId, channelId: channel, userId: getObjId(user), role: role,
                        commission: newCommission, realCommission: newCommission, netCommission: newCommission};
                });
                var updateProfitPromise = updateProfit(updateShares);
                deferred.resolve({
                    ret_code: 0,
                    msg: 'adding successfully (main) video share with length = ' + insertDocs.length,
                    updateProfitPromise: updateProfitPromise
                });
            });
        } else {
            deferred.resolve({ret_code: 1, msg: 'duplicate data'});
        }
    });
    return deferred.promise;
};

/**
 * Remove user profit in field `allCommissions`
 * @param listVideoShare list of json {videoName, videoId, channelName, removalUsers: [{userId, role}]}
 */
var removeProfit = function (listVideoShare) {
    // get all necessary query to fetch all relevant data at once
    var buildQuery = listVideoShare.map(function (share) {
        return {'videoName': {$in: [share.videoName, 'N/A']}, 'channelName': share.channelName};
    });

    if (buildQuery.length === 0) {
        console.log('[removeProfit] NO REMOVAL because input is empty');
        return;
    }

    console.log('[removeProfit] Fetch all relevant sale data - query length = ' + JSON.stringify(buildQuery));
    YoutubeSale.find({$or: buildQuery}).exec().then(function (updateSales) {
        console.log('Return from database: all related sale with length = ' + updateSales.length);
        var count = 0;
        updateSales.forEach(function (sale) {
            listVideoShare.forEach(function (relatedShare) {
                var removedChannel = relatedShare.channelName;
                var removedVideoName = relatedShare.videoName;
                var removedVideoId = relatedShare.videoId;
                var removalUsers = relatedShare.removalUsers;
                if (removedChannel === sale.channelName && (removedVideoName === sale.videoName || sale.videoName === 'N/A')) {
                    removalUsers.forEach(function (removal) {
                        var removeUser = removal.userId;
                        var removeRole = removal.role;
                        count += 1;
                        console.log(count + ' ~~> Removing (' + removeUser + '/' + removeRole + ') of this sale: (' + sale.channelName +
                            ', ' + sale.videoName + ') with quantity=' + sale.quantity + ' and profit=' + sale.profit);

                        // locate the user to update
                        var indexUser = -1;
                        for (var i = 0; i <  sale.allCommissions.length; i++) {
                            if (compareObjId(sale.allCommissions[i].userId, removeUser)) {
                                indexUser = i;
                                break;
                            }
                        }
                        if (indexUser === -1) {
                            console.log('[removeProfit] NO REMOVAL because user not found');
                            return;
                        }

                        var userProfit = sale.allCommissions[indexUser];
                        // locate which role to remove
                        var indexRole = -1;
                        for (var j = userProfit.detail.length - 1; j >= 0; j--) {
                            var listRole = userProfit.detail[j];
                            if (sale.videoName === 'N/A' && listRole.role === removeRole && compareObjId(removedVideoId, listRole.videoId)) {
                                indexRole = j;
                                break;
                            } else if (sale.videoName !== 'N/A' && listRole.role === removeRole) {
                                indexRole = j;
                                break;
                            }
                        }
                        if (indexRole === -1) {
                            console.log('[removeProfit] NO REMOVAL because role not found');
                            return;
                        }

                        userProfit.detail.splice(indexRole, 1);

                        if (userProfit.detail.length === 0) {
                            // after remove Role if it is empty, remove this user as well
                            sale.allCommissions.splice(indexUser, 1);
                        } else {
                            // update the totalValue
                            userProfit.totalValue = userProfit.detail.reduce(function (prev, current) {
                                return current.netValue + prev;
                            }, 0);
                        }
                    });
                }
            });

            // finish the update process, persist data to database
            var callBack = function (index) {
                var handleErr = function (saveErr) {
                    if (saveErr) {
                        console.log(index + ' ~~> Cannot update this sale with allCommissions = ' + JSON.stringify(sale.allCommissions));
                        console.log(saveErr);
                    }
                };
                return handleErr;
            };
            YoutubeSale.update(
                {_id: sale._id},
                {$set: {allCommissions: sale.allCommissions}},
                callBack(count));
        });
        console.log('[removeProfit] Total removed = ' + count + '!!!');
    });
};

/**
 * Recursively remove the share in field `shareWith`
 * @param videoId
 * @param channelId
 * @param userId
 * @param role
 * @returns {deferred.promise|{then, catch, finally}}
 */
var removeRecursiveChildrenShare = function (videoId, channelId, userId, role) {
    console.log('[removeRecursiveChildrenShare] user = ' + userId + ' with role = ' + role);
    var deferred = Q.defer();
    VideoShare
        .findOne({videoId: videoId, channelId: channelId, userId: userId, role: role})
        .exec().then(function (shareFromDB) {
            if (!shareFromDB) {
                deferred.resolve({'ret_code': 0, 'msg': 'video share not exists'});
                return;
            }
            // remove all child (user, role) in shareWith
            var arrPromise = shareFromDB.shareWith.map(function (childShare) {
                return removeRecursiveChildrenShare(videoId, channelId, childShare.userId, childShare.role);
            });

            if (arrPromise.length === 0) {
                var deferred2 = Q.defer();
                deferred2.resolve({ret_code: 0, 'msg': 'empty shareWith', removalUsers: []});
                arrPromise = [deferred2.promise];
            }

            Q.all(arrPromise).then(function (listResult) {
                var isOkAll = listResult.every(function (result) {return result.ret_code === 0;});
                if (!isOkAll) {
                    console.log('something wrong in recursive removal:\n' + JSON.stringify(listResult));
                }

                var childRemoval = flattenArray(listResult.map(function (result) {return result.removalUsers;}));
                childRemoval.push({userId: userId, role: role});

                shareFromDB.remove(function (err) {
                    if (err) {
                        console.log('cannot remove this share');
                        deferred.resolve({'ret_code': 1, 'msg': JSON.stringify(err)});
                        return;
                    }
                    deferred.resolve({'ret_code': 0, 'msg': 'Successfully remove videoShare ', removalUsers: childRemoval});
                });
            });
        });
    return deferred.promise;
};

/**
 * Remove video share
 * @param videoId
 * @param videoName
 * @param channelId
 * @param channelName
 * @param userId
 * @param role
 * @returns {deferred.promise|{then, catch, finally}}
 */
var removeVideoShares = function (videoId, videoName, channelId, channelName, userId, role) {
    var deferred = Q.defer();
    VideoShare
        .findOne({videoId: videoId, userId: userId, role: role})
        .exec().then(function (shareFromDB) {
            if (!shareFromDB) {
                deferred.resolve({'ret_code': 0, 'msg': 'video share not exists'});
                return;
            }
            // need to remove this (user, role) in shareWith of parent (directPayee)
            if (shareFromDB.directPayee && shareFromDB.directPayee.userId) {
                var parent = shareFromDB.directPayee;
                console.log('remove share in `shareWith` of parent');
                VideoShare.update(
                    {videoId: videoId, userId: parent.userId, role: parent.role},
                    {$pull: {shareWith: {userId : userId, role : role} }},
                    function (err) {
                        if (err) {
                            console.log('cannot update shareWith of parent');
                            console.log(err);
                        }
                    });
            }

            // remove all child (user, role) in shareWith
            var arrPromise = shareFromDB.shareWith.map(function (childShare) {
                return removeRecursiveChildrenShare(videoId, channelId, childShare.userId, childShare.role);
            });
            if (arrPromise.length === 0) {
                var deferred2 = Q.defer();
                deferred2.resolve({ret_code: 0, 'msg': 'empty shareWith', removalUsers: []});
                arrPromise = [deferred2.promise];
            }

            Q.all(arrPromise).then(function (listResult) {
                var isOkAll = listResult.every(function (result) { return result.ret_code === 0; });
                if (!isOkAll) {
                    console.log('something wrong in recursive removal:\n' + JSON.stringify(listResult));
                }

                var childRemoval = flattenArray(listResult.map(function (result) {return result.removalUsers;}));
                childRemoval.push({userId: userId, role: role});

                shareFromDB.remove(function (err) {
                    if (err) {
                        console.log('cannot remove this share');
                        deferred.resolve({'ret_code': 1, 'msg': JSON.stringify(err)});
                        return;
                    }
                    // then remove the share profit in YoutubeSale
                    removeProfit([{videoName: videoName, channelName: channelName, videoId: videoId, removalUsers: childRemoval}]);
                    deferred.resolve({'ret_code': 0, 'msg': 'Successfully remove videoShare '});
                });
            });
        });
    return deferred.promise;
};

exports.updateProfit = updateProfit;
exports.addVideoShares = addVideoShares;
exports.removeVideoShares = removeVideoShares;
exports.getObjId = getObjId;
