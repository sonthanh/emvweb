'use strict';

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
	AlbumInfo = mongoose.model('AlbumInfo'),
	TrackShare = mongoose.model('TrackShare'),
	Q = require('q');

exports.getAllAlbumIDNames = function() {
	var deferred = Q.defer();
	AlbumInfo.find().select({name: 1}).exec(function(err, albums) {
		if(err) {
			deferred.reject(false);
		}
		var albumMap = {};
		albums.forEach(function(album) {
			albumMap[album._id] = album.name;
		});
		deferred.resolve(albumMap);
	});
	return deferred.promise;
};

exports.getAllAlbumInfo = function() {
	var deferred = Q.defer();
	AlbumInfo.find().exec(function(err, albums) {
		if(err) {
			deferred.reject(false);
		}
		var albumMap = {};
		albums.forEach(function(album) {
			if(! albumMap[album._id]) {
				albumMap[album._id] = {};
			}
			album.trackList.forEach(function(track) {
				albumMap[album._id][track.trackId] = track.trackName;
			});
		});

		deferred.resolve(albumMap);
	});
	return deferred.promise;
};

exports.getAlbumByName = function(name) {
    var deferred = Q.defer();
	AlbumInfo.find({name: name}).select({trackList: 1}).exec(function(err, tracks) {
		if(err) {
			deferred.reject(false);
		}
		deferred.resolve(tracks);
	});
	return deferred.promise;
};
