'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    users = require('./users.server.controller'),
    ObjectId = mongoose.Types.ObjectId,
    //AlbumInfo = mongoose.model('AlbumInfo'),
    //TrackShare = mongoose.model('TrackShare'),
    promise = require('bluebird'),
    Q = require('q'),
    underscore = require('underscore')._,
    //SaleCommission = mongoose.model('SaleCommission'),
    TrackShareUtil = require('./utils/share.util.js'),
    userCtrl = require('./users.server.controller.js'),
    albumInfoCtrl = require('./album-info.server.controller.js'),
    config = require('../../config/config'),
    Invitation = mongoose.model('Invitation'),
    nodemailer = require('nodemailer'),
    smtpTransport = nodemailer.createTransport(config.mailer.options),
    async = require('async');

var Core = require("./services/core.js");

var isYoutube = function (req) {
    return req.query.api === 'youtube' || req.body.api === 'youtube' || (req.body.length >= 1 && req.body[0].api === 'youtube');
};

exports.setTypeShare = function (req, res, next) {
    if (isYoutube(req)) {
        TrackShareUtil.setTypeShare('youtube');
    } else {
        TrackShareUtil.setTypeShare('track');
    }

    next();
};

exports.getAlbumInfo = function (req, res) {
    //var cond = {};
    if (!userCtrl.isAdmin(req.user)) {
        return res.jsonp({data: []});
    }
/*    TrackShare.find(cond).select({albumId: 1}).exec().then(function (albumIds) {
        albumIds = albumIds.map(function(album) {
            return new ObjectId(album.albumId);
        });

        if (!userCtrl.isAdmin(req.user)) {
            cond = {_id: {$in: albumIds}};
        }

        AlbumInfo.find(cond).sort({name: 1}).exec().then(function (allAlbum) {
            return res.jsonp({data: allAlbum});
        });
    });*/
    TrackShareUtil.getAllShareInfo().then(function (allAlbum) {
        var filterAlbums = allAlbum;
        if (isYoutube(req)) {
            filterAlbums = filterAlbums.map(album => {
                album.trackList = album.trackList.slice(0, 5);
                return album;
            });
        }
        return res.jsonp({data: filterAlbums});
    });
};

exports.getAllUser = function (req, res) {
    if (!userCtrl.isAdmin(req.user)) {
        return res.jsonp({data: []});
    }
    User.find()
        .select({_id: 1, displayName: 1})
        .sort({displayName: 1})
        .exec().then(function (allusers) {
            return res.jsonp({
                data: allusers
            });
        });
};

function getAccessCollaboratorCond(user, isDetail) {
    var deferred = Q.defer();
    if(userCtrl.isAdmin(user)) {
        deferred.resolve({});
    } else {
        var match = {$match:{userId: user._id}};
        var unwind = {$unwind: '$shareWith'};
        var group = {$group: {
            _id: {userId: '$shareWith.userId', role: '$shareWith.role'}
        }};
        if(isDetail) {
            group.$group._id.trackId = '$trackId';
            group.$group._id.albumId = '$albumId';
        }
        TrackShareUtil.aggregateShare([match, unwind, group]).then(function(users) {
            var cond = [{userId: user._id}];
            users.forEach(function(item) {
                var row = {
                    userId: item._id.userId,
                    role: item._id.role
                };

                if(isDetail) {
                    row.albumId = item._id.albumId;
                    row.trackId = item._id.trackId;
                }

                cond.push(row);
            });

            deferred.resolve({$or: cond});
        });
    }
    return deferred.promise;
}

/**
 * Handle loading track share request
 * @param req
 * @param res
 */
exports.getTrackShares = function (req, res) {

    var userId = req.query.userId;
    var albumId = req.query.albumId;
    var search = req.query.search;

    getAccessCollaboratorCond(req.user, true).then(function (match) {
        if(! userCtrl.isAdmin(req.user)) {
            for (var i = match.$or.length - 1; i >= 0; i--) {
                var item = match.$or[i];
                if (userId && !item.userId.equals(new ObjectId(userId))) {
                    match.$or.splice(i, 1);
                } else {

                    if (albumId && !item.albumId) {
                        item.albumId = albumId;
                    }

                    if (albumId && !Core.compareObjId(item.albumId, albumId)) {
                        match.$or.splice(i, 1);
                    }
                }
            }
        } else {
            if(userId) {
                match.userId = userId;
            }
            if (albumId) {
                match.albumId = albumId;
            }
        }

        TrackShareUtil
            .getAllShares(match, userId, userCtrl.isAdmin(req.user))
            .then(function (allTrackShare) {
                var page = req.query.page;
                var numPerPage = req.query.numPerPage;
                var skip = (page - 1) * numPerPage;

                var escapeRegExp = function (str) {
                    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
                };

                var matchAllRegex = function (listRegex, matchingString) {
                    return listRegex.every(function (regex) {
                        return matchingString.match(regex);
                    });
                };

                var result = allTrackShare;
                if (search) {
                    result = allTrackShare.filter(trackShare => {
                        var multiRegex = search.split(' ').map(function (word) {
                            return new RegExp(escapeRegExp(word), 'i');
                        });
                        var fullText = trackShare.albumName + ' ' + trackShare.trackName + ' ' + trackShare.userName + ' ' +
                            trackShare.role + ' ' + trackShare.commission.toString();
                        if (matchAllRegex(multiRegex, fullText)) {
                            return true;
                        } else {
                            // search inside the Collaborator trackshare
                            var filter = trackShare.shareWith.filter(function (nestedShare) {
                                var nestedFullText = nestedShare.userName + ' ' +  nestedShare.role + ' ' + nestedShare.commission.toString();
                                return matchAllRegex(multiRegex, nestedFullText);
                            });
                            return filter.length > 0;
                        }
                    });
                } else {
                    result = allTrackShare.slice(skip, parseInt(skip) + parseInt(numPerPage));
                }

                res.jsonp({
                    data: result,
                    total: allTrackShare.length
                });
            });
    });
};



/**
 * Handle Add the main track share request
 * @param req
 * @param res
 */
exports.addTrackShares = function (req, res) {
    /* POST Data:
    {
        album:
            {   _id: '',
                name: '',
                trackList: [ {trackName: '', trackId: ''},... ] },
        addTracks: [ { trackName: '', trackId: '' } ],
        userId: { _id: '', displayName: ' },
        role: '',
        commission: ''
    }
    */
    console.log('[addTrackShares] Receive data from client: ');
    console.log(req.body);
    TrackShareUtil
        //.addTrackShares(req.body.album, req.body.addTracks || [], req.body.userId, req.body.role, req.body.commission)
        .addTrackShares(req.body)
        .then(function (msg) {
            res.jsonp(msg);
        });
};

/**
 * Handle Add collaborator track share request
 * @param req
 * @param res
 */
exports.addCollaboratorTrackShares = function (req, res) {
    /* POST DATA
    {
    listParent:
        [ { albumId: {
                _id: '',
                name: '',
                trackList: [ {trackName: '', trackId: ''},... ] },
            albumName: '',
            trackId: '',
            trackName: '',
            userId: { _id: '', displayName: ' },
            userName: '',
            role: '',
            commission: '',
            shareWith: [],
            selected: true } ],
    listCollaborator:
        [ { userId: '',
            userName: '',
            role: '',
            commission: '' }]
    }
    */
    console.log('[addCollaboratorTrackShares] Receive data from client: ');
    console.log(req.body);
    TrackShareUtil
        .addCollaboratorTrackShares(req.body.listCollaborator || [], req.body.listParent || [])
        .then(function (msg) {
            res.jsonp(msg);
        });
};

/**
 * Handle edit track share request
 * @param req
 * @param res
 */
exports.editTrackShares = function (req, res) {
    /* POST Data:
    {
        album:
            {   _id: '',
                name: '',
                trackList: [ {trackName: '', trackId: ''},... ] },
        trackId: '',
        userId: { _id: '', displayName: ' },
        role: '',
        commission: ''
    }
    */
    console.log('[editTrackShares] Receive data from client: ');
    console.log(req.body);
    TrackShareUtil.editOneTrackShare(req.body).then(function (msg) {
        res.jsonp(msg);
    });
};

/**
 * Handle delete request
 * @param req
 * @param res
 */
exports.deleteTrackShares = function (req, res) {
    /* POST Data:
    {
        albumId:
            { _id: '',
                name: '',
                trackList: [ {trackName: '', trackId: ''},... ] },
        trackId: '',
        userId: { _id: '', displayName: '' },
        role: '',
        albumName: '',
        trackName: '',
    }
    */
    console.log('[deleteTrackShares] Receive data from client: ');
    console.log(req.body);
    TrackShareUtil.removeMultiple([req.body]).then(function (msg) {
        res.jsonp(msg);
    });
};

/**
 * Delete multiple shares at once
 * @param req
 * @param res
 */
exports.deleteMultipleShares = function (req, res) {
    console.log('[deleteMultipleShares] Receive data from client: ');
    console.log(req.body);
    TrackShareUtil.removeMultiple(req.body).then(function (msg) {
        res.jsonp(msg);
    });
};

/**
 * Handle delete collaborator track share request
 * @param req
 * @param res
 */
exports.deleteCollaboratorTrackShares = function (req, res) {
    /* POST DATA
    {
        userId: '',
        role: '',
        listParent:
        [ {
            albumId:
            { _id: '',
                name: '',
                trackList: [ {trackName: '', trackId: ''},... ] },
            trackId: '',
            albumName: '',
            trackName: '',
            userId: { _id: '', displayName: '' },
            role: ''
        }]
    }
    */
    console.log('[deleteCollaboratorTrackShares] Receive data from client: ');
    console.log(req.body);
    var collaborator = req.body;
    var removalShares = collaborator.listParent.map(function (parent) {
        parent.userId = collaborator.userId;
        parent.role = collaborator.role;
        return parent;
    });
    TrackShareUtil.removeMultiple(removalShares).then(function (msg) {
        res.jsonp(msg);
    });
};

function getAlbumsAccess(user, isTrack) {
    var deferred = Q.defer();

    var project = {$project:{albumId: 1, userId: 1, role: 1}};

    if(userCtrl.isAdmin(user)) {
        deferred.resolve(project);
    } else {
        var aggArrs = [];
        var match = {$match: {userId: user._id}};
        var group = {$group:{
            _id: '$albumId',
            userRoles: {$addToSet: {user: '$userId', role: '$role'}}
        }};
        if(isTrack) {
            project.$project.trackId = 1;
            group.$group.userRoles.$addToSet.trackId = '$trackId';
        }
        aggArrs.push(match);
        aggArrs.push(project);
        aggArrs.push(group);

        var successHandler = data => {
            var albumMap = {};
            data.forEach(function(item) {
                albumMap[item._id] = item.userRoles;
            });

            deferred.resolve(albumMap);
        };
        var errHandler = error => deferred.reject(false);
        TrackShareUtil.aggregateShare(aggArrs).then(successHandler, errHandler);
    }
    return deferred.promise;
}

/**
 * Get all user's collaborators
 */

function getAllCollaborators(user, isTrack) {
    var deferred = Q.defer();
    getAlbumsAccess(user, isTrack).then(function(response) {
        var aggArrs = [];
        if(userCtrl.isAdmin(user)) {
            aggArrs.push(response);
        } else {
            var match = {$match: {
                userId: user._id,
                shareWith: {$not: {$size: 0}}
            }};

            var project1 = {$project:{albumId: 1, shareWith: 1}};
            var unwind = {$unwind: '$shareWith'};
            var project2 = {$project:{
                albumId: '$albumId',
                userId: '$shareWith.userId',
                role: '$shareWith.role'
            }};

            if(isTrack) {
                project1.$project.trackId = 1;
                project2.$project.trackId = '$trackId';
            }

            aggArrs.push(match);
            aggArrs.push(project1);
            aggArrs.push(unwind);
            aggArrs.push(project2);
        }

        var group = {$group:{
            _id: '$albumId',
            userRoles: {$addToSet: {user: '$userId', role: '$role'}}
        }};

        if(isTrack) {
            group.$group.userRoles.$addToSet.trackId = '$trackId';
        }

        aggArrs.push(group);

        albumInfoCtrl.getAllAlbumIDNames().then(function(albums, err) {
            if(err) {
                deferred.reject(false);
            }

            var successHandler = data => {
                data.forEach(function(item) {
                    item.albumName = albums[item._id];
                    if(! userCtrl.isAdmin(user)) {
                        item.userRoles = item.userRoles.concat.apply(item.userRoles, response[item._id]);
                        delete response[item._id];
                    }
                });

                if(! userCtrl.isAdmin(user)) {
                    for(var key in response) {
                        data.push({
                            _id: key,
                            albumName: albums[key],
                            userRoles: response[key]
                        });
                    }
                }

                deferred.resolve(data);
            };
            var errHandler = error => deferred.reject(false);
            TrackShareUtil.aggregateShare(aggArrs).then(successHandler, errHandler);
        });
    });

    return deferred.promise;
}

exports.getAllCollaborators = getAllCollaborators;

exports.getSaleAccessCond = function(user) {
    var deferred = Q.defer();
    if(userCtrl.isAdmin(user)) {
        deferred.resolve({});
    } else {
        albumInfoCtrl.getAllAlbumInfo().then(function(albumMap, err) {
            if (err) {
                deferred.reject(false);
            }
            getAllCollaborators(user, true).then(function (data, err) {
                if (err) {
                    deferred.reject(false);
                }

                var arrCond = [];
                data.forEach(function (album) {
                    album.userRoles.forEach(function (item) {
                        arrCond.push({
                            album_name: album.albumName,
                            $or:[{trackId: item.trackId}, {track_name: albumMap[album._id][item.trackId]}],
                            userId: item.user,
                            role: item.role
                        });
                    });
                });

                if (arrCond.length === 0) {
                    deferred.reject(false);
                }
                arrCond = {$or: arrCond};

                deferred.resolve(arrCond);
            });
        });
    }
    return deferred.promise;
};

exports.getAllAlbumUsers = function(req, res) {

    getAccessCollaboratorCond(req.user).then(function (match) {

        match = {$match: match};
        var project = {$project: {albumId: 1, userId: 1}};
        var group = {$group: {
            _id: {userId: '$userId'},
            albums: {$addToSet: {_id: '$albumId'}}
        }};

        userCtrl.getAllUserIDNames().then(function(userMap) {
            albumInfoCtrl.getAllAlbumIDNames().then(function(albumMap) {
                TrackShareUtil.aggregateShare([match, project, group]).then(function(data) {
                    data.forEach(function(user) {
                        user._id = user._id.userId;
                        user.displayName = userMap[user._id];
                        user.albums.forEach(function(album) {
                            album.name = albumMap[album._id];
                        });
                    });

                    res.jsonp({data: data});
                });
            });
        });
    });
};

exports.allCollaborators = function (req, res) {
    var userId = req.user._id;
    var errHandler = function (err) {res.jsonp({ret_code: 1, msg: JSON.stringify(err)});};
    getAllCollaborators(req.user).then(function (albumGroups) {

        // collect existing collaborator in TrackShare
        var trackshareCollaborators = Core.flattenArray(albumGroups.map(function (elem) {return elem.userRoles;}));

        // remove in the list user who send this request
        trackshareCollaborators = trackshareCollaborators.filter(function (userRole) {return !Core.compareObjId(userRole.user, userId);});

        // convert to list of string id
        trackshareCollaborators = trackshareCollaborators.map(function (userRole) {return userRole.user.str || userRole.user.toString();});
        trackshareCollaborators = underscore.uniq(trackshareCollaborators);

        // collect invited collaborator
        var match = {$match: {user_id: userId}};
        var group = {$group:{
            _id: '$user_id',
            invitedEmails: {$addToSet: '$invite_email'}
        }};
        Invitation.aggregate([match, group]).exec().then(function(invitations) {
            var invitedEmails = invitations.filter(function (invite) {
                return Core.compareObjId(invite._id, userId);
            });
            invitedEmails = (invitedEmails[0] && invitedEmails[0].invitedEmails) || [];
            var buildQuery1 = invitedEmails.map(function (email) {return {email: email};});
            var buildQuery2 = trackshareCollaborators.map(function (idStr) {return {_id: idStr};});
            var buildQuery = buildQuery1.concat(buildQuery2);

            var finalQuery = {$or: buildQuery};
            if (userCtrl.isAdmin(req.user)) {
                finalQuery = {};
            } else if (buildQuery.length === 0) {
                res.jsonp({ret_code: 0, data: [], msg: 'No collaborators'});
                return;
            }

            User.find(finalQuery)
                .select({_id: 1, displayName: 1})
                .sort({displayName: 1})
                .exec().then(function (usersInSystem) {
                    res.jsonp({
                        ret_code: 0,
                        msg: 'Found ' + usersInSystem.length + ' collaborators',
                        data: usersInSystem
                    });
                }, errHandler);
        }, errHandler);
    }, errHandler);
};

exports.inviteCollaborator = function(req, res) {
    var listEmail = req.body.emails;
    var userId = new ObjectId(req.user._id);
    var displayName = req.user.displayName;
    var buildQuery = listEmail.map(function (email) {return {email: email};});
    User.find({$or: buildQuery}).exec().then(function (usersInSystem) {
        usersInSystem.forEach(function (user) {
            var acceptedInvite = {user_id: userId, invite_email: user.email, status: 'A'};
            Invitation.update(acceptedInvite, acceptedInvite, {upsert: true}).exec();
        });

        var emailNotInSystem = listEmail.filter(function (email) {
            return usersInSystem.filter(function (user) {return user.email === email;}).length === 0;
        });

        var arrPromises = emailNotInSystem.map(function (email) {
            var deferred = Q.defer();
            var token = Q.defer();
            Invitation.findOne({user_id: userId, invite_email: email, status: 'P'}).exec().then(function (oldInvite) {
                if (!oldInvite) {
                    Invitation.create({user_id: userId, invite_email: email, status: 'P'}, function (err, newInvite) {
                        token.resolve(newInvite._id);
                    });
                } else {
                    token.resolve(oldInvite._id);
                }
            });
            token.promise.then(function (objId) {
                console.log('Email: ' + email + ' with token = ' + objId);
                res.render('templates/invite-collaborator', {
                    name: email.substring(0, email.indexOf('@')),
                    senderName: displayName,
                    appName: config.app.title,
                    url: 'http://' + req.headers.host + '/acc/signup?token=' + objId + '&email=' + email
                }, function (err, emailHTML) {
                    if (err) {
                        console.log('Cannot render template invite-collaborator'); console.log(err);
                        deferred.resolve(false); return;
                    }
                    var mailOptions = {
                        to: email,
                        from: config.mailer.from,
                        subject: 'Collaborator Invitation',
                        html: emailHTML
                    };
                    smtpTransport.sendMail(mailOptions, function (err) {
                        if (err) {
                            deferred.resolve({isOk: false});
                        } else {
                            deferred.resolve({isOk: true, email: email});
                        }
                    });
                });
            });
            return deferred.promise;
        });
        Q.all(arrPromises).then(function (listResult, err) {
            var success = listResult.filter(function (result) {return result.isOk;});
            success = success.map(function (result) {return result.email;});
            if (err) {
                res.jsonp({ret_code: 1, msg: 'Failure sending email'});
            } else {
                var msg = 'Email has been successfully sent to ' + success.toString();
                if(success.length === 0) {
                    msg = 'Users have been successfully added into collaborator list';
                }
                res.jsonp({ret_code: 0, msg: msg});
            }
        });
    });
};

exports.getTrackSharesByAlbum = TrackShareUtil.getTrackSharesByAlbum;
