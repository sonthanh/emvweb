'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	ObjectId = mongoose.Types.ObjectId,
	underscore = require('underscore')._,
	saleCtrl = require('./sales-commission.server.controller'),
	SaleCommission = mongoose.model('SaleCommission'),
	youtubeCtrl = require('./youtube.server.controller'),
    YoutubeSale = mongoose.model('YoutubeSale');

/**
 * Create a Payment
 */

exports.createSalePayment = function(req, res) {
	var payments = req.body;
	var idx = payments.length;
	payments.forEach(function(payment) {
		var cond = saleCtrl.queryCondition(
			payment.albumName,
			payment.userId,
			payment.role
		);

		if(payment.date) {
			cond.sale_date = new Date(parseInt(payment.date));
		} else {
			if(payment.startDate) {
				cond.sale_date = {};
				cond.sale_date.$gte = parseInt(payment.startDate);
			}

			if(payment.endDate) {
				if(! cond.sale_date) {
					cond.sale_date = {};
				}
				cond.sale_date.$lt = parseInt(payment.endDate);
			}
		}

		SaleCommission.find(cond).exec(function(err, data) {
			if(err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}

			data.forEach(function(item) {
				item.allCommissions.forEach(function(user) {
					if(payment.userId === '' || user.userId.equals(new ObjectId(payment.userId))) {
						user.detail.forEach(function(userRole) {
							if(payment.role === '' || userRole.role === payment.role) {
								userRole.ispaid = Boolean(payment.status);
							}
						});
					}
				});

				item.save(function(err) {
					if(err) {console.log(err);
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					}
					idx--;
					if(idx === 0) {
						return res.status(200).send({});
					}
				});
			});
		});
	});
};

exports.createYouTubePayment = function(req, res) {
	var payments = req.body;
	var idx = payments.length;
	payments.forEach(function(payment) {
		var cond = youtubeCtrl.queryCondition(
			payment.albumName,
			payment.userId,
			payment.role
		);

		if(payment.date) {
			cond.import_date = new Date(parseInt(payment.date));
		} else {
			if(payment.startDate) {
				cond.import_date = {};
				cond.import_date.$gte = parseInt(payment.startDate);
			}

			if(payment.endDate) {
				if(! cond.import_date) {
					cond.import_date = {};
				}
				cond.import_date.$lt = parseInt(payment.endDate);
			}
		}

		YoutubeSale.find(cond).exec(function(err, data) {
			if(err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}

			data.forEach(function(item) {
				item.allCommissions.forEach(function(user) {
					if(payment.userId === '' || user.userId.equals(new ObjectId(payment.userId))) {
						user.detail.forEach(function(userRole) {
							if(payment.role === '' || userRole.role === payment.role) {
								userRole.ispaid = Boolean(payment.status);
							}
						});
					}
				});

				item.save(function(err) {
					if(err) {console.log(err);
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					}
					idx--;
					if(idx === 0) {
						return res.status(200).send({});
					}
				});
			});
		});
	});
};
