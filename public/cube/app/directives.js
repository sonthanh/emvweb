angular.module('cube.directives', []).

	// Layout Related Directives
	directive('headerMenu', function(){
		return {
			restrict: 'E',
			templateUrl: 'modules/core/views/layout/header.client.view.html'
		};
	}).
	directive('pageTitle', function(){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: 'modules/core/views/layout/title.client.view.html',
			link: function(scope, el, attr){
				scope.title = attr.title;
			}
		};
	});
