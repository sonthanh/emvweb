'use strict';

//Sales service used to communicate Sales REST endpoints
angular.module('sales').factory('TrackShareService', function($http, $q, $cacheFactory, $rootScope, Utility) {
        var cache = $cacheFactory('TrackShareService');
        var removeCache = function() {
            cache.removeAll();
        };

        return {
            removeCache: removeCache,

            loadTrackShares: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading track shares with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/track-shares', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getAlbumInfo: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading all album info with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/albuminfo', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getAllAlbumUsers: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading all album users with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/allAlbumUsers', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getAllUser: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading all users info with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/allusers', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getAllCollaborators: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading all collaborators');
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/allcollaborators', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            addTrackShares: function (body) {
                if ($rootScope.debug) {
                    console.log('Add track share with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl +'/track-shares/add', body).success(function (response) {
                    removeCache();
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            addCollaboratorTrackShares: function (body) {
                if ($rootScope.debug) {
                    console.log('Add Collaborator track share with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/track-shares/add-collaborator', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            editCommission: function (body) {
                if ($rootScope.debug) {
                    console.log('Edit track share with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/track-shares/edit', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            removeTrackShare: function (body) {
                if ($rootScope.debug) {
                    console.log('Remove track share with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl +'/track-shares/delete', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            removeCollaboratorTrackShare: function (body) {
                if ($rootScope.debug) {
                    console.log('Remove collaborator track share with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl +'/track-shares/delete-collaborator', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            removeMultiple: function (body) {
                if ($rootScope.debug) {
                    console.log('Remove multiple with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/track-shares/delete-multiple', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            inviteCollaborator: function (body) {
                if ($rootScope.debug) {
                    console.log('Invite collaborator: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/track-shares/invite-collaborator', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            }
        };
    }
);
