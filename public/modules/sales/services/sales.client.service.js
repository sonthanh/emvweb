/**
 * Created by steventran on 5/17/15.
 */

'use strict';

//Sales service used to communicate Sales REST endpoints
angular.module('sales').factory('SalesCache', function($http, $q, $cacheFactory, $rootScope, Utility) {
        var cache = $cacheFactory('SalesCache');
        var removeCache = function() {
            cache.removeAll();
        };

        return {
            removeCache: removeCache,

            getAlbumTrackShareInfo: function() {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/albumtrackshareinfo', {cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadSaleCommission: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading sale commission with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionGroupByTrack: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading track commission with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission-byTrack', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionGroupByArtist: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading group commission with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission-byArtist', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionByGroup: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading group commission with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission-byGroup', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionSummary: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading summary commission with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission-summary', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadChartSummary: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading chart summary with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission-chart', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            createOrUpdatePayment: function (body) {
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/payment', body).success(function (response) {
                    //TrackShareService.removeCache();
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadUserEarningCommission: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading user earning with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-user-earning', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            }
        };
    }
);
