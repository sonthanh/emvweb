'use strict';

var SalesModule = angular.module('sales');

SalesModule.directive('headerFilter', function () {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/headerFilter.client.view.html'
	};
});

SalesModule.directive('tablePagination', function () {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/tablePagination.client.view.html'
	};
});

SalesModule.directive('tableSaleGroup', function(){
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/tableSaleGroup.client.view.html'
	};
});

SalesModule.directive('tableYoutubeGroup', function(){
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/tableYoutubeGroup.client.view.html'
	};
});

SalesModule.directive('tabChartSummary', function(){
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/tabChartSummary.client.view.html'
	};
});

SalesModule.directive('paymentInfo', function(){
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/paymentInfo.client.view.html'
	};
});

SalesModule.directive('focusMe', function($timeout, $parse) {
	return {
		//scope: true,   // optionally create a child scope
		link: function(scope, element, attrs) {
			var model = $parse(attrs.focusMe);
			scope.$watch(model, function(value) {
				if(value === true) {
					$timeout(function() {
						element[0].focus();
					});
				}
			});
		}
	};
});

SalesModule.directive('select2', function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attr, ngModel) {
			//$this becomes element
			element.select2({
				//options removed for clarity
			});
			/*element.on('change', function() {
			 var val = $(this).value;
			 scope.$apply(function(){
			 //will cause the ng-model to be updated.
			 ngModel.$setViewValue(val);
			 });
			 });*/
			/*          ngModel.$render = function() {
			 //if this is called, the model was changed outside of select, and we need to set the value
			 //not sure what the select2 api is, but something like:
			 element.value = ngModel.$viewValue;
			 };*/
		}
	};
});

SalesModule.directive('ngEnter', function() {
	return function(scope, element, attrs) {
		element.bind('keydown keypress', function(event) {
			if(event.which === 13) {
				scope.$apply(function(){
					scope.$eval(attrs.ngEnter, {'event': event});
				});

				event.preventDefault();
			}
		});
	};
});

SalesModule.directive('ngChangeOnBlur', function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elm, attrs, ngModelCtrl) {
			if (attrs.type === 'radio' || attrs.type === 'checkbox')
				return;

			var expressionToCall = attrs.ngChangeOnBlur;

			var oldValue = null;
			elm.bind('focus',function() {
				scope.$apply(function() {
					oldValue = elm.val();
				});
			});
			elm.bind('blur', function() {
				scope.$apply(function() {
					var newValue = elm.val();
					if (newValue.toString() !== oldValue.toString()){
						scope.$eval(expressionToCall);
					}
				});
			});
		}
	};
});
