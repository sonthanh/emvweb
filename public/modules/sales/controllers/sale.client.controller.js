'use strict';

// Sales controller
angular.module('sales').controller('SalesController',
    function ($scope, $location, $window, Authentication,
              SalesCache, Authorization, $filter) {

        $scope.authentication = Authentication;
        if (! $scope.authentication.user) {
            $location.path('/signin');
            return;
        }

        $scope.auth = Authorization;
        $scope.totalPages = 1;
        $scope.currentPage = 1;
        $scope.listAlbum = [];
        $scope.saleCommission = [];
        $scope.selectedTabDisplay = 'tabChartSummary';
        $scope.selectedAlbum = '';
        $scope.selectedUser = '';
        $scope.selectedRole = '';
        $scope.tableHeader = [];
        $scope.earningSummary = {commission: 0, paid: 0, balance: 0, DA: 0, DT: 0, S: 0};
        $scope.isfirstLoad = true;
        $scope.isLoadedChart = false;
        $scope.userNameByID = [];
        $scope.dateRangeStart = [];
        $scope.dateRangeEnd = [];

        $scope.detailHeader = [
            {name: 'ALBUM', sort: 'sorting', schema: 'album_name'},
            {name: 'TRACK', sort: 'sorting', schema: 'track_name'},
            {name: 'PARTNER', sort: 'sorting', schema: 'partner'},
            {name: 'TYPE', sort: 'sorting', schema: 'type'},
            {name: 'QUANTITY', sort: 'sorting', schema: 'quantity'},
            {name: 'PRICE', sort: 'sorting', schema: 'unit'},
            {name: 'SUBTOTAL', sort: 'sorting', schema: 'profit'},
            {name: 'COMISSION', sort: 'sorting', schema: 'allCommissions.detail.value'},
            {name: 'DATE', sort: 'sorting', schema: 'sale_date'}
        ];

        $scope.trackHeader = [
            {name: 'ALBUM', sort: 'sorting', schema: 'album_name'},
            {name: 'TRACK', sort: 'sorting', schema: 'track_name'},
            {name: 'TYPE', sort: 'sorting', schema: 'type'},
            {name: 'QUANTITY', sort: 'sorting', schema: 'quantity'},
            {name: 'SUBTOTAL', sort: 'sorting', schema: 'profit'},
            {name: 'COMISSION', sort: 'sorting', schema: 'commission'}
        ];

        $scope.tableArtistHeader = [
            {name: 'Artist', sort: 'sorting', schema: 'name'},
            {name: 'ROLE', sort: 'sorting', schema: 'role'},
            {name: 'DA', sort: 'sorting', schema: 'da_quantity'},
            {name: '$DA', sort: 'sorting', schema: 'da_profit'},
            {name: 'DT', sort: 'sorting', schema: 'dt_quantity'},
            {name: '$DT', sort: 'sorting', schema: 'dt_profit'},
            {name: 'S', sort: 'sorting', schema: 's_quantity'},
            {name: '$S', sort: 'sorting', schema: 's_profit'},
            {name: 'SUBTOTAL', sort: 'sorting', schema: 'profit'},
            {name: 'COMISSION', sort: 'sorting', schema: 'commission'}
        ];

        $scope.getGroupHeader = function() {
            return [
                {name: $scope.selTblCol, sort: 'sorting', schema: 'name'},
                {name: 'DA', sort: 'sorting', schema: 'da_quantity'},
                {name: '$DA', sort: 'sorting', schema: 'da_profit'},
                {name: 'DT', sort: 'sorting', schema: 'dt_quantity'},
                {name: '$DT', sort: 'sorting', schema: 'dt_profit'},
                {name: 'S', sort: 'sorting', schema: 's_quantity'},
                {name: '$S', sort: 'sorting', schema: 's_profit'},
                {name: 'SUBTOTAL', sort: 'sorting', schema: 'profit'},
                {name: 'COMISSION', sort: 'sorting', schema: 'commission'}
            ];
        };

        $scope.sortedSchema = 'sale_date';
        $scope.isAscending = 1;
        $scope.isLoading = true;

        $scope.showEntries = [10, 25, 50, 100];
        $scope.numEntryPerPage = 10;

        $scope.inputSearchText = undefined;

        $scope.initLoad = function() {
            SalesCache.getAlbumTrackShareInfo().then(function (response) {
                $scope.listAlbum = response.data;
                if($scope.listAlbum.length === 1) {
                    $scope.selectedAlbum = $scope.listAlbum[0];
                    $('#s2id_album').find('.select2-chosen').html($scope.selectedAlbum.albumName);
                }
                $scope.listAlbum.forEach(function(album) {
                    album.users.forEach(function(user) {
                        $scope.userNameByID[user.userId] = user.userName;
                    });
                });
                $scope.setAlbum();
            });
        };

        $scope.setAlbum = function() {
            $('#s2id_user').find('.select2-chosen').html('All Users');
            $scope.selectedUser = '';
            if($scope.selectedAlbum && $scope.selectedAlbum.users) {
                if($scope.selectedAlbum.users.length === 1) {
                    $scope.selectedUser = $scope.selectedAlbum.users[0];
                    $('#s2id_user').find('.select2-chosen').html($scope.selectedUser.userName);
                }
            }
            $scope.setUser();
        };

        $scope.setUser = function() {
            $('#s2id_role').find('.select2-chosen').html('All Roles');
            $scope.selectedRole = '';
            if($scope.selectedUser && $scope.selectedUser.roles) {
                if($scope.selectedUser.roles.length === 1) {
                    $scope.selectedRole = $scope.selectedUser.roles[0];
                    $('#s2id_role').find('.select2-chosen').html($scope.selectedRole.role);
                }
            }
            $scope.setRole();
        };

        $scope.setRole = function () {
            $scope.loadCommission();
        };

        $scope.setTabDisplay = function(tabName) {
            $scope.selectedTabDisplay = tabName;
            $scope.totalPages = 1;
            $scope.currentPage = 1;
            $scope.isAscending = 1;
            $scope.numEntryPerPage = 10;
            $scope.inputSearchText = undefined;
            $scope.groupName = undefined;
            $scope.sortedSchema = $scope.groupName;

            switch(tabName) {
                case 'tabDetail':
                    $scope.tableHeader = $scope.detailHeader;
                    $scope.sortedSchema = 'sale_date';
                    break;
                case 'tabTrack':
                    $scope.tableHeader = $scope.trackHeader;
                    $scope.sortedSchema = 'track_name';
                    break;
                case 'tabLabel':
                    $scope.selTblCol = 'Label';
                    $scope.groupName = 'label';
                    $scope.tableHeader = $scope.getGroupHeader();
                    break;
                case 'tabArtist':
                    break;
                case 'tabAlbum':
                    $scope.selTblCol = 'Album';
                    $scope.groupName = 'album_name';
                    $scope.tableHeader = $scope.getGroupHeader();
                    break;
                case 'tabStore':
                    $scope.selTblCol = 'Store';
                    $scope.groupName = 'partner';
                    $scope.tableHeader = $scope.getGroupHeader();
                    break;

                case 'tabChartSummary':
                    break;
            }
            $scope.loadCommission();
        };

        $scope.loadPageDetail = function (pageNumber) {
            pageNumber = $window.Math.max(pageNumber, 1);
            $scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
            $scope.loadCommission();
        };

        var toggleSortClass = function (sortClass) {
            switch (sortClass) {
                case 'sorting':
                    return 'sorting_asc';
                case 'sorting_asc':
                    return 'sorting_desc';
                case 'sorting_desc':
                    return 'sorting_asc';
                default:
                    return 'sorting';
            }
        };

        var checkIfAscending = function (className) {
            if (className === 'sorting_asc') {
                return 1;
            } else {
                return -1;
            }
        };

        $scope.sortByHeader = function (index) {

            if ($scope.isLoading) {return;}

            console.log('sorting this column ' + index);

            // toggle the sort class at current column `index`
            var newClassName = toggleSortClass($scope.tableHeader[index].sort);
            $scope.tableHeader[index].sort = newClassName;

            // reset other column to 'sorting'
            for (var i = 0; i < $scope.tableHeader.length; i++) {
                if (i === index) { continue; }
                $scope.tableHeader[i].sort = 'sorting';
            }

            // store the sorted state in global variable
            $scope.sortedSchema = $scope.tableHeader[index].schema;
            $scope.isAscending = checkIfAscending(newClassName);

            // reload data
            $scope.loadCommission();
        };

        $scope.updateEntry = function () {
            //console.log('Showing ' + $scope.numEntryPerPage + ' entries');
            // reload data
            $scope.loadCommission();
        };

        $scope.searchEnter = function (keyEvent) {
            if (keyEvent.which === 13) {
                //Enter is pressed
                $scope.search();
            }
        };

        $scope.search = function () {
            if ($scope.isLoading || !$scope.inputSearchText || $scope.inputSearchText && $scope.inputSearchText.trim() === '') {
                return;
            }
            console.log('query with searchText = ' + $scope.inputSearchText);
            // reload data
            $scope.loadCommission();
        };

        $scope.range=function(min, max, total) {
            min = $window.Math.max(min, 1);
            max = $window.Math.min(max, total);

            for(var input=[],i=min; max>=i;i++) {
                input.push(i);
            }
            return input;
        };

        $scope.setTotalPages = function(total) {
            $scope.totalPages = parseInt(total / $scope.numEntryPerPage);
            if ($scope.totalPages * $scope.numEntryPerPage !== total) {
                $scope.totalPages += 1;
            }
        };

        Array.prototype.sum = function (prop) {
            var total = 0;
            for ( var i = 0, _len = this.length; i < _len; i++ ) {
                total += this[i][prop];
            }
            return total;
        };

        $scope.getParameters = function () {
            var userId = '', albumName = '', roleId = '';
            if ($scope.selectedUser && $scope.selectedUser !== '') {
                userId = $scope.selectedUser.userId;
            }

            if($scope.selectedAlbum && $scope.selectedAlbum !== '') {
                albumName = $scope.selectedAlbum.albumName;
            }

            if($scope.selectedRole && $scope.selectedRole !== '') {
                roleId = $scope.selectedRole.role;
            }

            var params = {
                albumName: albumName,
                userId: userId,
                role: roleId,
                startDate: $scope.startDate,
                endDate: $scope.endDate
            };

            if($scope.selectedTabDisplay !== 'tabChartSummary') {
                params.page = $scope.currentPage;
                params.numPerPage = $scope.numEntryPerPage;
                params.sort = $scope.sortedSchema;
                params.isAscending = $scope.isAscending;

                if($scope.groupName) {
                    params.groupName = $scope.groupName;
                }

                if (typeof $scope.inputSearchText !== 'undefined' && $scope.inputSearchText !== '') {
                    params.searchText = $scope.inputSearchText;
                }
            }

            return params;
        };

        $scope.loadCommission = function() {
            if($scope.listAlbum.length > 0) {

                $scope.saleCommission = [];
                $scope.isLoading = true;

                switch ($scope.selectedTabDisplay) {
                    case 'tabDetail':
                        $scope.loadDetailsCommission();
                        break;
                    case 'tabTrack':
                        $scope.loadCommissionGroupByTrack();
                        break;
                    case 'tabArtist':
                        $scope.loadCommissionGroupByArtist();
                        break;
                    case 'tabLabel':
                    case 'tabAlbum':
                    case 'tabStore':
                        $scope.loadCommissionByGroup();
                        break;
                    case 'tabChartSummary':
                        if ($scope.isfirstLoad || $scope.isLoadedChart) {
                            $scope.loadChartSummary();
                        } else {
                            $scope.loadCommissionSummary();
                        }
                        break;
                }
            }
        };

        $scope.assignDateRange = function(min, max) {
            var start = new Date(min);
            var end = new Date(max);

            $scope.dateRangeStart.forEach(function(item, idx) {
                var dte = new Date(item.value);
                if(dte.getFullYear() === start.getFullYear() && dte.getMonth() === start.getMonth() || $scope.dateRangeStart.length === 1) {
                    if(start.getDate() > 2 && idx + 1 < $scope.dateRangeStart.length) {
                        $scope.startDate = $scope.dateRangeStart[idx + 1].value;
                    } else {
                        $scope.startDate = item.value;
                    }
                }
            });

            $scope.dateRangeEnd.forEach(function(item) {
                var dte = new Date(item.value);
                if(dte.getFullYear() === end.getFullYear() && dte.getMonth() === end.getMonth() || $scope.dateRangeEnd.length === 1) {
                    $scope.endDate = item.value;
                }
            });

            start = $filter('date')(new Date($scope.startDate), 'MMM - yyyy');
            end = $filter('date')(new Date($scope.endDate), 'MMM - yyyy');
            $('#s2id_startDate').find('.select2-chosen').html(start);
            $('#s2id_endDate').find('.select2-chosen').html(end);
        };

        $scope.loadChartSummary = function() {
            var params = $scope.getParameters();
            SalesCache.loadChartSummary(params).then(function (response) {
                var data = [];
                for(var i = 0; i < response.data.length; i++) {
                    var date = new Date(response.data[i]._id.sale_date);
                    date = new Date(date.getFullYear(), date.getMonth(), 2);
                    if(i + 1 === response.data.length) {
                        var int_d = new Date(date.getFullYear(), date.getMonth() + 1, 1);
                        date = new Date(int_d - 1);
                    }
                    data.push([date.getTime(), response.data[i].commission]);
                    if($scope.isfirstLoad) {
                        date = new Date(date.getFullYear(), date.getMonth(), 2);
                        $scope.dateRangeStart.push({
                            value: date.getTime(),
                            text: date
                        });

                        var int_dd = new Date(date.getFullYear(), date.getMonth() + 1, 1);
                        date = new Date(int_dd - 1);
                        $scope.dateRangeEnd.push({
                            value: date.getTime(),
                            text: date
                        });
                    }
                }
                $scope.isLoading = false;

                var chartOpt = {
                    colors: ['#689f38'],
                    title : {text : 'Monthly Earning'},
                    series : [{
                        name: 'Earning',
                        data : data,
                        marker : {enabled : true, radius : 3},
                        tooltip: {valueDecimals: 2}
                    }],
                    xAxis: {
                        events: {
                            setExtremes: function (e) {
                                if(e.trigger === 'rangeSelectorButton' || e.DOMEvent && e.DOMEvent.type === 'mouseup') {
                                    $scope.assignDateRange(e.min, e.max);
                                    $scope.isLoadedChart = false;
                                    $scope.loadCommission();
                                }
                            }
                        },
                        minRange: 30 * 24 * 3600
                    },
                    navigator: {
                        maskInside: false,
                        series: {
                            color: '#ffffff',
                            lineColor: '#689f38'
                        }
                    }
                };
                if($scope.isfirstLoad) {
                    chartOpt.rangeSelector = {selected : 1};
                }


                $('#roll14').highcharts('StockChart', chartOpt, function(e) {
                    $scope.assignDateRange(e.xAxis[0].min, e.xAxis[0].max);
                    if($scope.isfirstLoad || $scope.isLoadedChart) {
                        $scope.isfirstLoad = false;
                        $scope.isLoadedChart = false;
                        $scope.loadCommission();
                    }
                });
            });
        };

        $scope.loadCommissionSummary = function() {
            var params = $scope.getParameters();
            return SalesCache.loadCommissionSummary(params).then(function (response) {
                $scope.earningSummary = response.data;
                $scope.isLoading = false;
                if($scope.isLoadedChart) {
                    $scope.loadChartSummary();
                }

                $scope.isLoadedChart = true;
            });
        };

        $scope.loadCommissionGroupByArtist = function() {
            var params = $scope.getParameters();
            return SalesCache.loadCommissionGroupByArtist(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.saleCommission = response.data;
                $scope.isLoading = false;
            });
        };

        $scope.loadCommissionByGroup = function() {
            var params = $scope.getParameters();
            return SalesCache.loadCommissionByGroup(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.saleCommission = response.data;
                $scope.isLoading = false;
            });
        };

        $scope.loadCommissionGroupByTrack = function() {
            var params = $scope.getParameters();
            return SalesCache.loadCommissionGroupByTrack(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.saleCommission = response.data;
                $scope.isLoading = false;
            });
        };

        $scope.isLink = function(item) {
            if(item.dms && item.dms.indexOf('http') >= 0) {
                return true;
            }

            return false;
        };

        $scope.loadDetailsCommission = function () {

            var params = $scope.getParameters();
            return SalesCache.loadSaleCommission(params).then(function (response) {
                $scope.setTotalPages(response.total);
                //console.log('raw data size = ' + data.length);
                $scope.saleCommission = response.data;
                //console.log('Total Page = ' +  $scope.totalPages);
                $scope.isLoading = false;
                return 0;
            });
        };
    }
);
