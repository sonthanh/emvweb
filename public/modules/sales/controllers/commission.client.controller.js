'use strict';

// Sales controller
angular.module('sales').controller('ComissionController',
    function ($scope, $state, $stateParams, $location, $http, SalesCache,
              $upload, $window, Authentication, Notifier, Utility, Authorization) {
        $scope.authentication = Authentication;
        $scope.auth = Authorization;

        $scope.uploadFile = function ($files) {
            if (!$files || !$files[0] || $files[0].type !== 'text/csv') {
                Notifier.error('Invalid CSV File', 'Failed!');
            } else {

                $scope.upload = $upload.upload({
                    url: Utility.apiUrl + '/sales/report',
                    method: 'POST',
                    withCredentials: true,
                    file: $files[0]
                })
                    .progress(function (evt) {
                        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                    })
                    .success(function () {
                        $('#load').css('display', 'none');
                        $scope.success = true;
                        SalesCache.removeCache();
                        Notifier.notify('Kindly waiting until the import process completed...', 'Data Successfully Uploaded!');
                        $window.location.href = '/acc/sale/import-report';
                    })
                    .error(function (response) {
                        $('#load').css('display', 'none');
                        Notifier.error(response.message, 'Failed!');
                    });
                $('#load').css('display', 'block');
            }
        };

        $scope.statusClass = function(status) {
            var label = 'label label-warning';
            if(status === 'Active') {
                label = 'label label-success';
            }
            return label;
        };

        $scope.approveCommission = function(albumId, share, action) {
            $http.post(Utility.apiUrl + '/albums/status-comission', {
                albumId: albumId,
                share: share,
                action: action
            }).success(function (response) {
                $scope.findSaleAlbum();
            }).error(function(err) {
                $scope.findSaleAlbum();
            });
        };
    }
);
