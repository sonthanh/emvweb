/**
 * Created by steventran on 06/07/2015.
 */

'use strict';

var SalesModule = angular.module('sales');
SalesModule
    .filter('filterUnpaid', function() {
        return function(data, key) {
            if (typeof(data) === 'undefined' || typeof(key) === 'undefined') {
                return 0;
            }

            var sum = 0;
            for (var i = data.length - 1; i >= 0; i--) {
                if(data[i].status === false) {
                    sum += parseFloat(data[i][key]);
                } else if(! data[i].status) {
                    return 0;
                }
            }

            return sum;
        };
    });
