'use strict';

//Setting up route
angular.module('sales').config(
	function($stateProvider) {
		// Sales state routing
		$stateProvider.
		state('importSaleReport', {
			url: '/sale/import-report',
			templateUrl: 'modules/sales/views/import-report.client.view.html'
		}).
		state('viewTrackShares', {
			url: '/sale/view-track-shares',
			templateUrl: 'modules/sales/views/view-track-share.client.view.html'
		}).
		state('salePayment', {
				url: '/sale/payment',
				templateUrl: 'modules/sales/views/payment.client.view.html'
	    }).
		state('viewSaleCommission', {
			url: '/view-sale-comission',
			templateUrl: 'modules/sales/views/view-commission.client.view.html'
		});
	}
);
