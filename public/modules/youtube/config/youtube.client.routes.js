'use strict';

//Setting up route
angular.module('youtube').config(
	function($stateProvider) {
		// Sales state routing
		$stateProvider.
		state('importYoutubeReport', {
			url: '/youtube/import-report',
			templateUrl: 'modules/youtube/views/import-report.client.view.html'
		}).
		state('viewYoutubeReport', {
			url: '/youtube/view-report',
			templateUrl: 'modules/youtube/views/youtube-report.client.view.html'
		}).
		state('viewVideoShare', {
			url: '/youtube/view-video-shares',
			templateUrl: 'modules/youtube/views/view-video-share.client.view.html'
		}).
		state('youtubePayment', {
        	url: '/youtube/payment',
        	templateUrl: 'modules/youtube/views/payment.client.view.html'
        });
	}
);
