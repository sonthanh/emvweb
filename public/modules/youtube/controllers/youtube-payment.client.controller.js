'use strict';

// Sales controller
angular.module('youtube').controller('YouTubePaymentController',
    function ($scope, $location, $window, YouTubeCache, Notifier, Authentication, Authorization) {
        $scope.authentication = Authentication;
        $scope.auth = Authorization;
        if (! $scope.authentication.user) {
            $location.path('/signin');
            return;
        }

        $scope.tableHeader = [
            {name: 'DATE', sort: 'sorting_asc', schema: 'sale_date'},
            {name: 'ALBUM', sort: 'sorting', schema: 'album_name'},
            {name: 'USER', sort: 'sorting', schema: 'user_name'},
            {name: 'ROLE', sort: 'sorting', schema: 'role'},
            {name: 'EARN', sort: 'sorting', schema: 'commission'},
            {name: 'STATUS', sort: 'sorting', schema: 'status'}
        ];
        $scope.totalPages = 1;
        $scope.currentPage = 1;
        $scope.showEntries = [10, 25, 50, 100];
        $scope.numEntryPerPage = 10;
        $scope.sortedSchema = 'sale_date';
        $scope.isAscending = 1;
        $scope.isLoading = true;
        $scope.paymentList = [];
        $scope.albumIDByName = [];
        $scope.dateRangeSel = [];
        $scope.paymentStatus = {};
        $scope.totalEarning = 0;
        $scope.totalUnpaid = 0;

        $scope.range=function(min, max, total) {
            min = $window.Math.max(min, 1);
            max = $window.Math.min(max, total);

            for(var input=[],i=min; max>=i;i++) {
                input.push(i);
            }
            return input;
        };

        $scope.setTotalPages = function(total) {
            $scope.totalPages = parseInt(total / $scope.numEntryPerPage);
            if ($scope.totalPages * $scope.numEntryPerPage !== total) {
                $scope.totalPages += 1;
            }
        };

        Array.prototype.sum = function (prop) {
            var total = 0;
            for ( var i = 0, _len = this.length; i < _len; i++ ) {
                total += this[i][prop];
            }
            return total;
        };

        $scope.loadPageDetail = function (pageNumber) {
            pageNumber = $window.Math.max(pageNumber, 1);
            $scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
            $scope.loadUserEarningCommission();
        };

        $scope.updateEntry = function () {
            //console.log('Showing ' + $scope.numEntryPerPage + ' entries');
            // reload data
            $scope.loadUserEarningCommission();
        };

        $scope.searchEnter = function (keyEvent) {
            if (keyEvent.which === 13) {
                //Enter is pressed
                $scope.search();
            }
        };

        $scope.search = function () {
            if ($scope.isLoading || !$scope.inputSearchText || $scope.inputSearchText && $scope.inputSearchText.trim() === '') {
                return;
            }
            console.log('query with searchText = ' + $scope.inputSearchText);
            // reload data
            $scope.loadUserEarningCommission();
        };

        var toggleSortClass = function (sortClass) {
            switch (sortClass) {
                case 'sorting':
                    return 'sorting_asc';
                case 'sorting_asc':
                    return 'sorting_desc';
                case 'sorting_desc':
                    return 'sorting_asc';
                default:
                    return 'sorting';
            }
        };

        var checkIfAscending = function (className) {
            if (className === 'sorting_asc') {
                return 1;
            } else {
                return -1;
            }
        };

        $scope.sortByHeader = function (index) {

            if ($scope.isLoading) {return;}

            console.log('sorting this column ' + index);

            // toggle the sort class at current column `index`
            var newClassName = toggleSortClass($scope.tableHeader[index].sort);
            $scope.tableHeader[index].sort = newClassName;

            // reset other column to 'sorting'
            for (var i = 0; i < $scope.tableHeader.length; i++) {
                if (i === index) { continue; }
                $scope.tableHeader[i].sort = 'sorting';
            }

            // store the sorted state in global variable
            $scope.sortedSchema = $scope.tableHeader[index].schema;
            $scope.isAscending = checkIfAscending(newClassName);

            // reload data
            $scope.loadUserEarningCommission();
        };

        $scope.initLoad = function() {
            YouTubeCache.getChannelVideoShareInfo().then(function (response) {
                $scope.listAlbum = response.data;
                $scope.listAlbum.forEach(function(row) {
                    row['albumId'] = row['channelId'];
                    row['albumName'] = row['channelName'];
                    delete row['channelId'];
                    delete row['channelName'];
                });
                $scope.dateRangeSel = response.saleDate;
                $scope.setAlbum();
            });
        };

        $scope.setAlbum = function() {
            $('#s2id_user').find('.select2-chosen').html('All Users');
            $scope.selectedUser = '';
            $scope.setUser();
        };

        $scope.getPaymentClass = function(status) {
            if(status) {
                return 'btn btn-success';
            }
            return 'btn btn-danger';
        };

        $scope.getPaymentButton = function(status) {
            if(status) {
                return 'Paid';
            }
            return 'UnPaid';
        };

        $scope.setUser = function() {
            $('#s2id_role').find('.select2-chosen').html('All Roles');
            $scope.selectedRole = '';
            $scope.setRole();
        };

        $scope.setRole = function () {
            $scope.loadUserEarningCommission();
        };

        $scope.getParameters = function () {
            var userId = '', albumName = '', roleId = '';

            if($scope.selectedRole && $scope.selectedRole !== '') {
                roleId = $scope.selectedRole.role;
            }

            if ($scope.selectedUser && $scope.selectedUser !== '') {
                userId = $scope.selectedUser.userId;
            }

            if($scope.selectedAlbum && $scope.selectedAlbum !== '') {
                albumName = $scope.selectedAlbum.albumName;
            }

            var params = {
                albumName: albumName,
                userId: userId,
                role: roleId
            };

            params.page = $scope.currentPage;
            params.numPerPage = $scope.numEntryPerPage;
            params.sort = $scope.sortedSchema;
            params.isAscending = $scope.isAscending;
            params.startDate = $scope.startDate;
            params.endDate = $scope.endDate;

            if (typeof $scope.inputSearchText !== 'undefined' && $scope.inputSearchText !== '') {
                params.searchText = $scope.inputSearchText;
            }

            return params;
        };

        $scope.loadUserEarningCommission = function() {
            var params = $scope.getParameters();
            YouTubeCache.loadUserEarningCommission(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.paymentList = response.data;
                $scope.dateRangeSel = response.dateRangeSel;
                $scope.totalEarning = response.totalEarning;
                $scope.totalUnpaid = response.totalUnpaid;
                $scope.isLoading = false;
            });
        };

        $scope.submitPayment = function(index) {
            var body = [];
            if(index === parseInt(index)) {
                body.push({
                    date: new Date($scope.paymentList[index].sale_date).getTime(),
                    albumName: $scope.paymentList[index].album_name,
                    userId: $scope.paymentList[index].user_id,
                    role: $scope.paymentList[index].role,
                    status: ! $scope.paymentList[index].status
                });
            } else {
                if(index === 'true') {
                    index = true;
                } else {
                    index = false;
                }

                body = {
                    albumName: '',
                    userId: '',
                    role: '',
                    status: index
                };

                if($scope.startDate) {
                    body.startDate = new Date($scope.startDate).getTime();
                }
                if($scope.endDate) {
                    body.endDate = new Date($scope.endDate).getTime();
                }
                if($scope.selectedAlbum && $scope.selectedAlbum.albumName) {
                    body.albumName = $scope.selectedAlbum.albumName;
                }
                if($scope.selectedUser.userId) {
                    body.userId = $scope.selectedUser.userId;
                }
                if($scope.selectedRole.role) {
                    body.role = $scope.selectedRole.role;
                }
                body = [body];
            }

            YouTubeCache.createOrUpdatePayment(body).then(function () {
                if(index === parseInt(index)) {
                    $scope.paymentList[index].status = !$scope.paymentList[index].status;
                    if(! $scope.paymentList[index].status) {
                        $scope.totalUnpaid += $scope.paymentList[index].commission;
                    } else {
                        $scope.totalUnpaid -= $scope.paymentList[index].commission;
                    }
                } else {
                    for(var idx in $scope.paymentList) {
                        $scope.paymentList[idx].status = index;
                    }
                    if(index) {
                        $scope.totalUnpaid = 0;
                    } else {
                        $scope.totalUnpaid = $scope.totalEarning;
                    }
                }
                Notifier.notify('Payment Updated', 'Success!');
            });
        };
    }
);
