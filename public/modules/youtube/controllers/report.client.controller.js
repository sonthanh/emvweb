'use strict';

// Sales controller
angular.module('youtube').controller('ReportController',
    function ($scope,
              $upload, Authentication, Notifier, Authorization, YouTubeCache) {
        $scope.authentication = Authentication;
        $scope.auth = Authorization;
        $scope.originalCommission = 40;

        $scope.uploadFile = function ($files) {
            if (!$files || !$files[0] || $files[0].type !== 'text/csv') {
                Notifier.error('Invalid CSV File', 'Failed!');
            } else {

                $scope.upload = $upload.upload({
                    headers: {'import-date': $scope.import_date, 'original-commission': $scope.originalCommission},
                    url: '/api/youtube/report',
                    method: 'POST',
                    withCredentials: true,
                    file: $files[0]
                })
                    .progress(function (evt) {
                        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                    })
                    .success(function (response) {
                        $('#load').css('display', 'none');
                        $scope.success = true;
                        Notifier.notify('YouTube report successfully imported', 'Success!');
                    })
                    .error(function (response) {
                        $('#load').css('display', 'none');
                        Notifier.error(response.message, 'Failed!');
                    });
                $('#load').css('display', 'block');
            }
        };
    }
);
