'use strict';

// Sales controller
var youtubeModule = angular.module('youtube');

youtubeModule.controller('VideoSharesController',
    function ($scope, $location, $http, $state, $stateParams, $modal, $window, SalesCache,
              Authentication, Notifier, TrackShareService, Authorization, Utility, $rootScope, YouTubeCache, $timeout) {
        $scope.authentication = Authentication;
        $scope.auth = Authorization;
        if (! $scope.authentication.user) {
            $location.path('/signin');
            return;
        }

        // data for all track share
        $scope.allTrackShare = [];
        $scope.currentUser = {};

        // data for adding track share
        $scope.listAlbum = [];
        $scope.selectedAlbum = {};
        $scope.listTrack = [];
        $scope.selectedTrack = {};
        $scope.listUser = [];
        $scope.filterUser = [];
        $scope.selectedUser = {};
        $scope.listRole = Utility.channelRoles;
        $scope.selectedRole = {};
        $scope.selectedCommission = {};
        $scope.filterAlbum = '';
        $scope.numEntryPerPage = 30;
        $scope.showEntries = [30, 50, 100];
        $scope.totalPages = 1;
        $scope.currentPage = 1;

        // variable used for Modal
        $scope.modalInstance = undefined;
        $scope.parentSelected = undefined;
        $scope.selectedTrackShare = undefined;

        // variable control the sort and filter table
        $scope.sortAlbum = 'albumName';
        $scope.sortTrack = 'trackName';
        $scope.searchText = '';

        // variable used for Delete Modal
        $scope.deleteModalInstance = undefined;
        $scope.deletedShare = undefined;
        $scope.deletedIndexShareWith = undefined;
        $scope.deletedIndexItemModal = undefined;
        $scope.isMultipleRemoval = undefined;

        // variable used for Invite Modal
        $scope.inviteModalInstance = undefined;
        $scope.inviteEmails = {};

        // disable some element in UI when loading from server;
        $scope.isLoading = false;

        $scope.checkAll = undefined;
        $scope.expandedAll = undefined;

        $scope.isAdmin = function () {
            return $scope.auth.isAuthorized('admin');
        };

        $scope.allChannelInfo = [];

        $scope.initLoad = function() {
            YouTubeCache.getVisibleClip().then(function (response) {
                $scope.filterUser = response.data;
                if($scope.filterUser.length === 1) {
                    $scope.currentUser = $scope.filterUser[0];
                    $('#s2id_user').find('.select2-chosen').html($scope.currentUser.displayName);
                }

                if($scope.currentUser.albums && $scope.currentUser.albums.length === 1) {
                    $scope.filterAlbum = $scope.currentUser.albums[0];
                    $('#s2id_album').find('.select2-chosen').html($scope.filterAlbum.name);
                }

                $scope.currentUser = $scope.filterUser.filter(function (userWithAlbum) {
                    return userWithAlbum._id === Authentication.user._id;
                })[0];

                $scope.setUser();
            });

            TrackShareService.getAlbumInfo({api: 'youtube'}).then(function (response) {
                $scope.allChannelInfo = response.data;
            });

            TrackShareService.getAllCollaborators({api: 'youtube'}).then(function (response) {
                if (response.ret_code === 0) {
                    $scope.listUser = response.data;
                } else {
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    $scope.listUser = [];
                }

            });
        };

        $scope.setUser = function() {
            $('#s2id_album').find('.select2-chosen').html('All Albums');
            $scope.filterAlbum = '';
            if($scope.filterUser.length === 1) {
                $scope.currentUser = $scope.filterUser[0];
                $('#s2id_user').find('.select2-chosen').html($scope.currentUser.displayName);
            }

            if($scope.currentUser && $scope.currentUser.albums && $scope.currentUser.albums.length === 1) {
                $scope.filterAlbum = $scope.currentUser.albums[0];
                $('#s2id_album').find('.select2-chosen').html($scope.filterAlbum.name);
            }
            $scope.searchText = "";
            $scope.loadTrackShare();
        };

        $scope.range=function(min, max, total) {
            min = $window.Math.max(min, 1);
            max = $window.Math.min(max, total);

            for(var input=[],i=min; max>=i;i++) {
                input.push(i);
            }
            return input;
        };

        $scope.updateEntry = function () {
            if ($rootScope.debug) {
                console.log('Showing ' + $scope.numEntryPerPage + ' entries');
            }
            // reload data
            $scope.searchText = "";
            $scope.loadTrackShare();
        };

        $scope.setTotalPages = function(total) {
            $scope.totalPages = parseInt(total / $scope.numEntryPerPage);
            if ($scope.totalPages * $scope.numEntryPerPage !== total) {
                $scope.totalPages += 1;
            }
        };

        $scope.loadPageDetail = function (pageNumber) {
            pageNumber = $window.Math.max(pageNumber, 1);
            $scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
            $scope.searchText = "";
            $scope.loadTrackShare();
        };

        $scope.loadTrackShare = function () {
            $scope.allTrackShare = [];
            var sentUserId;
            if ($scope.currentUser && $scope.currentUser._id) {
                sentUserId = $scope.currentUser._id;
            }

            $scope.isLoading = true;
            if ($rootScope.debug) {
                console.log('Current user: ' + $scope.currentUser);
            }

            var sentAlbumId = ($scope.filterAlbum && $scope.filterAlbum._id) || undefined;
            var params = {userId: sentUserId, albumId: sentAlbumId};
            params.numPerPage = $scope.numEntryPerPage;
            params.page = $scope.currentPage;
            params.api = 'youtube';
            if ($scope.searchText && $scope.searchText !== "") {
                params.search = $scope.searchText;
            }

            TrackShareService.loadTrackShares(params).then(function (response) {
                // reset the data
                $scope.allTrackShare = response.data;
                $scope.setTotalPages(response.total);
                if ($rootScope.debug) {
                    console.log('Result size of allTrackShare = ' + $scope.allTrackShare.length);
                }
                $scope.isLoading = false;
            });
        };

        $scope.selectUser = function () {
            if (!$scope.selectedUser.value) {return;}
            console.log($scope.selectedUser.value);
            var userWithClip = $scope.filterUser.filter(function (user) {
                return user._id == $scope.selectedUser.value._id;
            });
            if (userWithClip.length > 0) {
                $scope.listAlbum = userWithClip[0].albums;
            } else {
                $scope.listAlbum = $scope.allChannelInfo;
            }

            //update the $scope.selectedAlbum based on $scope.listAlbum
            var filterChannel = $scope.listAlbum.filter(function (channel) {
                return channel._id == $scope.selectedAlbum._id;
            });
            if (filterChannel.length > 0) {
                $scope.selectedAlbum = filterChannel[0];
                $scope.setAlbum();
            }
        };

        $scope.setAlbum = function () {
            if (!$scope.selectedAlbum) {return;}
            if ($scope.selectedAlbum && !$scope.selectedAlbum.trackList) {return;}
            var all = {trackId: 'All', trackName: 'All tagged video'};
            var result = $scope.selectedAlbum.trackList.sort(function (track1, track2) {
                return track1.trackName.localeCompare(track2.trackName);
            });
            $scope.listTrack = [all].concat(result);
        };

        $scope.addTrackShare = function () {
            var value = parseInt($scope.selectedCommission.value);
            if (invalidCommission(value)) {
                Notifier.error('Commission must be a number from 0 to 100', 'Failed!');
                return;
            }
            var addTracks = [];
            if ($scope.selectedTrack.value.trackId === 'All') {
                addTracks = $scope.selectedAlbum.trackList;
            } else {
                addTracks.push($scope.selectedTrack.value);
            }
            var listNewData = addTracks.map(function (track) {
                return {
                    albumId: $scope.selectedAlbum,
                    albumName: $scope.selectedAlbum.name,
                    trackId: track.trackId,
                    trackName: track.trackName,
                    userId: $scope.selectedUser.value,
                    userName: $scope.selectedUser.value.displayName,
                    role: $scope.selectedRole.value,
                    commission: value,
                    shareWith: []
                };
            });

            $scope.isLoading = true;
            var params = {
                album: $scope.selectedAlbum,
                addTracks: addTracks,
                userId: $scope.selectedUser.value,
                role: $scope.selectedRole.value,
                commission: $scope.selectedCommission.value,
                api: 'youtube'
            };
            TrackShareService.addTrackShares(params).then(function (response) {
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    Notifier.notify('Add new track share successfully', 'Success!');

                    if ($scope.selectedTrack.value.trackId === 'All') {
                        $state.go($state.$current, null, { reload: true });
                    } else {
                        //$scope.allTrackShare = $scope.allTrackShare.concat(listNewData);
                        $scope.allTrackShare = listNewData;

                        $scope.addToFilter();
                        $scope.setTotalPages(listNewData.length);
                        $('#s2id_user').find('.select2-chosen').html($scope.currentUser.displayName);
                        $('#s2id_album').find('.select2-chosen').html($scope.filterAlbum.name);
                    }
                } else {
                    Notifier.error('Cannot add new track share because of ' + response.msg, 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        $scope.addToFilter = function(isCollaborator) {
            var isInfilterUser = false, isInFilterAlbum = false;
            var selUser = {}, selAlbum = {};
            $scope.filterUser.forEach(function(filterUser) {
                if(filterUser._id === $scope.selectedUser.value._id) {
                    isInfilterUser = true;
                    selUser = filterUser;
                    filterUser.albums.forEach(function(filterAlbum) {
                        if(filterAlbum._id === $scope.selectedAlbum._id) {
                            isInFilterAlbum = true;
                            selAlbum = filterAlbum;
                        }
                    });

                    if(! isInFilterAlbum) {
                        selAlbum = {
                            _id: $scope.selectedAlbum._id,
                            name: $scope.selectedAlbum.name
                        };
                        filterUser.albums.push(selAlbum);
                    }
                }
            });

            if(! isInfilterUser) {
                selAlbum = {
                    _id: $scope.selectedAlbum._id,
                    name: $scope.selectedAlbum.name
                };
                selUser = {
                    _id: $scope.selectedUser.value._id,
                    displayName: $scope.selectedUser.value.displayName,
                    albums: [selAlbum]
                };
                $scope.filterUser.push(selUser);
            }

            if(! isCollaborator) {
                $scope.currentUser = selUser;
                $scope.filterAlbum = selAlbum;
            }
        };

        $scope.showExpandedAll = function () {
            var sharesContainCollaborator = $scope.allTrackShare.filter(function (share) {
                return share.shareWith.length > 0;
            });
            return sharesContainCollaborator.length > 0;
        };

        $scope.toggleFieldAll = function () {

            $scope.expandedAll = !$scope.expandedAll;
            $scope.allTrackShare.forEach(function (share) {
                if (share.shareWith.length > 0) {
                    share.expanded = $scope.expandedAll;
                }
            });
        };

        $scope.toggleField = function (obj, field) {
            if (!$scope.isLoading) {
                obj[field] = !obj[field];
            }
        };

        $scope.toggleSort = function (field) {
            if (field.startsWith('-')) {
                return field.substring(1);
            } else {
                return '-' + field;
            }
        };

        $scope.isSortReversed = function (field) {
            return field.startsWith('-');
        };

        var escapeRegExp = function (str) {
            return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
        };

        var matchAllRegex = function (listRegex, matchingString) {
            return listRegex.every(function (regex) {
                return matchingString.match(regex);
            });
        };

        $scope.filterFunction = function(trackShare) {
            var multiRegex = $scope.searchText.split(' ').map(function (word) {
                return new RegExp(escapeRegExp(word), 'i');
            });
            var fullText = trackShare.albumName + ' ' + trackShare.trackName + ' ' + trackShare.userName + ' ' +
                           trackShare.role + ' ' + trackShare.commission.toString();
            if (matchAllRegex(multiRegex, fullText)) {
                return true;
            } else {
                // search inside the Collaborator trackshare
                var filter = trackShare.shareWith.filter(function (nestedShare) {
                    var nestedFullText = nestedShare.userName + ' ' +  nestedShare.role + ' ' + nestedShare.commission.toString();
                    return matchAllRegex(multiRegex, nestedFullText);
                });
                return filter.length > 0;
            }
        };

        $scope.oldEditCommission = undefined;
        $scope.keepOldCommission = function (old) {
            $scope.oldEditCommission = old;
        };

        $scope.editCommission = function (trackShare, field, parentShare) {
            var value = parseInt(trackShare.commission);
            if (isNaN(value) || value > 100 || value < 0) {
                Notifier.error('Commission must be a number from 0 to 100', 'Failed!');
                return;
            }
            if (parentShare) {
                // need to check the total % must not exceed 100
                parentShare.shareWith.forEach(function (share) {share.commission = parseInt(share.commission);});
                var total = parentShare.shareWith.reduce(function (total, share) {return total + share.commission;}, 0);
                if (total > 100) {
                    Notifier.error('Total commission of all Collaborator must be from 0 to 100', 'Failed!');
                    return;
                }
            }
            if (value === 0) {
                if (typeof parentShare === 'undefined') {
                    $scope.deletedShare = trackShare;
                    $scope.deletedIndexShareWith = undefined;
                } else {
                    $scope.deletedShare = parentShare;
                    // find index of child in parent.shareWith
                    var foundIndex = -1;
                    parentShare.shareWith.filter(function (child, index) {
                        if (getObjId(child.userId) === getObjId(trackShare.userId) && child.role === trackShare.role) {
                            foundIndex = index;
                        }
                    });
                    if (foundIndex === -1 && $rootScope.debug) {
                        console.log('NOT FOUND child in parent');
                        return;
                    }
                    $scope.deletedIndexShareWith = foundIndex;
                }
                $scope.deletedIndexItemModal = undefined;
                $scope.isMultipleRemoval = undefined;

                // Open new modal
                $scope.deleteModalInstance = $modal.open({
                    templateUrl: 'modules/sales/views/modal/zero-trackshare-modal.client.view.html',
                    size: 'md',
                    scope: $scope
                });
                $scope.deleteModalInstance.result.then(function () {
                    resetDeleteModal();
                }, function () {
                    trackShare.commission = $scope.oldEditCommission;
                    trackShare[field] = !trackShare[field];
                    resetDeleteModal();
                });
            } else {
                $scope.isLoading = true;
                var params = {
                    album: (trackShare.albumId || parentShare.albumId),
                    trackId: (trackShare.trackId || parentShare.trackId),
                    userId: trackShare.userId,
                    role: trackShare.role,
                    commission: value,
                    api: 'youtube'
                };
                TrackShareService.editCommission(params).then(function (response) {
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    if (response.ret_code === 0) {
                        SalesCache.removeCache();
                        Notifier.notify('Edit commission successfully', 'Success!');
                        trackShare[field] = !trackShare[field];
                    } else {
                        Notifier.error('Cannot save commission', 'Failed!');
                    }
                    $scope.isLoading = false;
                });
            }
        };

        $scope.disableEditCollaborator = function () {
            var selected = $scope.allTrackShare.filter(function (share) {return share.selected;});
            return $scope.isLoading || selected.length === 0;
        };

        function getObjId(obj) {
            if (obj && obj._id) {
                return obj._id;
            } else {
                return obj;
            }
        }

        /**
         * Edit Collaborator modal
         */
        $scope.editCollaborator = function () {
            var selected = $scope.allTrackShare.filter(function (share) {return share.selected;});
            var totalShareWith = [];
            for (var i = 0; i < selected.length; i++) {
                for (var j = 0; j < selected[i].shareWith.length; j++) {
                    var share = selected[i];
                    var childShare = selected[i].shareWith[j];
                    // check if this (user,role) exists in the array
                    var found = totalShareWith.filter(function (share) {
                        return getObjId(share.userId) === getObjId(childShare.userId) && share.role === childShare.role;
                    });
                    if (found.length === 0) {
                        totalShareWith.push({
                            userId: childShare.userId,
                            userName: childShare.userName,
                            role: childShare.role,
                            commission: childShare.commission
                        });
                    }
                }
            }

            $scope.parentSelected = selected;
            $scope.selectedTrackShare = totalShareWith;

            // Open new modal
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/sales/views/modal/add-track-share-modal.client.view.html',
                size: 'lg',
                scope: $scope
            });
        };

        $scope.addCollaborator = function () {
            var value = parseInt($scope.selectedCommission.value);
            if (invalidCommission(value)) {
                Notifier.error('Commission must be a number greater than 0 and less or equal to 100', 'Failed!');
                return;
            }
            var checkExists = $scope.selectedTrackShare.filter(function (share) {
                return getObjId(share.userId) === getObjId($scope.selectedUser.value) &&
                       share.role === $scope.selectedRole.value;
            });
            if (checkExists.length > 0) {
                Notifier.error('Duplicate data', 'Failed!');
                return;
            }
            $scope.selectedTrackShare.forEach(function (share) {share.commission = parseInt(share.commission);});
            var total = $scope.selectedTrackShare.reduce(function (total, share) {return total + share.commission;}, 0) + value;
            if (invalidCommission(total)) {
                if ($rootScope.debug) {
                    console.log('Total commission = ' + total);
                }
                Notifier.error('Total commission of all Collaborator must be from 0 to 100', 'Failed!');
                return;
            }
            $scope.selectedTrackShare.push({
                userId: $scope.selectedUser.value,
                userName: $scope.selectedUser.value.displayName,
                role: $scope.selectedRole.value,
                commission: value
            });
            $scope.addToFilter(true);
        };

        function validateEachCollaCommission(commission) {
            var value = parseInt(commission);
            var failed = false;
            var errMsg = '';
            if (!isNaN(value) && value <= 0) {
                errMsg = 'Please remove 0% trackshare';
                failed = true;
            } else if (isNaN(value)) {
                errMsg = 'Please enter a number';
                failed = true;
            }
            return {ok: !failed, msg: errMsg, value: value};
        }

        function invalidCommission(total) {
            return isNaN(total) || total <= 0 || total > 100;
        }

        $scope.validateAllCollaCommission = function (listTrackShare, validateShare) {
            // validate total commission in `listTrackShare` between (0, 100]
            var failed = false;
            var errMsg = '';
            var total = listTrackShare.reduce(function (total, share) {return total + parseInt(share.commission);}, 0);
            if (typeof validateShare !== 'undefined') {
                var result = validateEachCollaCommission(validateShare.commission);
                if (!result.ok) {
                    errMsg = result.msg;
                    failed = true;
                } else if (invalidCommission(total)) {
                    errMsg = 'Total commission of all Collaborator must be from 0 to 100';
                    failed = true;
                }

                if (!failed) {
                    validateShare.commission = result.value;
                }
            } else {
                var existsZeroShare = listTrackShare.filter(function (share) {
                    var value = parseInt(share.commission);
                    return !isNaN(value) && value === 0;
                });
                if (existsZeroShare.length > 0) {
                    errMsg = 'Please remove 0% trackshare';
                    failed = true;
                } else if (invalidCommission(total)) {
                    errMsg = 'Total commission of all Collaborator must be from 0 to 100';
                    failed = true;
                }
            }

            if (failed) {
                Notifier.error(errMsg, 'Failed!');
            }

            return !failed;
        };

        $scope.saveAll = function () {
            if (!$scope.validateAllCollaCommission($scope.selectedTrackShare, undefined)) {
                return;
            }
            $scope.isLoading = true;
            var params = {
                listParent: $scope.parentSelected,
                listCollaborator: $scope.selectedTrackShare,
                api: 'youtube'
            };
            TrackShareService.addCollaboratorTrackShares(params).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    Notifier.notify('Edit collaborator share successfully', 'Success!');
                    $scope.parentSelected.forEach(function (share) {
                        share.shareWith = JSON.parse(JSON.stringify($scope.selectedTrackShare));
                    });
                    $scope.modalInstance.close('Finish!');
                } else {
                    Notifier.error('Cannot edit', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        $scope.cancel = function () {
            $scope.modalInstance.dismiss('Cancel!');
        };

        /**
         * Delete confirmation modal
         */
        var resetDeleteModal = function () {
            $scope.deleteModalInstance = undefined;
            $scope.deletedShare = undefined;
            $scope.deletedIndexShareWith = undefined;
            $scope.deletedIndexItemModal = undefined;
            $scope.isMultipleRemoval = undefined;
        };

        $scope.openDeleteModal = function (removeTrackShare, removeIndexInShareWith, removeIndexInItemModal, multipleRemove) {
            $scope.deletedShare = removeTrackShare;
            $scope.deletedIndexShareWith = removeIndexInShareWith;
            $scope.deletedIndexItemModal = removeIndexInItemModal;
            $scope.isMultipleRemoval = multipleRemove;
            // Open new modal
            $scope.deleteModalInstance = $modal.open({
                templateUrl: 'modules/sales/views/modal/delete-confirm-modal.client.view.html',
                size: 'md',
                scope: $scope
            });
            $scope.deleteModalInstance.result.then(function () {
                resetDeleteModal();
            }, function () {
                resetDeleteModal();
            });
        };

        var findIndexOfTrackShare = function (listShare, searchShare) {
            var foundIndex = -1;
            for (var i = 0; i < listShare.length; i++) {
                var iteratingShare = listShare[i];
                if (iteratingShare.albumName === searchShare.albumName &&
                    iteratingShare.trackName === searchShare.trackName &&
                    iteratingShare.userName === searchShare.userName &&
                    iteratingShare.role === searchShare.role &&
                    iteratingShare.commission === searchShare.commission) {
                    foundIndex = i;
                    break;
                }
            }
            return foundIndex;
        };

        // remove the main track share
        var removeMainTrackShare = function (trackShare) {
            var removeIndex = findIndexOfTrackShare($scope.allTrackShare, trackShare);
            if (removeIndex !== -1) {
                $scope.isLoading = true;
                trackShare.api = 'youtube';
                TrackShareService.removeTrackShare(trackShare).then(function (response) {
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    if (response.ret_code === 0) {
                        SalesCache.removeCache();
                        Notifier.notify('Remove successfully', 'Success!');
                        $scope.allTrackShare.splice(removeIndex, 1);
                    } else {
                        Notifier.error('Cannot remove', 'Failed!');
                    }
                    $scope.isLoading = false;
                });
            }
        };

        // remove the track share of Collaborator
        var removeCollaboratorTrackShare = function (trackShare, indexInShareWith) {
            $scope.isLoading = true;
            var removeCollaborator = trackShare.shareWith[indexInShareWith];
            var params = {
                userId: removeCollaborator.userId,
                role: removeCollaborator.role,
                listParent: [trackShare],
                api: 'youtube'
            };
            TrackShareService.removeCollaboratorTrackShare(params).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    Notifier.notify('Remove successfully', 'Success!');
                    trackShare.shareWith.splice(indexInShareWith, 1);
                } else {
                    Notifier.error('Cannot remove', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        // remove track share in Edit Modal
        var removeItemInModal = function (selectedIndexModal) {
            $scope.isLoading = true;
            var removeCollaborator = $scope.selectedTrackShare[selectedIndexModal];
            var params = {
                userId: removeCollaborator.userId,
                role: removeCollaborator.role,
                listParent: $scope.parentSelected,
                api: 'youtube'
            };
            TrackShareService.removeCollaboratorTrackShare(params).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    // remove this collaborator in shareWith of all parents
                    $scope.parentSelected.forEach(function (parent) {
                        parent.shareWith = parent.shareWith.filter(function (colla) {
                            return !(getObjId(colla.userId) === getObjId(removeCollaborator.userId) &&
                                    colla.role === removeCollaborator.role);
                        });
                    });

                    Notifier.notify('Remove successfully', 'Success!');
                    $scope.selectedTrackShare.splice(selectedIndexModal, 1);
                } else {
                    Notifier.error('Cannot remove', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        // multiple remove
        var multipleRemove = function () {
            var selectedRemoval = $scope.allTrackShare.filter(function (share) {share.api = 'youtube'; return share.selected;});

            //var selectedRemoval = $scope.allTrackShare.map(function (share) {share.api = 'youtube';return share;});

            TrackShareService.removeMultiple(selectedRemoval).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    $scope.allTrackShare = $scope.allTrackShare.filter(function (share) {return !share.selected;});
                    Notifier.notify('Remove successfully', 'Success!');
                } else {
                    Notifier.error('Cannot remove', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        $scope.confirmDelete = function () {
            if (typeof $scope.deletedShare !== 'undefined' && typeof $scope.deletedIndexShareWith === 'undefined') {
                removeMainTrackShare($scope.deletedShare);
            }
            else if (typeof $scope.deletedShare !== 'undefined' && typeof $scope.deletedIndexShareWith !== 'undefined') {
                removeCollaboratorTrackShare($scope.deletedShare, $scope.deletedIndexShareWith);
            } else if (typeof $scope.deletedIndexItemModal !== 'undefined') {
                removeItemInModal($scope.deletedIndexItemModal);
            } else if ($scope.isMultipleRemoval === 'multiple') {
                multipleRemove();
            }
            $scope.deleteModalInstance.close('Yes!');
        };

        $scope.cancelDelete = function () {
            $scope.deleteModalInstance.dismiss('No!');
        };

        var extractField = function (path, obj) {
            return path.split('.').reduce(function (o, p) {
                return o && o[p];
            }, obj);
        };

        var resetUnCheckedAll = function (listShare) {
            listShare.forEach(function (share) {
                share.selected = false;
            });
        };

        $scope.toggleCheckAll = function () {
            resetUnCheckedAll($scope.allTrackShare);
            if (!$scope.checkAll) {
                // if unchecked all
                $scope.searchText = '';
            } else {
                var albumName = extractField('name', $scope.selectedAlbum) || '';
                //var trackName = extractField('trackName', $scope.selectedTrack) || '';
                //var userName = extractField('value.displayName', $scope.selectedUser) || '';
                //var role = extractField('value', $scope.selectedRole) || '';
                //var commission = extractField('value', $scope.selectedCommission) || '';
                //$scope.searchText = (albumName + ' ' + trackName + ' ' + userName + ' ' + role + ' ' + commission).trim();
                $scope.searchText = albumName.trim();
                var filterShare = $scope.allTrackShare;
                if ($scope.searchText !== '') {
                    filterShare = $scope.allTrackShare.filter($scope.filterFunction);
                }
                filterShare.forEach(function (share) {
                    share.selected = true;
                });
            }
        };

        /**
         * Invite Collaborator modal
         */
        $scope.inviteCollaborator = function () {
            // Open new modal
            $scope.inviteModalInstance = $modal.open({
                templateUrl: 'modules/sales/views/modal/track-share-invite-collaborators.client.view.html',
                size: 'md',
                scope: $scope
            });
        };

        $scope.confirmInvite = function () {
            var listEmail = ($scope.inviteEmails.value || '');
            listEmail = listEmail.split(',').map(function (email) {return email.trim();});
            TrackShareService.inviteCollaborator({emails: listEmail, api: 'youtube'}).then(function (response) {
                SalesCache.removeCache();
                if (response.ret_code === 0) {
                    TrackShareService.getAllCollaborators({api: 'youtube'}).then(function (response) {
                        if (response.ret_code === 0) {
                            $scope.listUser = response.data;
                        } else {
                            if ($rootScope.debug) {
                                console.log(response);
                            }
                            $scope.listUser = [];
                        }
                        Notifier.notify(response.msg, 'Success!');
                    });
                } else {
                    Notifier.error(response.msg, 'Failed!');
                }
            });
            $scope.inviteModalInstance.close('Yes!');
        };

        $scope.cancelInvite = function () {
            $scope.inviteModalInstance.dismiss('Cancel Invite!');
        };

        $scope.searchVideo = function (input) {
            if ($rootScope.debug) {
                console.log('Search video with query: ' + input);
            }
            if (input === '' || !$scope.selectedAlbum.channelId) { return; }
            var path = '/api/youtube/search-video';
            var query = '?channelId=' + $scope.selectedAlbum.channelId + '&search=' + input + '&limit=5';
            $http.get(path + query).then(
                function (response) {
                    var result = response.data;
                    if (result.ret_code === 0) {
                        var all = {trackId: 'All', trackName: 'All tagged video'};
                        $scope.listTrack = [all].concat(result.data);
                    }
                },
                function (err) {
                    if ($rootScope.debug) {
                        console.log('Something wrong in searcing video: ' + err);
                    }
                });
        };

        /*
        filter:filterVideo($select.search)
        $scope.filterVideo = function (typing) {
            return function(video) {
                if (typing && typing !== '') {
                    var multiRegex = typing.split(/\s+/).map(function (word) {
                        return new RegExp(escapeRegExp(word), 'i');
                    });
                    return matchAllRegex(multiRegex, video.trackName);
                } else {
                    return true;
                }
            };
        };
        */

        $scope.delay = (function() {
            var promise = null;
            return function(callback, ms) {
                $timeout.cancel(promise); //clearTimeout(timer);
                promise = $timeout(callback, ms); //timer = setTimeout(callback, ms);
            };
        })();

    }
);
