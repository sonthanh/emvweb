'use strict';

//YouTube service used to communicate YouTube REST endpoints
angular.module('youtube').factory('YouTubeCache', function($http, $q, $cacheFactory, $rootScope, Utility) {
        var cache = $cacheFactory('YouTubeCache');
        var removeCache = function() {
            cache.removeAll();
        };

        return {
            loadYouTubeSale: function(params) {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube/report', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getVideoTag: function(params) {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube/video-tag', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            listChannels: function() {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube/channels', {cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getYouTubeSaleByChannel: function() {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube/sale-by-channel', {cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            removeCache: function() {
                cache.removeAll();
            },

            getChannelVideoShareInfo: function() {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/channelvideoshareinfo', {cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadChartSummary: function (params) {
                //console.log('Loading chart summary with params: ');
                //console.log(params);
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube-commission-chart', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionSummary: function (params) {
                //console.log('Loading summary commission with params: ');
                //console.log(params);
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube-commission-summary', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionGroup: function (params) {
                //console.log('Loading group commission with params: ');
                //console.log(params);
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube-commission-byGroup', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionGroupByVideo: function (params) {
                //console.log('Loading video commission with params: ');
                //console.log(params);
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube-commission-byVideo', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadUserEarningCommission: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading user earning with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube-user-earning', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            createOrUpdatePayment: function (body) {
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/youtube-payment', body).success(function (response) {
                    //TrackShareService.removeCache();
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getVisibleClip: function () {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube/allUserClip').success(function (response) {
                    if ($rootScope.debug) {
                        console.log('Result from YouTubeCache.getVisibleClip()');
                        console.log(response);
                    }
                    dfd.resolve(response);
                });
                return dfd.promise;
            }
        };
    }
);
