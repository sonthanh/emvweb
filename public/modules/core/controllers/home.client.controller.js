'use strict';


angular.module('core').controller('HomeController',
	function($scope, $http, $state, $location, Authentication, Authorization, $rootScope) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		if (! $scope.authentication.user) {
			$location.path('/signin');
			return;
		} else {
			var state = 'viewSaleCommission';
			if($location.search().redirectState) {
				state = $location.search().redirectState;
				delete $location.search().redirectState;
			}
			var debug = $location.search().debug;
			$rootScope.debug = (debug === 'true');
			$state.go(state, $location.search());
		}

		$scope.auth = Authorization;
		
		$scope.getNewsAlert = function() {
			$scope.cntAlert = 0;
			//$http.get('/albums/status-comission')
			//	.success(function (response) {
			//		$scope.cntAlert = parseInt(response.data);
			//});
		};
	}
);
