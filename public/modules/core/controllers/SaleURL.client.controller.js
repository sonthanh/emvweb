'use strict';


angular.module('core').controller('SaleURLController',
	function($scope, $location, $window, Authentication, $stateParams, Notifier, SalesUrl) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		if (! $scope.authentication.user) {
			$location.path('/signin');
			return;
		}
        $scope.tableHeader = [
            {name: 'SOURCE URL', sort: 'sorting', schema: 'sourceUrl'},
            {name: 'SHORTEN URL', sort: 'sorting', schema: 'shortenUrl'},
			{name: 'COMPANY', sort: 'sorting', schema: 'companyName'},
			{name: 'ACTION', sort: 'sorting', schema: 'sourceUrl'}
        ];
		$scope.totalPages = 1;
		$scope.currentPage = 1;
		$scope.showEntries = [10, 25, 50, 100];
		$scope.numEntryPerPage = 10;
		$scope.sortedSchema = 'partnerName';
		$scope.isAscending = 1;
		$scope.isLoading = true;

		$scope.range=function(min, max, total) {
			min = $window.Math.max(min, 1);
			max = $window.Math.min(max, total);

			for(var input=[],i=min; max>=i;i++) {
				input.push(i);
			}
			return input;
		};

		$scope.setTotalPages = function(total) {
			$scope.totalPages = parseInt(total / $scope.numEntryPerPage);
			if ($scope.totalPages * $scope.numEntryPerPage !== total) {
				$scope.totalPages += 1;
			}
		};

		$scope.loadPageDetail = function (pageNumber) {
			pageNumber = $window.Math.max(pageNumber, 1);
			$scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
			$scope.initLoad();
		};

		$scope.updateEntry = function () {
			//console.log('Showing ' + $scope.numEntryPerPage + ' entries');
			// reload data
			$scope.initLoad();
		};

		$scope.searchEnter = function (keyEvent) {
			if (keyEvent.which === 13) {
				//Enter is pressed
				$scope.search();
			}
		};

		$scope.search = function () {
			if ($scope.isLoading || !$scope.inputSearchText || $scope.inputSearchText && $scope.inputSearchText.trim() === '') {
				return;
			}
			console.log('query with searchText = ' + $scope.inputSearchText);
			// reload data
			$scope.initLoad();
		};

		var toggleSortClass = function (sortClass) {
			switch (sortClass) {
				case 'sorting':
					return 'sorting_asc';
				case 'sorting_asc':
					return 'sorting_desc';
				case 'sorting_desc':
					return 'sorting_asc';
				default:
					return 'sorting';
			}
		};

		var checkIfAscending = function (className) {
			if (className === 'sorting_asc') {
				return 1;
			} else {
				return -1;
			}
		};

		$scope.sortByHeader = function (index) {

			if ($scope.isLoading) {return;}

			console.log('sorting this column ' + index);

			// toggle the sort class at current column `index`
			var newClassName = toggleSortClass($scope.tableHeader[index].sort);
			$scope.tableHeader[index].sort = newClassName;

			// reset other column to 'sorting'
			for (var i = 0; i < $scope.tableHeader.length; i++) {
				if (i === index) { continue; }
				$scope.tableHeader[i].sort = 'sorting';
			}

			// store the sorted state in global variable
			$scope.sortedSchema = $scope.tableHeader[index].schema;
			$scope.isAscending = checkIfAscending(newClassName);

			// reload data
			$scope.initLoad();
		};

		$scope.getParameters = function () {
			var params = {};
			params.page = $scope.currentPage;
			params.numPerPage = $scope.numEntryPerPage;
			params.sort = $scope.sortedSchema;
			params.isAscending = $scope.isAscending;

			if (typeof $scope.inputSearchText !== 'undefined' && $scope.inputSearchText !== '') {
				params.searchText = $scope.inputSearchText;
			}

			return params;
		};

		$scope.initLoad = function() {
			var params = $scope.getParameters();
			SalesUrl.listSaleUrl(params).then(function (response) {
				$scope.setTotalPages(response.total);
				$scope.listUrl = response.data;
				$scope.isLoading = false;
			});
		};

		// Open Simple Modal
		$scope.openModal = function(urlId) {
			$scope.removeurlId = urlId;
		};

		$scope.confirmRemove = function() {
			if($scope.removeurlId) {
				SalesUrl.deleteSaleURLByID({urlId: $scope.removeurlId}).then(function () {
					$scope.initLoad();
				});
			}
		};

		$scope.shortenSaleURL = function() {
			SalesUrl.shortenSaleURL({
				sourceUrl: $scope.sourceUrl,
				companyName: $scope.companyName
			}).then(function () {
				$scope.initLoad();
				Notifier.notify($stateParams.message, 'Success!');
			});
		};
	}
);
