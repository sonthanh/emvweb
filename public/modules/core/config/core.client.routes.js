'use strict';

// Setting up route
angular.module('core').config(function($stateProvider, $urlRouterProvider, $locationProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		})
		.state('notification', {
			url: '/notification',
			templateUrl: 'modules/core/views/notification.client.view.html'
		})
		.state('urlSale', {
			url: '/core/url-sale?code&message',
			templateUrl: 'modules/core/views/url-sale.client.view.html'
		});

		// use the HTML5 History API
        $locationProvider.html5Mode(true);
	}
);
