/**
 * Created by steventran on 5/17/15.
 */

'use strict';

//Sales service used to communicate Sales REST endpoints
angular.module('core').factory('SalesUrl', function($http, $q, Utility) {
        return {
            listSaleUrl: function(params) {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl +'/sale-url', {params: params}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            deleteSaleURLByID: function(body) {
                var dfd = $q.defer();
                $http.post(Utility.apiUrl +'/sale-url/delete', body).success(function () {
                    dfd.resolve(true);
                });
                return dfd.promise;
            },

            shortenSaleURL: function(body) {
                var dfd = $q.defer();
                $http.post(Utility.apiUrl +'/bitly-auth', body).success(function () {
                    dfd.resolve(true);
                });
                return dfd.promise;
            },
        };
    }
);
