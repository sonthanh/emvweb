'use strict';

angular.module('core').value('toastr', toastr);

angular.module('core').factory('Notifier', function(toastr) {
	var opts = {
		'closeButton': true,
		'debug': false,
		'positionClass': 'toast-bottom-right',
		'onclick': null,
		'showDuration': '300',
		'hideDuration': '1000',
		'timeOut': '5000',
		'extendedTimeOut': '1000',
		'showEasing': 'swing',
		'hideEasing': 'linear',
		'showMethod': 'fadeIn',
		'hideMethod': 'fadeOut'
	};

	return {
		notify: function(sms, title) {
			toastr.success(sms, title, opts);
		},
		error: function(sms, title) {
			toastr.error(sms, title, opts);
		}
	};
});
