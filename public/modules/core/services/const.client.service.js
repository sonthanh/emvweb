'use strict';

angular.module('core').factory('Utility', function($window) {

	return {
		albumRoles: [
			{id: 'composer', name: 'Composer'},
			{id: 'sound engineer', name: 'Sound Engineer'},
			{id: 'label', name: 'Label'},
			{id: 'vocalist', name: 'Vocalist'},
			{id: 'musician', name: 'Musician'},
			{id: 'ochestrator', name: 'Ochestrator'}
		],
		channelRoles: [
			{id: 'editor', name: 'Editor'},
			{id: 'motion designer', name: 'Motion Designer'},
			{id: 'graphic designer', name: 'Graphic Designer'}
		],
		apiUrl: $window.apiUrl
	};
});
