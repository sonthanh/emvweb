/**
 * Created by sonthanh on 4/6/15.
 */
'use strict';

//Setting up route
angular.module('musicsource').config(
    function($stateProvider) {
        // Sales state routing
        $stateProvider.
            state('submitmusicsource', {
                url: '/musicsource/submit',
                templateUrl: 'modules/musicsource/views/submit.client.view.html'
            });
    }
);
