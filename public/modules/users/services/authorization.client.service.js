'use strict';

// Authentication service for user variables
angular.module('users').factory('Authorization', function(Authentication) {
	var authentication = Authentication;
	return {
		isAuthorized: function(role) {
			return authentication.user && authentication.user.roles.indexOf(role) > -1;
		}
	};
});
