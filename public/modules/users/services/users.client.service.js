'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users',
	function($resource, Utility) {
		return $resource(Utility.apiUrl + '/users/:userId', { userId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
);
