'use strict';

angular.module('users').controller('SettingsController',
	function($scope, $http, $location, $state, $stateParams, $window,
			 Users, Authentication, Notifier, Authorization, SalesCache, TrackShareService, Utility) {
		$scope.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/acc');
		$scope.auth = Authorization;

		$scope.tableHeader = [
			{name: 'NAME', sort: 'sorting_asc', schema: 'displayName'},
			{name: 'EMAIL', sort: 'sorting', schema: 'email'},
			{name: 'ROLES', sort: 'sorting', schema: 'roles'},
			{name: 'ACTIONS', sort: 'sorting', schema: 'displayName'}
		];
		$scope.totalPages = 1;
		$scope.currentPage = 1;
		$scope.showEntries = [10, 25, 50, 100];
		$scope.numEntryPerPage = 10;
		$scope.sortedSchema = 'displayName';
		$scope.isAscending = 1;
		$scope.isLoading = true;

		$scope.range=function(min, max, total) {
			min = $window.Math.max(min, 1);
			max = $window.Math.min(max, total);

			for(var input=[],i=min; max>=i;i++) {
				input.push(i);
			}
			return input;
		};

		$scope.setTotalPages = function(total) {
			$scope.totalPages = parseInt(total / $scope.numEntryPerPage);
			if ($scope.totalPages * $scope.numEntryPerPage !== total) {
				$scope.totalPages += 1;
			}
		};

		$scope.loadPageDetail = function (pageNumber) {
			pageNumber = $window.Math.max(pageNumber, 1);
			$scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
			$scope.listUsers();
		};

		$scope.updateEntry = function () {
			//console.log('Showing ' + $scope.numEntryPerPage + ' entries');
			// reload data
			$scope.listUsers();
		};

		$scope.searchEnter = function (keyEvent) {
			if (keyEvent.which === 13) {
				//Enter is pressed
				$scope.search();
			}
		};

		$scope.search = function () {
			if ($scope.isLoading || !$scope.inputSearchText || $scope.inputSearchText && $scope.inputSearchText.trim() === '') {
				return;
			}
			console.log('query with searchText = ' + $scope.inputSearchText);
			// reload data
			$scope.listUsers();
		};

		var toggleSortClass = function (sortClass) {
			switch (sortClass) {
				case 'sorting':
					return 'sorting_asc';
				case 'sorting_asc':
					return 'sorting_desc';
				case 'sorting_desc':
					return 'sorting_asc';
				default:
					return 'sorting';
			}
		};

		var checkIfAscending = function (className) {
			if (className === 'sorting_asc') {
				return 1;
			} else {
				return -1;
			}
		};

		$scope.sortByHeader = function (index) {

			if ($scope.isLoading) {return;}

			console.log('sorting this column ' + index);

			// toggle the sort class at current column `index`
			var newClassName = toggleSortClass($scope.tableHeader[index].sort);
			$scope.tableHeader[index].sort = newClassName;

			// reset other column to 'sorting'
			for (var i = 0; i < $scope.tableHeader.length; i++) {
				if (i === index) { continue; }
				$scope.tableHeader[i].sort = 'sorting';
			}

			// store the sorted state in global variable
			$scope.sortedSchema = $scope.tableHeader[index].schema;
			$scope.isAscending = checkIfAscending(newClassName);

			// reload data
			$scope.listUsers();
		};

		// List all users
		$scope.listUsers = function() {
			var params = {};
			params.page = $scope.currentPage;
			params.numPerPage = $scope.numEntryPerPage;
			params.sort = $scope.sortedSchema;
			params.isAscending = $scope.isAscending;

			if (typeof $scope.inputSearchText !== 'undefined' && $scope.inputSearchText !== '') {
				params.searchText = $scope.inputSearchText;
			}

			Users.get(params, function(response) {
				$scope.isLoading = false;
				$scope.users = response.data;
				$scope.setTotalPages(response.total);
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				var user = new Users($scope.user);

				user.$update(function(response) {
					SalesCache.removeCache();
					TrackShareService.removeCache();
					Notifier.notify('You have successfully updated your profile', 'Profile Updated!');
					Authentication.user = response;
				}, function(response) {
					Notifier.error(response.data.message, 'Profile Update Failed!');
				});
			}
		};

		$scope.addUserProfile = function(isValid) {
			if (isValid) {
				var user = new Users($scope.member);
				user.$save(function() {
					SalesCache.removeCache();
					TrackShareService.removeCache();
					Notifier.notify('New user added', 'Success');
				}, function(response) {
					Notifier.error(response.data.message, 'Failed!');
				});
			}
		};

		$scope.editUserProfile = function(isValid) {
			if (isValid) {
				if($scope.member.cPassword === $scope.member.password) {
					delete $scope.member.cPassword;
					$scope.member.$update(function (response) {
						SalesCache.removeCache();
						TrackShareService.removeCache();
						$scope.member = response;
						if(Authentication.user._id === $scope.member._id) {
							Authentication.user = $scope.member;
						}
						Notifier.notify('User info Updated', 'Success');
					});
				} else {
					Notifier.error('Confirmed Password Mismatched', 'Invalid!');
				}
			}
		};

		$scope.getEditUser = function() {
			$scope.member = Users.get({userId: $stateParams._id});
		};

		$scope.editUserInfo = function(userId) {
			$state.go('edit-user', {'_id': userId});
		};

		// Change user password
		$scope.changeUserPassword = function(isValid) {
			if (isValid) {
				$http.post(Utility.apiUrl + '/users/password', $scope.passwordDetails).success(function(response) {
					// If successful show success message and clear form
					Notifier.notify('You have successfully changed your password', 'Password Changed!');
					$scope.passwordDetails = null;
				}).error(function(response) {
					Notifier.error(response.message, 'Change Password Failed!');
				});
			}
		};
		
		// Set css for role tag
		$scope.setRoleCss = function(role) {
			switch(role) {
				case 'user':
					return 'label label-success';
				case 'admin':
					return 'label label-danger';
				case 'label':
					return 'label label-primary';
				case 'sound engineer':
					return 'label label-warning';
			}
		};
		
		// Open Simple Modal
		$scope.openModal = function(user)
		{	if (user._id !== $scope.user._id) {
				$scope.removeUser = user;
			} else {
				Notifier.error('This is your own account', 'Failed!');
			}
		};

		$scope.confirmRemove = function() {
			if($scope.removeUser) {
				for (var i in $scope.users) {
					if ($scope.users[i] === $scope.removeUser) {
						$scope.users.splice(i, 1);
					}
				}

				$scope.removeUser = new Users($scope.removeUser);
				$scope.removeUser.$remove(function() {
					SalesCache.removeCache();
					TrackShareService.removeCache();
				});
			}
			$scope.removeUser = undefined;
		};

		$scope.getUserPayment = function() {
			$scope.payment = $scope.user.payment;
		};

		$scope.changePayment = function(isValid) {
			if (isValid) {
				$scope.payment.channel = 'paypal';
				$scope.user.payment = $scope.payment;
				var user = new Users($scope.user);

				user.$update(function(response) {
					Notifier.notify('Your payment has been updated', 'Success!');
					Authentication.user = response;
				}, function(response) {
					Notifier.error(response.data.message, 'Payment Update Failed!');
				});
			}
		};

		$scope.inviteUser = function(isValid) {
			if(isValid) {
				$http.post(Utility.apiUrl + '/users/invite-email', {email: $scope.inviteEmail}).success(function(response) {
					// If successful show success message and clear form
					$('#load').css('display', 'none');
					Notifier.notify(response.message, 'Success!');
					$scope.inviteEmail = null;
				}).error(function(response) {
					$('#load').css('display', 'none');
					Notifier.error(response.message, 'Error!');
				});
				$('#load').css('display', 'block');
			}
		};
	}
);
