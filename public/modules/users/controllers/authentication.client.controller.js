'use strict';

angular.module('users').controller('AuthenticationController',
	function($scope, $rootScope, $http, $window, $state, $location, Authentication, Notifier, Utility) {
		$rootScope.isLoginPage        = true;
		$rootScope.isMainPage         = false;
		$scope.authentication = Authentication;
		$scope.credentials = {};

		// If user is signed in then redirect back home
		if ($scope.authentication.user) {
			$window.location.href = '/acc';
			return;
		}

		if($state.current.name === 'signup') {
			$http.get(Utility.apiUrl + '/user/invite-user/' + $location.search().token, {params:{
				email: $location.search().email
			}}).success(function(response) {
				$scope.credentials.token = $location.search().token;
				$scope.credentials.email = $location.search().email;
			}).error(function(response) {
				Notifier.error('Unauthorized Request', 'Failed!');
				$state.go('signin');
				return;
			});
		}

		$scope.signup = function() {
			$http.post(Utility.apiUrl + '/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$window.location.href = '/acc';
			}).error(function(response) {
				Notifier.error(response.message, 'Registration Failed!');
			});
		};

		$scope.signin = function() {
			$http.post(Utility.apiUrl + '/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$window.location.href = '/acc';
			}).error(function(response) {
				Notifier.error(response.message, 'Invalid Login!');
			});
		};
	}
);
