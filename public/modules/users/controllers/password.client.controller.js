'use strict';

angular.module('users').controller('PasswordController',
	function($scope, $rootScope, $stateParams, $http, $location, $window, Authentication, Notifier, Utility) {
		$rootScope.isLoginPage        = true;
		$rootScope.isMainPage         = false;
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/acc');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

			$http.post(Utility.apiUrl + '/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				Notifier.notify(response.message, 'Success!');

			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				Notifier.error(response.message, 'Error!');
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post(Utility.apiUrl + '/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;

				// And redirect to the index page
				$location.path('/password/reset/success');
				Notifier.notify('Password has been sucessfully reset', 'Success!');
			}).error(function(response) {
				Notifier.error(response.message, 'Error!');
			});
		};
	}
);

angular.module('users').controller('ResetController', function($scope, $window) {
  $scope.toHomePage = function() {
    $window.location.href = '/acc';
  };
});
