'use strict';

// Setting up route
angular.module('users').config(
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('add-user', {
			url: '/settings/add-user',
			templateUrl: 'modules/users/views/settings/add-user.client.view.html'
		}).
		state('edit-user', {
			url: '/settings/edit-user?_id=:',
			templateUrl: 'modules/users/views/settings/edit-user.client.view.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('payment', {
			url: '/settings/payment',
			templateUrl: 'modules/users/views/settings/payment.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		}).
		state('list-user', {
			url: '/user/list-users',
			templateUrl: 'modules/users/views/view-user.client.view.html'
		}).
		state('invite-user', {
			url: '/user/invite-user',
			templateUrl: 'modules/users/views/settings/invite-user.client.view.html'
		});

	}
);
