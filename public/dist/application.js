'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'mean';
	var applicationModuleVendorDependencies = [
	    'ngResource',
	    'ngAnimate',
	    'angularFileUpload',
	    'ui.router',
		'cube.directives',
		'ui.bootstrap',
		'ui.select'
	];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

angular.module('cube.directives', []).

	// Layout Related Directives
	directive('headerMenu', function(){
		return {
			restrict: 'E',
			templateUrl: 'modules/core/views/layout/header.client.view.html'
		};
	}).
	directive('pageTitle', function(){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: 'modules/core/views/layout/title.client.view.html',
			link: function(scope, el, attr){
				scope.title = attr.title;
			}
		};
	});

$(function($) {
	setTimeout(function() {
		$('#content-wrapper > .row').css({
			opacity: 1
		});
	}, 200);
	
	$('#sidebar-nav,#nav-col-submenu').on('click', '.dropdown-toggle', function (e) {
		e.preventDefault();
		
		var $item = $(this).parent();

		if (!$item.hasClass('open')) {
			$item.parent().find('.open .submenu').slideUp('fast');
			$item.parent().find('.open').toggleClass('open');
		}
		
		$item.toggleClass('open');
		
		if ($item.hasClass('open')) {
			$item.children('.submenu').slideDown('fast');
		} 
		else {
			$item.children('.submenu').slideUp('fast');
		}
	});
	
	$('body').on('mouseenter', '#page-wrapper.nav-small #sidebar-nav .dropdown-toggle', function (e) {
		if ($( document ).width() >= 992) {
			var $item = $(this).parent();

			if ($('body').hasClass('fixed-leftmenu')) {
				var topPosition = $item.position().top;

				if ((topPosition + 4*$(this).outerHeight()) >= $(window).height()) {
					topPosition -= 6*$(this).outerHeight();
				}

				$('#nav-col-submenu').html($item.children('.submenu').clone());
				$('#nav-col-submenu > .submenu').css({'top' : topPosition});
			}

			$item.addClass('open');
			$item.children('.submenu').slideDown('fast');
		}
	});
	
	$('body').on('mouseleave', '#page-wrapper.nav-small #sidebar-nav > .nav-pills > li', function (e) {
		if ($( document ).width() >= 992) {
			var $item = $(this);
	
			if ($item.hasClass('open')) {
				$item.find('.open .submenu').slideUp('fast');
				$item.find('.open').removeClass('open');
				$item.children('.submenu').slideUp('fast');
			}
			
			$item.removeClass('open');
		}
	});
	$('body').on('mouseenter', '#page-wrapper.nav-small #sidebar-nav a:not(.dropdown-toggle)', function (e) {
		if ($('body').hasClass('fixed-leftmenu')) {
			$('#nav-col-submenu').html('');
		}
	});
	$('body').on('mouseleave', '#page-wrapper.nav-small #nav-col', function (e) {
		if ($('body').hasClass('fixed-leftmenu')) {
			$('#nav-col-submenu').html('');
		}
	});
	
	$('#make-small-nav').click(function (e) {
		$('#page-wrapper').toggleClass('nav-small');
	});
	
	$(window).smartresize(function(){
		if ($( document ).width() <= 991) {
			$('#page-wrapper').removeClass('nav-small');
		}
	});
	
	$('.mobile-search').click(function(e) {
		e.preventDefault();
		
		$('.mobile-search').addClass('active');
		$('.mobile-search form input.form-control').focus();
	});
	$(document).mouseup(function (e) {
		var container = $('.mobile-search');

		if (!container.is(e.target) // if the target of the click isn't the container...
			&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			container.removeClass('active');
		}
	});
	
	$('.fixed-leftmenu #col-left').nanoScroller({
    	alwaysVisible: false,
    	iOSNativeScrolling: false,
    	preventPageScrolling: true,
    	contentClass: 'col-left-nano-content'
    });
	
	// build all tooltips from data-attributes
	$("[data-toggle='tooltip']").each(function (index, el) {
		$(el).tooltip({
			placement: $(this).data("placement") || 'top'
		});
	});
});

$.fn.removeClassPrefix = function(prefix) {
    this.each(function(i, el) {
        var classes = el.className.split(" ").filter(function(c) {
            return c.lastIndexOf(prefix, 0) !== 0;
        });
        el.className = classes.join(" ");
    });
    return this;
};

(function($,sr){
	// debouncing function from John Hann
	// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
	var debounce = function (func, threshold, execAsap) {
		var timeout;

		return function debounced () {
			var obj = this, args = arguments;
			function delayed () {
				if (!execAsap)
					func.apply(obj, args);
				timeout = null;
			};

			if (timeout)
				clearTimeout(timeout);
			else if (execAsap)
				func.apply(obj, args);

			timeout = setTimeout(delayed, threshold || 100);
		};
	}
	// smartresize 
	jQuery.fn[sr] = function(fn){	return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
/*
Copyright 2012 Igor Vaynberg

Version: 3.4.3 Timestamp: Tue Sep 17 06:47:14 PDT 2013

This software is licensed under the Apache License, Version 2.0 (the "Apache License") or the GNU
General Public License version 2 (the "GPL License"). You may choose either license to govern your
use of this software only upon the condition that you accept all of the terms of either the Apache
License or the GPL License.

You may obtain a copy of the Apache License and the GPL License at:

http://www.apache.org/licenses/LICENSE-2.0
http://www.gnu.org/licenses/gpl-2.0.html

Unless required by applicable law or agreed to in writing, software distributed under the Apache License
or the GPL Licesnse is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
either express or implied. See the Apache License and the GPL License for the specific language governing
permissions and limitations under the Apache License and the GPL License.
*/
!function(a){"undefined"==typeof a.fn.each2&&a.extend(a.fn,{each2:function(b){for(var c=a([0]),d=-1,e=this.length;++d<e&&(c.context=c[0]=this[d])&&b.call(c[0],d,c)!==!1;);return this}})}(jQuery),function(a,b){"use strict";function n(a){var b,c,d,e;if(!a||a.length<1)return a;for(b="",c=0,d=a.length;d>c;c++)e=a.charAt(c),b+=m[e]||e;return b}function o(a,b){for(var c=0,d=b.length;d>c;c+=1)if(q(a,b[c]))return c;return-1}function p(){var b=a(l);b.appendTo("body");var c={width:b.width()-b[0].clientWidth,height:b.height()-b[0].clientHeight};return b.remove(),c}function q(a,c){return a===c?!0:a===b||c===b?!1:null===a||null===c?!1:a.constructor===String?a+""==c+"":c.constructor===String?c+""==a+"":!1}function r(b,c){var d,e,f;if(null===b||b.length<1)return[];for(d=b.split(c),e=0,f=d.length;f>e;e+=1)d[e]=a.trim(d[e]);return d}function s(a){return a.outerWidth(!1)-a.width()}function t(c){var d="keyup-change-value";c.on("keydown",function(){a.data(c,d)===b&&a.data(c,d,c.val())}),c.on("keyup",function(){var e=a.data(c,d);e!==b&&c.val()!==e&&(a.removeData(c,d),c.trigger("keyup-change"))})}function u(c){c.on("mousemove",function(c){var d=i;(d===b||d.x!==c.pageX||d.y!==c.pageY)&&a(c.target).trigger("mousemove-filtered",c)})}function v(a,c,d){d=d||b;var e;return function(){var b=arguments;window.clearTimeout(e),e=window.setTimeout(function(){c.apply(d,b)},a)}}function w(a){var c,b=!1;return function(){return b===!1&&(c=a(),b=!0),c}}function x(a,b){var c=v(a,function(a){b.trigger("scroll-debounced",a)});b.on("scroll",function(a){o(a.target,b.get())>=0&&c(a)})}function y(a){a[0]!==document.activeElement&&window.setTimeout(function(){var d,b=a[0],c=a.val().length;a.focus(),a.is(":visible")&&b===document.activeElement&&(b.setSelectionRange?b.setSelectionRange(c,c):b.createTextRange&&(d=b.createTextRange(),d.collapse(!1),d.select()))},0)}function z(b){b=a(b)[0];var c=0,d=0;if("selectionStart"in b)c=b.selectionStart,d=b.selectionEnd-c;else if("selection"in document){b.focus();var e=document.selection.createRange();d=document.selection.createRange().text.length,e.moveStart("character",-b.value.length),c=e.text.length-d}return{offset:c,length:d}}function A(a){a.preventDefault(),a.stopPropagation()}function B(a){a.preventDefault(),a.stopImmediatePropagation()}function C(b){if(!h){var c=b[0].currentStyle||window.getComputedStyle(b[0],null);h=a(document.createElement("div")).css({position:"absolute",left:"-10000px",top:"-10000px",display:"none",fontSize:c.fontSize,fontFamily:c.fontFamily,fontStyle:c.fontStyle,fontWeight:c.fontWeight,letterSpacing:c.letterSpacing,textTransform:c.textTransform,whiteSpace:"nowrap"}),h.attr("class","select2-sizer"),a("body").append(h)}return h.text(b.val()),h.width()}function D(b,c,d){var e,g,f=[];e=b.attr("class"),e&&(e=""+e,a(e.split(" ")).each2(function(){0===this.indexOf("select2-")&&f.push(this)})),e=c.attr("class"),e&&(e=""+e,a(e.split(" ")).each2(function(){0!==this.indexOf("select2-")&&(g=d(this),g&&f.push(this))})),b.attr("class",f.join(" "))}function E(a,b,c,d){var e=n(a.toUpperCase()).indexOf(n(b.toUpperCase())),f=b.length;return 0>e?(c.push(d(a)),void 0):(c.push(d(a.substring(0,e))),c.push("<span class='select2-match'>"),c.push(d(a.substring(e,e+f))),c.push("</span>"),c.push(d(a.substring(e+f,a.length))),void 0)}function F(a){var b={"\\":"&#92;","&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#39;","/":"&#47;"};return String(a).replace(/[&<>"'\/\\]/g,function(a){return b[a]})}function G(c){var d,e=null,f=c.quietMillis||100,g=c.url,h=this;return function(i){window.clearTimeout(d),d=window.setTimeout(function(){var d=c.data,f=g,j=c.transport||a.fn.select2.ajaxDefaults.transport,k={type:c.type||"GET",cache:c.cache||!1,jsonpCallback:c.jsonpCallback||b,dataType:c.dataType||"json"},l=a.extend({},a.fn.select2.ajaxDefaults.params,k);d=d?d.call(h,i.term,i.page,i.context):null,f="function"==typeof f?f.call(h,i.term,i.page,i.context):f,e&&e.abort(),c.params&&(a.isFunction(c.params)?a.extend(l,c.params.call(h)):a.extend(l,c.params)),a.extend(l,{url:f,dataType:c.dataType,data:d,success:function(a){var b=c.results(a,i.page);i.callback(b)}}),e=j.call(h,l)},f)}}function H(b){var d,e,c=b,f=function(a){return""+a.text};a.isArray(c)&&(e=c,c={results:e}),a.isFunction(c)===!1&&(e=c,c=function(){return e});var g=c();return g.text&&(f=g.text,a.isFunction(f)||(d=g.text,f=function(a){return a[d]})),function(b){var g,d=b.term,e={results:[]};return""===d?(b.callback(c()),void 0):(g=function(c,e){var h,i;if(c=c[0],c.children){h={};for(i in c)c.hasOwnProperty(i)&&(h[i]=c[i]);h.children=[],a(c.children).each2(function(a,b){g(b,h.children)}),(h.children.length||b.matcher(d,f(h),c))&&e.push(h)}else b.matcher(d,f(c),c)&&e.push(c)},a(c().results).each2(function(a,b){g(b,e.results)}),b.callback(e),void 0)}}function I(c){var d=a.isFunction(c);return function(e){var f=e.term,g={results:[]};a(d?c():c).each(function(){var a=this.text!==b,c=a?this.text:this;(""===f||e.matcher(f,c))&&g.results.push(a?this:{id:this,text:this})}),e.callback(g)}}function J(b,c){if(a.isFunction(b))return!0;if(!b)return!1;throw new Error(c+" must be a function or a falsy value")}function K(b){return a.isFunction(b)?b():b}function L(b){var c=0;return a.each(b,function(a,b){b.children?c+=L(b.children):c++}),c}function M(a,c,d,e){var h,i,j,k,l,f=a,g=!1;if(!e.createSearchChoice||!e.tokenSeparators||e.tokenSeparators.length<1)return b;for(;;){for(i=-1,j=0,k=e.tokenSeparators.length;k>j&&(l=e.tokenSeparators[j],i=a.indexOf(l),!(i>=0));j++);if(0>i)break;if(h=a.substring(0,i),a=a.substring(i+l.length),h.length>0&&(h=e.createSearchChoice.call(this,h,c),h!==b&&null!==h&&e.id(h)!==b&&null!==e.id(h))){for(g=!1,j=0,k=c.length;k>j;j++)if(q(e.id(h),e.id(c[j]))){g=!0;break}g||d(h)}}return f!==a?a:void 0}function N(b,c){var d=function(){};return d.prototype=new b,d.prototype.constructor=d,d.prototype.parent=b.prototype,d.prototype=a.extend(d.prototype,c),d}if(window.Select2===b){var c,d,e,f,g,h,j,k,i={x:0,y:0},c={TAB:9,ENTER:13,ESC:27,SPACE:32,LEFT:37,UP:38,RIGHT:39,DOWN:40,SHIFT:16,CTRL:17,ALT:18,PAGE_UP:33,PAGE_DOWN:34,HOME:36,END:35,BACKSPACE:8,DELETE:46,isArrow:function(a){switch(a=a.which?a.which:a){case c.LEFT:case c.RIGHT:case c.UP:case c.DOWN:return!0}return!1},isControl:function(a){var b=a.which;switch(b){case c.SHIFT:case c.CTRL:case c.ALT:return!0}return a.metaKey?!0:!1},isFunctionKey:function(a){return a=a.which?a.which:a,a>=112&&123>=a}},l="<div class='select2-measure-scrollbar'></div>",m={"\u24b6":"A","\uff21":"A","\xc0":"A","\xc1":"A","\xc2":"A","\u1ea6":"A","\u1ea4":"A","\u1eaa":"A","\u1ea8":"A","\xc3":"A","\u0100":"A","\u0102":"A","\u1eb0":"A","\u1eae":"A","\u1eb4":"A","\u1eb2":"A","\u0226":"A","\u01e0":"A","\xc4":"A","\u01de":"A","\u1ea2":"A","\xc5":"A","\u01fa":"A","\u01cd":"A","\u0200":"A","\u0202":"A","\u1ea0":"A","\u1eac":"A","\u1eb6":"A","\u1e00":"A","\u0104":"A","\u023a":"A","\u2c6f":"A","\ua732":"AA","\xc6":"AE","\u01fc":"AE","\u01e2":"AE","\ua734":"AO","\ua736":"AU","\ua738":"AV","\ua73a":"AV","\ua73c":"AY","\u24b7":"B","\uff22":"B","\u1e02":"B","\u1e04":"B","\u1e06":"B","\u0243":"B","\u0182":"B","\u0181":"B","\u24b8":"C","\uff23":"C","\u0106":"C","\u0108":"C","\u010a":"C","\u010c":"C","\xc7":"C","\u1e08":"C","\u0187":"C","\u023b":"C","\ua73e":"C","\u24b9":"D","\uff24":"D","\u1e0a":"D","\u010e":"D","\u1e0c":"D","\u1e10":"D","\u1e12":"D","\u1e0e":"D","\u0110":"D","\u018b":"D","\u018a":"D","\u0189":"D","\ua779":"D","\u01f1":"DZ","\u01c4":"DZ","\u01f2":"Dz","\u01c5":"Dz","\u24ba":"E","\uff25":"E","\xc8":"E","\xc9":"E","\xca":"E","\u1ec0":"E","\u1ebe":"E","\u1ec4":"E","\u1ec2":"E","\u1ebc":"E","\u0112":"E","\u1e14":"E","\u1e16":"E","\u0114":"E","\u0116":"E","\xcb":"E","\u1eba":"E","\u011a":"E","\u0204":"E","\u0206":"E","\u1eb8":"E","\u1ec6":"E","\u0228":"E","\u1e1c":"E","\u0118":"E","\u1e18":"E","\u1e1a":"E","\u0190":"E","\u018e":"E","\u24bb":"F","\uff26":"F","\u1e1e":"F","\u0191":"F","\ua77b":"F","\u24bc":"G","\uff27":"G","\u01f4":"G","\u011c":"G","\u1e20":"G","\u011e":"G","\u0120":"G","\u01e6":"G","\u0122":"G","\u01e4":"G","\u0193":"G","\ua7a0":"G","\ua77d":"G","\ua77e":"G","\u24bd":"H","\uff28":"H","\u0124":"H","\u1e22":"H","\u1e26":"H","\u021e":"H","\u1e24":"H","\u1e28":"H","\u1e2a":"H","\u0126":"H","\u2c67":"H","\u2c75":"H","\ua78d":"H","\u24be":"I","\uff29":"I","\xcc":"I","\xcd":"I","\xce":"I","\u0128":"I","\u012a":"I","\u012c":"I","\u0130":"I","\xcf":"I","\u1e2e":"I","\u1ec8":"I","\u01cf":"I","\u0208":"I","\u020a":"I","\u1eca":"I","\u012e":"I","\u1e2c":"I","\u0197":"I","\u24bf":"J","\uff2a":"J","\u0134":"J","\u0248":"J","\u24c0":"K","\uff2b":"K","\u1e30":"K","\u01e8":"K","\u1e32":"K","\u0136":"K","\u1e34":"K","\u0198":"K","\u2c69":"K","\ua740":"K","\ua742":"K","\ua744":"K","\ua7a2":"K","\u24c1":"L","\uff2c":"L","\u013f":"L","\u0139":"L","\u013d":"L","\u1e36":"L","\u1e38":"L","\u013b":"L","\u1e3c":"L","\u1e3a":"L","\u0141":"L","\u023d":"L","\u2c62":"L","\u2c60":"L","\ua748":"L","\ua746":"L","\ua780":"L","\u01c7":"LJ","\u01c8":"Lj","\u24c2":"M","\uff2d":"M","\u1e3e":"M","\u1e40":"M","\u1e42":"M","\u2c6e":"M","\u019c":"M","\u24c3":"N","\uff2e":"N","\u01f8":"N","\u0143":"N","\xd1":"N","\u1e44":"N","\u0147":"N","\u1e46":"N","\u0145":"N","\u1e4a":"N","\u1e48":"N","\u0220":"N","\u019d":"N","\ua790":"N","\ua7a4":"N","\u01ca":"NJ","\u01cb":"Nj","\u24c4":"O","\uff2f":"O","\xd2":"O","\xd3":"O","\xd4":"O","\u1ed2":"O","\u1ed0":"O","\u1ed6":"O","\u1ed4":"O","\xd5":"O","\u1e4c":"O","\u022c":"O","\u1e4e":"O","\u014c":"O","\u1e50":"O","\u1e52":"O","\u014e":"O","\u022e":"O","\u0230":"O","\xd6":"O","\u022a":"O","\u1ece":"O","\u0150":"O","\u01d1":"O","\u020c":"O","\u020e":"O","\u01a0":"O","\u1edc":"O","\u1eda":"O","\u1ee0":"O","\u1ede":"O","\u1ee2":"O","\u1ecc":"O","\u1ed8":"O","\u01ea":"O","\u01ec":"O","\xd8":"O","\u01fe":"O","\u0186":"O","\u019f":"O","\ua74a":"O","\ua74c":"O","\u01a2":"OI","\ua74e":"OO","\u0222":"OU","\u24c5":"P","\uff30":"P","\u1e54":"P","\u1e56":"P","\u01a4":"P","\u2c63":"P","\ua750":"P","\ua752":"P","\ua754":"P","\u24c6":"Q","\uff31":"Q","\ua756":"Q","\ua758":"Q","\u024a":"Q","\u24c7":"R","\uff32":"R","\u0154":"R","\u1e58":"R","\u0158":"R","\u0210":"R","\u0212":"R","\u1e5a":"R","\u1e5c":"R","\u0156":"R","\u1e5e":"R","\u024c":"R","\u2c64":"R","\ua75a":"R","\ua7a6":"R","\ua782":"R","\u24c8":"S","\uff33":"S","\u1e9e":"S","\u015a":"S","\u1e64":"S","\u015c":"S","\u1e60":"S","\u0160":"S","\u1e66":"S","\u1e62":"S","\u1e68":"S","\u0218":"S","\u015e":"S","\u2c7e":"S","\ua7a8":"S","\ua784":"S","\u24c9":"T","\uff34":"T","\u1e6a":"T","\u0164":"T","\u1e6c":"T","\u021a":"T","\u0162":"T","\u1e70":"T","\u1e6e":"T","\u0166":"T","\u01ac":"T","\u01ae":"T","\u023e":"T","\ua786":"T","\ua728":"TZ","\u24ca":"U","\uff35":"U","\xd9":"U","\xda":"U","\xdb":"U","\u0168":"U","\u1e78":"U","\u016a":"U","\u1e7a":"U","\u016c":"U","\xdc":"U","\u01db":"U","\u01d7":"U","\u01d5":"U","\u01d9":"U","\u1ee6":"U","\u016e":"U","\u0170":"U","\u01d3":"U","\u0214":"U","\u0216":"U","\u01af":"U","\u1eea":"U","\u1ee8":"U","\u1eee":"U","\u1eec":"U","\u1ef0":"U","\u1ee4":"U","\u1e72":"U","\u0172":"U","\u1e76":"U","\u1e74":"U","\u0244":"U","\u24cb":"V","\uff36":"V","\u1e7c":"V","\u1e7e":"V","\u01b2":"V","\ua75e":"V","\u0245":"V","\ua760":"VY","\u24cc":"W","\uff37":"W","\u1e80":"W","\u1e82":"W","\u0174":"W","\u1e86":"W","\u1e84":"W","\u1e88":"W","\u2c72":"W","\u24cd":"X","\uff38":"X","\u1e8a":"X","\u1e8c":"X","\u24ce":"Y","\uff39":"Y","\u1ef2":"Y","\xdd":"Y","\u0176":"Y","\u1ef8":"Y","\u0232":"Y","\u1e8e":"Y","\u0178":"Y","\u1ef6":"Y","\u1ef4":"Y","\u01b3":"Y","\u024e":"Y","\u1efe":"Y","\u24cf":"Z","\uff3a":"Z","\u0179":"Z","\u1e90":"Z","\u017b":"Z","\u017d":"Z","\u1e92":"Z","\u1e94":"Z","\u01b5":"Z","\u0224":"Z","\u2c7f":"Z","\u2c6b":"Z","\ua762":"Z","\u24d0":"a","\uff41":"a","\u1e9a":"a","\xe0":"a","\xe1":"a","\xe2":"a","\u1ea7":"a","\u1ea5":"a","\u1eab":"a","\u1ea9":"a","\xe3":"a","\u0101":"a","\u0103":"a","\u1eb1":"a","\u1eaf":"a","\u1eb5":"a","\u1eb3":"a","\u0227":"a","\u01e1":"a","\xe4":"a","\u01df":"a","\u1ea3":"a","\xe5":"a","\u01fb":"a","\u01ce":"a","\u0201":"a","\u0203":"a","\u1ea1":"a","\u1ead":"a","\u1eb7":"a","\u1e01":"a","\u0105":"a","\u2c65":"a","\u0250":"a","\ua733":"aa","\xe6":"ae","\u01fd":"ae","\u01e3":"ae","\ua735":"ao","\ua737":"au","\ua739":"av","\ua73b":"av","\ua73d":"ay","\u24d1":"b","\uff42":"b","\u1e03":"b","\u1e05":"b","\u1e07":"b","\u0180":"b","\u0183":"b","\u0253":"b","\u24d2":"c","\uff43":"c","\u0107":"c","\u0109":"c","\u010b":"c","\u010d":"c","\xe7":"c","\u1e09":"c","\u0188":"c","\u023c":"c","\ua73f":"c","\u2184":"c","\u24d3":"d","\uff44":"d","\u1e0b":"d","\u010f":"d","\u1e0d":"d","\u1e11":"d","\u1e13":"d","\u1e0f":"d","\u0111":"d","\u018c":"d","\u0256":"d","\u0257":"d","\ua77a":"d","\u01f3":"dz","\u01c6":"dz","\u24d4":"e","\uff45":"e","\xe8":"e","\xe9":"e","\xea":"e","\u1ec1":"e","\u1ebf":"e","\u1ec5":"e","\u1ec3":"e","\u1ebd":"e","\u0113":"e","\u1e15":"e","\u1e17":"e","\u0115":"e","\u0117":"e","\xeb":"e","\u1ebb":"e","\u011b":"e","\u0205":"e","\u0207":"e","\u1eb9":"e","\u1ec7":"e","\u0229":"e","\u1e1d":"e","\u0119":"e","\u1e19":"e","\u1e1b":"e","\u0247":"e","\u025b":"e","\u01dd":"e","\u24d5":"f","\uff46":"f","\u1e1f":"f","\u0192":"f","\ua77c":"f","\u24d6":"g","\uff47":"g","\u01f5":"g","\u011d":"g","\u1e21":"g","\u011f":"g","\u0121":"g","\u01e7":"g","\u0123":"g","\u01e5":"g","\u0260":"g","\ua7a1":"g","\u1d79":"g","\ua77f":"g","\u24d7":"h","\uff48":"h","\u0125":"h","\u1e23":"h","\u1e27":"h","\u021f":"h","\u1e25":"h","\u1e29":"h","\u1e2b":"h","\u1e96":"h","\u0127":"h","\u2c68":"h","\u2c76":"h","\u0265":"h","\u0195":"hv","\u24d8":"i","\uff49":"i","\xec":"i","\xed":"i","\xee":"i","\u0129":"i","\u012b":"i","\u012d":"i","\xef":"i","\u1e2f":"i","\u1ec9":"i","\u01d0":"i","\u0209":"i","\u020b":"i","\u1ecb":"i","\u012f":"i","\u1e2d":"i","\u0268":"i","\u0131":"i","\u24d9":"j","\uff4a":"j","\u0135":"j","\u01f0":"j","\u0249":"j","\u24da":"k","\uff4b":"k","\u1e31":"k","\u01e9":"k","\u1e33":"k","\u0137":"k","\u1e35":"k","\u0199":"k","\u2c6a":"k","\ua741":"k","\ua743":"k","\ua745":"k","\ua7a3":"k","\u24db":"l","\uff4c":"l","\u0140":"l","\u013a":"l","\u013e":"l","\u1e37":"l","\u1e39":"l","\u013c":"l","\u1e3d":"l","\u1e3b":"l","\u017f":"l","\u0142":"l","\u019a":"l","\u026b":"l","\u2c61":"l","\ua749":"l","\ua781":"l","\ua747":"l","\u01c9":"lj","\u24dc":"m","\uff4d":"m","\u1e3f":"m","\u1e41":"m","\u1e43":"m","\u0271":"m","\u026f":"m","\u24dd":"n","\uff4e":"n","\u01f9":"n","\u0144":"n","\xf1":"n","\u1e45":"n","\u0148":"n","\u1e47":"n","\u0146":"n","\u1e4b":"n","\u1e49":"n","\u019e":"n","\u0272":"n","\u0149":"n","\ua791":"n","\ua7a5":"n","\u01cc":"nj","\u24de":"o","\uff4f":"o","\xf2":"o","\xf3":"o","\xf4":"o","\u1ed3":"o","\u1ed1":"o","\u1ed7":"o","\u1ed5":"o","\xf5":"o","\u1e4d":"o","\u022d":"o","\u1e4f":"o","\u014d":"o","\u1e51":"o","\u1e53":"o","\u014f":"o","\u022f":"o","\u0231":"o","\xf6":"o","\u022b":"o","\u1ecf":"o","\u0151":"o","\u01d2":"o","\u020d":"o","\u020f":"o","\u01a1":"o","\u1edd":"o","\u1edb":"o","\u1ee1":"o","\u1edf":"o","\u1ee3":"o","\u1ecd":"o","\u1ed9":"o","\u01eb":"o","\u01ed":"o","\xf8":"o","\u01ff":"o","\u0254":"o","\ua74b":"o","\ua74d":"o","\u0275":"o","\u01a3":"oi","\u0223":"ou","\ua74f":"oo","\u24df":"p","\uff50":"p","\u1e55":"p","\u1e57":"p","\u01a5":"p","\u1d7d":"p","\ua751":"p","\ua753":"p","\ua755":"p","\u24e0":"q","\uff51":"q","\u024b":"q","\ua757":"q","\ua759":"q","\u24e1":"r","\uff52":"r","\u0155":"r","\u1e59":"r","\u0159":"r","\u0211":"r","\u0213":"r","\u1e5b":"r","\u1e5d":"r","\u0157":"r","\u1e5f":"r","\u024d":"r","\u027d":"r","\ua75b":"r","\ua7a7":"r","\ua783":"r","\u24e2":"s","\uff53":"s","\xdf":"s","\u015b":"s","\u1e65":"s","\u015d":"s","\u1e61":"s","\u0161":"s","\u1e67":"s","\u1e63":"s","\u1e69":"s","\u0219":"s","\u015f":"s","\u023f":"s","\ua7a9":"s","\ua785":"s","\u1e9b":"s","\u24e3":"t","\uff54":"t","\u1e6b":"t","\u1e97":"t","\u0165":"t","\u1e6d":"t","\u021b":"t","\u0163":"t","\u1e71":"t","\u1e6f":"t","\u0167":"t","\u01ad":"t","\u0288":"t","\u2c66":"t","\ua787":"t","\ua729":"tz","\u24e4":"u","\uff55":"u","\xf9":"u","\xfa":"u","\xfb":"u","\u0169":"u","\u1e79":"u","\u016b":"u","\u1e7b":"u","\u016d":"u","\xfc":"u","\u01dc":"u","\u01d8":"u","\u01d6":"u","\u01da":"u","\u1ee7":"u","\u016f":"u","\u0171":"u","\u01d4":"u","\u0215":"u","\u0217":"u","\u01b0":"u","\u1eeb":"u","\u1ee9":"u","\u1eef":"u","\u1eed":"u","\u1ef1":"u","\u1ee5":"u","\u1e73":"u","\u0173":"u","\u1e77":"u","\u1e75":"u","\u0289":"u","\u24e5":"v","\uff56":"v","\u1e7d":"v","\u1e7f":"v","\u028b":"v","\ua75f":"v","\u028c":"v","\ua761":"vy","\u24e6":"w","\uff57":"w","\u1e81":"w","\u1e83":"w","\u0175":"w","\u1e87":"w","\u1e85":"w","\u1e98":"w","\u1e89":"w","\u2c73":"w","\u24e7":"x","\uff58":"x","\u1e8b":"x","\u1e8d":"x","\u24e8":"y","\uff59":"y","\u1ef3":"y","\xfd":"y","\u0177":"y","\u1ef9":"y","\u0233":"y","\u1e8f":"y","\xff":"y","\u1ef7":"y","\u1e99":"y","\u1ef5":"y","\u01b4":"y","\u024f":"y","\u1eff":"y","\u24e9":"z","\uff5a":"z","\u017a":"z","\u1e91":"z","\u017c":"z","\u017e":"z","\u1e93":"z","\u1e95":"z","\u01b6":"z","\u0225":"z","\u0240":"z","\u2c6c":"z","\ua763":"z"};j=a(document),g=function(){var a=1;return function(){return a++}}(),j.on("mousemove",function(a){i.x=a.pageX,i.y=a.pageY}),d=N(Object,{bind:function(a){var b=this;return function(){a.apply(b,arguments)}},init:function(c){var d,e,h,i,f=".select2-results";this.opts=c=this.prepareOpts(c),this.id=c.id,c.element.data("select2")!==b&&null!==c.element.data("select2")&&c.element.data("select2").destroy(),this.container=this.createContainer(),this.containerId="s2id_"+(c.element.attr("id")||"autogen"+g()),this.containerSelector="#"+this.containerId.replace(/([;&,\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g,"\\$1"),this.container.attr("id",this.containerId),this.body=w(function(){return c.element.closest("body")}),D(this.container,this.opts.element,this.opts.adaptContainerCssClass),this.container.attr("style",c.element.attr("style")),this.container.css(K(c.containerCss)),this.container.addClass(K(c.containerCssClass)),this.elementTabIndex=this.opts.element.attr("tabindex"),this.opts.element.data("select2",this).attr("tabindex","-1").before(this.container).on("click.select2",A),this.container.data("select2",this),this.dropdown=this.container.find(".select2-drop"),D(this.dropdown,this.opts.element,this.opts.adaptDropdownCssClass),this.dropdown.addClass(K(c.dropdownCssClass)),this.dropdown.data("select2",this),this.dropdown.on("click",A),this.results=d=this.container.find(f),this.search=e=this.container.find("input.select2-input"),this.queryCount=0,this.resultsPage=0,this.context=null,this.initContainer(),this.container.on("click",A),u(this.results),this.dropdown.on("mousemove-filtered touchstart touchmove touchend",f,this.bind(this.highlightUnderEvent)),x(80,this.results),this.dropdown.on("scroll-debounced",f,this.bind(this.loadMoreIfNeeded)),a(this.container).on("change",".select2-input",function(a){a.stopPropagation()}),a(this.dropdown).on("change",".select2-input",function(a){a.stopPropagation()}),a.fn.mousewheel&&d.mousewheel(function(a,b,c,e){var f=d.scrollTop();e>0&&0>=f-e?(d.scrollTop(0),A(a)):0>e&&d.get(0).scrollHeight-d.scrollTop()+e<=d.height()&&(d.scrollTop(d.get(0).scrollHeight-d.height()),A(a))}),t(e),e.on("keyup-change input paste",this.bind(this.updateResults)),e.on("focus",function(){e.addClass("select2-focused")}),e.on("blur",function(){e.removeClass("select2-focused")}),this.dropdown.on("mouseup",f,this.bind(function(b){a(b.target).closest(".select2-result-selectable").length>0&&(this.highlightUnderEvent(b),this.selectHighlighted(b))})),this.dropdown.on("click mouseup mousedown",function(a){a.stopPropagation()}),a.isFunction(this.opts.initSelection)&&(this.initSelection(),this.monitorSource()),null!==c.maximumInputLength&&this.search.attr("maxlength",c.maximumInputLength);var h=c.element.prop("disabled");h===b&&(h=!1),this.enable(!h);var i=c.element.prop("readonly");i===b&&(i=!1),this.readonly(i),k=k||p(),this.autofocus=c.element.prop("autofocus"),c.element.prop("autofocus",!1),this.autofocus&&this.focus(),this.nextSearchTerm=b},destroy:function(){var a=this.opts.element,c=a.data("select2");this.close(),this.propertyObserver&&(delete this.propertyObserver,this.propertyObserver=null),c!==b&&(c.container.remove(),c.dropdown.remove(),a.removeClass("select2-offscreen").removeData("select2").off(".select2").prop("autofocus",this.autofocus||!1),this.elementTabIndex?a.attr({tabindex:this.elementTabIndex}):a.removeAttr("tabindex"),a.show())},optionToData:function(a){return a.is("option")?{id:a.prop("value"),text:a.text(),element:a.get(),css:a.attr("class"),disabled:a.prop("disabled"),locked:q(a.attr("locked"),"locked")||q(a.data("locked"),!0)}:a.is("optgroup")?{text:a.attr("label"),children:[],element:a.get(),css:a.attr("class")}:void 0},prepareOpts:function(c){var d,e,f,g,h=this;if(d=c.element,"select"===d.get(0).tagName.toLowerCase()&&(this.select=e=c.element),e&&a.each(["id","multiple","ajax","query","createSearchChoice","initSelection","data","tags"],function(){if(this in c)throw new Error("Option '"+this+"' is not allowed for Select2 when attached to a <select> element.")}),c=a.extend({},{populateResults:function(d,e,f){var g,l=this.opts.id;g=function(d,e,i){var j,k,m,n,o,p,q,r,s,t;for(d=c.sortResults(d,e,f),j=0,k=d.length;k>j;j+=1)m=d[j],o=m.disabled===!0,n=!o&&l(m)!==b,p=m.children&&m.children.length>0,q=a("<li></li>"),q.addClass("select2-results-dept-"+i),q.addClass("select2-result"),q.addClass(n?"select2-result-selectable":"select2-result-unselectable"),o&&q.addClass("select2-disabled"),p&&q.addClass("select2-result-with-children"),q.addClass(h.opts.formatResultCssClass(m)),r=a(document.createElement("div")),r.addClass("select2-result-label"),t=c.formatResult(m,r,f,h.opts.escapeMarkup),t!==b&&r.html(t),q.append(r),p&&(s=a("<ul></ul>"),s.addClass("select2-result-sub"),g(m.children,s,i+1),q.append(s)),q.data("select2-data",m),e.append(q)},g(e,d,0)}},a.fn.select2.defaults,c),"function"!=typeof c.id&&(f=c.id,c.id=function(a){return a[f]}),a.isArray(c.element.data("select2Tags"))){if("tags"in c)throw"tags specified as both an attribute 'data-select2-tags' and in options of Select2 "+c.element.attr("id");c.tags=c.element.data("select2Tags")}if(e?(c.query=this.bind(function(a){var f,g,i,c={results:[],more:!1},e=a.term;i=function(b,c){var d;b.is("option")?a.matcher(e,b.text(),b)&&c.push(h.optionToData(b)):b.is("optgroup")&&(d=h.optionToData(b),b.children().each2(function(a,b){i(b,d.children)}),d.children.length>0&&c.push(d))},f=d.children(),this.getPlaceholder()!==b&&f.length>0&&(g=this.getPlaceholderOption(),g&&(f=f.not(g))),f.each2(function(a,b){i(b,c.results)}),a.callback(c)}),c.id=function(a){return a.id},c.formatResultCssClass=function(a){return a.css}):"query"in c||("ajax"in c?(g=c.element.data("ajax-url"),g&&g.length>0&&(c.ajax.url=g),c.query=G.call(c.element,c.ajax)):"data"in c?c.query=H(c.data):"tags"in c&&(c.query=I(c.tags),c.createSearchChoice===b&&(c.createSearchChoice=function(b){return{id:a.trim(b),text:a.trim(b)}}),c.initSelection===b&&(c.initSelection=function(b,d){var e=[];a(r(b.val(),c.separator)).each(function(){var b={id:this,text:this},d=c.tags;a.isFunction(d)&&(d=d()),a(d).each(function(){return q(this.id,b.id)?(b=this,!1):void 0}),e.push(b)}),d(e)}))),"function"!=typeof c.query)throw"query function not defined for Select2 "+c.element.attr("id");return c},monitorSource:function(){var c,a=this.opts.element;a.on("change.select2",this.bind(function(){this.opts.element.data("select2-change-triggered")!==!0&&this.initSelection()})),c=this.bind(function(){var d,f=a.prop("disabled");f===b&&(f=!1),this.enable(!f);var d=a.prop("readonly");d===b&&(d=!1),this.readonly(d),D(this.container,this.opts.element,this.opts.adaptContainerCssClass),this.container.addClass(K(this.opts.containerCssClass)),D(this.dropdown,this.opts.element,this.opts.adaptDropdownCssClass),this.dropdown.addClass(K(this.opts.dropdownCssClass))}),a.on("propertychange.select2 DOMAttrModified.select2",c),this.mutationCallback===b&&(this.mutationCallback=function(a){a.forEach(c)}),"undefined"!=typeof WebKitMutationObserver&&(this.propertyObserver&&(delete this.propertyObserver,this.propertyObserver=null),this.propertyObserver=new WebKitMutationObserver(this.mutationCallback),this.propertyObserver.observe(a.get(0),{attributes:!0,subtree:!1}))},triggerSelect:function(b){var c=a.Event("select2-selecting",{val:this.id(b),object:b});return this.opts.element.trigger(c),!c.isDefaultPrevented()},triggerChange:function(b){b=b||{},b=a.extend({},b,{type:"change",val:this.val()}),this.opts.element.data("select2-change-triggered",!0),this.opts.element.trigger(b),this.opts.element.data("select2-change-triggered",!1),this.opts.element.click(),this.opts.blurOnChange&&this.opts.element.blur()},isInterfaceEnabled:function(){return this.enabledInterface===!0},enableInterface:function(){var a=this._enabled&&!this._readonly,b=!a;return a===this.enabledInterface?!1:(this.container.toggleClass("select2-container-disabled",b),this.close(),this.enabledInterface=a,!0)},enable:function(a){a===b&&(a=!0),this._enabled!==a&&(this._enabled=a,this.opts.element.prop("disabled",!a),this.enableInterface())},disable:function(){this.enable(!1)},readonly:function(a){return a===b&&(a=!1),this._readonly===a?!1:(this._readonly=a,this.opts.element.prop("readonly",a),this.enableInterface(),!0)},opened:function(){return this.container.hasClass("select2-dropdown-open")},positionDropdown:function(){var q,r,s,t,b=this.dropdown,c=this.container.offset(),d=this.container.outerHeight(!1),e=this.container.outerWidth(!1),f=b.outerHeight(!1),g=a(window).scrollLeft()+a(window).width(),h=a(window).scrollTop()+a(window).height(),i=c.top+d,j=c.left,l=h>=i+f,m=c.top-f>=this.body().scrollTop(),n=b.outerWidth(!1),o=g>=j+n,p=b.hasClass("select2-drop-above");this.opts.dropdownAutoWidth?(t=a(".select2-results",b)[0],b.addClass("select2-drop-auto-width"),b.css("width",""),n=b.outerWidth(!1)+(t.scrollHeight===t.clientHeight?0:k.width),n>e?e=n:n=e,o=g>=j+n):this.container.removeClass("select2-drop-auto-width"),"static"!==this.body().css("position")&&(q=this.body().offset(),i-=q.top,j-=q.left),p?(r=!0,!m&&l&&(r=!1)):(r=!1,!l&&m&&(r=!0)),o||(j=c.left+e-n),r?(i=c.top-f,this.container.addClass("select2-drop-above"),b.addClass("select2-drop-above")):(this.container.removeClass("select2-drop-above"),b.removeClass("select2-drop-above")),s=a.extend({top:i,left:j,width:e},K(this.opts.dropdownCss)),b.css(s)},shouldOpen:function(){var b;return this.opened()?!1:this._enabled===!1||this._readonly===!0?!1:(b=a.Event("select2-opening"),this.opts.element.trigger(b),!b.isDefaultPrevented())},clearDropdownAlignmentPreference:function(){this.container.removeClass("select2-drop-above"),this.dropdown.removeClass("select2-drop-above")},open:function(){return this.shouldOpen()?(this.opening(),!0):!1},opening:function(){var f,b=this.containerId,c="scroll."+b,d="resize."+b,e="orientationchange."+b;this.container.addClass("select2-dropdown-open").addClass("select2-container-active"),this.clearDropdownAlignmentPreference(),this.dropdown[0]!==this.body().children().last()[0]&&this.dropdown.detach().appendTo(this.body()),f=a("#select2-drop-mask"),0==f.length&&(f=a(document.createElement("div")),f.attr("id","select2-drop-mask").attr("class","select2-drop-mask"),f.hide(),f.appendTo(this.body()),f.on("mousedown touchstart click",function(b){var d,c=a("#select2-drop");c.length>0&&(d=c.data("select2"),d.opts.selectOnBlur&&d.selectHighlighted({noFocus:!0}),d.close({focus:!1}),b.preventDefault(),b.stopPropagation())})),this.dropdown.prev()[0]!==f[0]&&this.dropdown.before(f),a("#select2-drop").removeAttr("id"),this.dropdown.attr("id","select2-drop"),f.show(),this.positionDropdown(),this.dropdown.show(),this.positionDropdown(),this.dropdown.addClass("select2-drop-active");var h=this;this.container.parents().add(window).each(function(){a(this).on(d+" "+c+" "+e,function(){h.positionDropdown()})})},close:function(){if(this.opened()){var b=this.containerId,c="scroll."+b,d="resize."+b,e="orientationchange."+b;this.container.parents().add(window).each(function(){a(this).off(c).off(d).off(e)}),this.clearDropdownAlignmentPreference(),a("#select2-drop-mask").hide(),this.dropdown.removeAttr("id"),this.dropdown.hide(),this.container.removeClass("select2-dropdown-open").removeClass("select2-container-active"),this.results.empty(),this.clearSearch(),this.search.removeClass("select2-active"),this.opts.element.trigger(a.Event("select2-close"))}},externalSearch:function(a){this.open(),this.search.val(a),this.updateResults(!1)},clearSearch:function(){},getMaximumSelectionSize:function(){return K(this.opts.maximumSelectionSize)},ensureHighlightVisible:function(){var c,d,e,f,g,h,i,b=this.results;if(d=this.highlight(),!(0>d)){if(0==d)return b.scrollTop(0),void 0;c=this.findHighlightableChoices().find(".select2-result-label"),e=a(c[d]),f=e.offset().top+e.outerHeight(!0),d===c.length-1&&(i=b.find("li.select2-more-results"),i.length>0&&(f=i.offset().top+i.outerHeight(!0))),g=b.offset().top+b.outerHeight(!0),f>g&&b.scrollTop(b.scrollTop()+(f-g)),h=e.offset().top-b.offset().top,0>h&&"none"!=e.css("display")&&b.scrollTop(b.scrollTop()+h)}},findHighlightableChoices:function(){return this.results.find(".select2-result-selectable:not(.select2-disabled)")},moveHighlight:function(b){for(var c=this.findHighlightableChoices(),d=this.highlight();d>-1&&d<c.length;){d+=b;var e=a(c[d]);if(e.hasClass("select2-result-selectable")&&!e.hasClass("select2-disabled")&&!e.hasClass("select2-selected")){this.highlight(d);break}}},highlight:function(b){var d,e,c=this.findHighlightableChoices();return 0===arguments.length?o(c.filter(".select2-highlighted")[0],c.get()):(b>=c.length&&(b=c.length-1),0>b&&(b=0),this.removeHighlight(),d=a(c[b]),d.addClass("select2-highlighted"),this.ensureHighlightVisible(),e=d.data("select2-data"),e&&this.opts.element.trigger({type:"select2-highlight",val:this.id(e),choice:e}),void 0)},removeHighlight:function(){this.results.find(".select2-highlighted").removeClass("select2-highlighted")},countSelectableResults:function(){return this.findHighlightableChoices().length},highlightUnderEvent:function(b){var c=a(b.target).closest(".select2-result-selectable");if(c.length>0&&!c.is(".select2-highlighted")){var d=this.findHighlightableChoices();this.highlight(d.index(c))}else 0==c.length&&this.removeHighlight()},loadMoreIfNeeded:function(){var c,a=this.results,b=a.find("li.select2-more-results"),e=this.resultsPage+1,f=this,g=this.search.val(),h=this.context;0!==b.length&&(c=b.offset().top-a.offset().top-a.height(),c<=this.opts.loadMorePadding&&(b.addClass("select2-active"),this.opts.query({element:this.opts.element,term:g,page:e,context:h,matcher:this.opts.matcher,callback:this.bind(function(c){f.opened()&&(f.opts.populateResults.call(this,a,c.results,{term:g,page:e,context:h}),f.postprocessResults(c,!1,!1),c.more===!0?(b.detach().appendTo(a).text(f.opts.formatLoadMore(e+1)),window.setTimeout(function(){f.loadMoreIfNeeded()},10)):b.remove(),f.positionDropdown(),f.resultsPage=e,f.context=c.context,this.opts.element.trigger({type:"select2-loaded",items:c}))})})))},tokenize:function(){},updateResults:function(c){function m(){d.removeClass("select2-active"),h.positionDropdown()}function n(a){e.html(a),m()}var g,i,l,d=this.search,e=this.results,f=this.opts,h=this,j=d.val(),k=a.data(this.container,"select2-last-term");if((c===!0||!k||!q(j,k))&&(a.data(this.container,"select2-last-term",j),c===!0||this.showSearchInput!==!1&&this.opened())){l=++this.queryCount;var o=this.getMaximumSelectionSize();if(o>=1&&(g=this.data(),a.isArray(g)&&g.length>=o&&J(f.formatSelectionTooBig,"formatSelectionTooBig")))return n("<li class='select2-selection-limit'>"+f.formatSelectionTooBig(o)+"</li>"),void 0;if(d.val().length<f.minimumInputLength)return J(f.formatInputTooShort,"formatInputTooShort")?n("<li class='select2-no-results'>"+f.formatInputTooShort(d.val(),f.minimumInputLength)+"</li>"):n(""),c&&this.showSearch&&this.showSearch(!0),void 0;if(f.maximumInputLength&&d.val().length>f.maximumInputLength)return J(f.formatInputTooLong,"formatInputTooLong")?n("<li class='select2-no-results'>"+f.formatInputTooLong(d.val(),f.maximumInputLength)+"</li>"):n(""),void 0;
f.formatSearching&&0===this.findHighlightableChoices().length&&n("<li class='select2-searching'>"+f.formatSearching()+"</li>"),d.addClass("select2-active"),this.removeHighlight(),i=this.tokenize(),i!=b&&null!=i&&d.val(i),this.resultsPage=1,f.query({element:f.element,term:d.val(),page:this.resultsPage,context:null,matcher:f.matcher,callback:this.bind(function(g){var i;if(l==this.queryCount){if(!this.opened())return this.search.removeClass("select2-active"),void 0;if(this.context=g.context===b?null:g.context,this.opts.createSearchChoice&&""!==d.val()&&(i=this.opts.createSearchChoice.call(h,d.val(),g.results),i!==b&&null!==i&&h.id(i)!==b&&null!==h.id(i)&&0===a(g.results).filter(function(){return q(h.id(this),h.id(i))}).length&&g.results.unshift(i)),0===g.results.length&&J(f.formatNoMatches,"formatNoMatches"))return n("<li class='select2-no-results'>"+f.formatNoMatches(d.val())+"</li>"),void 0;e.empty(),h.opts.populateResults.call(this,e,g.results,{term:d.val(),page:this.resultsPage,context:null}),g.more===!0&&J(f.formatLoadMore,"formatLoadMore")&&(e.append("<li class='select2-more-results'>"+h.opts.escapeMarkup(f.formatLoadMore(this.resultsPage))+"</li>"),window.setTimeout(function(){h.loadMoreIfNeeded()},10)),this.postprocessResults(g,c),m(),this.opts.element.trigger({type:"select2-loaded",items:g})}})})}},cancel:function(){this.close()},blur:function(){this.opts.selectOnBlur&&this.selectHighlighted({noFocus:!0}),this.close(),this.container.removeClass("select2-container-active"),this.search[0]===document.activeElement&&this.search.blur(),this.clearSearch(),this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus")},focusSearch:function(){y(this.search)},selectHighlighted:function(a){var b=this.highlight(),c=this.results.find(".select2-highlighted"),d=c.closest(".select2-result").data("select2-data");d?(this.highlight(b),this.onSelect(d,a)):a&&a.noFocus&&this.close()},getPlaceholder:function(){var a;return this.opts.element.attr("placeholder")||this.opts.element.attr("data-placeholder")||this.opts.element.data("placeholder")||this.opts.placeholder||((a=this.getPlaceholderOption())!==b?a.text():b)},getPlaceholderOption:function(){if(this.select){var a=this.select.children().first();if(this.opts.placeholderOption!==b)return"first"===this.opts.placeholderOption&&a||"function"==typeof this.opts.placeholderOption&&this.opts.placeholderOption(this.select);if(""===a.text()&&""===a.val())return a}},initContainerWidth:function(){function c(){var c,d,e,f,g;if("off"===this.opts.width)return null;if("element"===this.opts.width)return 0===this.opts.element.outerWidth(!1)?"auto":this.opts.element.outerWidth(!1)+"px";if("copy"===this.opts.width||"resolve"===this.opts.width){if(c=this.opts.element.attr("style"),c!==b)for(d=c.split(";"),f=0,g=d.length;g>f;f+=1)if(e=d[f].replace(/\s/g,"").match(/[^-]width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i),null!==e&&e.length>=1)return e[1];return"resolve"===this.opts.width?(c=this.opts.element.css("width"),c.indexOf("%")>0?c:0===this.opts.element.outerWidth(!1)?"auto":this.opts.element.outerWidth(!1)+"px"):null}return a.isFunction(this.opts.width)?this.opts.width():this.opts.width}var d=c.call(this);null!==d&&this.container.css("width",d)}}),e=N(d,{createContainer:function(){var b=a(document.createElement("div")).attr({"class":"select2-container"}).html(["<a href='javascript:void(0)' onclick='return false;' class='select2-choice' tabindex='-1'>","   <span class='select2-chosen'>&nbsp;</span><abbr class='select2-search-choice-close'></abbr>","   <span class='select2-arrow'><b></b></span>","</a>","<input class='select2-focusser select2-offscreen' type='text'/>","<div class='select2-drop select2-display-none'>","   <div class='select2-search'>","       <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input'/>","   </div>","   <ul class='select2-results'>","   </ul>","</div>"].join(""));return b},enableInterface:function(){this.parent.enableInterface.apply(this,arguments)&&this.focusser.prop("disabled",!this.isInterfaceEnabled())},opening:function(){var c,d,e;this.opts.minimumResultsForSearch>=0&&this.showSearch(!0),this.parent.opening.apply(this,arguments),this.showSearchInput!==!1&&this.search.val(this.focusser.val()),this.search.focus(),c=this.search.get(0),c.createTextRange?(d=c.createTextRange(),d.collapse(!1),d.select()):c.setSelectionRange&&(e=this.search.val().length,c.setSelectionRange(e,e)),""===this.search.val()&&this.nextSearchTerm!=b&&(this.search.val(this.nextSearchTerm),this.search.select()),this.focusser.prop("disabled",!0).val(""),this.updateResults(!0),this.opts.element.trigger(a.Event("select2-open"))},close:function(a){this.opened()&&(this.parent.close.apply(this,arguments),a=a||{focus:!0},this.focusser.removeAttr("disabled"),a.focus&&this.focusser.focus())},focus:function(){this.opened()?this.close():(this.focusser.removeAttr("disabled"),this.focusser.focus())},isFocused:function(){return this.container.hasClass("select2-container-active")},cancel:function(){this.parent.cancel.apply(this,arguments),this.focusser.removeAttr("disabled"),this.focusser.focus()},destroy:function(){a("label[for='"+this.focusser.attr("id")+"']").attr("for",this.opts.element.attr("id")),this.parent.destroy.apply(this,arguments)},initContainer:function(){var b,d=this.container,e=this.dropdown;this.opts.minimumResultsForSearch<0?this.showSearch(!1):this.showSearch(!0),this.selection=b=d.find(".select2-choice"),this.focusser=d.find(".select2-focusser"),this.focusser.attr("id","s2id_autogen"+g()),a("label[for='"+this.opts.element.attr("id")+"']").attr("for",this.focusser.attr("id")),this.focusser.attr("tabindex",this.elementTabIndex),this.search.on("keydown",this.bind(function(a){if(this.isInterfaceEnabled()){if(a.which===c.PAGE_UP||a.which===c.PAGE_DOWN)return A(a),void 0;switch(a.which){case c.UP:case c.DOWN:return this.moveHighlight(a.which===c.UP?-1:1),A(a),void 0;case c.ENTER:return this.selectHighlighted(),A(a),void 0;case c.TAB:return this.selectHighlighted({noFocus:!0}),void 0;case c.ESC:return this.cancel(a),A(a),void 0}}})),this.search.on("blur",this.bind(function(){document.activeElement===this.body().get(0)&&window.setTimeout(this.bind(function(){this.search.focus()}),0)})),this.focusser.on("keydown",this.bind(function(a){if(this.isInterfaceEnabled()&&a.which!==c.TAB&&!c.isControl(a)&&!c.isFunctionKey(a)&&a.which!==c.ESC){if(this.opts.openOnEnter===!1&&a.which===c.ENTER)return A(a),void 0;if(a.which==c.DOWN||a.which==c.UP||a.which==c.ENTER&&this.opts.openOnEnter){if(a.altKey||a.ctrlKey||a.shiftKey||a.metaKey)return;return this.open(),A(a),void 0}return a.which==c.DELETE||a.which==c.BACKSPACE?(this.opts.allowClear&&this.clear(),A(a),void 0):void 0}})),t(this.focusser),this.focusser.on("keyup-change input",this.bind(function(a){if(this.opts.minimumResultsForSearch>=0){if(a.stopPropagation(),this.opened())return;this.open()}})),b.on("mousedown","abbr",this.bind(function(a){this.isInterfaceEnabled()&&(this.clear(),B(a),this.close(),this.selection.focus())})),b.on("mousedown",this.bind(function(b){this.container.hasClass("select2-container-active")||this.opts.element.trigger(a.Event("select2-focus")),this.opened()?this.close():this.isInterfaceEnabled()&&this.open(),A(b)})),e.on("mousedown",this.bind(function(){this.search.focus()})),b.on("focus",this.bind(function(a){A(a)})),this.focusser.on("focus",this.bind(function(){this.container.hasClass("select2-container-active")||this.opts.element.trigger(a.Event("select2-focus")),this.container.addClass("select2-container-active")})).on("blur",this.bind(function(){this.opened()||(this.container.removeClass("select2-container-active"),this.opts.element.trigger(a.Event("select2-blur")))})),this.search.on("focus",this.bind(function(){this.container.hasClass("select2-container-active")||this.opts.element.trigger(a.Event("select2-focus")),this.container.addClass("select2-container-active")})),this.initContainerWidth(),this.opts.element.addClass("select2-offscreen"),this.setPlaceholder()},clear:function(b){var c=this.selection.data("select2-data");if(c){var d=a.Event("select2-clearing");if(this.opts.element.trigger(d),d.isDefaultPrevented())return;var e=this.getPlaceholderOption();this.opts.element.val(e?e.val():""),this.selection.find(".select2-chosen").empty(),this.selection.removeData("select2-data"),this.setPlaceholder(),b!==!1&&(this.opts.element.trigger({type:"select2-removed",val:this.id(c),choice:c}),this.triggerChange({removed:c}))}},initSelection:function(){if(this.isPlaceholderOptionSelected())this.updateSelection(null),this.close(),this.setPlaceholder();else{var c=this;this.opts.initSelection.call(null,this.opts.element,function(a){a!==b&&null!==a&&(c.updateSelection(a),c.close(),c.setPlaceholder())})}},isPlaceholderOptionSelected:function(){var a;return this.getPlaceholder()?(a=this.getPlaceholderOption())!==b&&a.is(":selected")||""===this.opts.element.val()||this.opts.element.val()===b||null===this.opts.element.val():!1},prepareOpts:function(){var b=this.parent.prepareOpts.apply(this,arguments),c=this;return"select"===b.element.get(0).tagName.toLowerCase()?b.initSelection=function(a,b){var d=a.find(":selected");b(c.optionToData(d))}:"data"in b&&(b.initSelection=b.initSelection||function(c,d){var e=c.val(),f=null;b.query({matcher:function(a,c,d){var g=q(e,b.id(d));return g&&(f=d),g},callback:a.isFunction(d)?function(){d(f)}:a.noop})}),b},getPlaceholder:function(){return this.select&&this.getPlaceholderOption()===b?b:this.parent.getPlaceholder.apply(this,arguments)},setPlaceholder:function(){var a=this.getPlaceholder();if(this.isPlaceholderOptionSelected()&&a!==b){if(this.select&&this.getPlaceholderOption()===b)return;this.selection.find(".select2-chosen").html(this.opts.escapeMarkup(a)),this.selection.addClass("select2-default"),this.container.removeClass("select2-allowclear")}},postprocessResults:function(a,b,c){var d=0,e=this;if(this.findHighlightableChoices().each2(function(a,b){return q(e.id(b.data("select2-data")),e.opts.element.val())?(d=a,!1):void 0}),c!==!1&&(b===!0&&d>=0?this.highlight(d):this.highlight(0)),b===!0){var g=this.opts.minimumResultsForSearch;g>=0&&this.showSearch(L(a.results)>=g)}},showSearch:function(b){this.showSearchInput!==b&&(this.showSearchInput=b,this.dropdown.find(".select2-search").toggleClass("select2-search-hidden",!b),this.dropdown.find(".select2-search").toggleClass("select2-offscreen",!b),a(this.dropdown,this.container).toggleClass("select2-with-searchbox",b))},onSelect:function(a,b){if(this.triggerSelect(a)){var c=this.opts.element.val(),d=this.data();this.opts.element.val(this.id(a)),this.updateSelection(a),this.opts.element.trigger({type:"select2-selected",val:this.id(a),choice:a}),this.nextSearchTerm=this.opts.nextSearchTerm(a,this.search.val()),this.close(),b&&b.noFocus||this.focusser.focus(),q(c,this.id(a))||this.triggerChange({added:a,removed:d})}},updateSelection:function(a){var d,e,c=this.selection.find(".select2-chosen");this.selection.data("select2-data",a),c.empty(),null!==a&&(d=this.opts.formatSelection(a,c,this.opts.escapeMarkup)),d!==b&&c.append(d),e=this.opts.formatSelectionCssClass(a,c),e!==b&&c.addClass(e),this.selection.removeClass("select2-default"),this.opts.allowClear&&this.getPlaceholder()!==b&&this.container.addClass("select2-allowclear")},val:function(){var a,c=!1,d=null,e=this,f=this.data();if(0===arguments.length)return this.opts.element.val();if(a=arguments[0],arguments.length>1&&(c=arguments[1]),this.select)this.select.val(a).find(":selected").each2(function(a,b){return d=e.optionToData(b),!1}),this.updateSelection(d),this.setPlaceholder(),c&&this.triggerChange({added:d,removed:f});else{if(!a&&0!==a)return this.clear(c),void 0;if(this.opts.initSelection===b)throw new Error("cannot call val() if initSelection() is not defined");this.opts.element.val(a),this.opts.initSelection(this.opts.element,function(a){e.opts.element.val(a?e.id(a):""),e.updateSelection(a),e.setPlaceholder(),c&&e.triggerChange({added:a,removed:f})})}},clearSearch:function(){this.search.val(""),this.focusser.val("")},data:function(a){var c,d=!1;return 0===arguments.length?(c=this.selection.data("select2-data"),c==b&&(c=null),c):(arguments.length>1&&(d=arguments[1]),a?(c=this.data(),this.opts.element.val(a?this.id(a):""),this.updateSelection(a),d&&this.triggerChange({added:a,removed:c})):this.clear(d),void 0)}}),f=N(d,{createContainer:function(){var b=a(document.createElement("div")).attr({"class":"select2-container select2-container-multi"}).html(["<ul class='select2-choices'>","  <li class='select2-search-field'>","    <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input'>","  </li>","</ul>","<div class='select2-drop select2-drop-multi select2-display-none'>","   <ul class='select2-results'>","   </ul>","</div>"].join(""));return b},prepareOpts:function(){var b=this.parent.prepareOpts.apply(this,arguments),c=this;return"select"===b.element.get(0).tagName.toLowerCase()?b.initSelection=function(a,b){var d=[];a.find(":selected").each2(function(a,b){d.push(c.optionToData(b))}),b(d)}:"data"in b&&(b.initSelection=b.initSelection||function(c,d){var e=r(c.val(),b.separator),f=[];b.query({matcher:function(c,d,g){var h=a.grep(e,function(a){return q(a,b.id(g))}).length;return h&&f.push(g),h},callback:a.isFunction(d)?function(){for(var a=[],c=0;c<e.length;c++)for(var g=e[c],h=0;h<f.length;h++){var i=f[h];if(q(g,b.id(i))){a.push(i),f.splice(h,1);break}}d(a)}:a.noop})}),b},selectChoice:function(a){var b=this.container.find(".select2-search-choice-focus");b.length&&a&&a[0]==b[0]||(b.length&&this.opts.element.trigger("choice-deselected",b),b.removeClass("select2-search-choice-focus"),a&&a.length&&(this.close(),a.addClass("select2-search-choice-focus"),this.opts.element.trigger("choice-selected",a)))},destroy:function(){a("label[for='"+this.search.attr("id")+"']").attr("for",this.opts.element.attr("id")),this.parent.destroy.apply(this,arguments)},initContainer:function(){var d,b=".select2-choices";this.searchContainer=this.container.find(".select2-search-field"),this.selection=d=this.container.find(b);var e=this;this.selection.on("click",".select2-search-choice:not(.select2-locked)",function(){e.search[0].focus(),e.selectChoice(a(this))}),this.search.attr("id","s2id_autogen"+g()),a("label[for='"+this.opts.element.attr("id")+"']").attr("for",this.search.attr("id")),this.search.on("input paste",this.bind(function(){this.isInterfaceEnabled()&&(this.opened()||this.open())})),this.search.attr("tabindex",this.elementTabIndex),this.keydowns=0,this.search.on("keydown",this.bind(function(a){if(this.isInterfaceEnabled()){++this.keydowns;var b=d.find(".select2-search-choice-focus"),e=b.prev(".select2-search-choice:not(.select2-locked)"),f=b.next(".select2-search-choice:not(.select2-locked)"),g=z(this.search);if(b.length&&(a.which==c.LEFT||a.which==c.RIGHT||a.which==c.BACKSPACE||a.which==c.DELETE||a.which==c.ENTER)){var h=b;return a.which==c.LEFT&&e.length?h=e:a.which==c.RIGHT?h=f.length?f:null:a.which===c.BACKSPACE?(this.unselect(b.first()),this.search.width(10),h=e.length?e:f):a.which==c.DELETE?(this.unselect(b.first()),this.search.width(10),h=f.length?f:null):a.which==c.ENTER&&(h=null),this.selectChoice(h),A(a),h&&h.length||this.open(),void 0}if((a.which===c.BACKSPACE&&1==this.keydowns||a.which==c.LEFT)&&0==g.offset&&!g.length)return this.selectChoice(d.find(".select2-search-choice:not(.select2-locked)").last()),A(a),void 0;if(this.selectChoice(null),this.opened())switch(a.which){case c.UP:case c.DOWN:return this.moveHighlight(a.which===c.UP?-1:1),A(a),void 0;case c.ENTER:return this.selectHighlighted(),A(a),void 0;case c.TAB:return this.selectHighlighted({noFocus:!0}),this.close(),void 0;case c.ESC:return this.cancel(a),A(a),void 0}if(a.which!==c.TAB&&!c.isControl(a)&&!c.isFunctionKey(a)&&a.which!==c.BACKSPACE&&a.which!==c.ESC){if(a.which===c.ENTER){if(this.opts.openOnEnter===!1)return;if(a.altKey||a.ctrlKey||a.shiftKey||a.metaKey)return}this.open(),(a.which===c.PAGE_UP||a.which===c.PAGE_DOWN)&&A(a),a.which===c.ENTER&&A(a)}}})),this.search.on("keyup",this.bind(function(){this.keydowns=0,this.resizeSearch()})),this.search.on("blur",this.bind(function(b){this.container.removeClass("select2-container-active"),this.search.removeClass("select2-focused"),this.selectChoice(null),this.opened()||this.clearSearch(),b.stopImmediatePropagation(),this.opts.element.trigger(a.Event("select2-blur"))})),this.container.on("click",b,this.bind(function(b){this.isInterfaceEnabled()&&(a(b.target).closest(".select2-search-choice").length>0||(this.selectChoice(null),this.clearPlaceholder(),this.container.hasClass("select2-container-active")||this.opts.element.trigger(a.Event("select2-focus")),this.open(),this.focusSearch(),b.preventDefault()))})),this.container.on("focus",b,this.bind(function(){this.isInterfaceEnabled()&&(this.container.hasClass("select2-container-active")||this.opts.element.trigger(a.Event("select2-focus")),this.container.addClass("select2-container-active"),this.dropdown.addClass("select2-drop-active"),this.clearPlaceholder())})),this.initContainerWidth(),this.opts.element.addClass("select2-offscreen"),this.clearSearch()},enableInterface:function(){this.parent.enableInterface.apply(this,arguments)&&this.search.prop("disabled",!this.isInterfaceEnabled())},initSelection:function(){if(""===this.opts.element.val()&&""===this.opts.element.text()&&(this.updateSelection([]),this.close(),this.clearSearch()),this.select||""!==this.opts.element.val()){var c=this;this.opts.initSelection.call(null,this.opts.element,function(a){a!==b&&null!==a&&(c.updateSelection(a),c.close(),c.clearSearch())})}},clearSearch:function(){var a=this.getPlaceholder(),c=this.getMaxSearchWidth();a!==b&&0===this.getVal().length&&this.search.hasClass("select2-focused")===!1?(this.search.val(a).addClass("select2-default"),this.search.width(c>0?c:this.container.css("width"))):this.search.val("").width(10)},clearPlaceholder:function(){this.search.hasClass("select2-default")&&this.search.val("").removeClass("select2-default")},opening:function(){this.clearPlaceholder(),this.resizeSearch(),this.parent.opening.apply(this,arguments),this.focusSearch(),this.updateResults(!0),this.search.focus(),this.opts.element.trigger(a.Event("select2-open"))},close:function(){this.opened()&&this.parent.close.apply(this,arguments)},focus:function(){this.close(),this.search.focus()},isFocused:function(){return this.search.hasClass("select2-focused")},updateSelection:function(b){var c=[],d=[],e=this;a(b).each(function(){o(e.id(this),c)<0&&(c.push(e.id(this)),d.push(this))}),b=d,this.selection.find(".select2-search-choice").remove(),a(b).each(function(){e.addSelectedChoice(this)}),e.postprocessResults()},tokenize:function(){var a=this.search.val();a=this.opts.tokenizer.call(this,a,this.data(),this.bind(this.onSelect),this.opts),null!=a&&a!=b&&(this.search.val(a),a.length>0&&this.open())},onSelect:function(a,b){this.triggerSelect(a)&&(this.addSelectedChoice(a),this.opts.element.trigger({type:"selected",val:this.id(a),choice:a}),(this.select||!this.opts.closeOnSelect)&&this.postprocessResults(a,!1,this.opts.closeOnSelect===!0),this.opts.closeOnSelect?(this.close(),this.search.width(10)):this.countSelectableResults()>0?(this.search.width(10),this.resizeSearch(),this.getMaximumSelectionSize()>0&&this.val().length>=this.getMaximumSelectionSize()&&this.updateResults(!0),this.positionDropdown()):(this.close(),this.search.width(10)),this.triggerChange({added:a}),b&&b.noFocus||this.focusSearch())},cancel:function(){this.close(),this.focusSearch()},addSelectedChoice:function(c){var j,k,d=!c.locked,e=a("<li class='select2-search-choice'>    <div></div>    <a href='#' onclick='return false;' class='select2-search-choice-close' tabindex='-1'></a></li>"),f=a("<li class='select2-search-choice select2-locked'><div></div></li>"),g=d?e:f,h=this.id(c),i=this.getVal();j=this.opts.formatSelection(c,g.find("div"),this.opts.escapeMarkup),j!=b&&g.find("div").replaceWith("<div>"+j+"</div>"),k=this.opts.formatSelectionCssClass(c,g.find("div")),k!=b&&g.addClass(k),d&&g.find(".select2-search-choice-close").on("mousedown",A).on("click dblclick",this.bind(function(b){this.isInterfaceEnabled()&&(a(b.target).closest(".select2-search-choice").fadeOut("fast",this.bind(function(){this.unselect(a(b.target)),this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus"),this.close(),this.focusSearch()})).dequeue(),A(b))})).on("focus",this.bind(function(){this.isInterfaceEnabled()&&(this.container.addClass("select2-container-active"),this.dropdown.addClass("select2-drop-active"))})),g.data("select2-data",c),g.insertBefore(this.searchContainer),i.push(h),this.setVal(i)},unselect:function(a){var c,d,b=this.getVal();if(a=a.closest(".select2-search-choice"),0===a.length)throw"Invalid argument: "+a+". Must be .select2-search-choice";if(c=a.data("select2-data")){for(;(d=o(this.id(c),b))>=0;)b.splice(d,1),this.setVal(b),this.select&&this.postprocessResults();a.remove(),this.opts.element.trigger({type:"removed",val:this.id(c),choice:c}),this.triggerChange({removed:c})}},postprocessResults:function(a,b,c){var d=this.getVal(),e=this.results.find(".select2-result"),f=this.results.find(".select2-result-with-children"),g=this;e.each2(function(a,b){var c=g.id(b.data("select2-data"));o(c,d)>=0&&(b.addClass("select2-selected"),b.find(".select2-result-selectable").addClass("select2-selected"))}),f.each2(function(a,b){b.is(".select2-result-selectable")||0!==b.find(".select2-result-selectable:not(.select2-selected)").length||b.addClass("select2-selected")}),-1==this.highlight()&&c!==!1&&g.highlight(0),!this.opts.createSearchChoice&&!e.filter(".select2-result:not(.select2-selected)").length>0&&(!a||a&&!a.more&&0===this.results.find(".select2-no-results").length)&&J(g.opts.formatNoMatches,"formatNoMatches")&&this.results.append("<li class='select2-no-results'>"+g.opts.formatNoMatches(g.search.val())+"</li>")},getMaxSearchWidth:function(){return this.selection.width()-s(this.search)},resizeSearch:function(){var a,b,c,d,e,f=s(this.search);a=C(this.search)+10,b=this.search.offset().left,c=this.selection.width(),d=this.selection.offset().left,e=c-(b-d)-f,a>e&&(e=c-f),40>e&&(e=c-f),0>=e&&(e=a),this.search.width(Math.floor(e))},getVal:function(){var a;return this.select?(a=this.select.val(),null===a?[]:a):(a=this.opts.element.val(),r(a,this.opts.separator))},setVal:function(b){var c;this.select?this.select.val(b):(c=[],a(b).each(function(){o(this,c)<0&&c.push(this)}),this.opts.element.val(0===c.length?"":c.join(this.opts.separator)))},buildChangeDetails:function(a,b){for(var b=b.slice(0),a=a.slice(0),c=0;c<b.length;c++)for(var d=0;d<a.length;d++)q(this.opts.id(b[c]),this.opts.id(a[d]))&&(b.splice(c,1),c--,a.splice(d,1),d--);return{added:b,removed:a}},val:function(c,d){var e,f=this;if(0===arguments.length)return this.getVal();if(e=this.data(),e.length||(e=[]),!c&&0!==c)return this.opts.element.val(""),this.updateSelection([]),this.clearSearch(),d&&this.triggerChange({added:this.data(),removed:e}),void 0;if(this.setVal(c),this.select)this.opts.initSelection(this.select,this.bind(this.updateSelection)),d&&this.triggerChange(this.buildChangeDetails(e,this.data()));else{if(this.opts.initSelection===b)throw new Error("val() cannot be called if initSelection() is not defined");this.opts.initSelection(this.opts.element,function(b){var c=a.map(b,f.id);f.setVal(c),f.updateSelection(b),f.clearSearch(),d&&f.triggerChange(f.buildChangeDetails(e,this.data()))})}this.clearSearch()},onSortStart:function(){if(this.select)throw new Error("Sorting of elements is not supported when attached to <select>. Attach to <input type='hidden'/> instead.");this.search.width(0),this.searchContainer.hide()},onSortEnd:function(){var b=[],c=this;this.searchContainer.show(),this.searchContainer.appendTo(this.searchContainer.parent()),this.resizeSearch(),this.selection.find(".select2-search-choice").each(function(){b.push(c.opts.id(a(this).data("select2-data")))}),this.setVal(b),this.triggerChange()},data:function(b,c){var e,f,d=this;return 0===arguments.length?this.selection.find(".select2-search-choice").map(function(){return a(this).data("select2-data")}).get():(f=this.data(),b||(b=[]),e=a.map(b,function(a){return d.opts.id(a)}),this.setVal(e),this.updateSelection(b),this.clearSearch(),c&&this.triggerChange(this.buildChangeDetails(f,this.data())),void 0)}}),a.fn.select2=function(){var d,g,h,i,j,c=Array.prototype.slice.call(arguments,0),k=["val","destroy","opened","open","close","focus","isFocused","container","dropdown","onSortStart","onSortEnd","enable","disable","readonly","positionDropdown","data","search"],l=["opened","isFocused","container","dropdown"],m=["val","data"],n={search:"externalSearch"};return this.each(function(){if(0===c.length||"object"==typeof c[0])d=0===c.length?{}:a.extend({},c[0]),d.element=a(this),"select"===d.element.get(0).tagName.toLowerCase()?j=d.element.prop("multiple"):(j=d.multiple||!1,"tags"in d&&(d.multiple=j=!0)),g=j?new f:new e,g.init(d);else{if("string"!=typeof c[0])throw"Invalid arguments to select2 plugin: "+c;if(o(c[0],k)<0)throw"Unknown method: "+c[0];if(i=b,g=a(this).data("select2"),g===b)return;if(h=c[0],"container"===h?i=g.container:"dropdown"===h?i=g.dropdown:(n[h]&&(h=n[h]),i=g[h].apply(g,c.slice(1))),o(c[0],l)>=0||o(c[0],m)&&1==c.length)return!1}}),i===b?this:i},a.fn.select2.defaults={width:"copy",loadMorePadding:0,closeOnSelect:!0,openOnEnter:!0,containerCss:{},dropdownCss:{},containerCssClass:"",dropdownCssClass:"",formatResult:function(a,b,c,d){var e=[];return E(a.text,c.term,e,d),e.join("")},formatSelection:function(a,c,d){return a?d(a.text):b},sortResults:function(a){return a},formatResultCssClass:function(){return b},formatSelectionCssClass:function(){return b},formatNoMatches:function(){return"No matches found"},formatInputTooShort:function(a,b){var c=b-a.length;return"Please enter "+c+" more character"+(1==c?"":"s")},formatInputTooLong:function(a,b){var c=a.length-b;return"Please delete "+c+" character"+(1==c?"":"s")},formatSelectionTooBig:function(a){return"You can only select "+a+" item"+(1==a?"":"s")},formatLoadMore:function(){return"Loading more results..."},formatSearching:function(){return"Searching..."},minimumResultsForSearch:0,minimumInputLength:0,maximumInputLength:null,maximumSelectionSize:0,id:function(a){return a.id},matcher:function(a,b){return n(""+b).toUpperCase().indexOf(n(""+a).toUpperCase())>=0},separator:",",tokenSeparators:[],tokenizer:M,escapeMarkup:F,blurOnChange:!1,selectOnBlur:!1,adaptContainerCssClass:function(a){return a},adaptDropdownCssClass:function(){return null},nextSearchTerm:function(){return b}},a.fn.select2.ajaxDefaults={transport:a.ajax,params:{type:"GET",cache:!1,dataType:"json"}},window.Select2={query:{ajax:G,local:H,tags:I},util:{debounce:v,markMatch:E,escapeMarkup:F,stripDiacritics:n},"class":{"abstract":d,single:e,multi:f}}}}(jQuery);
// moment.js
// version : 2.1.0
// author : Tim Wood
// license : MIT
// momentjs.com
!function(t){function e(t,e){return function(n){return u(t.call(this,n),e)}}function n(t,e){return function(n){return this.lang().ordinal(t.call(this,n),e)}}function s(){}function i(t){a(this,t)}function r(t){var e=t.years||t.year||t.y||0,n=t.months||t.month||t.M||0,s=t.weeks||t.week||t.w||0,i=t.days||t.day||t.d||0,r=t.hours||t.hour||t.h||0,a=t.minutes||t.minute||t.m||0,o=t.seconds||t.second||t.s||0,u=t.milliseconds||t.millisecond||t.ms||0;this._input=t,this._milliseconds=u+1e3*o+6e4*a+36e5*r,this._days=i+7*s,this._months=n+12*e,this._data={},this._bubble()}function a(t,e){for(var n in e)e.hasOwnProperty(n)&&(t[n]=e[n]);return t}function o(t){return 0>t?Math.ceil(t):Math.floor(t)}function u(t,e){for(var n=t+"";n.length<e;)n="0"+n;return n}function h(t,e,n,s){var i,r,a=e._milliseconds,o=e._days,u=e._months;a&&t._d.setTime(+t._d+a*n),(o||u)&&(i=t.minute(),r=t.hour()),o&&t.date(t.date()+o*n),u&&t.month(t.month()+u*n),a&&!s&&H.updateOffset(t),(o||u)&&(t.minute(i),t.hour(r))}function d(t){return"[object Array]"===Object.prototype.toString.call(t)}function c(t,e){var n,s=Math.min(t.length,e.length),i=Math.abs(t.length-e.length),r=0;for(n=0;s>n;n++)~~t[n]!==~~e[n]&&r++;return r+i}function f(t){return t?ie[t]||t.toLowerCase().replace(/(.)s$/,"$1"):t}function l(t,e){return e.abbr=t,x[t]||(x[t]=new s),x[t].set(e),x[t]}function _(t){if(!t)return H.fn._lang;if(!x[t]&&A)try{require("./lang/"+t)}catch(e){return H.fn._lang}return x[t]}function m(t){return t.match(/\[.*\]/)?t.replace(/^\[|\]$/g,""):t.replace(/\\/g,"")}function y(t){var e,n,s=t.match(E);for(e=0,n=s.length;n>e;e++)s[e]=ue[s[e]]?ue[s[e]]:m(s[e]);return function(i){var r="";for(e=0;n>e;e++)r+=s[e]instanceof Function?s[e].call(i,t):s[e];return r}}function M(t,e){function n(e){return t.lang().longDateFormat(e)||e}for(var s=5;s--&&N.test(e);)e=e.replace(N,n);return re[e]||(re[e]=y(e)),re[e](t)}function g(t,e){switch(t){case"DDDD":return V;case"YYYY":return X;case"YYYYY":return $;case"S":case"SS":case"SSS":case"DDD":return I;case"MMM":case"MMMM":case"dd":case"ddd":case"dddd":return R;case"a":case"A":return _(e._l)._meridiemParse;case"X":return B;case"Z":case"ZZ":return j;case"T":return q;case"MM":case"DD":case"YY":case"HH":case"hh":case"mm":case"ss":case"M":case"D":case"d":case"H":case"h":case"m":case"s":return J;default:return new RegExp(t.replace("\\",""))}}function p(t){var e=(j.exec(t)||[])[0],n=(e+"").match(ee)||["-",0,0],s=+(60*n[1])+~~n[2];return"+"===n[0]?-s:s}function D(t,e,n){var s,i=n._a;switch(t){case"M":case"MM":i[1]=null==e?0:~~e-1;break;case"MMM":case"MMMM":s=_(n._l).monthsParse(e),null!=s?i[1]=s:n._isValid=!1;break;case"D":case"DD":case"DDD":case"DDDD":null!=e&&(i[2]=~~e);break;case"YY":i[0]=~~e+(~~e>68?1900:2e3);break;case"YYYY":case"YYYYY":i[0]=~~e;break;case"a":case"A":n._isPm=_(n._l).isPM(e);break;case"H":case"HH":case"h":case"hh":i[3]=~~e;break;case"m":case"mm":i[4]=~~e;break;case"s":case"ss":i[5]=~~e;break;case"S":case"SS":case"SSS":i[6]=~~(1e3*("0."+e));break;case"X":n._d=new Date(1e3*parseFloat(e));break;case"Z":case"ZZ":n._useUTC=!0,n._tzm=p(e)}null==e&&(n._isValid=!1)}function Y(t){var e,n,s=[];if(!t._d){for(e=0;7>e;e++)t._a[e]=s[e]=null==t._a[e]?2===e?1:0:t._a[e];s[3]+=~~((t._tzm||0)/60),s[4]+=~~((t._tzm||0)%60),n=new Date(0),t._useUTC?(n.setUTCFullYear(s[0],s[1],s[2]),n.setUTCHours(s[3],s[4],s[5],s[6])):(n.setFullYear(s[0],s[1],s[2]),n.setHours(s[3],s[4],s[5],s[6])),t._d=n}}function w(t){var e,n,s=t._f.match(E),i=t._i;for(t._a=[],e=0;e<s.length;e++)n=(g(s[e],t).exec(i)||[])[0],n&&(i=i.slice(i.indexOf(n)+n.length)),ue[s[e]]&&D(s[e],n,t);i&&(t._il=i),t._isPm&&t._a[3]<12&&(t._a[3]+=12),t._isPm===!1&&12===t._a[3]&&(t._a[3]=0),Y(t)}function k(t){var e,n,s,r,o,u=99;for(r=0;r<t._f.length;r++)e=a({},t),e._f=t._f[r],w(e),n=new i(e),o=c(e._a,n.toArray()),n._il&&(o+=n._il.length),u>o&&(u=o,s=n);a(t,s)}function v(t){var e,n=t._i,s=K.exec(n);if(s){for(t._f="YYYY-MM-DD"+(s[2]||" "),e=0;4>e;e++)if(te[e][1].exec(n)){t._f+=te[e][0];break}j.exec(n)&&(t._f+=" Z"),w(t)}else t._d=new Date(n)}function T(e){var n=e._i,s=G.exec(n);n===t?e._d=new Date:s?e._d=new Date(+s[1]):"string"==typeof n?v(e):d(n)?(e._a=n.slice(0),Y(e)):e._d=n instanceof Date?new Date(+n):new Date(n)}function b(t,e,n,s,i){return i.relativeTime(e||1,!!n,t,s)}function S(t,e,n){var s=W(Math.abs(t)/1e3),i=W(s/60),r=W(i/60),a=W(r/24),o=W(a/365),u=45>s&&["s",s]||1===i&&["m"]||45>i&&["mm",i]||1===r&&["h"]||22>r&&["hh",r]||1===a&&["d"]||25>=a&&["dd",a]||45>=a&&["M"]||345>a&&["MM",W(a/30)]||1===o&&["y"]||["yy",o];return u[2]=e,u[3]=t>0,u[4]=n,b.apply({},u)}function F(t,e,n){var s,i=n-e,r=n-t.day();return r>i&&(r-=7),i-7>r&&(r+=7),s=H(t).add("d",r),{week:Math.ceil(s.dayOfYear()/7),year:s.year()}}function O(t){var e=t._i,n=t._f;return null===e||""===e?null:("string"==typeof e&&(t._i=e=_().preparse(e)),H.isMoment(e)?(t=a({},e),t._d=new Date(+e._d)):n?d(n)?k(t):w(t):T(t),new i(t))}function z(t,e){H.fn[t]=H.fn[t+"s"]=function(t){var n=this._isUTC?"UTC":"";return null!=t?(this._d["set"+n+e](t),H.updateOffset(this),this):this._d["get"+n+e]()}}function C(t){H.duration.fn[t]=function(){return this._data[t]}}function L(t,e){H.duration.fn["as"+t]=function(){return+this/e}}for(var H,P,U="2.1.0",W=Math.round,x={},A="undefined"!=typeof module&&module.exports,G=/^\/?Date\((\-?\d+)/i,Z=/(\-)?(\d*)?\.?(\d+)\:(\d+)\:(\d+)\.?(\d{3})?/,E=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|SS?S?|X|zz?|ZZ?|.)/g,N=/(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g,J=/\d\d?/,I=/\d{1,3}/,V=/\d{3}/,X=/\d{1,4}/,$=/[+\-]?\d{1,6}/,R=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,j=/Z|[\+\-]\d\d:?\d\d/i,q=/T/i,B=/[\+\-]?\d+(\.\d{1,3})?/,K=/^\s*\d{4}-\d\d-\d\d((T| )(\d\d(:\d\d(:\d\d(\.\d\d?\d?)?)?)?)?([\+\-]\d\d:?\d\d)?)?/,Q="YYYY-MM-DDTHH:mm:ssZ",te=[["HH:mm:ss.S",/(T| )\d\d:\d\d:\d\d\.\d{1,3}/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],ee=/([\+\-]|\d\d)/gi,ne="Date|Hours|Minutes|Seconds|Milliseconds".split("|"),se={Milliseconds:1,Seconds:1e3,Minutes:6e4,Hours:36e5,Days:864e5,Months:2592e6,Years:31536e6},ie={ms:"millisecond",s:"second",m:"minute",h:"hour",d:"day",w:"week",M:"month",y:"year"},re={},ae="DDD w W M D d".split(" "),oe="M D H h m s w W".split(" "),ue={M:function(){return this.month()+1},MMM:function(t){return this.lang().monthsShort(this,t)},MMMM:function(t){return this.lang().months(this,t)},D:function(){return this.date()},DDD:function(){return this.dayOfYear()},d:function(){return this.day()},dd:function(t){return this.lang().weekdaysMin(this,t)},ddd:function(t){return this.lang().weekdaysShort(this,t)},dddd:function(t){return this.lang().weekdays(this,t)},w:function(){return this.week()},W:function(){return this.isoWeek()},YY:function(){return u(this.year()%100,2)},YYYY:function(){return u(this.year(),4)},YYYYY:function(){return u(this.year(),5)},gg:function(){return u(this.weekYear()%100,2)},gggg:function(){return this.weekYear()},ggggg:function(){return u(this.weekYear(),5)},GG:function(){return u(this.isoWeekYear()%100,2)},GGGG:function(){return this.isoWeekYear()},GGGGG:function(){return u(this.isoWeekYear(),5)},e:function(){return this.weekday()},E:function(){return this.isoWeekday()},a:function(){return this.lang().meridiem(this.hours(),this.minutes(),!0)},A:function(){return this.lang().meridiem(this.hours(),this.minutes(),!1)},H:function(){return this.hours()},h:function(){return this.hours()%12||12},m:function(){return this.minutes()},s:function(){return this.seconds()},S:function(){return~~(this.milliseconds()/100)},SS:function(){return u(~~(this.milliseconds()/10),2)},SSS:function(){return u(this.milliseconds(),3)},Z:function(){var t=-this.zone(),e="+";return 0>t&&(t=-t,e="-"),e+u(~~(t/60),2)+":"+u(~~t%60,2)},ZZ:function(){var t=-this.zone(),e="+";return 0>t&&(t=-t,e="-"),e+u(~~(10*t/6),4)},z:function(){return this.zoneAbbr()},zz:function(){return this.zoneName()},X:function(){return this.unix()}};ae.length;)P=ae.pop(),ue[P+"o"]=n(ue[P],P);for(;oe.length;)P=oe.pop(),ue[P+P]=e(ue[P],2);for(ue.DDDD=e(ue.DDD,3),s.prototype={set:function(t){var e,n;for(n in t)e=t[n],"function"==typeof e?this[n]=e:this["_"+n]=e},_months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),months:function(t){return this._months[t.month()]},_monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),monthsShort:function(t){return this._monthsShort[t.month()]},monthsParse:function(t){var e,n,s;for(this._monthsParse||(this._monthsParse=[]),e=0;12>e;e++)if(this._monthsParse[e]||(n=H([2e3,e]),s="^"+this.months(n,"")+"|^"+this.monthsShort(n,""),this._monthsParse[e]=new RegExp(s.replace(".",""),"i")),this._monthsParse[e].test(t))return e},_weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdays:function(t){return this._weekdays[t.day()]},_weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysShort:function(t){return this._weekdaysShort[t.day()]},_weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),weekdaysMin:function(t){return this._weekdaysMin[t.day()]},weekdaysParse:function(t){var e,n,s;for(this._weekdaysParse||(this._weekdaysParse=[]),e=0;7>e;e++)if(this._weekdaysParse[e]||(n=H([2e3,1]).day(e),s="^"+this.weekdays(n,"")+"|^"+this.weekdaysShort(n,"")+"|^"+this.weekdaysMin(n,""),this._weekdaysParse[e]=new RegExp(s.replace(".",""),"i")),this._weekdaysParse[e].test(t))return e},_longDateFormat:{LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D YYYY",LLL:"MMMM D YYYY LT",LLLL:"dddd, MMMM D YYYY LT"},longDateFormat:function(t){var e=this._longDateFormat[t];return!e&&this._longDateFormat[t.toUpperCase()]&&(e=this._longDateFormat[t.toUpperCase()].replace(/MMMM|MM|DD|dddd/g,function(t){return t.slice(1)}),this._longDateFormat[t]=e),e},isPM:function(t){return"p"===(t+"").toLowerCase()[0]},_meridiemParse:/[ap]\.?m?\.?/i,meridiem:function(t,e,n){return t>11?n?"pm":"PM":n?"am":"AM"},_calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},calendar:function(t,e){var n=this._calendar[t];return"function"==typeof n?n.apply(e):n},_relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},relativeTime:function(t,e,n,s){var i=this._relativeTime[n];return"function"==typeof i?i(t,e,n,s):i.replace(/%d/i,t)},pastFuture:function(t,e){var n=this._relativeTime[t>0?"future":"past"];return"function"==typeof n?n(e):n.replace(/%s/i,e)},ordinal:function(t){return this._ordinal.replace("%d",t)},_ordinal:"%d",preparse:function(t){return t},postformat:function(t){return t},week:function(t){return F(t,this._week.dow,this._week.doy).week},_week:{dow:0,doy:6}},H=function(t,e,n){return O({_i:t,_f:e,_l:n,_isUTC:!1})},H.utc=function(t,e,n){return O({_useUTC:!0,_isUTC:!0,_l:n,_i:t,_f:e})},H.unix=function(t){return H(1e3*t)},H.duration=function(t,e){var n,s,i=H.isDuration(t),a="number"==typeof t,o=i?t._input:a?{}:t,u=Z.exec(t);return a?e?o[e]=t:o.milliseconds=t:u&&(n="-"===u[1]?-1:1,o={y:0,d:~~u[2]*n,h:~~u[3]*n,m:~~u[4]*n,s:~~u[5]*n,ms:~~u[6]*n}),s=new r(o),i&&t.hasOwnProperty("_lang")&&(s._lang=t._lang),s},H.version=U,H.defaultFormat=Q,H.updateOffset=function(){},H.lang=function(t,e){return t?(e?l(t,e):x[t]||_(t),H.duration.fn._lang=H.fn._lang=_(t),void 0):H.fn._lang._abbr},H.langData=function(t){return t&&t._lang&&t._lang._abbr&&(t=t._lang._abbr),_(t)},H.isMoment=function(t){return t instanceof i},H.isDuration=function(t){return t instanceof r},H.fn=i.prototype={clone:function(){return H(this)},valueOf:function(){return+this._d+6e4*(this._offset||0)},unix:function(){return Math.floor(+this/1e3)},toString:function(){return this.format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")},toDate:function(){return this._offset?new Date(+this):this._d},toISOString:function(){return M(H(this).utc(),"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]")},toArray:function(){var t=this;return[t.year(),t.month(),t.date(),t.hours(),t.minutes(),t.seconds(),t.milliseconds()]},isValid:function(){return null==this._isValid&&(this._isValid=this._a?!c(this._a,(this._isUTC?H.utc(this._a):H(this._a)).toArray()):!isNaN(this._d.getTime())),!!this._isValid},utc:function(){return this.zone(0)},local:function(){return this.zone(0),this._isUTC=!1,this},format:function(t){var e=M(this,t||H.defaultFormat);return this.lang().postformat(e)},add:function(t,e){var n;return n="string"==typeof t?H.duration(+e,t):H.duration(t,e),h(this,n,1),this},subtract:function(t,e){var n;return n="string"==typeof t?H.duration(+e,t):H.duration(t,e),h(this,n,-1),this},diff:function(t,e,n){var s,i,r=this._isUTC?H(t).zone(this._offset||0):H(t).local(),a=6e4*(this.zone()-r.zone());return e=f(e),"year"===e||"month"===e?(s=432e5*(this.daysInMonth()+r.daysInMonth()),i=12*(this.year()-r.year())+(this.month()-r.month()),i+=(this-H(this).startOf("month")-(r-H(r).startOf("month")))/s,i-=6e4*(this.zone()-H(this).startOf("month").zone()-(r.zone()-H(r).startOf("month").zone()))/s,"year"===e&&(i/=12)):(s=this-r,i="second"===e?s/1e3:"minute"===e?s/6e4:"hour"===e?s/36e5:"day"===e?(s-a)/864e5:"week"===e?(s-a)/6048e5:s),n?i:o(i)},from:function(t,e){return H.duration(this.diff(t)).lang(this.lang()._abbr).humanize(!e)},fromNow:function(t){return this.from(H(),t)},calendar:function(){var t=this.diff(H().startOf("day"),"days",!0),e=-6>t?"sameElse":-1>t?"lastWeek":0>t?"lastDay":1>t?"sameDay":2>t?"nextDay":7>t?"nextWeek":"sameElse";return this.format(this.lang().calendar(e,this))},isLeapYear:function(){var t=this.year();return 0===t%4&&0!==t%100||0===t%400},isDST:function(){return this.zone()<this.clone().month(0).zone()||this.zone()<this.clone().month(5).zone()},day:function(t){var e=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=t?"string"==typeof t&&(t=this.lang().weekdaysParse(t),"number"!=typeof t)?this:this.add({d:t-e}):e},month:function(t){var e,n=this._isUTC?"UTC":"";return null!=t?"string"==typeof t&&(t=this.lang().monthsParse(t),"number"!=typeof t)?this:(e=this.date(),this.date(1),this._d["set"+n+"Month"](t),this.date(Math.min(e,this.daysInMonth())),H.updateOffset(this),this):this._d["get"+n+"Month"]()},startOf:function(t){switch(t=f(t)){case"year":this.month(0);case"month":this.date(1);case"week":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===t&&this.weekday(0),this},endOf:function(t){return this.startOf(t).add(t,1).subtract("ms",1)},isAfter:function(t,e){return e="undefined"!=typeof e?e:"millisecond",+this.clone().startOf(e)>+H(t).startOf(e)},isBefore:function(t,e){return e="undefined"!=typeof e?e:"millisecond",+this.clone().startOf(e)<+H(t).startOf(e)},isSame:function(t,e){return e="undefined"!=typeof e?e:"millisecond",+this.clone().startOf(e)===+H(t).startOf(e)},min:function(t){return t=H.apply(null,arguments),this>t?this:t},max:function(t){return t=H.apply(null,arguments),t>this?this:t},zone:function(t){var e=this._offset||0;return null==t?this._isUTC?e:this._d.getTimezoneOffset():("string"==typeof t&&(t=p(t)),Math.abs(t)<16&&(t=60*t),this._offset=t,this._isUTC=!0,e!==t&&h(this,H.duration(e-t,"m"),1,!0),this)},zoneAbbr:function(){return this._isUTC?"UTC":""},zoneName:function(){return this._isUTC?"Coordinated Universal Time":""},daysInMonth:function(){return H.utc([this.year(),this.month()+1,0]).date()},dayOfYear:function(t){var e=W((H(this).startOf("day")-H(this).startOf("year"))/864e5)+1;return null==t?e:this.add("d",t-e)},weekYear:function(t){var e=F(this,this.lang()._week.dow,this.lang()._week.doy).year;return null==t?e:this.add("y",t-e)},isoWeekYear:function(t){var e=F(this,1,4).year;return null==t?e:this.add("y",t-e)},week:function(t){var e=this.lang().week(this);return null==t?e:this.add("d",7*(t-e))},isoWeek:function(t){var e=F(this,1,4).week;return null==t?e:this.add("d",7*(t-e))},weekday:function(t){var e=(this._d.getDay()+7-this.lang()._week.dow)%7;return null==t?e:this.add("d",t-e)},isoWeekday:function(t){return null==t?this.day()||7:this.day(this.day()%7?t:t-7)},lang:function(e){return e===t?this._lang:(this._lang=_(e),this)}},P=0;P<ne.length;P++)z(ne[P].toLowerCase().replace(/s$/,""),ne[P]);z("year","FullYear"),H.fn.days=H.fn.day,H.fn.months=H.fn.month,H.fn.weeks=H.fn.week,H.fn.isoWeeks=H.fn.isoWeek,H.fn.toJSON=H.fn.toISOString,H.duration.fn=r.prototype={_bubble:function(){var t,e,n,s,i=this._milliseconds,r=this._days,a=this._months,u=this._data;u.milliseconds=i%1e3,t=o(i/1e3),u.seconds=t%60,e=o(t/60),u.minutes=e%60,n=o(e/60),u.hours=n%24,r+=o(n/24),u.days=r%30,a+=o(r/30),u.months=a%12,s=o(a/12),u.years=s},weeks:function(){return o(this.days()/7)},valueOf:function(){return this._milliseconds+864e5*this._days+2592e6*(this._months%12)+31536e6*~~(this._months/12)},humanize:function(t){var e=+this,n=S(e,!t,this.lang());return t&&(n=this.lang().pastFuture(e,n)),this.lang().postformat(n)},add:function(t,e){var n=H.duration(t,e);return this._milliseconds+=n._milliseconds,this._days+=n._days,this._months+=n._months,this._bubble(),this},subtract:function(t,e){var n=H.duration(t,e);return this._milliseconds-=n._milliseconds,this._days-=n._days,this._months-=n._months,this._bubble(),this},get:function(t){return t=f(t),this[t.toLowerCase()+"s"]()},as:function(t){return t=f(t),this["as"+t.charAt(0).toUpperCase()+t.slice(1)+"s"]()},lang:H.fn.lang};for(P in se)se.hasOwnProperty(P)&&(L(P,se[P]),C(P.toLowerCase()));L("Weeks",6048e5),H.duration.fn.asMonths=function(){return(+this-31536e6*this.years())/2592e6+12*this.years()},H.lang("en",{ordinal:function(t){var e=t%10,n=1===~~(t%100/10)?"th":1===e?"st":2===e?"nd":3===e?"rd":"th";return t+n}}),A&&(module.exports=H),"undefined"==typeof ender&&(this.moment=H),"function"==typeof define&&define.amd&&define("moment",[],function(){return H})}.call(this);

/**
* @version: 1.2
* @author: Dan Grossman http://www.dangrossman.info/
* @date: 2013-07-25
* @copyright: Copyright (c) 2012-2013 Dan Grossman. All rights reserved.
* @license: Licensed under Apache License v2.0. See http://www.apache.org/licenses/LICENSE-2.0
* @website: http://www.improvely.com/
*/
!function ($) {

    var DateRangePicker = function (element, options, cb) {
        var hasOptions = typeof options == 'object';
        var localeObject;

        //option defaults

        this.startDate = moment().startOf('day');
        this.endDate = moment().startOf('day');
        this.minDate = false;
        this.maxDate = false;
        this.dateLimit = false;

        this.showDropdowns = false;
        this.showWeekNumbers = false;
        this.timePicker = false;
        this.timePickerIncrement = 30;
        this.timePicker12Hour = true;
        this.ranges = {};
        this.opens = 'right';

        this.buttonClasses = ['btn', 'btn-small'];
        this.applyClass = 'btn-success';
        this.cancelClass = 'btn-default';

        this.format = 'MM/DD/YYYY';
        this.separator = ' - ';

        this.locale = {
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            weekLabel: 'W',
            customRangeLabel: 'Custom Range',
            daysOfWeek: moment()._lang._weekdaysMin.slice(),
            monthNames: moment()._lang._monthsShort.slice(),
            firstDay: 0
        };

        this.cb = function () { };

        // by default, the daterangepicker element is placed at the bottom of HTML body
        this.parentEl = 'body';

        //element that triggered the date range picker
        this.element = $(element);

        if (this.element.hasClass('pull-right'))
            this.opens = 'left';

        if (this.element.is('input')) {
            this.element.on({
                click: $.proxy(this.show, this),
                focus: $.proxy(this.show, this)
            });
        } else {
            this.element.on('click', $.proxy(this.show, this));
        }

        localeObject = this.locale;

        if (hasOptions) {
            if (typeof options.locale == 'object') {
                $.each(localeObject, function (property, value) {
                    localeObject[property] = options.locale[property] || value;
                });
            }

            if (options.applyClass) {
                this.applyClass = options.applyClass;
            }

            if (options.cancelClass) {
                this.cancelClass = options.cancelClass;
            }
        }

        var DRPTemplate = '<div class="daterangepicker dropdown-menu">' +
                '<div class="calendar left"></div>' +
                '<div class="calendar right"></div>' +
                '<div class="ranges">' +
                  '<div class="range_inputs">' +
                    '<div class="daterangepicker_start_input" style="float: left">' +
                      '<label for="daterangepicker_start">' + this.locale.fromLabel + '</label>' +
                      '<input class="input-mini" type="text" name="daterangepicker_start" value="" disabled="disabled" />' +
                    '</div>' +
                    '<div class="daterangepicker_end_input" style="float: left; padding-left: 11px">' +
                      '<label for="daterangepicker_end">' + this.locale.toLabel + '</label>' +
                      '<input class="input-mini" type="text" name="daterangepicker_end" value="" disabled="disabled" />' +
                    '</div>' +
                    '<button class="' + this.applyClass + ' applyBtn" disabled="disabled">' + this.locale.applyLabel + '</button>&nbsp;' +
                    '<button class="' + this.cancelClass + ' cancelBtn">' + this.locale.cancelLabel + '</button>' +
                  '</div>' +
                '</div>' +
              '</div>';

        this.parentEl = (hasOptions && options.parentEl && $(options.parentEl)) || $(this.parentEl);
        //the date range picker
        this.container = $(DRPTemplate).appendTo(this.parentEl);

        if (hasOptions) {

            if (typeof options.format == 'string')
                this.format = options.format;

            if (typeof options.separator == 'string')
                this.separator = options.separator;

            if (typeof options.startDate == 'string')
                this.startDate = moment(options.startDate, this.format);

            if (typeof options.endDate == 'string')
                this.endDate = moment(options.endDate, this.format);

            if (typeof options.minDate == 'string')
                this.minDate = moment(options.minDate, this.format);

            if (typeof options.maxDate == 'string')
                this.maxDate = moment(options.maxDate, this.format);

            if (typeof options.startDate == 'object')
                this.startDate = moment(options.startDate);

            if (typeof options.endDate == 'object')
                this.endDate = moment(options.endDate);

            if (typeof options.minDate == 'object')
                this.minDate = moment(options.minDate);

            if (typeof options.maxDate == 'object')
                this.maxDate = moment(options.maxDate);

            if (typeof options.ranges == 'object') {
                for (var range in options.ranges) {

                    var start = moment(options.ranges[range][0]);
                    var end = moment(options.ranges[range][1]);

                    // If we have a min/max date set, bound this range
                    // to it, but only if it would otherwise fall
                    // outside of the min/max.
                    if (this.minDate && start.isBefore(this.minDate))
                        start = moment(this.minDate);

                    if (this.maxDate && end.isAfter(this.maxDate))
                        end = moment(this.maxDate);

                    // If the end of the range is before the minimum (if min is set) OR
                    // the start of the range is after the max (also if set) don't display this
                    // range option.
                    if ((this.minDate && end.isBefore(this.minDate)) || (this.maxDate && start.isAfter(this.maxDate))) {
                        continue;
                    }

                    this.ranges[range] = [start, end];
                }

                var list = '<ul>';
                for (var range in this.ranges) {
                    list += '<li>' + range + '</li>';
                }
                list += '<li>' + this.locale.customRangeLabel + '</li>';
                list += '</ul>';
                this.container.find('.ranges').prepend(list);
            }

            if (typeof options.dateLimit == 'object')
                this.dateLimit = options.dateLimit;

            // update day names order to firstDay
            if (typeof options.locale == 'object') {
                if (typeof options.locale.firstDay == 'number') {
                    this.locale.firstDay = options.locale.firstDay;
                    var iterator = options.locale.firstDay;
                    while (iterator > 0) {
                        this.locale.daysOfWeek.push(this.locale.daysOfWeek.shift());
                        iterator--;
                    }
                }
            }

            if (typeof options.opens == 'string')
                this.opens = options.opens;

            if (typeof options.showWeekNumbers == 'boolean') {
                this.showWeekNumbers = options.showWeekNumbers;
            }

            if (typeof options.buttonClasses == 'string') {
                this.buttonClasses = [options.buttonClasses];
            }

            if (typeof options.buttonClasses == 'object') {
                this.buttonClasses = options.buttonClasses;
            }

            if (typeof options.showDropdowns == 'boolean') {
                this.showDropdowns = options.showDropdowns;
            }

            if (typeof options.timePicker == 'boolean') {
                this.timePicker = options.timePicker;
            }

            if (typeof options.timePickerIncrement == 'number') {
                this.timePickerIncrement = options.timePickerIncrement;
            }

            if (typeof options.timePicker12Hour == 'boolean') {
                this.timePicker12Hour = options.timePicker12Hour;
            }

        }

        if (!this.timePicker) {
            this.startDate = this.startDate.startOf('day');
            this.endDate = this.endDate.startOf('day');
        }

        //apply CSS classes to buttons
        var c = this.container;
        $.each(this.buttonClasses, function (idx, val) {
            c.find('button').addClass(val);
        });

        if (this.opens == 'right') {
            //swap calendar positions
            var left = this.container.find('.calendar.left');
            var right = this.container.find('.calendar.right');
            left.removeClass('left').addClass('right');
            right.removeClass('right').addClass('left');
        }

        if (typeof options == 'undefined' || typeof options.ranges == 'undefined') {
            this.container.find('.calendar').show();
            this.move();
        }

        if (typeof cb == 'function')
            this.cb = cb;

        this.container.addClass('opens' + this.opens);

        //try parse date if in text input
        if (!hasOptions || (typeof options.startDate == 'undefined' && typeof options.endDate == 'undefined')) {
            if ($(this.element).is('input[type=text]')) {
                var val = $(this.element).val();
                var split = val.split(this.separator);
                var start, end;
                if (split.length == 2) {
                    start = moment(split[0], this.format);
                    end = moment(split[1], this.format);
                }
                if (start != null && end != null) {
                    this.startDate = start;
                    this.endDate = end;
                }
            }
        }

        //state
        this.oldStartDate = this.startDate.clone();
        this.oldEndDate = this.endDate.clone();

        this.leftCalendar = {
            month: moment([this.startDate.year(), this.startDate.month(), 1, this.startDate.hour(), this.startDate.minute()]),
            calendar: []
        };

        this.rightCalendar = {
            month: moment([this.endDate.year(), this.endDate.month(), 1, this.endDate.hour(), this.endDate.minute()]),
            calendar: []
        };

        //event listeners
        this.container.on('mousedown', $.proxy(this.mousedown, this));
        this.container.find('.calendar').on('click', '.prev', $.proxy(this.clickPrev, this));
        this.container.find('.calendar').on('click', '.next', $.proxy(this.clickNext, this));
        this.container.find('.ranges').on('click', 'button.applyBtn', $.proxy(this.clickApply, this));
        this.container.find('.ranges').on('click', 'button.cancelBtn', $.proxy(this.clickCancel, this));

        this.container.find('.ranges').on('click', '.daterangepicker_start_input', $.proxy(this.showCalendars, this));
        this.container.find('.ranges').on('click', '.daterangepicker_end_input', $.proxy(this.showCalendars, this));

        this.container.find('.calendar').on('click', 'td.available', $.proxy(this.clickDate, this));
        this.container.find('.calendar').on('mouseenter', 'td.available', $.proxy(this.enterDate, this));
        this.container.find('.calendar').on('mouseleave', 'td.available', $.proxy(this.updateView, this));

        this.container.find('.ranges').on('click', 'li', $.proxy(this.clickRange, this));
        this.container.find('.ranges').on('mouseenter', 'li', $.proxy(this.enterRange, this));
        this.container.find('.ranges').on('mouseleave', 'li', $.proxy(this.updateView, this));

        this.container.find('.calendar').on('change', 'select.yearselect', $.proxy(this.updateMonthYear, this));
        this.container.find('.calendar').on('change', 'select.monthselect', $.proxy(this.updateMonthYear, this));

        this.container.find('.calendar').on('change', 'select.hourselect', $.proxy(this.updateTime, this));
        this.container.find('.calendar').on('change', 'select.minuteselect', $.proxy(this.updateTime, this));
        this.container.find('.calendar').on('change', 'select.ampmselect', $.proxy(this.updateTime, this));

        this.element.on('keyup', $.proxy(this.updateFromControl, this));

        this.updateView();
        this.updateCalendars();

    };

    DateRangePicker.prototype = {

        constructor: DateRangePicker,

        mousedown: function (e) {
            e.stopPropagation();
        },

        updateView: function () {
            this.leftCalendar.month.month(this.startDate.month()).year(this.startDate.year());
            this.rightCalendar.month.month(this.endDate.month()).year(this.endDate.year());

            this.container.find('input[name=daterangepicker_start]').val(this.startDate.format(this.format));
            this.container.find('input[name=daterangepicker_end]').val(this.endDate.format(this.format));

            if (this.startDate.isSame(this.endDate) || this.startDate.isBefore(this.endDate)) {
                this.container.find('button.applyBtn').removeAttr('disabled');
            } else {
                this.container.find('button.applyBtn').attr('disabled', 'disabled');
            }
        },

        updateFromControl: function () {
            if (!this.element.is('input')) return;
            if (!this.element.val().length) return;

            var dateString = this.element.val().split(this.separator);
            var start = moment(dateString[0], this.format);
            var end = moment(dateString[1], this.format);

            if (start == null || end == null) return;
            if (end.isBefore(start)) return;

            this.startDate = start;
            this.endDate = end;

            this.notify();
            this.updateCalendars();
        },

        notify: function () {
            this.updateView();
            this.cb(this.startDate, this.endDate);
        },

        move: function () {
            var parentOffset = {
                top: this.parentEl.offset().top - (this.parentEl.is('body') ? 0 : this.parentEl.scrollTop()),
                left: this.parentEl.offset().left - (this.parentEl.is('body') ? 0 : this.parentEl.scrollLeft())
            };
            if (this.opens == 'left') {
                this.container.css({
                    top: this.element.offset().top + this.element.outerHeight() - parentOffset.top,
                    right: $(window).width() - this.element.offset().left - this.element.outerWidth() - parentOffset.left,
                    left: 'auto'
                });
                if (this.container.offset().left < 0) {
                    this.container.css({
                        right: 'auto',
                        left: 9
                    });
                }
            } else {
                this.container.css({
                    top: this.element.offset().top + this.element.outerHeight() - parentOffset.top,
                    left: this.element.offset().left - parentOffset.left,
                    right: 'auto'
                });
                if (this.container.offset().left + this.container.outerWidth() > $(window).width()) {
                    this.container.css({
                        left: 'auto',
                        right: 0
                    });
                }
            }
        },

        show: function (e) {
            this.container.show();
            this.move();

            if (e) {
                e.stopPropagation();
                e.preventDefault();
            }

            $(document).on('mousedown', $.proxy(this.hide, this));
            this.element.trigger('shown', {target: e.target, picker: this});
        },

        hide: function (e) {
            this.container.hide();

            if (!this.startDate.isSame(this.oldStartDate) || !this.endDate.isSame(this.oldEndDate))
                this.notify();

            this.oldStartDate = this.startDate.clone();
            this.oldEndDate = this.endDate.clone();

            $(document).off('mousedown', this.hide);
            this.element.trigger('hidden', { picker: this });
        },

        enterRange: function (e) {
            var label = e.target.innerHTML;
            if (label == this.locale.customRangeLabel) {
                this.updateView();
            } else {
                var dates = this.ranges[label];
                this.container.find('input[name=daterangepicker_start]').val(dates[0].format(this.format));
                this.container.find('input[name=daterangepicker_end]').val(dates[1].format(this.format));
            }
        },

        showCalendars: function() {
            this.container.find('.calendar').show();
            this.move();
        },

        updateInputText: function() {
            if (this.element.is('input'))
                this.element.val(this.startDate.format(this.format) + this.separator + this.endDate.format(this.format));
        },

        clickRange: function (e) {
            var label = e.target.innerHTML;
            if (label == this.locale.customRangeLabel) {
                this.showCalendars();
            } else {
                var dates = this.ranges[label];

                this.startDate = dates[0];
                this.endDate = dates[1];

                if (!this.timePicker) {
                    this.startDate.startOf('day');
                    this.endDate.startOf('day');
                }

                this.leftCalendar.month.month(this.startDate.month()).year(this.startDate.year()).hour(this.startDate.hour()).minute(this.startDate.minute());
                this.rightCalendar.month.month(this.endDate.month()).year(this.endDate.year()).hour(this.endDate.hour()).minute(this.endDate.minute());
                this.updateCalendars();

                this.updateInputText();

                this.container.find('.calendar').hide();
                this.hide();
            }
        },

        clickPrev: function (e) {
            var cal = $(e.target).parents('.calendar');
            if (cal.hasClass('left')) {
                this.leftCalendar.month.subtract('month', 1);
            } else {
                this.rightCalendar.month.subtract('month', 1);
            }
            this.updateCalendars();
        },

        clickNext: function (e) {
            var cal = $(e.target).parents('.calendar');
            if (cal.hasClass('left')) {
                this.leftCalendar.month.add('month', 1);
            } else {
                this.rightCalendar.month.add('month', 1);
            }
            this.updateCalendars();
        },

        enterDate: function (e) {

            var title = $(e.target).attr('data-title');
            var row = title.substr(1, 1);
            var col = title.substr(3, 1);
            var cal = $(e.target).parents('.calendar');

            if (cal.hasClass('left')) {
                this.container.find('input[name=daterangepicker_start]').val(this.leftCalendar.calendar[row][col].format(this.format));
            } else {
                this.container.find('input[name=daterangepicker_end]').val(this.rightCalendar.calendar[row][col].format(this.format));
            }

        },

        clickDate: function (e) {
            var title = $(e.target).attr('data-title');
            var row = title.substr(1, 1);
            var col = title.substr(3, 1);
            var cal = $(e.target).parents('.calendar');

            if (cal.hasClass('left')) {
                var startDate = this.leftCalendar.calendar[row][col];
                var endDate = this.endDate;
                if (typeof this.dateLimit == 'object') {
                    var maxDate = moment(startDate).add(this.dateLimit).startOf('day');
                    if (endDate.isAfter(maxDate)) {
                        endDate = maxDate;
                    }
                }
            } else {
                var startDate = this.startDate;
                var endDate = this.rightCalendar.calendar[row][col];
                if (typeof this.dateLimit == 'object') {
                    var minDate = moment(endDate).subtract(this.dateLimit).startOf('day');
                    if (startDate.isBefore(minDate)) {
                        startDate = minDate;
                    }
                }
            }

            cal.find('td').removeClass('active');

            if (startDate.isSame(endDate) || startDate.isBefore(endDate)) {
                $(e.target).addClass('active');
                this.startDate = startDate;
                this.endDate = endDate;
            } else if (startDate.isAfter(endDate)) {
                $(e.target).addClass('active');
                this.startDate = startDate;
                this.endDate = moment(startDate).add('day', 1).startOf('day');
            }

            this.leftCalendar.month.month(this.startDate.month()).year(this.startDate.year());
            this.rightCalendar.month.month(this.endDate.month()).year(this.endDate.year());
            this.updateCalendars();
        },

        clickApply: function (e) {
            this.updateInputText();
            this.hide();
        },

        clickCancel: function (e) {
            this.startDate = this.oldStartDate;
            this.endDate = this.oldEndDate;
            this.updateView();
            this.updateCalendars();
            this.hide();
        },

        updateMonthYear: function (e) {

            var isLeft = $(e.target).closest('.calendar').hasClass('left');
            var cal = this.container.find('.calendar.left');
            if (!isLeft)
                cal = this.container.find('.calendar.right');

            var month = parseInt(cal.find('.monthselect').val());
            var year = cal.find('.yearselect').val();

            if (isLeft) {
                this.leftCalendar.month.month(month).year(year);
            } else {
                this.rightCalendar.month.month(month).year(year);
            }

            this.updateCalendars();

        },

        updateTime: function(e) {

            var isLeft = $(e.target).closest('.calendar').hasClass('left');
            var cal = this.container.find('.calendar.left');
            if (!isLeft)
                cal = this.container.find('.calendar.right');

            var hour = parseInt(cal.find('.hourselect').val());
            var minute = parseInt(cal.find('.minuteselect').val());

            if (this.timePicker12Hour) {
                var ampm = cal.find('.ampmselect').val();
                if (ampm == 'PM' && hour < 12)
                    hour += 12;
                if (ampm == 'AM' && hour == 12)
                    hour = 0;
            }

            if (isLeft) {
                var start = this.startDate;
                start.hour(hour);
                start.minute(minute);
                this.startDate = start;
                this.leftCalendar.month.hour(hour).minute(minute);
            } else {
                var end = this.endDate;
                end.hour(hour);
                end.minute(minute);
                this.endDate = end;
                this.rightCalendar.month.hour(hour).minute(minute);
            }

            this.updateCalendars();

        },

        updateCalendars: function () {
            this.leftCalendar.calendar = this.buildCalendar(this.leftCalendar.month.month(), this.leftCalendar.month.year(), this.leftCalendar.month.hour(), this.leftCalendar.month.minute(), 'left');
            this.rightCalendar.calendar = this.buildCalendar(this.rightCalendar.month.month(), this.rightCalendar.month.year(), this.rightCalendar.month.hour(), this.rightCalendar.month.minute(), 'right');
            this.container.find('.calendar.left').html(this.renderCalendar(this.leftCalendar.calendar, this.startDate, this.minDate, this.maxDate));
            this.container.find('.calendar.right').html(this.renderCalendar(this.rightCalendar.calendar, this.endDate, this.startDate, this.maxDate));

            this.container.find('.ranges li').removeClass('active');
            var customRange = true;
            var i = 0;
            for (var range in this.ranges) {
                if (this.timePicker) {
                    if (this.startDate.isSame(this.ranges[range][0]) && this.endDate.isSame(this.ranges[range][1])) {
                        customRange = false;
                        this.container.find('.ranges li:eq(' + i + ')').addClass('active');
                    }
                } else {
                    //ignore times when comparing dates if time picker is not enabled
                    if (this.startDate.format('YYYY-MM-DD') == this.ranges[range][0].format('YYYY-MM-DD') && this.endDate.format('YYYY-MM-DD') == this.ranges[range][1].format('YYYY-MM-DD')) {
                        customRange = false;
                        this.container.find('.ranges li:eq(' + i + ')').addClass('active');
                    }
                }
                i++;
            }
            if (customRange)
                this.container.find('.ranges li:last').addClass('active');
        },

        buildCalendar: function (month, year, hour, minute, side) {

            var firstDay = moment([year, month, 1]);
            var lastMonth = moment(firstDay).subtract('month', 1).month();
            var lastYear = moment(firstDay).subtract('month', 1).year();

            var daysInLastMonth = moment([lastYear, lastMonth]).daysInMonth();

            var dayOfWeek = firstDay.day();

            //initialize a 6 rows x 7 columns array for the calendar
            var calendar = [];
            for (var i = 0; i < 6; i++) {
                calendar[i] = [];
            }

            //populate the calendar with date objects
            var startDay = daysInLastMonth - dayOfWeek + this.locale.firstDay + 1;
            if (startDay > daysInLastMonth)
                startDay -= 7;

            if (dayOfWeek == this.locale.firstDay)
                startDay = daysInLastMonth - 6;

            var curDate = moment([lastYear, lastMonth, startDay, hour, minute]);
            for (var i = 0, col = 0, row = 0; i < 42; i++, col++, curDate = moment(curDate).add('day', 1)) {
                if (i > 0 && col % 7 == 0) {
                    col = 0;
                    row++;
                }
                calendar[row][col] = curDate;
            }

            return calendar;

        },

        renderDropdowns: function (selected, minDate, maxDate) {
            var currentMonth = selected.month();
            var monthHtml = '<select class="monthselect">';
            var inMinYear = false;
            var inMaxYear = false;

            for (var m = 0; m < 12; m++) {
                if ((!inMinYear || m >= minDate.month()) && (!inMaxYear || m <= maxDate.month())) {
                    monthHtml += "<option value='" + m + "'" +
                        (m === currentMonth ? " selected='selected'" : "") +
                        ">" + this.locale.monthNames[m] + "</option>";
                }
            }
            monthHtml += "</select>";

            var currentYear = selected.year();
            var maxYear = (maxDate && maxDate.year()) || (currentYear + 5);
            var minYear = (minDate && minDate.year()) || (currentYear - 50);
            var yearHtml = '<select class="yearselect">'

            for (var y = minYear; y <= maxYear; y++) {
                yearHtml += '<option value="' + y + '"' +
                    (y === currentYear ? ' selected="selected"' : '') +
                    '>' + y + '</option>';
            }

            yearHtml += '</select>';

            return monthHtml + yearHtml;
        },

        renderCalendar: function (calendar, selected, minDate, maxDate) {

            var html = '<div class="calendar-date">';
            html += '<table class="table-condensed">';
            html += '<thead>';
            html += '<tr>';

            // add empty cell for week number
            if (this.showWeekNumbers)
                html += '<th></th>';

            if (!minDate || minDate.isBefore(calendar[1][1])) {
                html += '<th class="prev available"><i class="icon-arrow-left glyphicon glyphicon-arrow-left"></i></th>';
            } else {
                html += '<th></th>';
            }

            var dateHtml = this.locale.monthNames[calendar[1][1].month()] + calendar[1][1].format(" YYYY");

            if (this.showDropdowns) {
                dateHtml = this.renderDropdowns(calendar[1][1], minDate, maxDate);
            }

            html += '<th colspan="5" style="width: auto">' + dateHtml + '</th>';
            if (!maxDate || maxDate.isAfter(calendar[1][1])) {
                html += '<th class="next available"><i class="icon-arrow-right glyphicon glyphicon-arrow-right"></i></th>';
            } else {
                html += '<th></th>';
            }

            html += '</tr>';
            html += '<tr>';

            // add week number label
            if (this.showWeekNumbers)
                html += '<th class="week">' + this.locale.weekLabel + '</th>';

            $.each(this.locale.daysOfWeek, function (index, dayOfWeek) {
                html += '<th>' + dayOfWeek + '</th>';
            });

            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            for (var row = 0; row < 6; row++) {
                html += '<tr>';

                // add week number
                if (this.showWeekNumbers)
                    html += '<td class="week">' + calendar[row][0].week() + '</td>';

                for (var col = 0; col < 7; col++) {
                    var cname = 'available ';
                    cname += (calendar[row][col].month() == calendar[1][1].month()) ? '' : 'off';

                    if ((minDate && calendar[row][col].isBefore(minDate)) || (maxDate && calendar[row][col].isAfter(maxDate))) {
                        cname = ' off disabled ';
                    } else if (calendar[row][col].format('YYYY-MM-DD') == selected.format('YYYY-MM-DD')) {
                        cname += ' active ';
                        if (calendar[row][col].format('YYYY-MM-DD') == this.startDate.format('YYYY-MM-DD')) {
                            cname += ' start-date ';
                        }
                        if (calendar[row][col].format('YYYY-MM-DD') == this.endDate.format('YYYY-MM-DD')) {
                            cname += ' end-date ';
                        }
                    } else if (calendar[row][col] >= this.startDate && calendar[row][col] <= this.endDate) {
                        cname += ' in-range ';
                        if (calendar[row][col].isSame(this.startDate)) { cname += ' start-date '; }
                        if (calendar[row][col].isSame(this.endDate)) { cname += ' end-date '; }
                    }

                    var title = 'r' + row + 'c' + col;
                    html += '<td class="' + cname.replace(/\s+/g, ' ').replace(/^\s?(.*?)\s?$/, '$1') + '" data-title="' + title + '">' + calendar[row][col].date() + '</td>';
                }
                html += '</tr>';
            }

            html += '</tbody>';
            html += '</table>';
            html += '</div>';

            if (this.timePicker) {

                html += '<div class="calendar-time">';
                html += '<select class="hourselect">';
                var start = 0;
                var end = 23;
                var selected_hour = selected.hour();
                if (this.timePicker12Hour) {
                    start = 1;
                    end = 12;
                    if (selected_hour >= 12)
                        selected_hour -= 12;
                    if (selected_hour == 0)
                        selected_hour = 12;
                }

                for (var i = start; i <= end; i++) {
                    if (i == selected_hour) {
                        html += '<option value="' + i + '" selected="selected">' + i + '</option>';
                    } else {
                        html += '<option value="' + i + '">' + i + '</option>';
                    }
                }

                html += '</select> : ';

                html += '<select class="minuteselect">';

                for (var i = 0; i < 60; i += this.timePickerIncrement) {
                    var num = i;
                    if (num < 10)
                        num = '0' + num;
                    if (i == selected.minute()) {
                        html += '<option value="' + i + '" selected="selected">' + num + '</option>';
                    } else {
                        html += '<option value="' + i + '">' + num + '</option>';
                    }
                }

                html += '</select> ';

                if (this.timePicker12Hour) {
                    html += '<select class="ampmselect">';
                    if (selected.hour() >= 12) {
                        html += '<option value="AM">AM</option><option value="PM" selected="selected">PM</option>';
                    } else {
                        html += '<option value="AM" selected="selected">AM</option><option value="PM">PM</option>';
                    }
                    html += '</select>';
                }

                html += '</div>';

            }

            return html;

        }

    };

    $.fn.daterangepicker = function (options, cb) {
        this.each(function () {
            var el = $(this);
            if (!el.data('daterangepicker'))
                el.data('daterangepicker', new DateRangePicker(el, options, cb));
        });
        return this;
    };

}(window.jQuery);

/*
 Highstock JS v2.1.4 (2015-03-10)

 (c) 2009-2014 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(){function y(){var a,b=arguments,c,d={},e=function(a,b){var c,d;typeof a!=="object"&&(a={});for(d in b)b.hasOwnProperty(d)&&(c=b[d],a[d]=c&&typeof c==="object"&&Object.prototype.toString.call(c)!=="[object Array]"&&d!=="renderTo"&&typeof c.nodeType!=="number"?e(a[d]||{},c):b[d]);return a};b[0]===!0&&(d=b[1],b=Array.prototype.slice.call(b,2));c=b.length;for(a=0;a<c;a++)d=e(d,b[a]);return d}function C(a,b){return parseInt(a,b||10)}function Ja(a){return typeof a==="string"}function ia(a){return a&&
typeof a==="object"}function Ka(a){return Object.prototype.toString.call(a)==="[object Array]"}function sa(a){return typeof a==="number"}function La(a){return Y.log(a)/Y.LN10}function ta(a){return Y.pow(10,a)}function ua(a,b){for(var c=a.length;c--;)if(a[c]===b){a.splice(c,1);break}}function s(a){return a!==r&&a!==null}function W(a,b,c){var d,e;if(Ja(b))s(c)?a.setAttribute(b,c):a&&a.getAttribute&&(e=a.getAttribute(b));else if(s(b)&&ia(b))for(d in b)a.setAttribute(d,b[d]);return e}function pa(a){return Ka(a)?
a:[a]}function M(a,b){if(Ea&&!ea&&b&&b.opacity!==r)b.filter="alpha(opacity="+b.opacity*100+")";x(a.style,b)}function aa(a,b,c,d,e){a=E.createElement(a);b&&x(a,b);e&&M(a,{padding:0,border:Z,margin:0});c&&M(a,c);d&&d.appendChild(a);return a}function ja(a,b){var c=function(){return r};c.prototype=new a;x(c.prototype,b);return c}function Ra(a,b){return Array((b||2)+1-String(a).length).join(0)+a}function bb(a){return(kb&&kb(a)||ub||0)*6E4}function Ma(a,b){for(var c="{",d=!1,e,f,g,h,i,j=[];(c=a.indexOf(c))!==
-1;){e=a.slice(0,c);if(d){f=e.split(":");g=f.shift().split(".");i=g.length;e=b;for(h=0;h<i;h++)e=e[g[h]];if(f.length)f=f.join(":"),g=/\.([0-9])/,h=F.lang,i=void 0,/f$/.test(f)?(i=(i=f.match(g))?i[1]:-1,e!==null&&(e=z.numberFormat(e,i,h.decimalPoint,f.indexOf(",")>-1?h.thousandsSep:""))):e=ka(f,e)}j.push(e);a=a.slice(c+1);c=(d=!d)?"}":"{"}j.push(a);return j.join("")}function vb(a){return Y.pow(10,X(Y.log(a)/Y.LN10))}function wb(a,b,c,d,e){var f,g=a,c=p(c,1);f=a/c;b||(b=[1,2,2.5,5,10],d===!1&&(c===
1?b=[1,2,5,10]:c<=0.1&&(b=[1/c])));for(d=0;d<b.length;d++)if(g=b[d],e&&g*c>=a||!e&&f<=(b[d]+(b[d+1]||b[d]))/2)break;g*=c;return g}function xb(a,b){var c=a.length,d,e;for(e=0;e<c;e++)a[e].ss_i=e;a.sort(function(a,c){d=b(a,c);return d===0?a.ss_i-c.ss_i:d});for(e=0;e<c;e++)delete a[e].ss_i}function Sa(a){for(var b=a.length,c=a[0];b--;)a[b]<c&&(c=a[b]);return c}function Fa(a){for(var b=a.length,c=a[0];b--;)a[b]>c&&(c=a[b]);return c}function Na(a,b){for(var c in a)a[c]&&a[c]!==b&&a[c].destroy&&a[c].destroy(),
delete a[c]}function Ta(a){lb||(lb=aa(Ua));a&&lb.appendChild(a);lb.innerHTML=""}function qa(a,b){var c="Highcharts error #"+a+": www.highcharts.com/errors/"+a;if(b)throw c;T.console&&console.log(c)}function la(a){return parseFloat(a.toPrecision(14))}function Ya(a,b){Ga=p(a,b.animation)}function Nb(){var a=F.global,b=a.useUTC,c=b?"getUTC":"get",d=b?"setUTC":"set";fa=a.Date||window.Date;ub=b&&a.timezoneOffset;kb=b&&a.getTimezoneOffset;mb=function(a,c,d,h,i,j){var k;b?(k=fa.UTC.apply(0,arguments),k+=
bb(k)):k=(new fa(a,c,p(d,1),p(h,0),p(i,0),p(j,0))).getTime();return k};yb=c+"Minutes";zb=c+"Hours";Ab=c+"Day";cb=c+"Date";db=c+"Month";eb=c+"FullYear";Ob=d+"Minutes";Pb=d+"Hours";Bb=d+"Date";Cb=d+"Month";Db=d+"FullYear"}function $(){}function Za(a,b,c,d){this.axis=a;this.pos=b;this.type=c||"";this.isNew=!0;!c&&!d&&this.addLabel()}function Qb(a,b,c,d,e){var f=a.chart.inverted;this.axis=a;this.isNegative=c;this.options=b;this.x=d;this.total=null;this.points={};this.stack=e;this.alignOptions={align:b.align||
(f?c?"left":"right":"center"),verticalAlign:b.verticalAlign||(f?"middle":c?"bottom":"top"),y:p(b.y,f?4:c?14:-6),x:p(b.x,f?c?-6:6:0)};this.textAlign=b.textAlign||(f?c?"right":"left":"center")}function Eb(a){var b=a.options,c=b.navigator,d=c.enabled,b=b.scrollbar,e=b.enabled,f=d?c.height:0,g=e?b.height:0;this.handles=[];this.scrollbarButtons=[];this.elementsToDestroy=[];this.chart=a;this.setBaseSeries();this.height=f;this.scrollbarHeight=g;this.scrollbarEnabled=e;this.navigatorEnabled=d;this.navigatorOptions=
c;this.scrollbarOptions=b;this.outlineHeight=f+g;this.init()}function Fb(a){this.init(a)}var r,E=document,T=window,Y=Math,w=Y.round,X=Y.floor,za=Y.ceil,v=Y.max,B=Y.min,R=Y.abs,ba=Y.cos,ga=Y.sin,va=Y.PI,ra=va*2/360,Ha=navigator.userAgent,Rb=T.opera,Ea=/(msie|trident)/i.test(Ha)&&!Rb,nb=E.documentMode===8,Gb=/AppleWebKit/.test(Ha),Va=/Firefox/.test(Ha),fb=/(Mobile|Android|Windows Phone)/.test(Ha),Ia="http://www.w3.org/2000/svg",ea=!!E.createElementNS&&!!E.createElementNS(Ia,"svg").createSVGRect,Wb=
Va&&parseInt(Ha.split("Firefox/")[1],10)<4,ma=!ea&&!Ea&&!!E.createElement("canvas").getContext,Wa,$a,Sb={},Hb=0,lb,F,ka,Ga,Ib,H,ha=function(){return r},ca=[],gb=0,Ua="div",Z="none",Xb=/^[0-9]+$/,ob=["plotTop","marginRight","marginBottom","plotLeft"],Yb="stroke-width",fa,mb,ub,kb,yb,zb,Ab,cb,db,eb,Ob,Pb,Bb,Cb,Db,I={},z;z=T.Highcharts=T.Highcharts?qa(16,!0):{};z.seriesTypes=I;var x=z.extend=function(a,b){var c;a||(a={});for(c in b)a[c]=b[c];return a},p=z.pick=function(){var a=arguments,b,c,d=a.length;
for(b=0;b<d;b++)if(c=a[b],c!==r&&c!==null)return c},S=z.wrap=function(a,b,c){var d=a[b];a[b]=function(){var a=Array.prototype.slice.call(arguments);a.unshift(d);return c.apply(this,a)}};ka=function(a,b,c){if(!s(b)||isNaN(b))return"Invalid date";var a=p(a,"%Y-%m-%d %H:%M:%S"),d=new fa(b-bb(b)),e,f=d[zb](),g=d[Ab](),h=d[cb](),i=d[db](),j=d[eb](),k=F.lang,l=k.weekdays,d=x({a:l[g].substr(0,3),A:l[g],d:Ra(h),e:h,w:g,b:k.shortMonths[i],B:k.months[i],m:Ra(i+1),y:j.toString().substr(2,2),Y:j,H:Ra(f),I:Ra(f%
12||12),l:f%12||12,M:Ra(d[yb]()),p:f<12?"AM":"PM",P:f<12?"am":"pm",S:Ra(d.getSeconds()),L:Ra(w(b%1E3),3)},z.dateFormats);for(e in d)for(;a.indexOf("%"+e)!==-1;)a=a.replace("%"+e,typeof d[e]==="function"?d[e](b):d[e]);return c?a.substr(0,1).toUpperCase()+a.substr(1):a};H={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5,month:24192E5,year:314496E5};z.numberFormat=function(a,b,c,d){var e=F.lang,a=+a||0,f=b===-1?B((a.toString().split(".")[1]||"").length,20):isNaN(b=R(b))?2:b,b=c===
void 0?e.decimalPoint:c,d=d===void 0?e.thousandsSep:d,e=a<0?"-":"",c=String(C(a=R(a).toFixed(f))),g=c.length>3?c.length%3:0;return e+(g?c.substr(0,g)+d:"")+c.substr(g).replace(/(\d{3})(?=\d)/g,"$1"+d)+(f?b+R(a-c).toFixed(f).slice(2):"")};Ib={init:function(a,b,c){var b=b||"",d=a.shift,e=b.indexOf("C")>-1,f=e?7:3,g,b=b.split(" "),c=[].concat(c),h,i,j=function(a){for(g=a.length;g--;)a[g]==="M"&&a.splice(g+1,0,a[g+1],a[g+2],a[g+1],a[g+2])};e&&(j(b),j(c));a.isArea&&(h=b.splice(b.length-6,6),i=c.splice(c.length-
6,6));if(d<=c.length/f&&b.length===c.length)for(;d--;)c=[].concat(c).splice(0,f).concat(c);a.shift=0;if(b.length)for(a=c.length;b.length<a;)d=[].concat(b).splice(b.length-f,f),e&&(d[f-6]=d[f-2],d[f-5]=d[f-1]),b=b.concat(d);h&&(b=b.concat(h),c=c.concat(i));return[b,c]},step:function(a,b,c,d){var e=[],f=a.length;if(c===1)e=d;else if(f===b.length&&c<1)for(;f--;)d=parseFloat(a[f]),e[f]=isNaN(d)?a[f]:c*parseFloat(b[f]-d)+d;else e=b;return e}};(function(a){T.HighchartsAdapter=T.HighchartsAdapter||a&&{init:function(b){var c=
a.fx;a.extend(a.easing,{easeOutQuad:function(a,b,c,g,h){return-g*(b/=h)*(b-2)+c}});a.each(["cur","_default","width","height","opacity"],function(b,e){var f=c.step,g;e==="cur"?f=c.prototype:e==="_default"&&a.Tween&&(f=a.Tween.propHooks[e],e="set");(g=f[e])&&(f[e]=function(a){var c,a=b?a:this;if(a.prop!=="align")return c=a.elem,c.attr?c.attr(a.prop,e==="cur"?r:a.now):g.apply(this,arguments)})});S(a.cssHooks.opacity,"get",function(a,b,c){return b.attr?b.opacity||0:a.call(this,b,c)});this.addAnimSetter("d",
function(a){var c=a.elem,f;if(!a.started)f=b.init(c,c.d,c.toD),a.start=f[0],a.end=f[1],a.started=!0;c.attr("d",b.step(a.start,a.end,a.pos,c.toD))});this.each=Array.prototype.forEach?function(a,b){return Array.prototype.forEach.call(a,b)}:function(a,b){var c,g=a.length;for(c=0;c<g;c++)if(b.call(a[c],a[c],c,a)===!1)return c};a.fn.highcharts=function(){var a="Chart",b=arguments,c,g;if(this[0]){Ja(b[0])&&(a=b[0],b=Array.prototype.slice.call(b,1));c=b[0];if(c!==r)c.chart=c.chart||{},c.chart.renderTo=this[0],
new z[a](c,b[1]),g=this;c===r&&(g=ca[W(this[0],"data-highcharts-chart")])}return g}},addAnimSetter:function(b,c){a.Tween?a.Tween.propHooks[b]={set:c}:a.fx.step[b]=c},getScript:a.getScript,inArray:a.inArray,adapterRun:function(b,c){return a(b)[c]()},grep:a.grep,map:function(a,c){for(var d=[],e=0,f=a.length;e<f;e++)d[e]=c.call(a[e],a[e],e,a);return d},offset:function(b){return a(b).offset()},addEvent:function(b,c,d){a(b).bind(c,d)},removeEvent:function(b,c,d){var e=E.removeEventListener?"removeEventListener":
"detachEvent";E[e]&&b&&!b[e]&&(b[e]=function(){});a(b).unbind(c,d)},fireEvent:function(b,c,d,e){var f=a.Event(c),g="detached"+c,h;!Ea&&d&&(delete d.layerX,delete d.layerY,delete d.returnValue);x(f,d);b[c]&&(b[g]=b[c],b[c]=null);a.each(["preventDefault","stopPropagation"],function(a,b){var c=f[b];f[b]=function(){try{c.call(f)}catch(a){b==="preventDefault"&&(h=!0)}}});a(b).trigger(f);b[g]&&(b[c]=b[g],b[g]=null);e&&!f.isDefaultPrevented()&&!h&&e(f)},washMouseEvent:function(a){var c=a.originalEvent||
a;if(c.pageX===r)c.pageX=a.pageX,c.pageY=a.pageY;return c},animate:function(b,c,d){var e=a(b);if(!b.style)b.style={};if(c.d)b.toD=c.d,c.d=1;e.stop();c.opacity!==r&&b.attr&&(c.opacity+="px");b.hasAnim=1;e.animate(c,d)},stop:function(b){b.hasAnim&&a(b).stop()}}})(T.jQuery);var Q=T.HighchartsAdapter,G=Q||{};Q&&Q.init.call(Q,Ib);var pb=G.adapterRun,Zb=G.getScript,Oa=G.inArray,n=z.each=G.each,hb=G.grep,$b=G.offset,Aa=G.map,D=G.addEvent,U=G.removeEvent,K=G.fireEvent,ac=G.washMouseEvent,qb=G.animate,ab=
G.stop;F={colors:"#7cb5ec,#434348,#90ed7d,#f7a35c,#8085e9,#f15c80,#e4d354,#2b908f,#f45b5b,#91e8e1".split(","),symbols:["circle","diamond","square","triangle","triangle-down"],lang:{loading:"Loading...",months:"January,February,March,April,May,June,July,August,September,October,November,December".split(","),shortMonths:"Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","),weekdays:"Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(","),decimalPoint:".",numericSymbols:"k,M,G,T,P,E".split(","),
resetZoom:"Reset zoom",resetZoomTitle:"Reset zoom level 1:1",thousandsSep:" "},global:{useUTC:!0,canvasToolsURL:"http://code.highcharts.com/stock/2.1.4/modules/canvas-tools.js",VMLRadialGradientURL:"http://code.highcharts.com/stock/2.1.4/gfx/vml-radial-gradient.png"},chart:{borderColor:"#4572A7",borderRadius:0,defaultSeriesType:"line",ignoreHiddenSeries:!0,spacing:[10,10,15,10],backgroundColor:"#FFFFFF",plotBorderColor:"#C0C0C0",resetZoomButton:{theme:{zIndex:20},position:{align:"right",x:-10,y:10}}},
title:{text:"Chart title",align:"center",margin:15,style:{color:"#333333",fontSize:"18px"}},subtitle:{text:"",align:"center",style:{color:"#555555"}},plotOptions:{line:{allowPointSelect:!1,showCheckbox:!1,animation:{duration:1E3},events:{},lineWidth:2,marker:{lineWidth:0,radius:4,lineColor:"#FFFFFF",states:{hover:{enabled:!0,lineWidthPlus:1,radiusPlus:2},select:{fillColor:"#FFFFFF",lineColor:"#000000",lineWidth:2}}},point:{events:{}},dataLabels:{align:"center",formatter:function(){return this.y===
null?"":z.numberFormat(this.y,-1)},style:{color:"contrast",fontSize:"11px",fontWeight:"bold",textShadow:"0 0 6px contrast, 0 0 3px contrast"},verticalAlign:"bottom",x:0,y:0,padding:5},cropThreshold:300,pointRange:0,states:{hover:{lineWidthPlus:1,marker:{},halo:{size:10,opacity:0.25}},select:{marker:{}}},stickyTracking:!0,turboThreshold:1E3}},labels:{style:{position:"absolute",color:"#3E576F"}},legend:{enabled:!0,align:"center",layout:"horizontal",labelFormatter:function(){return this.name},borderColor:"#909090",
borderRadius:0,navigation:{activeColor:"#274b6d",inactiveColor:"#CCC"},shadow:!1,itemStyle:{color:"#333333",fontSize:"12px",fontWeight:"bold"},itemHoverStyle:{color:"#000"},itemHiddenStyle:{color:"#CCC"},itemCheckboxStyle:{position:"absolute",width:"13px",height:"13px"},symbolPadding:5,verticalAlign:"bottom",x:0,y:0,title:{style:{fontWeight:"bold"}}},loading:{labelStyle:{fontWeight:"bold",position:"relative",top:"45%"},style:{position:"absolute",backgroundColor:"white",opacity:0.5,textAlign:"center"}},
tooltip:{enabled:!0,animation:ea,backgroundColor:"rgba(249, 249, 249, .85)",borderWidth:1,borderRadius:3,dateTimeLabelFormats:{millisecond:"%A, %b %e, %H:%M:%S.%L",second:"%A, %b %e, %H:%M:%S",minute:"%A, %b %e, %H:%M",hour:"%A, %b %e, %H:%M",day:"%A, %b %e, %Y",week:"Week from %A, %b %e, %Y",month:"%B %Y",year:"%Y"},footerFormat:"",headerFormat:'<span style="font-size: 10px">{point.key}</span><br/>',pointFormat:'<span style="color:{point.color}">●</span> {series.name}: <b>{point.y}</b><br/>',shadow:!0,
snap:fb?25:10,style:{color:"#333333",cursor:"default",fontSize:"12px",padding:"8px",whiteSpace:"nowrap"}},credits:{enabled:!0,text:"",href:"",position:{align:"right",x:-10,verticalAlign:"bottom",y:-5},style:{cursor:"pointer",color:"#909090",fontSize:"9px"}}};var V=F.plotOptions,Q=V.line;Nb();var bc=/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/,cc=/#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/,dc=/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/,
wa=function(a){var b=[],c,d;(function(a){a&&a.stops?d=Aa(a.stops,function(a){return wa(a[1])}):(c=bc.exec(a))?b=[C(c[1]),C(c[2]),C(c[3]),parseFloat(c[4],10)]:(c=cc.exec(a))?b=[C(c[1],16),C(c[2],16),C(c[3],16),1]:(c=dc.exec(a))&&(b=[C(c[1]),C(c[2]),C(c[3]),1])})(a);return{get:function(c){var f;d?(f=y(a),f.stops=[].concat(f.stops),n(d,function(a,b){f.stops[b]=[f.stops[b][0],a.get(c)]})):f=b&&!isNaN(b[0])?c==="rgb"?"rgb("+b[0]+","+b[1]+","+b[2]+")":c==="a"?b[3]:"rgba("+b.join(",")+")":a;return f},brighten:function(a){if(d)n(d,
function(b){b.brighten(a)});else if(sa(a)&&a!==0){var c;for(c=0;c<3;c++)b[c]+=C(a*255),b[c]<0&&(b[c]=0),b[c]>255&&(b[c]=255)}return this},rgba:b,setOpacity:function(a){b[3]=a;return this},raw:a}};$.prototype={opacity:1,textProps:"fontSize,fontWeight,fontFamily,color,lineHeight,width,textDecoration,textShadow".split(","),init:function(a,b){this.element=b==="span"?aa(b):E.createElementNS(Ia,b);this.renderer=a},animate:function(a,b,c){b=p(b,Ga,!0);ab(this);if(b){b=y(b,{});if(c)b.complete=c;qb(this,a,
b)}else this.attr(a),c&&c();return this},colorGradient:function(a,b,c){var d=this.renderer,e,f,g,h,i,j,k,l,m,o,q=[];a.linearGradient?f="linearGradient":a.radialGradient&&(f="radialGradient");if(f){g=a[f];h=d.gradients;j=a.stops;m=c.radialReference;Ka(g)&&(a[f]=g={x1:g[0],y1:g[1],x2:g[2],y2:g[3],gradientUnits:"userSpaceOnUse"});f==="radialGradient"&&m&&!s(g.gradientUnits)&&(g=y(g,{cx:m[0]-m[2]/2+g.cx*m[2],cy:m[1]-m[2]/2+g.cy*m[2],r:g.r*m[2],gradientUnits:"userSpaceOnUse"}));for(o in g)o!=="id"&&q.push(o,
g[o]);for(o in j)q.push(j[o]);q=q.join(",");h[q]?a=h[q].attr("id"):(g.id=a="highcharts-"+Hb++,h[q]=i=d.createElement(f).attr(g).add(d.defs),i.stops=[],n(j,function(a){a[1].indexOf("rgba")===0?(e=wa(a[1]),k=e.get("rgb"),l=e.get("a")):(k=a[1],l=1);a=d.createElement("stop").attr({offset:a[0],"stop-color":k,"stop-opacity":l}).add(i);i.stops.push(a)}));c.setAttribute(b,"url("+d.url+"#"+a+")")}},applyTextShadow:function(a){var b=this.element,c,d=a.indexOf("contrast")!==-1,e=this.renderer.forExport||b.style.textShadow!==
r&&!Ea;d&&(a=a.replace(/contrast/g,this.renderer.getContrast(b.style.fill)));e?d&&M(b,{textShadow:a}):(this.fakeTS=!0,this.ySetter=this.xSetter,c=[].slice.call(b.getElementsByTagName("tspan")),n(a.split(/\s?,\s?/g),function(a){var d=b.firstChild,e,i,a=a.split(" ");e=a[a.length-1];(i=a[a.length-2])&&n(c,function(a,c){var f;c===0&&(a.setAttribute("x",b.getAttribute("x")),c=b.getAttribute("y"),a.setAttribute("y",c||0),c===null&&b.setAttribute("y",0));f=a.cloneNode(1);W(f,{"class":"highcharts-text-shadow",
fill:e,stroke:e,"stroke-opacity":1/v(C(i),3),"stroke-width":i,"stroke-linejoin":"round"});b.insertBefore(f,d)})}))},attr:function(a,b){var c,d,e=this.element,f,g=this,h;typeof a==="string"&&b!==r&&(c=a,a={},a[c]=b);if(typeof a==="string")g=(this[a+"Getter"]||this._defaultGetter).call(this,a,e);else{for(c in a){d=a[c];h=!1;this.symbolName&&/^(x|y|width|height|r|start|end|innerR|anchorX|anchorY)/.test(c)&&(f||(this.symbolAttr(a),f=!0),h=!0);if(this.rotation&&(c==="x"||c==="y"))this.doTransform=!0;h||
(this[c+"Setter"]||this._defaultSetter).call(this,d,c,e);this.shadows&&/^(width|height|visibility|x|y|d|transform|cx|cy|r)$/.test(c)&&this.updateShadows(c,d)}if(this.doTransform)this.updateTransform(),this.doTransform=!1}return g},updateShadows:function(a,b){for(var c=this.shadows,d=c.length;d--;)c[d].setAttribute(a,a==="height"?v(b-(c[d].cutHeight||0),0):a==="d"?this.d:b)},addClass:function(a){var b=this.element,c=W(b,"class")||"";c.indexOf(a)===-1&&W(b,"class",c+" "+a);return this},symbolAttr:function(a){var b=
this;n("x,y,r,start,end,width,height,innerR,anchorX,anchorY".split(","),function(c){b[c]=p(a[c],b[c])});b.attr({d:b.renderer.symbols[b.symbolName](b.x,b.y,b.width,b.height,b)})},clip:function(a){return this.attr("clip-path",a?"url("+this.renderer.url+"#"+a.id+")":Z)},crisp:function(a){var b,c={},d,e=a.strokeWidth||this.strokeWidth||0;d=w(e)%2/2;a.x=X(a.x||this.x||0)+d;a.y=X(a.y||this.y||0)+d;a.width=X((a.width||this.width||0)-2*d);a.height=X((a.height||this.height||0)-2*d);a.strokeWidth=e;for(b in a)this[b]!==
a[b]&&(this[b]=c[b]=a[b]);return c},css:function(a){var b=this.styles,c={},d=this.element,e,f,g="";e=!b;if(a&&a.color)a.fill=a.color;if(b)for(f in a)a[f]!==b[f]&&(c[f]=a[f],e=!0);if(e){e=this.textWidth=a&&a.width&&d.nodeName.toLowerCase()==="text"&&C(a.width)||this.textWidth;b&&(a=x(b,c));this.styles=a;e&&(ma||!ea&&this.renderer.forExport)&&delete a.width;if(Ea&&!ea)M(this.element,a);else{b=function(a,b){return"-"+b.toLowerCase()};for(f in a)g+=f.replace(/([A-Z])/g,b)+":"+a[f]+";";W(d,"style",g)}e&&
this.added&&this.renderer.buildText(this)}return this},on:function(a,b){var c=this,d=c.element;$a&&a==="click"?(d.ontouchstart=function(a){c.touchEventFired=fa.now();a.preventDefault();b.call(d,a)},d.onclick=function(a){(Ha.indexOf("Android")===-1||fa.now()-(c.touchEventFired||0)>1100)&&b.call(d,a)}):d["on"+a]=b;return this},setRadialReference:function(a){this.element.radialReference=a;return this},translate:function(a,b){return this.attr({translateX:a,translateY:b})},invert:function(){this.inverted=
!0;this.updateTransform();return this},updateTransform:function(){var a=this.translateX||0,b=this.translateY||0,c=this.scaleX,d=this.scaleY,e=this.inverted,f=this.rotation,g=this.element;e&&(a+=this.attr("width"),b+=this.attr("height"));a=["translate("+a+","+b+")"];e?a.push("rotate(90) scale(-1,1)"):f&&a.push("rotate("+f+" "+(g.getAttribute("x")||0)+" "+(g.getAttribute("y")||0)+")");(s(c)||s(d))&&a.push("scale("+p(c,1)+" "+p(d,1)+")");a.length&&g.setAttribute("transform",a.join(" "))},toFront:function(){var a=
this.element;a.parentNode.appendChild(a);return this},align:function(a,b,c){var d,e,f,g,h={};e=this.renderer;f=e.alignedObjects;if(a){if(this.alignOptions=a,this.alignByTranslate=b,!c||Ja(c))this.alignTo=d=c||"renderer",ua(f,this),f.push(this),c=null}else a=this.alignOptions,b=this.alignByTranslate,d=this.alignTo;c=p(c,e[d],e);d=a.align;e=a.verticalAlign;f=(c.x||0)+(a.x||0);g=(c.y||0)+(a.y||0);if(d==="right"||d==="center")f+=(c.width-(a.width||0))/{right:1,center:2}[d];h[b?"translateX":"x"]=w(f);
if(e==="bottom"||e==="middle")g+=(c.height-(a.height||0))/({bottom:1,middle:2}[e]||1);h[b?"translateY":"y"]=w(g);this[this.placed?"animate":"attr"](h);this.placed=!0;this.alignAttr=h;return this},getBBox:function(a){var b,c=this.renderer,d,e=this.rotation,f=this.element,g=this.styles,h=e*ra;d=this.textStr;var i,j=f.style,k,l;d!==r&&(l=["",e||0,g&&g.fontSize,f.style.width].join(","),l=d===""||Xb.test(d)?"num:"+d.toString().length+l:d+l);l&&!a&&(b=c.cache[l]);if(!b){if(f.namespaceURI===Ia||c.forExport){try{k=
this.fakeTS&&function(a){n(f.querySelectorAll(".highcharts-text-shadow"),function(b){b.style.display=a})},Va&&j.textShadow?(i=j.textShadow,j.textShadow=""):k&&k(Z),b=f.getBBox?x({},f.getBBox()):{width:f.offsetWidth,height:f.offsetHeight},i?j.textShadow=i:k&&k("")}catch(m){}if(!b||b.width<0)b={width:0,height:0}}else b=this.htmlGetBBox();if(c.isSVG){a=b.width;d=b.height;if(Ea&&g&&g.fontSize==="11px"&&d.toPrecision(3)==="16.9")b.height=d=14;if(e)b.width=R(d*ga(h))+R(a*ba(h)),b.height=R(d*ba(h))+R(a*
ga(h))}c.cache[l]=b}return b},show:function(a){a&&this.element.namespaceURI===Ia?this.element.removeAttribute("visibility"):this.attr({visibility:a?"inherit":"visible"});return this},hide:function(){return this.attr({visibility:"hidden"})},fadeOut:function(a){var b=this;b.animate({opacity:0},{duration:a||150,complete:function(){b.attr({y:-9999})}})},add:function(a){var b=this.renderer,c=this.element,d;if(a)this.parentGroup=a;this.parentInverted=a&&a.inverted;this.textStr!==void 0&&b.buildText(this);
this.added=!0;if(!a||a.handleZ||this.zIndex)d=this.zIndexSetter();d||(a?a.element:b.box).appendChild(c);if(this.onAdd)this.onAdd();return this},safeRemoveChild:function(a){var b=a.parentNode;b&&b.removeChild(a)},destroy:function(){var a=this,b=a.element||{},c=a.shadows,d=a.renderer.isSVG&&b.nodeName==="SPAN"&&a.parentGroup,e,f;b.onclick=b.onmouseout=b.onmouseover=b.onmousemove=b.point=null;ab(a);if(a.clipPath)a.clipPath=a.clipPath.destroy();if(a.stops){for(f=0;f<a.stops.length;f++)a.stops[f]=a.stops[f].destroy();
a.stops=null}a.safeRemoveChild(b);for(c&&n(c,function(b){a.safeRemoveChild(b)});d&&d.div&&d.div.childNodes.length===0;)b=d.parentGroup,a.safeRemoveChild(d.div),delete d.div,d=b;a.alignTo&&ua(a.renderer.alignedObjects,a);for(e in a)delete a[e];return null},shadow:function(a,b,c){var d=[],e,f,g=this.element,h,i,j,k;if(a){i=p(a.width,3);j=(a.opacity||0.15)/i;k=this.parentInverted?"(-1,-1)":"("+p(a.offsetX,1)+", "+p(a.offsetY,1)+")";for(e=1;e<=i;e++){f=g.cloneNode(0);h=i*2+1-2*e;W(f,{isShadow:"true",
stroke:a.color||"black","stroke-opacity":j*e,"stroke-width":h,transform:"translate"+k,fill:Z});if(c)W(f,"height",v(W(f,"height")-h,0)),f.cutHeight=h;b?b.element.appendChild(f):g.parentNode.insertBefore(f,g);d.push(f)}this.shadows=d}return this},xGetter:function(a){this.element.nodeName==="circle"&&(a={x:"cx",y:"cy"}[a]||a);return this._defaultGetter(a)},_defaultGetter:function(a){a=p(this[a],this.element?this.element.getAttribute(a):null,0);/^[\-0-9\.]+$/.test(a)&&(a=parseFloat(a));return a},dSetter:function(a,
b,c){a&&a.join&&(a=a.join(" "));/(NaN| {2}|^$)/.test(a)&&(a="M 0 0");c.setAttribute(b,a);this[b]=a},dashstyleSetter:function(a){var b;if(a=a&&a.toLowerCase()){a=a.replace("shortdashdotdot","3,1,1,1,1,1,").replace("shortdashdot","3,1,1,1").replace("shortdot","1,1,").replace("shortdash","3,1,").replace("longdash","8,3,").replace(/dot/g,"1,3,").replace("dash","4,3,").replace(/,$/,"").split(",");for(b=a.length;b--;)a[b]=C(a[b])*this["stroke-width"];a=a.join(",").replace("NaN","none");this.element.setAttribute("stroke-dasharray",
a)}},alignSetter:function(a){this.element.setAttribute("text-anchor",{left:"start",center:"middle",right:"end"}[a])},opacitySetter:function(a,b,c){this[b]=a;c.setAttribute(b,a)},titleSetter:function(a){var b=this.element.getElementsByTagName("title")[0];b||(b=E.createElementNS(Ia,"title"),this.element.appendChild(b));b.textContent=String(p(a),"").replace(/<[^>]*>/g,"")},textSetter:function(a){if(a!==this.textStr)delete this.bBox,this.textStr=a,this.added&&this.renderer.buildText(this)},fillSetter:function(a,
b,c){typeof a==="string"?c.setAttribute(b,a):a&&this.colorGradient(a,b,c)},zIndexSetter:function(a,b){var c=this.renderer,d=this.parentGroup,c=(d||c).element||c.box,e,f,g=this.element,h;e=this.added;var i;s(a)&&(g.setAttribute(b,a),a=+a,this[b]===a&&(e=!1),this[b]=a);if(e){if((a=this.zIndex)&&d)d.handleZ=!0;d=c.childNodes;for(i=0;i<d.length&&!h;i++)if(e=d[i],f=W(e,"zIndex"),e!==g&&(C(f)>a||!s(a)&&s(f)))c.insertBefore(g,e),h=!0;h||c.appendChild(g)}return h},_defaultSetter:function(a,b,c){c.setAttribute(b,
a)}};$.prototype.yGetter=$.prototype.xGetter;$.prototype.translateXSetter=$.prototype.translateYSetter=$.prototype.rotationSetter=$.prototype.verticalAlignSetter=$.prototype.scaleXSetter=$.prototype.scaleYSetter=function(a,b){this[b]=a;this.doTransform=!0};$.prototype["stroke-widthSetter"]=$.prototype.strokeSetter=function(a,b,c){this[b]=a;if(this.stroke&&this["stroke-width"])this.strokeWidth=this["stroke-width"],$.prototype.fillSetter.call(this,this.stroke,"stroke",c),c.setAttribute("stroke-width",
this["stroke-width"]),this.hasStroke=!0;else if(b==="stroke-width"&&a===0&&this.hasStroke)c.removeAttribute("stroke"),this.hasStroke=!1};var na=function(){this.init.apply(this,arguments)};na.prototype={Element:$,init:function(a,b,c,d,e){var f=location,g,d=this.createElement("svg").attr({version:"1.1"}).css(this.getStyle(d));g=d.element;a.appendChild(g);a.innerHTML.indexOf("xmlns")===-1&&W(g,"xmlns",Ia);this.isSVG=!0;this.box=g;this.boxWrapper=d;this.alignedObjects=[];this.url=(Va||Gb)&&E.getElementsByTagName("base").length?
f.href.replace(/#.*?$/,"").replace(/([\('\)])/g,"\\$1").replace(/ /g,"%20"):"";this.createElement("desc").add().element.appendChild(E.createTextNode("Created with Highstock 2.1.4"));this.defs=this.createElement("defs").add();this.forExport=e;this.gradients={};this.cache={};this.setSize(b,c,!1);var h;if(Va&&a.getBoundingClientRect)this.subPixelFix=b=function(){M(a,{left:0,top:0});h=a.getBoundingClientRect();M(a,{left:za(h.left)-h.left+"px",top:za(h.top)-h.top+"px"})},b(),D(T,"resize",b)},getStyle:function(a){return this.style=
x({fontFamily:'"Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif',fontSize:"12px"},a)},isHidden:function(){return!this.boxWrapper.getBBox().width},destroy:function(){var a=this.defs;this.box=null;this.boxWrapper=this.boxWrapper.destroy();Na(this.gradients||{});this.gradients=null;if(a)this.defs=a.destroy();this.subPixelFix&&U(T,"resize",this.subPixelFix);return this.alignedObjects=null},createElement:function(a){var b=new this.Element;b.init(this,a);return b},draw:function(){},
buildText:function(a){for(var b=a.element,c=this,d=c.forExport,e=p(a.textStr,"").toString(),f=e.indexOf("<")!==-1,g=b.childNodes,h,i,j=W(b,"x"),k=a.styles,l=a.textWidth,m=k&&k.lineHeight,o=k&&k.textShadow,q=k&&k.textOverflow==="ellipsis",t=g.length,J=l&&!a.added&&this.box,N=function(a){return m?C(m):c.fontMetrics(/(px|em)$/.test(a&&a.style.fontSize)?a.style.fontSize:k&&k.fontSize||c.style.fontSize||12,a).h},u=function(a){return a.replace(/&lt;/g,"<").replace(/&gt;/g,">")};t--;)b.removeChild(g[t]);
!f&&!o&&!q&&e.indexOf(" ")===-1?b.appendChild(E.createTextNode(u(e))):(h=/<.*style="([^"]+)".*>/,i=/<.*href="(http[^"]+)".*>/,J&&J.appendChild(b),e=f?e.replace(/<(b|strong)>/g,'<span style="font-weight:bold">').replace(/<(i|em)>/g,'<span style="font-style:italic">').replace(/<a/g,"<span").replace(/<\/(b|strong|i|em|a)>/g,"</span>").split(/<br.*?>/g):[e],e[e.length-1]===""&&e.pop(),n(e,function(e,f){var g,m=0,e=e.replace(/<span/g,"|||<span").replace(/<\/span>/g,"</span>|||");g=e.split("|||");n(g,function(e){if(e!==
""||g.length===1){var o={},t=E.createElementNS(Ia,"tspan"),J;h.test(e)&&(J=e.match(h)[1].replace(/(;| |^)color([ :])/,"$1fill$2"),W(t,"style",J));i.test(e)&&!d&&(W(t,"onclick",'location.href="'+e.match(i)[1]+'"'),M(t,{cursor:"pointer"}));e=u(e.replace(/<(.|\n)*?>/g,"")||" ");if(e!==" "){t.appendChild(E.createTextNode(e));if(m)o.dx=0;else if(f&&j!==null)o.x=j;W(t,o);b.appendChild(t);!m&&f&&(!ea&&d&&M(t,{display:"block"}),W(t,"dy",N(t)));if(l){for(var o=e.replace(/([^\^])-/g,"$1- ").split(" "),n=g.length>
1||f||o.length>1&&k.whiteSpace!=="nowrap",p,A,r,s=[],w=N(t),v=1,x=a.rotation,y=e,B=y.length;(n||q)&&(o.length||s.length);)a.rotation=0,p=a.getBBox(!0),r=p.width,!ea&&c.forExport&&(r=c.measureSpanWidth(t.firstChild.data,a.styles)),p=r>l,A===void 0&&(A=p),q&&A?(B/=2,y===""||!p&&B<0.5?o=[]:(p&&(A=!0),y=e.substring(0,y.length+(p?-1:1)*za(B)),o=[y+"…"],t.removeChild(t.firstChild))):!p||o.length===1?(o=s,s=[],o.length&&(v++,t=E.createElementNS(Ia,"tspan"),W(t,{dy:w,x:j}),J&&W(t,"style",J),b.appendChild(t)),
r>l&&(l=r)):(t.removeChild(t.firstChild),s.unshift(o.pop())),o.length&&t.appendChild(E.createTextNode(o.join(" ").replace(/- /g,"-")));A&&a.attr("title",a.textStr);a.rotation=x}m++}}})}),J&&J.removeChild(b),o&&a.applyTextShadow&&a.applyTextShadow(o))},getContrast:function(a){a=wa(a).rgba;return a[0]+a[1]+a[2]>384?"#000":"#FFF"},button:function(a,b,c,d,e,f,g,h,i){var j=this.label(a,b,c,i,null,null,null,null,"button"),k=0,l,m,o,q,t,J,a={x1:0,y1:0,x2:0,y2:1},e=y({"stroke-width":1,stroke:"#CCCCCC",fill:{linearGradient:a,
stops:[[0,"#FEFEFE"],[1,"#F6F6F6"]]},r:2,padding:5,style:{color:"black"}},e);o=e.style;delete e.style;f=y(e,{stroke:"#68A",fill:{linearGradient:a,stops:[[0,"#FFF"],[1,"#ACF"]]}},f);q=f.style;delete f.style;g=y(e,{stroke:"#68A",fill:{linearGradient:a,stops:[[0,"#9BD"],[1,"#CDF"]]}},g);t=g.style;delete g.style;h=y(e,{style:{color:"#CCC"}},h);J=h.style;delete h.style;D(j.element,Ea?"mouseover":"mouseenter",function(){k!==3&&j.attr(f).css(q)});D(j.element,Ea?"mouseout":"mouseleave",function(){k!==3&&
(l=[e,f,g][k],m=[o,q,t][k],j.attr(l).css(m))});j.setState=function(a){(j.state=k=a)?a===2?j.attr(g).css(t):a===3&&j.attr(h).css(J):j.attr(e).css(o)};return j.on("click",function(){k!==3&&d.call(j)}).attr(e).css(x({cursor:"default"},o))},crispLine:function(a,b){a[1]===a[4]&&(a[1]=a[4]=w(a[1])-b%2/2);a[2]===a[5]&&(a[2]=a[5]=w(a[2])+b%2/2);return a},path:function(a){var b={fill:Z};Ka(a)?b.d=a:ia(a)&&x(b,a);return this.createElement("path").attr(b)},circle:function(a,b,c){a=ia(a)?a:{x:a,y:b,r:c};b=this.createElement("circle");
b.xSetter=function(a){this.element.setAttribute("cx",a)};b.ySetter=function(a){this.element.setAttribute("cy",a)};return b.attr(a)},arc:function(a,b,c,d,e,f){if(ia(a))b=a.y,c=a.r,d=a.innerR,e=a.start,f=a.end,a=a.x;a=this.symbol("arc",a||0,b||0,c||0,c||0,{innerR:d||0,start:e||0,end:f||0});a.r=c;return a},rect:function(a,b,c,d,e,f){var e=ia(a)?a.r:e,g=this.createElement("rect"),a=ia(a)?a:a===r?{}:{x:a,y:b,width:v(c,0),height:v(d,0)};if(f!==r)a.strokeWidth=f,a=g.crisp(a);if(e)a.r=e;g.rSetter=function(a){W(this.element,
{rx:a,ry:a})};return g.attr(a)},setSize:function(a,b,c){var d=this.alignedObjects,e=d.length;this.width=a;this.height=b;for(this.boxWrapper[p(c,!0)?"animate":"attr"]({width:a,height:b});e--;)d[e].align()},g:function(a){var b=this.createElement("g");return s(a)?b.attr({"class":"highcharts-"+a}):b},image:function(a,b,c,d,e){var f={preserveAspectRatio:Z};arguments.length>1&&x(f,{x:b,y:c,width:d,height:e});f=this.createElement("image").attr(f);f.element.setAttributeNS?f.element.setAttributeNS("http://www.w3.org/1999/xlink",
"href",a):f.element.setAttribute("hc-svg-href",a);return f},symbol:function(a,b,c,d,e,f){var g,h=this.symbols[a],h=h&&h(w(b),w(c),d,e,f),i=/^url\((.*?)\)$/,j,k;if(h)g=this.path(h),x(g,{symbolName:a,x:b,y:c,width:d,height:e}),f&&x(g,f);else if(i.test(a))k=function(a,b){a.element&&(a.attr({width:b[0],height:b[1]}),a.alignByTranslate||a.translate(w((d-b[0])/2),w((e-b[1])/2)))},j=a.match(i)[1],a=Sb[j]||f&&f.width&&f.height&&[f.width,f.height],g=this.image(j).attr({x:b,y:c}),g.isImg=!0,a?k(g,a):(g.attr({width:0,
height:0}),aa("img",{onload:function(){k(g,Sb[j]=[this.width,this.height])},src:j}));return g},symbols:{circle:function(a,b,c,d){var e=0.166*c;return["M",a+c/2,b,"C",a+c+e,b,a+c+e,b+d,a+c/2,b+d,"C",a-e,b+d,a-e,b,a+c/2,b,"Z"]},square:function(a,b,c,d){return["M",a,b,"L",a+c,b,a+c,b+d,a,b+d,"Z"]},triangle:function(a,b,c,d){return["M",a+c/2,b,"L",a+c,b+d,a,b+d,"Z"]},"triangle-down":function(a,b,c,d){return["M",a,b,"L",a+c,b,a+c/2,b+d,"Z"]},diamond:function(a,b,c,d){return["M",a+c/2,b,"L",a+c,b+d/2,a+
c/2,b+d,a,b+d/2,"Z"]},arc:function(a,b,c,d,e){var f=e.start,c=e.r||c||d,g=e.end-0.001,d=e.innerR,h=e.open,i=ba(f),j=ga(f),k=ba(g),g=ga(g),e=e.end-f<va?0:1;return["M",a+c*i,b+c*j,"A",c,c,0,e,1,a+c*k,b+c*g,h?"M":"L",a+d*k,b+d*g,"A",d,d,0,e,0,a+d*i,b+d*j,h?"":"Z"]},callout:function(a,b,c,d,e){var f=B(e&&e.r||0,c,d),g=f+6,h=e&&e.anchorX,i=e&&e.anchorY,e=w(e.strokeWidth||0)%2/2;a+=e;b+=e;e=["M",a+f,b,"L",a+c-f,b,"C",a+c,b,a+c,b,a+c,b+f,"L",a+c,b+d-f,"C",a+c,b+d,a+c,b+d,a+c-f,b+d,"L",a+f,b+d,"C",a,b+d,
a,b+d,a,b+d-f,"L",a,b+f,"C",a,b,a,b,a+f,b];h&&h>c&&i>b+g&&i<b+d-g?e.splice(13,3,"L",a+c,i-6,a+c+6,i,a+c,i+6,a+c,b+d-f):h&&h<0&&i>b+g&&i<b+d-g?e.splice(33,3,"L",a,i+6,a-6,i,a,i-6,a,b+f):i&&i>d&&h>a+g&&h<a+c-g?e.splice(23,3,"L",h+6,b+d,h,b+d+6,h-6,b+d,a+f,b+d):i&&i<0&&h>a+g&&h<a+c-g&&e.splice(3,3,"L",h-6,b,h,b-6,h+6,b,c-f,b);return e}},clipRect:function(a,b,c,d){var e="highcharts-"+Hb++,f=this.createElement("clipPath").attr({id:e}).add(this.defs),a=this.rect(a,b,c,d,0).add(f);a.id=e;a.clipPath=f;a.count=
0;return a},text:function(a,b,c,d){var e=ma||!ea&&this.forExport,f={};if(d&&!this.forExport)return this.html(a,b,c);f.x=Math.round(b||0);if(c)f.y=Math.round(c);if(a||a===0)f.text=a;a=this.createElement("text").attr(f);e&&a.css({position:"absolute"});if(!d)a.xSetter=function(a,b,c){var d=c.getElementsByTagName("tspan"),e,f=c.getAttribute(b),m;for(m=0;m<d.length;m++)e=d[m],e.getAttribute(b)===f&&e.setAttribute(b,a);c.setAttribute(b,a)};return a},fontMetrics:function(a,b){a=a||this.style.fontSize;if(b&&
T.getComputedStyle)b=b.element||b,a=T.getComputedStyle(b,"").fontSize;var a=/px/.test(a)?C(a):/em/.test(a)?parseFloat(a)*12:12,c=a<24?a+3:w(a*1.2),d=w(c*0.8);return{h:c,b:d,f:a}},rotCorr:function(a,b,c){var d=a;b&&c&&(d=v(d*ba(b*ra),4));return{x:-a/3*ga(b*ra),y:d}},label:function(a,b,c,d,e,f,g,h,i){function j(){var a,b;a=q.element.style;J=(oa===void 0||v===void 0||o.styles.textAlign)&&s(q.textStr)&&q.getBBox();o.width=(oa||J.width||0)+2*u+A;o.height=(v||J.height||0)+2*u;D=u+m.fontMetrics(a&&a.fontSize,
q).b;if(E){if(!t)a=w(-p*u),b=h?-D:0,o.box=t=d?m.symbol(d,a,b,o.width,o.height,L):m.rect(a,b,o.width,o.height,0,L[Yb]),t.attr("fill",Z).add(o);t.isImg||t.attr(x({width:w(o.width),height:w(o.height)},L));L=null}}function k(){var a=o.styles,a=a&&a.textAlign,b=A+u*(1-p),c;c=h?0:D;if(s(oa)&&J&&(a==="center"||a==="right"))b+={center:0.5,right:1}[a]*(oa-J.width);if(b!==q.x||c!==q.y)q.attr("x",b),c!==r&&q.attr(q.element.nodeName==="SPAN"?"y":"translateY",c);q.x=b;q.y=c}function l(a,b){t?t.attr(a,b):L[a]=
b}var m=this,o=m.g(i),q=m.text("",0,0,g).attr({zIndex:1}),t,J,p=0,u=3,A=0,oa,v,B,Jb,z=0,L={},D,E;o.onAdd=function(){q.add(o);o.attr({text:a||a===0?a:"",x:b,y:c});t&&s(e)&&o.attr({anchorX:e,anchorY:f})};o.widthSetter=function(a){oa=a};o.heightSetter=function(a){v=a};o.paddingSetter=function(a){if(s(a)&&a!==u)u=o.padding=a,k()};o.paddingLeftSetter=function(a){s(a)&&a!==A&&(A=a,k())};o.alignSetter=function(a){p={left:0,center:0.5,right:1}[a]};o.textSetter=function(a){a!==r&&q.textSetter(a);j();k()};
o["stroke-widthSetter"]=function(a,b){a&&(E=!0);z=a%2/2;l(b,a)};o.strokeSetter=o.fillSetter=o.rSetter=function(a,b){b==="fill"&&a&&(E=!0);l(b,a)};o.anchorXSetter=function(a,b){e=a;l(b,a+z-B)};o.anchorYSetter=function(a,b){f=a;l(b,a-Jb)};o.xSetter=function(a){o.x=a;p&&(a-=p*((oa||J.width)+u));B=w(a);o.attr("translateX",B)};o.ySetter=function(a){Jb=o.y=w(a);o.attr("translateY",Jb)};var C=o.css;return x(o,{css:function(a){if(a){var b={},a=y(a);n(o.textProps,function(c){a[c]!==r&&(b[c]=a[c],delete a[c])});
q.css(b)}return C.call(o,a)},getBBox:function(){return{width:J.width+2*u,height:J.height+2*u,x:J.x-u,y:J.y-u}},shadow:function(a){t&&t.shadow(a);return o},destroy:function(){U(o.element,"mouseenter");U(o.element,"mouseleave");q&&(q=q.destroy());t&&(t=t.destroy());$.prototype.destroy.call(o);o=m=j=k=l=null}})}};Wa=na;x($.prototype,{htmlCss:function(a){var b=this.element;if(b=a&&b.tagName==="SPAN"&&a.width)delete a.width,this.textWidth=b,this.updateTransform();if(a&&a.textOverflow==="ellipsis")a.whiteSpace=
"nowrap",a.overflow="hidden";this.styles=x(this.styles,a);M(this.element,a);return this},htmlGetBBox:function(){var a=this.element;if(a.nodeName==="text")a.style.position="absolute";return{x:a.offsetLeft,y:a.offsetTop,width:a.offsetWidth,height:a.offsetHeight}},htmlUpdateTransform:function(){if(this.added){var a=this.renderer,b=this.element,c=this.translateX||0,d=this.translateY||0,e=this.x||0,f=this.y||0,g=this.textAlign||"left",h={left:0,center:0.5,right:1}[g],i=this.shadows,j=this.styles;M(b,{marginLeft:c,
marginTop:d});i&&n(i,function(a){M(a,{marginLeft:c+1,marginTop:d+1})});this.inverted&&n(b.childNodes,function(c){a.invertChild(c,b)});if(b.tagName==="SPAN"){var k=this.rotation,l,m=C(this.textWidth),o=[k,g,b.innerHTML,this.textWidth].join(",");if(o!==this.cTT){l=a.fontMetrics(b.style.fontSize).b;s(k)&&this.setSpanRotation(k,h,l);i=p(this.elemWidth,b.offsetWidth);if(i>m&&/[ \-]/.test(b.textContent||b.innerText))M(b,{width:m+"px",display:"block",whiteSpace:j&&j.whiteSpace||"normal"}),i=m;this.getSpanCorrection(i,
l,h,k,g)}M(b,{left:e+(this.xCorr||0)+"px",top:f+(this.yCorr||0)+"px"});if(Gb)l=b.offsetHeight;this.cTT=o}}else this.alignOnAdd=!0},setSpanRotation:function(a,b,c){var d={},e=Ea?"-ms-transform":Gb?"-webkit-transform":Va?"MozTransform":Rb?"-o-transform":"";d[e]=d.transform="rotate("+a+"deg)";d[e+(Va?"Origin":"-origin")]=d.transformOrigin=b*100+"% "+c+"px";M(this.element,d)},getSpanCorrection:function(a,b,c){this.xCorr=-a*c;this.yCorr=-b}});x(na.prototype,{html:function(a,b,c){var d=this.createElement("span"),
e=d.element,f=d.renderer;d.textSetter=function(a){a!==e.innerHTML&&delete this.bBox;e.innerHTML=this.textStr=a};d.xSetter=d.ySetter=d.alignSetter=d.rotationSetter=function(a,b){b==="align"&&(b="textAlign");d[b]=a;d.htmlUpdateTransform()};d.attr({text:a,x:w(b),y:w(c)}).css({position:"absolute",fontFamily:this.style.fontFamily,fontSize:this.style.fontSize});e.style.whiteSpace="nowrap";d.css=d.htmlCss;if(f.isSVG)d.add=function(a){var b,c=f.box.parentNode,j=[];if(this.parentGroup=a){if(b=a.div,!b){for(;a;)j.push(a),
a=a.parentGroup;n(j.reverse(),function(a){var d;b=a.div=a.div||aa(Ua,{className:W(a.element,"class")},{position:"absolute",left:(a.translateX||0)+"px",top:(a.translateY||0)+"px"},b||c);d=b.style;x(a,{translateXSetter:function(b,c){d.left=b+"px";a[c]=b;a.doTransform=!0},translateYSetter:function(b,c){d.top=b+"px";a[c]=b;a.doTransform=!0},visibilitySetter:function(a,b){d[b]=a}})})}}else b=c;b.appendChild(e);d.added=!0;d.alignOnAdd&&d.htmlUpdateTransform();return d};return d}});var ib;if(!ea&&!ma)G=
{init:function(a,b){var c=["<",b,' filled="f" stroked="f"'],d=["position: ","absolute",";"],e=b===Ua;(b==="shape"||e)&&d.push("left:0;top:0;width:1px;height:1px;");d.push("visibility: ",e?"hidden":"visible");c.push(' style="',d.join(""),'"/>');if(b)c=e||b==="span"||b==="img"?c.join(""):a.prepVML(c),this.element=aa(c);this.renderer=a},add:function(a){var b=this.renderer,c=this.element,d=b.box,d=a?a.element||a:d;a&&a.inverted&&b.invertChild(c,d);d.appendChild(c);this.added=!0;this.alignOnAdd&&!this.deferUpdateTransform&&
this.updateTransform();if(this.onAdd)this.onAdd();return this},updateTransform:$.prototype.htmlUpdateTransform,setSpanRotation:function(){var a=this.rotation,b=ba(a*ra),c=ga(a*ra);M(this.element,{filter:a?["progid:DXImageTransform.Microsoft.Matrix(M11=",b,", M12=",-c,", M21=",c,", M22=",b,", sizingMethod='auto expand')"].join(""):Z})},getSpanCorrection:function(a,b,c,d,e){var f=d?ba(d*ra):1,g=d?ga(d*ra):0,h=p(this.elemHeight,this.element.offsetHeight),i;this.xCorr=f<0&&-a;this.yCorr=g<0&&-h;i=f*g<
0;this.xCorr+=g*b*(i?1-c:c);this.yCorr-=f*b*(d?i?c:1-c:1);e&&e!=="left"&&(this.xCorr-=a*c*(f<0?-1:1),d&&(this.yCorr-=h*c*(g<0?-1:1)),M(this.element,{textAlign:e}))},pathToVML:function(a){for(var b=a.length,c=[];b--;)if(sa(a[b]))c[b]=w(a[b]*10)-5;else if(a[b]==="Z")c[b]="x";else if(c[b]=a[b],a.isArc&&(a[b]==="wa"||a[b]==="at"))c[b+5]===c[b+7]&&(c[b+7]+=a[b+7]>a[b+5]?1:-1),c[b+6]===c[b+8]&&(c[b+8]+=a[b+8]>a[b+6]?1:-1);return c.join(" ")||"x"},clip:function(a){var b=this,c;a?(c=a.members,ua(c,b),c.push(b),
b.destroyClip=function(){ua(c,b)},a=a.getCSS(b)):(b.destroyClip&&b.destroyClip(),a={clip:nb?"inherit":"rect(auto)"});return b.css(a)},css:$.prototype.htmlCss,safeRemoveChild:function(a){a.parentNode&&Ta(a)},destroy:function(){this.destroyClip&&this.destroyClip();return $.prototype.destroy.apply(this)},on:function(a,b){this.element["on"+a]=function(){var a=T.event;a.target=a.srcElement;b(a)};return this},cutOffPath:function(a,b){var c,a=a.split(/[ ,]/);c=a.length;if(c===9||c===11)a[c-4]=a[c-2]=C(a[c-
2])-10*b;return a.join(" ")},shadow:function(a,b,c){var d=[],e,f=this.element,g=this.renderer,h,i=f.style,j,k=f.path,l,m,o,q;k&&typeof k.value!=="string"&&(k="x");m=k;if(a){o=p(a.width,3);q=(a.opacity||0.15)/o;for(e=1;e<=3;e++){l=o*2+1-2*e;c&&(m=this.cutOffPath(k.value,l+0.5));j=['<shape isShadow="true" strokeweight="',l,'" filled="false" path="',m,'" coordsize="10 10" style="',f.style.cssText,'" />'];h=aa(g.prepVML(j),null,{left:C(i.left)+p(a.offsetX,1),top:C(i.top)+p(a.offsetY,1)});if(c)h.cutOff=
l+1;j=['<stroke color="',a.color||"black",'" opacity="',q*e,'"/>'];aa(g.prepVML(j),null,null,h);b?b.element.appendChild(h):f.parentNode.insertBefore(h,f);d.push(h)}this.shadows=d}return this},updateShadows:ha,setAttr:function(a,b){nb?this.element[a]=b:this.element.setAttribute(a,b)},classSetter:function(a){this.element.className=a},dashstyleSetter:function(a,b,c){(c.getElementsByTagName("stroke")[0]||aa(this.renderer.prepVML(["<stroke/>"]),null,null,c))[b]=a||"solid";this[b]=a},dSetter:function(a,
b,c){var d=this.shadows,a=a||[];this.d=a.join&&a.join(" ");c.path=a=this.pathToVML(a);if(d)for(c=d.length;c--;)d[c].path=d[c].cutOff?this.cutOffPath(a,d[c].cutOff):a;this.setAttr(b,a)},fillSetter:function(a,b,c){var d=c.nodeName;if(d==="SPAN")c.style.color=a;else if(d!=="IMG")c.filled=a!==Z,this.setAttr("fillcolor",this.renderer.color(a,c,b,this))},opacitySetter:ha,rotationSetter:function(a,b,c){c=c.style;this[b]=c[b]=a;c.left=-w(ga(a*ra)+1)+"px";c.top=w(ba(a*ra))+"px"},strokeSetter:function(a,b,
c){this.setAttr("strokecolor",this.renderer.color(a,c,b))},"stroke-widthSetter":function(a,b,c){c.stroked=!!a;this[b]=a;sa(a)&&(a+="px");this.setAttr("strokeweight",a)},titleSetter:function(a,b){this.setAttr(b,a)},visibilitySetter:function(a,b,c){a==="inherit"&&(a="visible");this.shadows&&n(this.shadows,function(c){c.style[b]=a});c.nodeName==="DIV"&&(a=a==="hidden"?"-999em":0,nb||(c.style[b]=a?"visible":"hidden"),b="top");c.style[b]=a},xSetter:function(a,b,c){this[b]=a;b==="x"?b="left":b==="y"&&(b=
"top");this.updateClipping?(this[b]=a,this.updateClipping()):c.style[b]=a},zIndexSetter:function(a,b,c){c.style[b]=a}},z.VMLElement=G=ja($,G),G.prototype.ySetter=G.prototype.widthSetter=G.prototype.heightSetter=G.prototype.xSetter,G={Element:G,isIE8:Ha.indexOf("MSIE 8.0")>-1,init:function(a,b,c,d){var e;this.alignedObjects=[];d=this.createElement(Ua).css(x(this.getStyle(d),{position:"relative"}));e=d.element;a.appendChild(d.element);this.isVML=!0;this.box=e;this.boxWrapper=d;this.cache={};this.setSize(b,
c,!1);if(!E.namespaces.hcv){E.namespaces.add("hcv","urn:schemas-microsoft-com:vml");try{E.createStyleSheet().cssText="hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "}catch(f){E.styleSheets[0].cssText+="hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "}}},isHidden:function(){return!this.box.offsetWidth},clipRect:function(a,b,c,d){var e=this.createElement(),f=ia(a);return x(e,{members:[],
count:0,left:(f?a.x:a)+1,top:(f?a.y:b)+1,width:(f?a.width:c)-1,height:(f?a.height:d)-1,getCSS:function(a){var b=a.element,c=b.nodeName,a=a.inverted,d=this.top-(c==="shape"?b.offsetTop:0),e=this.left,b=e+this.width,f=d+this.height,d={clip:"rect("+w(a?e:d)+"px,"+w(a?f:b)+"px,"+w(a?b:f)+"px,"+w(a?d:e)+"px)"};!a&&nb&&c==="DIV"&&x(d,{width:b+"px",height:f+"px"});return d},updateClipping:function(){n(e.members,function(a){a.element&&a.css(e.getCSS(a))})}})},color:function(a,b,c,d){var e=this,f,g=/^rgba/,
h,i,j=Z;a&&a.linearGradient?i="gradient":a&&a.radialGradient&&(i="pattern");if(i){var k,l,m=a.linearGradient||a.radialGradient,o,q,t,J,p,u="",a=a.stops,A,oa=[],r=function(){h=['<fill colors="'+oa.join(",")+'" opacity="',t,'" o:opacity2="',q,'" type="',i,'" ',u,'focus="100%" method="any" />'];aa(e.prepVML(h),null,null,b)};o=a[0];A=a[a.length-1];o[0]>0&&a.unshift([0,o[1]]);A[0]<1&&a.push([1,A[1]]);n(a,function(a,b){g.test(a[1])?(f=wa(a[1]),k=f.get("rgb"),l=f.get("a")):(k=a[1],l=1);oa.push(a[0]*100+
"% "+k);b?(t=l,J=k):(q=l,p=k)});if(c==="fill")if(i==="gradient")c=m.x1||m[0]||0,a=m.y1||m[1]||0,o=m.x2||m[2]||0,m=m.y2||m[3]||0,u='angle="'+(90-Y.atan((m-a)/(o-c))*180/va)+'"',r();else{var j=m.r,s=j*2,w=j*2,v=m.cx,x=m.cy,y=b.radialReference,B,j=function(){y&&(B=d.getBBox(),v+=(y[0]-B.x)/B.width-0.5,x+=(y[1]-B.y)/B.height-0.5,s*=y[2]/B.width,w*=y[2]/B.height);u='src="'+F.global.VMLRadialGradientURL+'" size="'+s+","+w+'" origin="0.5,0.5" position="'+v+","+x+'" color2="'+p+'" ';r()};d.added?j():d.onAdd=
j;j=J}else j=k}else if(g.test(a)&&b.tagName!=="IMG")f=wa(a),h=["<",c,' opacity="',f.get("a"),'"/>'],aa(this.prepVML(h),null,null,b),j=f.get("rgb");else{j=b.getElementsByTagName(c);if(j.length)j[0].opacity=1,j[0].type="solid";j=a}return j},prepVML:function(a){var b=this.isIE8,a=a.join("");b?(a=a.replace("/>",' xmlns="urn:schemas-microsoft-com:vml" />'),a=a.indexOf('style="')===-1?a.replace("/>",' style="display:inline-block;behavior:url(#default#VML);" />'):a.replace('style="','style="display:inline-block;behavior:url(#default#VML);')):
a=a.replace("<","<hcv:");return a},text:na.prototype.html,path:function(a){var b={coordsize:"10 10"};Ka(a)?b.d=a:ia(a)&&x(b,a);return this.createElement("shape").attr(b)},circle:function(a,b,c){var d=this.symbol("circle");if(ia(a))c=a.r,b=a.y,a=a.x;d.isCircle=!0;d.r=c;return d.attr({x:a,y:b})},g:function(a){var b;a&&(b={className:"highcharts-"+a,"class":"highcharts-"+a});return this.createElement(Ua).attr(b)},image:function(a,b,c,d,e){var f=this.createElement("img").attr({src:a});arguments.length>
1&&f.attr({x:b,y:c,width:d,height:e});return f},createElement:function(a){return a==="rect"?this.symbol(a):na.prototype.createElement.call(this,a)},invertChild:function(a,b){var c=this,d=b.style,e=a.tagName==="IMG"&&a.style;M(a,{flip:"x",left:C(d.width)-(e?C(e.top):1),top:C(d.height)-(e?C(e.left):1),rotation:-90});n(a.childNodes,function(b){c.invertChild(b,a)})},symbols:{arc:function(a,b,c,d,e){var f=e.start,g=e.end,h=e.r||c||d,c=e.innerR,d=ba(f),i=ga(f),j=ba(g),k=ga(g);if(g-f===0)return["x"];f=["wa",
a-h,b-h,a+h,b+h,a+h*d,b+h*i,a+h*j,b+h*k];e.open&&!c&&f.push("e","M",a,b);f.push("at",a-c,b-c,a+c,b+c,a+c*j,b+c*k,a+c*d,b+c*i,"x","e");f.isArc=!0;return f},circle:function(a,b,c,d,e){e&&(c=d=2*e.r);e&&e.isCircle&&(a-=c/2,b-=d/2);return["wa",a,b,a+c,b+d,a+c,b+d/2,a+c,b+d/2,"e"]},rect:function(a,b,c,d,e){return na.prototype.symbols[!s(e)||!e.r?"square":"callout"].call(0,a,b,c,d,e)}}},z.VMLRenderer=ib=function(){this.init.apply(this,arguments)},ib.prototype=y(na.prototype,G),Wa=ib;na.prototype.measureSpanWidth=
function(a,b){var c=E.createElement("span"),d;d=E.createTextNode(a);c.appendChild(d);M(c,b);this.box.appendChild(c);d=c.offsetWidth;Ta(c);return d};var Tb;if(ma)z.CanVGRenderer=G=function(){Ia="http://www.w3.org/1999/xhtml"},G.prototype.symbols={},Tb=function(){function a(){var a=b.length,d;for(d=0;d<a;d++)b[d]();b=[]}var b=[];return{push:function(c,d){b.length===0&&Zb(d,a);b.push(c)}}}(),Wa=G;Za.prototype={addLabel:function(){var a=this.axis,b=a.options,c=a.chart,d=a.categories,e=a.names,f=this.pos,
g=b.labels,h=a.tickPositions,i=f===h[0],j=f===h[h.length-1],e=d?p(d[f],e[f],f):f,d=this.label,h=h.info,k;a.isDatetimeAxis&&h&&(k=b.dateTimeLabelFormats[h.higherRanks[f]||h.unitName]);this.isFirst=i;this.isLast=j;b=a.labelFormatter.call({axis:a,chart:c,isFirst:i,isLast:j,dateTimeLabelFormat:k,value:a.isLog?la(ta(e)):e});s(d)?d&&d.attr({text:b}):(this.labelLength=(this.label=d=s(b)&&g.enabled?c.renderer.text(b,0,0,g.useHTML).css(y(g.style)).add(a.labelGroup):null)&&d.getBBox().width,this.rotation=0)},
getLabelSize:function(){return this.label?this.label.getBBox()[this.axis.horiz?"height":"width"]:0},handleOverflow:function(a){var b=this.axis,c=a.x,d=b.chart.chartWidth,e=b.chart.spacing,f=p(b.labelLeft,e[3]),e=p(b.labelRight,d-e[1]),g=this.label,h=this.rotation,i={left:0,center:0.5,right:1}[b.labelAlign],j=g.getBBox().width,b=b.slotWidth,k;if(h)h<0&&c-i*j<f?k=w(c/ba(h*ra)-f):h>0&&c+i*j>e&&(k=w((d-c)/ba(h*ra)));else{d=c-i*j;c+=i*j;if(d<f)b-=f-d,a.x=f,g.attr({align:"left"});else if(c>e)b-=c-e,a.x=
e,g.attr({align:"right"});j>b&&(k=b)}k&&g.css({width:k,textOverflow:"ellipsis"})},getPosition:function(a,b,c,d){var e=this.axis,f=e.chart,g=d&&f.oldChartHeight||f.chartHeight;return{x:a?e.translate(b+c,null,null,d)+e.transB:e.left+e.offset+(e.opposite?(d&&f.oldChartWidth||f.chartWidth)-e.right-e.left:0),y:a?g-e.bottom+e.offset-(e.opposite?e.height:0):g-e.translate(b+c,null,null,d)-e.transB}},getLabelPosition:function(a,b,c,d,e,f,g,h){var i=this.axis,j=i.transA,k=i.reversed,l=i.staggerLines,m=i.tickRotCorr||
{x:0,y:0},c=p(e.y,m.y+(i.side===2?8:-(c.getBBox().height/2))),a=a+e.x+m.x-(f&&d?f*j*(k?-1:1):0),b=b+c-(f&&!d?f*j*(k?1:-1):0);l&&(b+=g/(h||1)%l*(i.labelOffset/l));return{x:a,y:w(b)}},getMarkPath:function(a,b,c,d,e,f){return f.crispLine(["M",a,b,"L",a+(e?0:-c),b+(e?c:0)],d)},render:function(a,b,c){var d=this.axis,e=d.options,f=d.chart.renderer,g=d.horiz,h=this.type,i=this.label,j=this.pos,k=e.labels,l=this.gridLine,m=h?h+"Grid":"grid",o=h?h+"Tick":"tick",q=e[m+"LineWidth"],t=e[m+"LineColor"],J=e[m+
"LineDashStyle"],n=e[o+"Length"],m=e[o+"Width"]||0,u=e[o+"Color"],A=e[o+"Position"],o=this.mark,oa=k.step,s=!0,w=d.tickmarkOffset,v=this.getPosition(g,j,w,b),x=v.x,v=v.y,y=g&&x===d.pos+d.len||!g&&v===d.pos?-1:1,c=p(c,1);this.isActive=!0;if(q){j=d.getPlotLinePath(j+w,q*y,b,!0);if(l===r){l={stroke:t,"stroke-width":q};if(J)l.dashstyle=J;if(!h)l.zIndex=1;if(b)l.opacity=0;this.gridLine=l=q?f.path(j).attr(l).add(d.gridGroup):null}if(!b&&l&&j)l[this.isNew?"attr":"animate"]({d:j,opacity:c})}if(m&&n)A==="inside"&&
(n=-n),d.opposite&&(n=-n),h=this.getMarkPath(x,v,n,m*y,g,f),o?o.animate({d:h,opacity:c}):this.mark=f.path(h).attr({stroke:u,"stroke-width":m,opacity:c}).add(d.axisGroup);if(i&&!isNaN(x))i.xy=v=this.getLabelPosition(x,v,i,g,k,w,a,oa),this.isFirst&&!this.isLast&&!p(e.showFirstLabel,1)||this.isLast&&!this.isFirst&&!p(e.showLastLabel,1)?s=!1:g&&!d.isRadial&&!k.step&&!k.rotation&&!b&&c!==0&&this.handleOverflow(v),oa&&a%oa&&(s=!1),s&&!isNaN(v.y)?(v.opacity=c,i[this.isNew?"attr":"animate"](v),this.isNew=
!1):i.attr("y",-9999)},destroy:function(){Na(this,this.axis)}};z.PlotLineOrBand=function(a,b){this.axis=a;if(b)this.options=b,this.id=b.id};z.PlotLineOrBand.prototype={render:function(){var a=this,b=a.axis,c=b.horiz,d=a.options,e=d.label,f=a.label,g=d.width,h=d.to,i=d.from,j=s(i)&&s(h),k=d.value,l=d.dashStyle,m=a.svgElem,o=[],q,t=d.color,p=d.zIndex,n=d.events,u={},A=b.chart.renderer;b.isLog&&(i=La(i),h=La(h),k=La(k));if(g){if(o=b.getPlotLinePath(k,g),u={stroke:t,"stroke-width":g},l)u.dashstyle=l}else if(j){o=
b.getPlotBandPath(i,h,d);if(t)u.fill=t;if(d.borderWidth)u.stroke=d.borderColor,u["stroke-width"]=d.borderWidth}else return;if(s(p))u.zIndex=p;if(m)if(o)m.animate({d:o},null,m.onGetPath);else{if(m.hide(),m.onGetPath=function(){m.show()},f)a.label=f=f.destroy()}else if(o&&o.length&&(a.svgElem=m=A.path(o).attr(u).add(),n))for(q in d=function(b){m.on(b,function(c){n[b].apply(a,[c])})},n)d(q);if(e&&s(e.text)&&o&&o.length&&b.width>0&&b.height>0){e=y({align:c&&j&&"center",x:c?!j&&4:10,verticalAlign:!c&&
j&&"middle",y:c?j?16:10:j?6:-4,rotation:c&&!j&&90},e);if(!f){u={align:e.textAlign||e.align,rotation:e.rotation};if(s(p))u.zIndex=p;a.label=f=A.text(e.text,0,0,e.useHTML).attr(u).css(e.style).add()}b=[o[1],o[4],j?o[6]:o[1]];j=[o[2],o[5],j?o[7]:o[2]];o=Sa(b);c=Sa(j);f.align(e,!1,{x:o,y:c,width:Fa(b)-o,height:Fa(j)-c});f.show()}else f&&f.hide();return a},destroy:function(){ua(this.axis.plotLinesAndBands,this);delete this.axis;Na(this)}};var O=z.Axis=function(){this.init.apply(this,arguments)};O.prototype=
{defaultOptions:{dateTimeLabelFormats:{millisecond:"%H:%M:%S.%L",second:"%H:%M:%S",minute:"%H:%M",hour:"%H:%M",day:"%e. %b",week:"%e. %b",month:"%b '%y",year:"%Y"},endOnTick:!1,gridLineColor:"#D8D8D8",labels:{enabled:!0,style:{color:"#606060",cursor:"default",fontSize:"11px"},x:0,y:15},lineColor:"#C0D0E0",lineWidth:1,minPadding:0.01,maxPadding:0.01,minorGridLineColor:"#E0E0E0",minorGridLineWidth:1,minorTickColor:"#A0A0A0",minorTickLength:2,minorTickPosition:"outside",startOfWeek:1,startOnTick:!1,
tickColor:"#C0D0E0",tickLength:10,tickmarkPlacement:"between",tickPixelInterval:100,tickPosition:"outside",tickWidth:1,title:{align:"middle",style:{color:"#707070"}},type:"linear"},defaultYAxisOptions:{endOnTick:!0,gridLineWidth:1,tickPixelInterval:72,showLastLabel:!0,labels:{x:-8,y:3},lineWidth:0,maxPadding:0.05,minPadding:0.05,startOnTick:!0,tickWidth:0,title:{rotation:270,text:"Values"},stackLabels:{enabled:!1,formatter:function(){return z.numberFormat(this.total,-1)},style:y(V.line.dataLabels.style,
{color:"#000000"})}},defaultLeftAxisOptions:{labels:{x:-15,y:null},title:{rotation:270}},defaultRightAxisOptions:{labels:{x:15,y:null},title:{rotation:90}},defaultBottomAxisOptions:{labels:{autoRotation:[-45],x:0,y:null},title:{rotation:0}},defaultTopAxisOptions:{labels:{autoRotation:[-45],x:0,y:-15},title:{rotation:0}},init:function(a,b){var c=b.isX;this.horiz=a.inverted?!c:c;this.coll=(this.isXAxis=c)?"xAxis":"yAxis";this.opposite=b.opposite;this.side=b.side||(this.horiz?this.opposite?0:2:this.opposite?
1:3);this.setOptions(b);var d=this.options,e=d.type;this.labelFormatter=d.labels.formatter||this.defaultLabelFormatter;this.userOptions=b;this.minPixelPadding=0;this.chart=a;this.reversed=d.reversed;this.zoomEnabled=d.zoomEnabled!==!1;this.categories=d.categories||e==="category";this.names=this.names||[];this.isLog=e==="logarithmic";this.isDatetimeAxis=e==="datetime";this.isLinked=s(d.linkedTo);this.ticks={};this.labelEdge=[];this.minorTicks={};this.plotLinesAndBands=[];this.alternateBands={};this.len=
0;this.minRange=this.userMinRange=d.minRange||d.maxZoom;this.range=d.range;this.offset=d.offset||0;this.stacks={};this.oldStacks={};this.min=this.max=null;this.crosshair=p(d.crosshair,pa(a.options.tooltip.crosshairs)[c?0:1],!1);var f,d=this.options.events;Oa(this,a.axes)===-1&&(c&&!this.isColorAxis?a.axes.splice(a.xAxis.length,0,this):a.axes.push(this),a[this.coll].push(this));this.series=this.series||[];if(a.inverted&&c&&this.reversed===r)this.reversed=!0;this.removePlotLine=this.removePlotBand=
this.removePlotBandOrLine;for(f in d)D(this,f,d[f]);if(this.isLog)this.val2lin=La,this.lin2val=ta},setOptions:function(a){this.options=y(this.defaultOptions,this.isXAxis?{}:this.defaultYAxisOptions,[this.defaultTopAxisOptions,this.defaultRightAxisOptions,this.defaultBottomAxisOptions,this.defaultLeftAxisOptions][this.side],y(F[this.coll],a))},defaultLabelFormatter:function(){var a=this.axis,b=this.value,c=a.categories,d=this.dateTimeLabelFormat,e=F.lang.numericSymbols,f=e&&e.length,g,h=a.options.labels.format,
a=a.isLog?b:a.tickInterval;if(h)g=Ma(h,this);else if(c)g=b;else if(d)g=ka(d,b);else if(f&&a>=1E3)for(;f--&&g===r;)c=Math.pow(1E3,f+1),a>=c&&e[f]!==null&&(g=z.numberFormat(b/c,-1)+e[f]);g===r&&(g=R(b)>=1E4?z.numberFormat(b,0):z.numberFormat(b,-1,r,""));return g},getSeriesExtremes:function(){var a=this,b=a.chart;a.hasVisibleSeries=!1;a.dataMin=a.dataMax=a.ignoreMinPadding=a.ignoreMaxPadding=null;a.buildStacks&&a.buildStacks();n(a.series,function(c){if(c.visible||!b.options.chart.ignoreHiddenSeries){var d;
d=c.options.threshold;var e;a.hasVisibleSeries=!0;a.isLog&&d<=0&&(d=null);if(a.isXAxis){if(d=c.xData,d.length)a.dataMin=B(p(a.dataMin,d[0]),Sa(d)),a.dataMax=v(p(a.dataMax,d[0]),Fa(d))}else{c.getExtremes();e=c.dataMax;c=c.dataMin;if(s(c)&&s(e))a.dataMin=B(p(a.dataMin,c),c),a.dataMax=v(p(a.dataMax,e),e);if(s(d))if(a.dataMin>=d)a.dataMin=d,a.ignoreMinPadding=!0;else if(a.dataMax<d)a.dataMax=d,a.ignoreMaxPadding=!0}}})},translate:function(a,b,c,d,e,f){var g=1,h=0,i=d?this.oldTransA:this.transA,d=d?this.oldMin:
this.min,j=this.minPixelPadding,e=(this.doPostTranslate||this.isLog&&e)&&this.lin2val;if(!i)i=this.transA;if(c)g*=-1,h=this.len;this.reversed&&(g*=-1,h-=g*(this.sector||this.len));b?(a=a*g+h,a-=j,a=a/i+d,e&&(a=this.lin2val(a))):(e&&(a=this.val2lin(a)),f==="between"&&(f=0.5),a=g*(a-d)*i+h+g*j+(sa(f)?i*f*this.pointRange:0));return a},toPixels:function(a,b){return this.translate(a,!1,!this.horiz,null,!0)+(b?0:this.pos)},toValue:function(a,b){return this.translate(a-(b?0:this.pos),!0,!this.horiz,null,
!0)},getPlotLinePath:function(a,b,c,d,e){var f=this.chart,g=this.left,h=this.top,i,j,k=c&&f.oldChartHeight||f.chartHeight,l=c&&f.oldChartWidth||f.chartWidth,m;i=this.transB;var o=function(a,b,c){if(a<b||a>c)d?a=B(v(b,a),c):m=!0;return a},e=p(e,this.translate(a,null,null,c)),a=c=w(e+i);i=j=w(k-e-i);isNaN(e)?m=!0:this.horiz?(i=h,j=k-this.bottom,a=c=o(a,g,g+this.width)):(a=g,c=l-this.right,i=j=o(i,h,h+this.height));return m&&!d?null:f.renderer.crispLine(["M",a,i,"L",c,j],b||1)},getLinearTickPositions:function(a,
b,c){var d,e=la(X(b/a)*a),f=la(za(c/a)*a),g=[];if(b===c&&sa(b))return[b];for(b=e;b<=f;){g.push(b);b=la(b+a);if(b===d)break;d=b}return g},getMinorTickPositions:function(){var a=this.options,b=this.tickPositions,c=this.minorTickInterval,d=[],e,f=this.min;e=this.max;var g=e-f;if(g&&g/c<this.len/3)if(this.isLog){a=b.length;for(e=1;e<a;e++)d=d.concat(this.getLogTickPositions(c,b[e-1],b[e],!0))}else if(this.isDatetimeAxis&&a.minorTickInterval==="auto")d=d.concat(this.getTimeTicks(this.normalizeTimeTickInterval(c),
f,e,a.startOfWeek));else for(b=f+(b[0]-f)%c;b<=e;b+=c)d.push(b);this.trimTicks(d);return d},adjustForMinRange:function(){var a=this.options,b=this.min,c=this.max,d,e=this.dataMax-this.dataMin>=this.minRange,f,g,h,i,j;if(this.isXAxis&&this.minRange===r&&!this.isLog)s(a.min)||s(a.max)?this.minRange=null:(n(this.series,function(a){i=a.xData;for(g=j=a.xIncrement?1:i.length-1;g>0;g--)if(h=i[g]-i[g-1],f===r||h<f)f=h}),this.minRange=B(f*5,this.dataMax-this.dataMin));if(c-b<this.minRange){var k=this.minRange;
d=(k-c+b)/2;d=[b-d,p(a.min,b-d)];if(e)d[2]=this.dataMin;b=Fa(d);c=[b+k,p(a.max,b+k)];if(e)c[2]=this.dataMax;c=Sa(c);c-b<k&&(d[0]=c-k,d[1]=p(a.min,c-k),b=Fa(d))}this.min=b;this.max=c},setAxisTranslation:function(a){var b=this,c=b.max-b.min,d=b.axisPointRange||0,e,f=0,g=0,h=b.linkedParent,i=!!b.categories,j=b.transA,k=b.isXAxis;if(k||i||d)if(h?(f=h.minPointOffset,g=h.pointRangePadding):n(b.series,function(a){var h=i?1:k?a.pointRange:b.axisPointRange||0,j=a.options.pointPlacement,q=a.closestPointRange;
h>c&&(h=0);d=v(d,h);b.single||(f=v(f,Ja(j)?0:h/2),g=v(g,j==="on"?0:h));!a.noSharedTooltip&&s(q)&&(e=s(e)?B(e,q):q)}),h=b.ordinalSlope&&e?b.ordinalSlope/e:1,b.minPointOffset=f*=h,b.pointRangePadding=g*=h,b.pointRange=B(d,c),k)b.closestPointRange=e;if(a)b.oldTransA=j;b.translationSlope=b.transA=j=b.len/(c+g||1);b.transB=b.horiz?b.left:b.bottom;b.minPixelPadding=j*f},setTickInterval:function(a){var b=this,c=b.chart,d=b.options,e=b.isLog,f=b.isDatetimeAxis,g=b.isXAxis,h=b.isLinked,i=d.maxPadding,j=d.minPadding,
k=d.tickInterval,l=d.tickPixelInterval,m=b.categories;!f&&!m&&!h&&this.getTickAmount();h?(b.linkedParent=c[b.coll][d.linkedTo],c=b.linkedParent.getExtremes(),b.min=p(c.min,c.dataMin),b.max=p(c.max,c.dataMax),d.type!==b.linkedParent.options.type&&qa(11,1)):(b.min=p(b.userMin,d.min,b.dataMin),b.max=p(b.userMax,d.max,b.dataMax));if(e)!a&&B(b.min,p(b.dataMin,b.min))<=0&&qa(10,1),b.min=la(La(b.min)),b.max=la(La(b.max));if(b.range&&s(b.max))b.userMin=b.min=v(b.min,b.max-b.range),b.userMax=b.max,b.range=
null;b.beforePadding&&b.beforePadding();b.adjustForMinRange();if(!m&&!b.axisPointRange&&!b.usePercentage&&!h&&s(b.min)&&s(b.max)&&(c=b.max-b.min)){if(!s(d.min)&&!s(b.userMin)&&j&&(b.dataMin<0||!b.ignoreMinPadding))b.min-=c*j;if(!s(d.max)&&!s(b.userMax)&&i&&(b.dataMax>0||!b.ignoreMaxPadding))b.max+=c*i}if(sa(d.floor))b.min=v(b.min,d.floor);if(sa(d.ceiling))b.max=B(b.max,d.ceiling);b.tickInterval=b.min===b.max||b.min===void 0||b.max===void 0?1:h&&!k&&l===b.linkedParent.options.tickPixelInterval?b.linkedParent.tickInterval:
p(k,this.tickAmount?(b.max-b.min)/v(this.tickAmount-1,1):void 0,m?1:(b.max-b.min)*l/v(b.len,l));g&&!a&&n(b.series,function(a){a.processData(b.min!==b.oldMin||b.max!==b.oldMax)});b.setAxisTranslation(!0);b.beforeSetTickPositions&&b.beforeSetTickPositions();if(b.postProcessTickInterval)b.tickInterval=b.postProcessTickInterval(b.tickInterval);if(b.pointRange)b.tickInterval=v(b.pointRange,b.tickInterval);a=p(d.minTickInterval,b.isDatetimeAxis&&b.closestPointRange);if(!k&&b.tickInterval<a)b.tickInterval=
a;if(!f&&!e&&!k)b.tickInterval=wb(b.tickInterval,null,vb(b.tickInterval),p(d.allowDecimals,!(b.tickInterval>0.5&&b.tickInterval<5&&b.max>1E3&&b.max<9999)),!!this.tickAmount);if(!this.tickAmount&&this.len)b.tickInterval=b.unsquish();this.setTickPositions()},setTickPositions:function(){var a=this.options,b,c=a.tickPositions,d=a.tickPositioner,e=a.startOnTick,f=a.endOnTick,g;this.tickmarkOffset=this.categories&&a.tickmarkPlacement==="between"&&this.tickInterval===1?0.5:0;this.minorTickInterval=a.minorTickInterval===
"auto"&&this.tickInterval?this.tickInterval/5:a.minorTickInterval;this.tickPositions=b=a.tickPositions&&a.tickPositions.slice();if(!b&&(this.tickPositions=b=this.isDatetimeAxis?this.getTimeTicks(this.normalizeTimeTickInterval(this.tickInterval,a.units),this.min,this.max,a.startOfWeek,this.ordinalPositions,this.closestPointRange,!0):this.isLog?this.getLogTickPositions(this.tickInterval,this.min,this.max):this.getLinearTickPositions(this.tickInterval,this.min,this.max),d&&(d=d.apply(this,[this.min,
this.max]))))this.tickPositions=b=d;if(!this.isLinked)this.trimTicks(b,e,f),this.min===this.max&&s(this.min)&&!this.tickAmount&&(g=!0,this.min-=0.5,this.max+=0.5),this.single=g,!c&&!d&&this.adjustTickAmount()},trimTicks:function(a,b,c){var d=a[0],e=a[a.length-1],f=this.minPointOffset||0;b?this.min=d:this.min-f>d&&a.shift();c?this.max=e:this.max+f<e&&a.pop();a.length===0&&s(d)&&a.push((e+d)/2)},getTickAmount:function(){var a={},b,c=this.options,d=c.tickAmount,e=c.tickPixelInterval;!s(c.tickInterval)&&
this.len<e&&!this.isRadial&&!this.isLog&&c.startOnTick&&c.endOnTick&&(d=2);!d&&this.chart.options.chart.alignTicks!==!1&&c.alignTicks!==!1&&(n(this.chart[this.coll],function(c){var d=c.options,c=c.horiz,d=[c?d.left:d.top,c?d.width:d.height,d.pane].join(",");a[d]?b=!0:a[d]=1}),b&&(d=za(this.len/e)+1));if(d<4)this.finalTickAmt=d,d=5;this.tickAmount=d},adjustTickAmount:function(){var a=this.tickInterval,b=this.tickPositions,c=this.tickAmount,d=this.finalTickAmt,e=b&&b.length;if(e<c){for(;b.length<c;)b.push(la(b[b.length-
1]+a));this.transA*=(e-1)/(c-1);this.max=b[b.length-1]}else e>c&&(this.tickInterval*=2,this.setTickPositions());if(s(d)){for(a=c=b.length;a--;)(d===3&&a%2===1||d<=2&&a>0&&a<c-1)&&b.splice(a,1);this.finalTickAmt=r}},setScale:function(){var a=this.stacks,b,c,d,e;this.oldMin=this.min;this.oldMax=this.max;this.oldAxisLength=this.len;this.setAxisSize();e=this.len!==this.oldAxisLength;n(this.series,function(a){if(a.isDirtyData||a.isDirty||a.xAxis.isDirty)d=!0});if(e||d||this.isLinked||this.forceRedraw||
this.userMin!==this.oldUserMin||this.userMax!==this.oldUserMax){if(!this.isXAxis)for(b in a)for(c in a[b])a[b][c].total=null,a[b][c].cum=0;this.forceRedraw=!1;this.getSeriesExtremes();this.setTickInterval();this.oldUserMin=this.userMin;this.oldUserMax=this.userMax;if(!this.isDirty)this.isDirty=e||this.min!==this.oldMin||this.max!==this.oldMax}else if(!this.isXAxis){if(this.oldStacks)a=this.stacks=this.oldStacks;for(b in a)for(c in a[b])a[b][c].cum=a[b][c].total}},setExtremes:function(a,b,c,d,e){var f=
this,g=f.chart,c=p(c,!0);n(f.series,function(a){delete a.kdTree});e=x(e,{min:a,max:b});K(f,"setExtremes",e,function(){f.userMin=a;f.userMax=b;f.eventArgs=e;f.isDirtyExtremes=!0;c&&g.redraw(d)})},zoom:function(a,b){var c=this.dataMin,d=this.dataMax,e=this.options;this.allowZoomOutside||(s(c)&&a<=B(c,p(e.min,c))&&(a=r),s(d)&&b>=v(d,p(e.max,d))&&(b=r));this.displayBtn=a!==r||b!==r;this.setExtremes(a,b,!1,r,{trigger:"zoom"});return!0},setAxisSize:function(){var a=this.chart,b=this.options,c=b.offsetLeft||
0,d=this.horiz,e=p(b.width,a.plotWidth-c+(b.offsetRight||0)),f=p(b.height,a.plotHeight),g=p(b.top,a.plotTop),b=p(b.left,a.plotLeft+c),c=/%$/;c.test(f)&&(f=parseFloat(f)/100*a.plotHeight);c.test(g)&&(g=parseFloat(g)/100*a.plotHeight+a.plotTop);this.left=b;this.top=g;this.width=e;this.height=f;this.bottom=a.chartHeight-f-g;this.right=a.chartWidth-e-b;this.len=v(d?e:f,0);this.pos=d?b:g},getExtremes:function(){var a=this.isLog;return{min:a?la(ta(this.min)):this.min,max:a?la(ta(this.max)):this.max,dataMin:this.dataMin,
dataMax:this.dataMax,userMin:this.userMin,userMax:this.userMax}},getThreshold:function(a){var b=this.isLog,c=b?ta(this.min):this.min,b=b?ta(this.max):this.max;c>a||a===null?a=c:b<a&&(a=b);return this.translate(a,0,1,0,1)},autoLabelAlign:function(a){a=(p(a,0)-this.side*90+720)%360;return a>15&&a<165?"right":a>195&&a<345?"left":"center"},unsquish:function(){var a=this.ticks,b=this.options.labels,c=this.horiz,d=this.tickInterval,e=d,f=this.len/(((this.categories?1:0)+this.max-this.min)/d),g,h=b.rotation,
i=this.chart.renderer.fontMetrics(b.style.fontSize,a[0]&&a[0].label),j,k=Number.MAX_VALUE,l,m=function(a){a/=f||1;a=a>1?za(a):1;return a*d};c?(l=s(h)?[h]:f<80&&!b.staggerLines&&!b.step&&b.autoRotation)&&n(l,function(a){var b;if(a===h||a&&a>=-90&&a<=90)j=m(R(i.h/ga(ra*a))),b=j+R(a/360),b<k&&(k=b,g=a,e=j)}):e=m(i.h);this.autoRotation=l;this.labelRotation=g;return e},renderUnsquish:function(){var a=this.chart,b=a.renderer,c=this.tickPositions,d=this.ticks,e=this.options.labels,f=this.horiz,g=a.margin,
h=this.slotWidth=f&&!e.step&&!e.rotation&&(this.staggerLines||1)*a.plotWidth/c.length||!f&&(g[3]&&g[3]-a.spacing[3]||a.chartWidth*0.33),i=v(1,w(h-2*(e.padding||5))),j={},g=b.fontMetrics(e.style.fontSize,d[0]&&d[0].label),k,l=0;if(!Ja(e.rotation))j.rotation=e.rotation;if(this.autoRotation)n(c,function(a){if((a=d[a])&&a.labelLength>l)l=a.labelLength}),l>i&&l>g.h?j.rotation=this.labelRotation:this.labelRotation=0;else if(h){k={width:i+"px",textOverflow:"clip"};for(h=c.length;!f&&h--;)if(i=c[h],(i=d[i].label)&&
this.len/c.length-4<i.getBBox().height)i.specCss={textOverflow:"ellipsis"}}j.rotation&&(k={width:(l>a.chartHeight*0.5?a.chartHeight*0.33:a.chartHeight)+"px",textOverflow:"ellipsis"});this.labelAlign=j.align=e.align||this.autoLabelAlign(this.labelRotation);n(c,function(a){var b=(a=d[a])&&a.label;if(b)k&&b.css(y(k,b.specCss)),delete b.specCss,b.attr(j),a.rotation=j.rotation});this.tickRotCorr=b.rotCorr(g.b,this.labelRotation||0,this.side===2)},getOffset:function(){var a=this,b=a.chart,c=b.renderer,
d=a.options,e=a.tickPositions,f=a.ticks,g=a.horiz,h=a.side,i=b.inverted?[1,0,3,2][h]:h,j,k,l=0,m,o=0,q=d.title,t=d.labels,J=0,N=b.axisOffset,b=b.clipOffset,u=[-1,1,1,-1][h],A;a.hasData=j=a.hasVisibleSeries||s(a.min)&&s(a.max)&&!!e;a.showAxis=k=j||p(d.showEmpty,!0);a.staggerLines=a.horiz&&t.staggerLines;if(!a.axisGroup)a.gridGroup=c.g("grid").attr({zIndex:d.gridZIndex||1}).add(),a.axisGroup=c.g("axis").attr({zIndex:d.zIndex||2}).add(),a.labelGroup=c.g("axis-labels").attr({zIndex:t.zIndex||7}).addClass("highcharts-"+
a.coll.toLowerCase()+"-labels").add();if(j||a.isLinked){if(n(e,function(b){f[b]?f[b].addLabel():f[b]=new Za(a,b)}),a.renderUnsquish(),n(e,function(b){if(h===0||h===2||{1:"left",3:"right"}[h]===a.labelAlign)J=v(f[b].getLabelSize(),J)}),a.staggerLines)J*=a.staggerLines,a.labelOffset=J}else for(A in f)f[A].destroy(),delete f[A];if(q&&q.text&&q.enabled!==!1){if(!a.axisTitle)a.axisTitle=c.text(q.text,0,0,q.useHTML).attr({zIndex:7,rotation:q.rotation||0,align:q.textAlign||{low:"left",middle:"center",high:"right"}[q.align]}).addClass("highcharts-"+
this.coll.toLowerCase()+"-title").css(q.style).add(a.axisGroup),a.axisTitle.isNew=!0;if(k)l=a.axisTitle.getBBox()[g?"height":"width"],m=q.offset,o=s(m)?0:p(q.margin,g?5:10);a.axisTitle[k?"show":"hide"]()}a.offset=u*p(d.offset,N[h]);a.tickRotCorr=a.tickRotCorr||{x:0,y:0};c=h===2?a.tickRotCorr.y:0;g=J+o+(J&&u*(g?p(t.y,a.tickRotCorr.y+8):t.x)-c);a.axisTitleMargin=p(m,g);N[h]=v(N[h],a.axisTitleMargin+l+u*a.offset,g);b[i]=v(b[i],X(d.lineWidth/2)*2)},getLinePath:function(a){var b=this.chart,c=this.opposite,
d=this.offset,e=this.horiz,f=this.left+(c?this.width:0)+d,d=b.chartHeight-this.bottom-(c?this.height:0)+d;c&&(a*=-1);return b.renderer.crispLine(["M",e?this.left:f,e?d:this.top,"L",e?b.chartWidth-this.right:f,e?d:b.chartHeight-this.bottom],a)},getTitlePosition:function(){var a=this.horiz,b=this.left,c=this.top,d=this.len,e=this.options.title,f=a?b:c,g=this.opposite,h=this.offset,i=C(e.style.fontSize||12),d={low:f+(a?0:d),middle:f+d/2,high:f+(a?d:0)}[e.align],b=(a?c+this.height:b)+(a?1:-1)*(g?-1:1)*
this.axisTitleMargin+(this.side===2?i:0);return{x:a?d:b+(g?this.width:0)+h+(e.x||0),y:a?b-(g?this.height:0)+h:d+(e.y||0)}},render:function(){var a=this,b=a.chart,c=b.renderer,d=a.options,e=a.isLog,f=a.isLinked,g=a.tickPositions,h=a.axisTitle,i=a.ticks,j=a.minorTicks,k=a.alternateBands,l=d.stackLabels,m=d.alternateGridColor,o=a.tickmarkOffset,q=d.lineWidth,t,p=b.hasRendered&&s(a.oldMin)&&!isNaN(a.oldMin);t=a.hasData;var N=a.showAxis,u,A;a.labelEdge.length=0;a.overlap=!1;n([i,j,k],function(a){for(var b in a)a[b].isActive=
!1});if(t||f){a.minorTickInterval&&!a.categories&&n(a.getMinorTickPositions(),function(b){j[b]||(j[b]=new Za(a,b,"minor"));p&&j[b].isNew&&j[b].render(null,!0);j[b].render(null,!1,1)});if(g.length&&(n(g,function(b,c){if(!f||b>=a.min&&b<=a.max)i[b]||(i[b]=new Za(a,b)),p&&i[b].isNew&&i[b].render(c,!0,0.1),i[b].render(c)}),o&&(a.min===0||a.single)))i[-1]||(i[-1]=new Za(a,-1,null,!0)),i[-1].render(-1);m&&n(g,function(b,c){if(c%2===0&&b<a.max)k[b]||(k[b]=new z.PlotLineOrBand(a)),u=b+o,A=g[c+1]!==r?g[c+
1]+o:a.max,k[b].options={from:e?ta(u):u,to:e?ta(A):A,color:m},k[b].render(),k[b].isActive=!0});if(!a._addedPlotLB)n((d.plotLines||[]).concat(d.plotBands||[]),function(b){a.addPlotBandOrLine(b)}),a._addedPlotLB=!0}n([i,j,k],function(a){var c,d,e=[],f=Ga?Ga.duration||500:0,g=function(){for(d=e.length;d--;)a[e[d]]&&!a[e[d]].isActive&&(a[e[d]].destroy(),delete a[e[d]])};for(c in a)if(!a[c].isActive)a[c].render(c,!1,0),a[c].isActive=!1,e.push(c);a===k||!b.hasRendered||!f?g():f&&setTimeout(g,f)});if(q)t=
a.getLinePath(q),a.axisLine?a.axisLine.animate({d:t}):a.axisLine=c.path(t).attr({stroke:d.lineColor,"stroke-width":q,zIndex:7}).add(a.axisGroup),a.axisLine[N?"show":"hide"]();if(h&&N)h[h.isNew?"attr":"animate"](a.getTitlePosition()),h.isNew=!1;l&&l.enabled&&a.renderStackTotals();a.isDirty=!1},redraw:function(){this.render();n(this.plotLinesAndBands,function(a){a.render()});n(this.series,function(a){a.isDirty=!0})},destroy:function(a){var b=this,c=b.stacks,d,e=b.plotLinesAndBands;a||U(b);for(d in c)Na(c[d]),
c[d]=null;n([b.ticks,b.minorTicks,b.alternateBands],function(a){Na(a)});for(a=e.length;a--;)e[a].destroy();n("stackTotalGroup,axisLine,axisTitle,axisGroup,cross,gridGroup,labelGroup".split(","),function(a){b[a]&&(b[a]=b[a].destroy())});this.cross&&this.cross.destroy()},drawCrosshair:function(a,b){var c,d=this.crosshair,e=d.animation;if(!this.crosshair||(s(b)||!p(this.crosshair.snap,!0))===!1)this.hideCrosshair();else if(p(d.snap,!0)?s(b)&&(c=this.isXAxis?b.plotX:this.len-b.plotY):c=this.horiz?a.chartX-
this.pos:this.len-a.chartY+this.pos,c=this.isRadial?this.getPlotLinePath(this.isXAxis?b.x:p(b.stackY,b.y))||null:this.getPlotLinePath(null,null,null,null,c)||null,c===null)this.hideCrosshair();else if(this.cross)this.cross.attr({visibility:"visible"})[e?"animate":"attr"]({d:c},e);else{e=this.categories&&!this.isRadial;e={"stroke-width":d.width||(e?this.transA:1),stroke:d.color||(e?"rgba(155,200,255,0.2)":"#C0C0C0"),zIndex:d.zIndex||2};if(d.dashStyle)e.dashstyle=d.dashStyle;this.cross=this.chart.renderer.path(c).attr(e).add()}},
hideCrosshair:function(){this.cross&&this.cross.hide()}};x(O.prototype,{getPlotBandPath:function(a,b){var c=this.getPlotLinePath(b,null,null,!0),d=this.getPlotLinePath(a,null,null,!0);d&&c&&d.toString()!==c.toString()?d.push(c[4],c[5],c[1],c[2]):d=null;return d},addPlotBand:function(a){return this.addPlotBandOrLine(a,"plotBands")},addPlotLine:function(a){return this.addPlotBandOrLine(a,"plotLines")},addPlotBandOrLine:function(a,b){var c=(new z.PlotLineOrBand(this,a)).render(),d=this.userOptions;c&&
(b&&(d[b]=d[b]||[],d[b].push(a)),this.plotLinesAndBands.push(c));return c},removePlotBandOrLine:function(a){for(var b=this.plotLinesAndBands,c=this.options,d=this.userOptions,e=b.length;e--;)b[e].id===a&&b[e].destroy();n([c.plotLines||[],d.plotLines||[],c.plotBands||[],d.plotBands||[]],function(b){for(e=b.length;e--;)b[e].id===a&&ua(b,b[e])})}});O.prototype.getTimeTicks=function(a,b,c,d){var e=[],f={},g=F.global.useUTC,h,i=new fa(b-bb(b)),j=a.unitRange,k=a.count;if(s(b)){i.setMilliseconds(j>=H.second?
0:k*X(i.getMilliseconds()/k));j>=H.second&&i.setSeconds(j>=H.minute?0:k*X(i.getSeconds()/k));if(j>=H.minute)i[Ob](j>=H.hour?0:k*X(i[yb]()/k));if(j>=H.hour)i[Pb](j>=H.day?0:k*X(i[zb]()/k));if(j>=H.day)i[Bb](j>=H.month?1:k*X(i[cb]()/k));j>=H.month&&(i[Cb](j>=H.year?0:k*X(i[db]()/k)),h=i[eb]());j>=H.year&&(h-=h%k,i[Db](h));if(j===H.week)i[Bb](i[cb]()-i[Ab]()+p(d,1));b=1;if(ub||kb)i=i.getTime(),i=new fa(i+bb(i));h=i[eb]();for(var d=i.getTime(),l=i[db](),m=i[cb](),o=(H.day+(g?bb(i):i.getTimezoneOffset()*
6E4))%H.day;d<c;)e.push(d),j===H.year?d=mb(h+b*k,0):j===H.month?d=mb(h,l+b*k):!g&&(j===H.day||j===H.week)?d=mb(h,l,m+b*k*(j===H.day?1:7)):d+=j*k,b++;e.push(d);n(hb(e,function(a){return j<=H.hour&&a%H.day===o}),function(a){f[a]="day"})}e.info=x(a,{higherRanks:f,totalRange:j*k});return e};O.prototype.normalizeTimeTickInterval=function(a,b){var c=b||[["millisecond",[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,2,3,4,6,8,12]],["day",[1,2]],["week",
[1,2]],["month",[1,2,3,4,6]],["year",null]],d=c[c.length-1],e=H[d[0]],f=d[1],g;for(g=0;g<c.length;g++)if(d=c[g],e=H[d[0]],f=d[1],c[g+1]&&a<=(e*f[f.length-1]+H[c[g+1][0]])/2)break;e===H.year&&a<5*e&&(f=[1,2,5]);c=wb(a/e,f,d[0]==="year"?v(vb(a/e),1):1);return{unitRange:e,count:c,unitName:d[0]}};O.prototype.getLogTickPositions=function(a,b,c,d){var e=this.options,f=this.len,g=[];if(!d)this._minorAutoInterval=null;if(a>=0.5)a=w(a),g=this.getLinearTickPositions(a,b,c);else if(a>=0.08)for(var f=X(b),h,
i,j,k,l,e=a>0.3?[1,2,4]:a>0.15?[1,2,4,6,8]:[1,2,3,4,5,6,7,8,9];f<c+1&&!l;f++){i=e.length;for(h=0;h<i&&!l;h++)j=La(ta(f)*e[h]),j>b&&(!d||k<=c)&&k!==r&&g.push(k),k>c&&(l=!0),k=j}else if(b=ta(b),c=ta(c),a=e[d?"minorTickInterval":"tickInterval"],a=p(a==="auto"?null:a,this._minorAutoInterval,(c-b)*(e.tickPixelInterval/(d?5:1))/((d?f/this.tickPositions.length:f)||1)),a=wb(a,null,vb(a)),g=Aa(this.getLinearTickPositions(a,b,c),La),!d)this._minorAutoInterval=a/5;if(!d)this.tickInterval=a;return g};var Kb=
z.Tooltip=function(){this.init.apply(this,arguments)};Kb.prototype={init:function(a,b){var c=b.borderWidth,d=b.style,e=C(d.padding);this.chart=a;this.options=b;this.crosshairs=[];this.now={x:0,y:0};this.isHidden=!0;this.label=a.renderer.label("",0,0,b.shape||"callout",null,null,b.useHTML,null,"tooltip").attr({padding:e,fill:b.backgroundColor,"stroke-width":c,r:b.borderRadius,zIndex:8}).css(d).css({padding:0}).add().attr({y:-9999});ma||this.label.shadow(b.shadow);this.shared=b.shared},destroy:function(){if(this.label)this.label=
this.label.destroy();clearTimeout(this.hideTimer);clearTimeout(this.tooltipTimeout)},move:function(a,b,c,d){var e=this,f=e.now,g=e.options.animation!==!1&&!e.isHidden&&(R(a-f.x)>1||R(b-f.y)>1),h=e.followPointer||e.len>1;x(f,{x:g?(2*f.x+a)/3:a,y:g?(f.y+b)/2:b,anchorX:h?r:g?(2*f.anchorX+c)/3:c,anchorY:h?r:g?(f.anchorY+d)/2:d});e.label.attr(f);if(g)clearTimeout(this.tooltipTimeout),this.tooltipTimeout=setTimeout(function(){e&&e.move(a,b,c,d)},32)},hide:function(a){var b=this,c;clearTimeout(this.hideTimer);
if(!this.isHidden)c=this.chart.hoverPoints,this.hideTimer=setTimeout(function(){b.label.fadeOut();b.isHidden=!0},p(a,this.options.hideDelay,500)),c&&n(c,function(a){a.setState()}),this.chart.hoverPoints=null,this.chart.hoverSeries=null},getAnchor:function(a,b){var c,d=this.chart,e=d.inverted,f=d.plotTop,g=d.plotLeft,h=0,i=0,j,k,a=pa(a);c=a[0].tooltipPos;this.followPointer&&b&&(b.chartX===r&&(b=d.pointer.normalize(b)),c=[b.chartX-d.plotLeft,b.chartY-f]);c||(n(a,function(a){j=a.series.yAxis;k=a.series.xAxis;
h+=a.plotX+(!e&&k?k.left-g:0);i+=(a.plotLow?(a.plotLow+a.plotHigh)/2:a.plotY)+(!e&&j?j.top-f:0)}),h/=a.length,i/=a.length,c=[e?d.plotWidth-i:h,this.shared&&!e&&a.length>1&&b?b.chartY-f:e?d.plotHeight-h:i]);return Aa(c,w)},getPosition:function(a,b,c){var d=this.chart,e=this.distance,f={},g,h=["y",d.chartHeight,b,c.plotY+d.plotTop],i=["x",d.chartWidth,a,c.plotX+d.plotLeft],j=p(c.ttBelow,d.inverted&&!c.negative||!d.inverted&&c.negative),k=function(a,b,c,d){var g=c<d-e,b=d+e+c<b,c=d-e-c;d+=e;if(j&&b)f[a]=
d;else if(!j&&g)f[a]=c;else if(g)f[a]=c;else if(b)f[a]=d;else return!1},l=function(a,b,c,d){if(d<e||d>b-e)return!1;else f[a]=d<c/2?1:d>b-c/2?b-c-2:d-c/2},m=function(a){var b=h;h=i;i=b;g=a},o=function(){k.apply(0,h)!==!1?l.apply(0,i)===!1&&!g&&(m(!0),o()):g?f.x=f.y=0:(m(!0),o())};(d.inverted||this.len>1)&&m();o();return f},defaultFormatter:function(a){var b=this.points||pa(this),c;c=[a.tooltipFooterHeaderFormatter(b[0])];c=c.concat(a.bodyFormatter(b));c.push(a.tooltipFooterHeaderFormatter(b[0],!0));
return c.join("")},refresh:function(a,b){var c=this.chart,d=this.label,e=this.options,f,g,h={},i,j=[];i=e.formatter||this.defaultFormatter;var h=c.hoverPoints,k,l=this.shared;clearTimeout(this.hideTimer);this.followPointer=pa(a)[0].series.tooltipOptions.followPointer;g=this.getAnchor(a,b);f=g[0];g=g[1];l&&(!a.series||!a.series.noSharedTooltip)?(c.hoverPoints=a,h&&n(h,function(a){a.setState()}),n(a,function(a){a.setState("hover");j.push(a.getLabelConfig())}),h={x:a[0].category,y:a[0].y},h.points=j,
this.len=j.length,a=a[0]):h=a.getLabelConfig();i=i.call(h,this);h=a.series;this.distance=p(h.tooltipOptions.distance,16);i===!1?this.hide():(this.isHidden&&(ab(d),d.attr("opacity",1).show()),d.attr({text:i}),k=e.borderColor||a.color||h.color||"#606060",d.attr({stroke:k}),this.updatePosition({plotX:f,plotY:g,negative:a.negative,ttBelow:a.ttBelow}),this.isHidden=!1);K(c,"tooltipRefresh",{text:i,x:f+c.plotLeft,y:g+c.plotTop,borderColor:k})},updatePosition:function(a){var b=this.chart,c=this.label,c=
(this.options.positioner||this.getPosition).call(this,c.width,c.height,a);this.move(w(c.x),w(c.y),a.plotX+b.plotLeft,a.plotY+b.plotTop)},getXDateFormat:function(a,b,c){var d,b=b.dateTimeLabelFormats,e=c&&c.closestPointRange,f,g={millisecond:15,second:12,minute:9,hour:6,day:3},h,i;if(e){h=ka("%m-%d %H:%M:%S.%L",a.x);for(f in H){if(e===H.week&&+ka("%w",a.x)===c.options.startOfWeek&&h.substr(6)==="00:00:00.000"){f="week";break}else if(H[f]>e){f=i;break}else if(g[f]&&h.substr(g[f])!=="01-01 00:00:00.000".substr(g[f]))break;
f!=="week"&&(i=f)}f&&(d=b[f])}else d=b.day;return d||b.year},tooltipFooterHeaderFormatter:function(a,b){var c=b?"footer":"header",d=a.series,e=d.tooltipOptions,f=e.xDateFormat,g=d.xAxis,h=g&&g.options.type==="datetime"&&sa(a.key),c=e[c+"Format"];h&&!f&&(f=this.getXDateFormat(a,e,g));h&&f&&(c=c.replace("{point.key}","{point.key:"+f+"}"));return Ma(c,{point:a,series:d})},bodyFormatter:function(a){return Aa(a,function(a){var c=a.series.tooltipOptions;return(c.pointFormatter||a.point.tooltipFormatter).call(a.point,
c.pointFormat)})}};var xa;$a=E.documentElement.ontouchstart!==r;var Xa=z.Pointer=function(a,b){this.init(a,b)};Xa.prototype={init:function(a,b){var c=b.chart,d=c.events,e=ma?"":c.zoomType,c=a.inverted,f;this.options=b;this.chart=a;this.zoomX=f=/x/.test(e);this.zoomY=e=/y/.test(e);this.zoomHor=f&&!c||e&&c;this.zoomVert=e&&!c||f&&c;this.hasZoom=f||e;this.runChartClick=d&&!!d.click;this.pinchDown=[];this.lastValidTouch={};if(z.Tooltip&&b.tooltip.enabled)a.tooltip=new Kb(a,b.tooltip),this.followTouchMove=
p(b.tooltip.followTouchMove,!0);this.setDOMEvents()},normalize:function(a,b){var c,d,a=a||window.event,a=ac(a);if(!a.target)a.target=a.srcElement;d=a.touches?a.touches.length?a.touches.item(0):a.changedTouches[0]:a;if(!b)this.chartPosition=b=$b(this.chart.container);d.pageX===r?(c=v(a.x,a.clientX-b.left),d=a.y):(c=d.pageX-b.left,d=d.pageY-b.top);return x(a,{chartX:w(c),chartY:w(d)})},getCoordinates:function(a){var b={xAxis:[],yAxis:[]};n(this.chart.axes,function(c){b[c.isXAxis?"xAxis":"yAxis"].push({axis:c,
value:c.toValue(a[c.horiz?"chartX":"chartY"])})});return b},runPointActions:function(a){var b=this.chart,c=b.series,d=b.tooltip,e=d?d.shared:!1,f,g=b.hoverPoint,h=b.hoverSeries,i=b.chartWidth,j=b.chartWidth,k,l=[],m,o;if(!e&&!h)for(f=0;f<c.length;f++)if(c[f].directTouch||!c[f].options.stickyTracking)c=[];(!h||!h.noSharedTooltip)&&(e||!h)?(n(c,function(b){k=b.noSharedTooltip&&e;b.visible&&!k&&p(b.options.enableMouseTracking,!0)&&(o=b.searchPoint(a))&&l.push(o)}),n(l,function(a){if(a&&s(a.plotX)&&s(a.plotY)&&
(a.dist.distX<i||(a.dist.distX===i||a.series.kdDimensions>1)&&a.dist.distR<j))i=a.dist.distX,j=a.dist.distR,m=a})):m=h.directTouch&&g||h&&h.searchPoint(a);if(m&&(m!==g||d&&d.isHidden)){if(e&&!m.series.noSharedTooltip){f=l.length;for(c=m.clientX;f--;)h=l[f].clientX,(l[f].x!==m.x||h!==c||l[f].series.noSharedTooltip)&&l.splice(f,1);l.length&&d&&d.refresh(l,a);n(l,function(b){if(b!==m)b.onMouseOver(a)})}else d&&d.refresh(m,a);m.onMouseOver(a)}else f=h&&h.tooltipOptions.followPointer,d&&f&&!d.isHidden&&
(f=d.getAnchor([{}],a),d.updatePosition({plotX:f[0],plotY:f[1]}));if(d&&!this._onDocumentMouseMove)this._onDocumentMouseMove=function(a){if(ca[xa])ca[xa].pointer.onDocumentMouseMove(a)},D(E,"mousemove",this._onDocumentMouseMove);n(b.axes,function(b){b.drawCrosshair(a,p(m,g))})},reset:function(a,b){var c=this.chart,d=c.hoverSeries,e=c.hoverPoint,f=c.tooltip,g=f&&f.shared?c.hoverPoints:e;(a=a&&f&&g)&&pa(g)[0].plotX===r&&(a=!1);if(a)f.refresh(g),e&&(e.setState(e.state,!0),n(c.axes,function(b){p(b.options.crosshair&&
b.options.crosshair.snap,!0)?b.drawCrosshair(null,a):b.hideCrosshair()}));else{if(e)e.onMouseOut();if(d)d.onMouseOut();f&&f.hide(b);if(this._onDocumentMouseMove)U(E,"mousemove",this._onDocumentMouseMove),this._onDocumentMouseMove=null;n(c.axes,function(a){a.hideCrosshair()});this.hoverX=null}},scaleGroups:function(a,b){var c=this.chart,d;n(c.series,function(e){d=a||e.getPlotBox();e.xAxis&&e.xAxis.zoomEnabled&&(e.group.attr(d),e.markerGroup&&(e.markerGroup.attr(d),e.markerGroup.clip(b?c.clipRect:null)),
e.dataLabelsGroup&&e.dataLabelsGroup.attr(d))});c.clipRect.attr(b||c.clipBox)},dragStart:function(a){var b=this.chart;b.mouseIsDown=a.type;b.cancelClick=!1;b.mouseDownX=this.mouseDownX=a.chartX;b.mouseDownY=this.mouseDownY=a.chartY},drag:function(a){var b=this.chart,c=b.options.chart,d=a.chartX,e=a.chartY,f=this.zoomHor,g=this.zoomVert,h=b.plotLeft,i=b.plotTop,j=b.plotWidth,k=b.plotHeight,l,m=this.mouseDownX,o=this.mouseDownY,q=c.panKey&&a[c.panKey+"Key"];d<h?d=h:d>h+j&&(d=h+j);e<i?e=i:e>i+k&&(e=
i+k);this.hasDragged=Math.sqrt(Math.pow(m-d,2)+Math.pow(o-e,2));if(this.hasDragged>10){l=b.isInsidePlot(m-h,o-i);if(b.hasCartesianSeries&&(this.zoomX||this.zoomY)&&l&&!q&&!this.selectionMarker)this.selectionMarker=b.renderer.rect(h,i,f?1:j,g?1:k,0).attr({fill:c.selectionMarkerFill||"rgba(69,114,167,0.25)",zIndex:7}).add();this.selectionMarker&&f&&(d-=m,this.selectionMarker.attr({width:R(d),x:(d>0?0:d)+m}));this.selectionMarker&&g&&(d=e-o,this.selectionMarker.attr({height:R(d),y:(d>0?0:d)+o}));l&&
!this.selectionMarker&&c.panning&&b.pan(a,c.panning)}},drop:function(a){var b=this,c=this.chart,d=this.hasPinched;if(this.selectionMarker){var e={xAxis:[],yAxis:[],originalEvent:a.originalEvent||a},f=this.selectionMarker,g=f.attr?f.attr("x"):f.x,h=f.attr?f.attr("y"):f.y,i=f.attr?f.attr("width"):f.width,j=f.attr?f.attr("height"):f.height,k;if(this.hasDragged||d)n(c.axes,function(c){if(c.zoomEnabled&&s(c.min)&&(d||b[{xAxis:"zoomX",yAxis:"zoomY"}[c.coll]])){var f=c.horiz,o=a.type==="touchend"?c.minPixelPadding:
0,q=c.toValue((f?g:h)+o),f=c.toValue((f?g+i:h+j)-o);e[c.coll].push({axis:c,min:B(q,f),max:v(q,f)});k=!0}}),k&&K(c,"selection",e,function(a){c.zoom(x(a,d?{animation:!1}:null))});this.selectionMarker=this.selectionMarker.destroy();d&&this.scaleGroups()}if(c)M(c.container,{cursor:c._cursor}),c.cancelClick=this.hasDragged>10,c.mouseIsDown=this.hasDragged=this.hasPinched=!1,this.pinchDown=[]},onContainerMouseDown:function(a){a=this.normalize(a);a.preventDefault&&a.preventDefault();this.dragStart(a)},onDocumentMouseUp:function(a){ca[xa]&&
ca[xa].pointer.drop(a)},onDocumentMouseMove:function(a){var b=this.chart,c=this.chartPosition,a=this.normalize(a,c);c&&!this.inClass(a.target,"highcharts-tracker")&&!b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop)&&this.reset()},onContainerMouseLeave:function(){var a=ca[xa];if(a)a.pointer.reset(),a.pointer.chartPosition=null},onContainerMouseMove:function(a){var b=this.chart;xa=b.index;a=this.normalize(a);a.returnValue=!1;b.mouseIsDown==="mousedown"&&this.drag(a);(this.inClass(a.target,"highcharts-tracker")||
b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop))&&!b.openMenu&&this.runPointActions(a)},inClass:function(a,b){for(var c;a;){if(c=W(a,"class"))if(c.indexOf(b)!==-1)return!0;else if(c.indexOf("highcharts-container")!==-1)return!1;a=a.parentNode}},onTrackerMouseOut:function(a){var b=this.chart.hoverSeries,c=(a=a.relatedTarget||a.toElement)&&a.point&&a.point.series;if(b&&!b.options.stickyTracking&&!this.inClass(a,"highcharts-tooltip")&&c!==b)b.onMouseOut()},onContainerClick:function(a){var b=this.chart,
c=b.hoverPoint,d=b.plotLeft,e=b.plotTop,a=this.normalize(a);a.originalEvent=a;a.cancelBubble=!0;b.cancelClick||(c&&this.inClass(a.target,"highcharts-tracker")?(K(c.series,"click",x(a,{point:c})),b.hoverPoint&&c.firePointEvent("click",a)):(x(a,this.getCoordinates(a)),b.isInsidePlot(a.chartX-d,a.chartY-e)&&K(b,"click",a)))},setDOMEvents:function(){var a=this,b=a.chart.container;b.onmousedown=function(b){a.onContainerMouseDown(b)};b.onmousemove=function(b){a.onContainerMouseMove(b)};b.onclick=function(b){a.onContainerClick(b)};
D(b,"mouseleave",a.onContainerMouseLeave);gb===1&&D(E,"mouseup",a.onDocumentMouseUp);if($a)b.ontouchstart=function(b){a.onContainerTouchStart(b)},b.ontouchmove=function(b){a.onContainerTouchMove(b)},gb===1&&D(E,"touchend",a.onDocumentTouchEnd)},destroy:function(){var a;U(this.chart.container,"mouseleave",this.onContainerMouseLeave);gb||(U(E,"mouseup",this.onDocumentMouseUp),U(E,"touchend",this.onDocumentTouchEnd));clearInterval(this.tooltipTimeout);for(a in this)this[a]=null}};x(z.Pointer.prototype,
{pinchTranslate:function(a,b,c,d,e,f){(this.zoomHor||this.pinchHor)&&this.pinchTranslateDirection(!0,a,b,c,d,e,f);(this.zoomVert||this.pinchVert)&&this.pinchTranslateDirection(!1,a,b,c,d,e,f)},pinchTranslateDirection:function(a,b,c,d,e,f,g,h){var i=this.chart,j=a?"x":"y",k=a?"X":"Y",l="chart"+k,m=a?"width":"height",o=i["plot"+(a?"Left":"Top")],q,t,p=h||1,n=i.inverted,u=i.bounds[a?"h":"v"],A=b.length===1,r=b[0][l],s=c[0][l],v=!A&&b[1][l],w=!A&&c[1][l],x,c=function(){!A&&R(r-v)>20&&(p=h||R(s-w)/R(r-
v));t=(o-s)/p+r;q=i["plot"+(a?"Width":"Height")]/p};c();b=t;b<u.min?(b=u.min,x=!0):b+q>u.max&&(b=u.max-q,x=!0);x?(s-=0.8*(s-g[j][0]),A||(w-=0.8*(w-g[j][1])),c()):g[j]=[s,w];n||(f[j]=t-o,f[m]=q);f=n?1/p:p;e[m]=q;e[j]=b;d[n?a?"scaleY":"scaleX":"scale"+k]=p;d["translate"+k]=f*o+(s-f*r)},pinch:function(a){var b=this,c=b.chart,d=b.pinchDown,e=a.touches,f=e.length,g=b.lastValidTouch,h=b.hasZoom,i=b.selectionMarker,j={},k=f===1&&(b.inClass(a.target,"highcharts-tracker")&&c.runTrackerClick||b.runChartClick),
l={};h&&!k&&a.preventDefault();Aa(e,function(a){return b.normalize(a)});if(a.type==="touchstart")n(e,function(a,b){d[b]={chartX:a.chartX,chartY:a.chartY}}),g.x=[d[0].chartX,d[1]&&d[1].chartX],g.y=[d[0].chartY,d[1]&&d[1].chartY],n(c.axes,function(a){if(a.zoomEnabled){var b=c.bounds[a.horiz?"h":"v"],d=a.minPixelPadding,e=a.toPixels(p(a.options.min,a.dataMin)),f=a.toPixels(p(a.options.max,a.dataMax)),g=B(e,f),e=v(e,f);b.min=B(a.pos,g-d);b.max=v(a.pos+a.len,e+d)}}),b.res=!0;else if(d.length){if(!i)b.selectionMarker=
i=x({destroy:ha},c.plotBox);b.pinchTranslate(d,e,j,i,l,g);b.hasPinched=h;b.scaleGroups(j,l);if(!h&&b.followTouchMove&&f===1)this.runPointActions(b.normalize(a));else if(b.res)b.res=!1,this.reset(!1,0)}},onContainerTouchStart:function(a){var b=this.chart;xa=b.index;a.touches.length===1?(a=this.normalize(a),b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop)&&!b.openMenu?(this.runPointActions(a),this.pinch(a)):this.reset()):a.touches.length===2&&this.pinch(a)},onContainerTouchMove:function(a){(a.touches.length===
1||a.touches.length===2)&&this.pinch(a)},onDocumentTouchEnd:function(a){ca[xa]&&ca[xa].pointer.drop(a)}});if(T.PointerEvent||T.MSPointerEvent){var Ba={},Lb=!!T.PointerEvent,ec=function(){var a,b=[];b.item=function(a){return this[a]};for(a in Ba)Ba.hasOwnProperty(a)&&b.push({pageX:Ba[a].pageX,pageY:Ba[a].pageY,target:Ba[a].target});return b},Mb=function(a,b,c,d){a=a.originalEvent||a;if((a.pointerType==="touch"||a.pointerType===a.MSPOINTER_TYPE_TOUCH)&&ca[xa])d(a),d=ca[xa].pointer,d[b]({type:c,target:a.currentTarget,
preventDefault:ha,touches:ec()})};x(Xa.prototype,{onContainerPointerDown:function(a){Mb(a,"onContainerTouchStart","touchstart",function(a){Ba[a.pointerId]={pageX:a.pageX,pageY:a.pageY,target:a.currentTarget}})},onContainerPointerMove:function(a){Mb(a,"onContainerTouchMove","touchmove",function(a){Ba[a.pointerId]={pageX:a.pageX,pageY:a.pageY};if(!Ba[a.pointerId].target)Ba[a.pointerId].target=a.currentTarget})},onDocumentPointerUp:function(a){Mb(a,"onContainerTouchEnd","touchend",function(a){delete Ba[a.pointerId]})},
batchMSEvents:function(a){a(this.chart.container,Lb?"pointerdown":"MSPointerDown",this.onContainerPointerDown);a(this.chart.container,Lb?"pointermove":"MSPointerMove",this.onContainerPointerMove);a(E,Lb?"pointerup":"MSPointerUp",this.onDocumentPointerUp)}});S(Xa.prototype,"init",function(a,b,c){a.call(this,b,c);(this.hasZoom||this.followTouchMove)&&M(b.container,{"-ms-touch-action":Z,"touch-action":Z})});S(Xa.prototype,"setDOMEvents",function(a){a.apply(this);(this.hasZoom||this.followTouchMove)&&
this.batchMSEvents(D)});S(Xa.prototype,"destroy",function(a){this.batchMSEvents(U);a.call(this)})}var rb=z.Legend=function(a,b){this.init(a,b)};rb.prototype={init:function(a,b){var c=this,d=b.itemStyle,e=b.itemMarginTop||0;this.options=b;if(b.enabled)c.itemStyle=d,c.itemHiddenStyle=y(d,b.itemHiddenStyle),c.itemMarginTop=e,c.padding=d=p(b.padding,8),c.initialItemX=d,c.initialItemY=d-5,c.maxItemWidth=0,c.chart=a,c.itemHeight=0,c.symbolWidth=p(b.symbolWidth,16),c.pages=[],c.render(),D(c.chart,"endResize",
function(){c.positionCheckboxes()})},colorizeItem:function(a,b){var c=this.options,d=a.legendItem,e=a.legendLine,f=a.legendSymbol,g=this.itemHiddenStyle.color,c=b?c.itemStyle.color:g,h=b?a.legendColor||a.color||"#CCC":g,g=a.options&&a.options.marker,i={fill:h},j;d&&d.css({fill:c,color:c});e&&e.attr({stroke:h});if(f){if(g&&f.isMarker)for(j in i.stroke=h,g=a.convertAttribs(g),g)d=g[j],d!==r&&(i[j]=d);f.attr(i)}},positionItem:function(a){var b=this.options,c=b.symbolPadding,b=!b.rtl,d=a._legendItemPos,
e=d[0],d=d[1],f=a.checkbox;a.legendGroup&&a.legendGroup.translate(b?e:this.legendWidth-e-2*c-4,d);if(f)f.x=e,f.y=d},destroyItem:function(a){var b=a.checkbox;n(["legendItem","legendLine","legendSymbol","legendGroup"],function(b){a[b]&&(a[b]=a[b].destroy())});b&&Ta(a.checkbox)},clearItems:function(){var a=this;n(a.getAllItems(),function(b){a.destroyItem(b)})},destroy:function(){var a=this.group,b=this.box;if(b)this.box=b.destroy();if(a)this.group=a.destroy()},positionCheckboxes:function(a){var b=this.group.alignAttr,
c,d=this.clipHeight||this.legendHeight;if(b)c=b.translateY,n(this.allItems,function(e){var f=e.checkbox,g;f&&(g=c+f.y+(a||0)+3,M(f,{left:b.translateX+e.checkboxOffset+f.x-20+"px",top:g+"px",display:g>c-6&&g<c+d-6?"":Z}))})},renderTitle:function(){var a=this.padding,b=this.options.title,c=0;if(b.text){if(!this.title)this.title=this.chart.renderer.label(b.text,a-3,a-4,null,null,null,null,null,"legend-title").attr({zIndex:1}).css(b.style).add(this.group);a=this.title.getBBox();c=a.height;this.offsetWidth=
a.width;this.contentGroup.attr({translateY:c})}this.titleHeight=c},renderItem:function(a){var b=this.chart,c=b.renderer,d=this.options,e=d.layout==="horizontal",f=this.symbolWidth,g=d.symbolPadding,h=this.itemStyle,i=this.itemHiddenStyle,j=this.padding,k=e?p(d.itemDistance,20):0,l=!d.rtl,m=d.width,o=d.itemMarginBottom||0,q=this.itemMarginTop,t=this.initialItemX,n=a.legendItem,N=a.series&&a.series.drawLegendSymbol?a.series:a,u=N.options,u=this.createCheckboxForItem&&u&&u.showCheckbox,A=d.useHTML;if(!n){a.legendGroup=
c.g("legend-item").attr({zIndex:1}).add(this.scrollGroup);a.legendItem=n=c.text(d.labelFormat?Ma(d.labelFormat,a):d.labelFormatter.call(a),l?f+g:-g,this.baseline||0,A).css(y(a.visible?h:i)).attr({align:l?"left":"right",zIndex:2}).add(a.legendGroup);if(!this.baseline)this.baseline=c.fontMetrics(h.fontSize,n).f+3+q,n.attr("y",this.baseline);N.drawLegendSymbol(this,a);this.setItemEvents&&this.setItemEvents(a,n,A,h,i);this.colorizeItem(a,a.visible);u&&this.createCheckboxForItem(a)}c=n.getBBox();f=a.checkboxOffset=
d.itemWidth||a.legendItemWidth||f+g+c.width+k+(u?20:0);this.itemHeight=g=w(a.legendItemHeight||c.height);if(e&&this.itemX-t+f>(m||b.chartWidth-2*j-t-d.x))this.itemX=t,this.itemY+=q+this.lastLineHeight+o;this.maxItemWidth=v(this.maxItemWidth,f);this.lastItemY=q+this.itemY+o;this.lastLineHeight=v(g,this.lastLineHeight);a._legendItemPos=[this.itemX,this.itemY];e?this.itemX+=f:(this.itemY+=q+g+o,this.lastLineHeight=g);this.offsetWidth=m||v((e?this.itemX-t-k:f)+j,this.offsetWidth)},getAllItems:function(){var a=
[];n(this.chart.series,function(b){var c=b.options;if(p(c.showInLegend,!s(c.linkedTo)?r:!1,!0))a=a.concat(b.legendItems||(c.legendType==="point"?b.data:b))});return a},adjustMargins:function(a,b){var c=this.chart,d=this.options,e=d.align[0]+d.verticalAlign[0]+d.layout[0];this.display&&!d.floating&&n([/(lth|ct|rth)/,/(rtv|rm|rbv)/,/(rbh|cb|lbh)/,/(lbv|lm|ltv)/],function(f,g){f.test(e)&&!s(a[g])&&(c[ob[g]]=v(c[ob[g]],c.legend[(g+1)%2?"legendHeight":"legendWidth"]+[1,-1,-1,1][g]*d[g%2?"x":"y"]+p(d.margin,
12)+b[g]))})},render:function(){var a=this,b=a.chart,c=b.renderer,d=a.group,e,f,g,h,i=a.box,j=a.options,k=a.padding,l=j.borderWidth,m=j.backgroundColor;a.itemX=a.initialItemX;a.itemY=a.initialItemY;a.offsetWidth=0;a.lastItemY=0;if(!d)a.group=d=c.g("legend").attr({zIndex:7}).add(),a.contentGroup=c.g().attr({zIndex:1}).add(d),a.scrollGroup=c.g().add(a.contentGroup);a.renderTitle();e=a.getAllItems();xb(e,function(a,b){return(a.options&&a.options.legendIndex||0)-(b.options&&b.options.legendIndex||0)});
j.reversed&&e.reverse();a.allItems=e;a.display=f=!!e.length;a.lastLineHeight=0;n(e,function(b){a.renderItem(b)});g=(j.width||a.offsetWidth)+k;h=a.lastItemY+a.lastLineHeight+a.titleHeight;h=a.handleOverflow(h);h+=k;if(l||m){if(i){if(g>0&&h>0)i[i.isNew?"attr":"animate"](i.crisp({width:g,height:h})),i.isNew=!1}else a.box=i=c.rect(0,0,g,h,j.borderRadius,l||0).attr({stroke:j.borderColor,"stroke-width":l||0,fill:m||Z}).add(d).shadow(j.shadow),i.isNew=!0;i[f?"show":"hide"]()}a.legendWidth=g;a.legendHeight=
h;n(e,function(b){a.positionItem(b)});f&&d.align(x({width:g,height:h},j),!0,"spacingBox");b.isResizing||this.positionCheckboxes()},handleOverflow:function(a){var b=this,c=this.chart,d=c.renderer,e=this.options,f=e.y,f=c.spacingBox.height+(e.verticalAlign==="top"?-f:f)-this.padding,g=e.maxHeight,h,i=this.clipRect,j=e.navigation,k=p(j.animation,!0),l=j.arrowSize||12,m=this.nav,o=this.pages,q,t=this.allItems;e.layout==="horizontal"&&(f/=2);g&&(f=B(f,g));o.length=0;if(a>f&&!e.useHTML){this.clipHeight=
h=v(f-20-this.titleHeight-this.padding,0);this.currentPage=p(this.currentPage,1);this.fullHeight=a;n(t,function(a,b){var c=a._legendItemPos[1],d=w(a.legendItem.getBBox().height),e=o.length;if(!e||c-o[e-1]>h&&(q||c)!==o[e-1])o.push(q||c),e++;b===t.length-1&&c+d-o[e-1]>h&&o.push(c);c!==q&&(q=c)});if(!i)i=b.clipRect=d.clipRect(0,this.padding,9999,0),b.contentGroup.clip(i);i.attr({height:h});if(!m)this.nav=m=d.g().attr({zIndex:1}).add(this.group),this.up=d.symbol("triangle",0,0,l,l).on("click",function(){b.scroll(-1,
k)}).add(m),this.pager=d.text("",15,10).css(j.style).add(m),this.down=d.symbol("triangle-down",0,0,l,l).on("click",function(){b.scroll(1,k)}).add(m);b.scroll(0);a=f}else if(m)i.attr({height:c.chartHeight}),m.hide(),this.scrollGroup.attr({translateY:1}),this.clipHeight=0;return a},scroll:function(a,b){var c=this.pages,d=c.length,e=this.currentPage+a,f=this.clipHeight,g=this.options.navigation,h=g.activeColor,g=g.inactiveColor,i=this.pager,j=this.padding;e>d&&(e=d);if(e>0)b!==r&&Ya(b,this.chart),this.nav.attr({translateX:j,
translateY:f+this.padding+7+this.titleHeight,visibility:"visible"}),this.up.attr({fill:e===1?g:h}).css({cursor:e===1?"default":"pointer"}),i.attr({text:e+"/"+d}),this.down.attr({x:18+this.pager.getBBox().width,fill:e===d?g:h}).css({cursor:e===d?"default":"pointer"}),c=-c[e-1]+this.initialItemY,this.scrollGroup.animate({translateY:c}),this.currentPage=e,this.positionCheckboxes(c)}};G=z.LegendSymbolMixin={drawRectangle:function(a,b){var c=a.options.symbolHeight||12;b.legendSymbol=this.chart.renderer.rect(0,
a.baseline-5-c/2,a.symbolWidth,c,a.options.symbolRadius||0).attr({zIndex:3}).add(b.legendGroup)},drawLineMarker:function(a){var b=this.options,c=b.marker,d;d=a.symbolWidth;var e=this.chart.renderer,f=this.legendGroup,a=a.baseline-w(e.fontMetrics(a.options.itemStyle.fontSize,this.legendItem).b*0.3),g;if(b.lineWidth){g={"stroke-width":b.lineWidth};if(b.dashStyle)g.dashstyle=b.dashStyle;this.legendLine=e.path(["M",0,a,"L",d,a]).attr(g).add(f)}if(c&&c.enabled!==!1)b=c.radius,this.legendSymbol=d=e.symbol(this.symbol,
d/2-b,a-b,2*b,2*b).add(f),d.isMarker=!0}};(/Trident\/7\.0/.test(Ha)||Va)&&S(rb.prototype,"positionItem",function(a,b){var c=this,d=function(){b._legendItemPos&&a.call(c,b)};d();setTimeout(d)});var Pa=z.Chart=function(){this.init.apply(this,arguments)};Pa.prototype={callbacks:[],init:function(a,b){var c,d=a.series;a.series=null;c=y(F,a);c.series=a.series=d;this.userOptions=a;d=c.chart;this.margin=this.splashArray("margin",d);this.spacing=this.splashArray("spacing",d);var e=d.events;this.bounds={h:{},
v:{}};this.callback=b;this.isResizing=0;this.options=c;this.axes=[];this.series=[];this.hasCartesianSeries=d.showAxes;var f=this,g;f.index=ca.length;ca.push(f);gb++;d.reflow!==!1&&D(f,"load",function(){f.initReflow()});if(e)for(g in e)D(f,g,e[g]);f.xAxis=[];f.yAxis=[];f.animation=ma?!1:p(d.animation,!0);f.pointCount=f.colorCounter=f.symbolCounter=0;f.firstRender()},initSeries:function(a){var b=this.options.chart;(b=I[a.type||b.type||b.defaultSeriesType])||qa(17,!0);b=new b;b.init(this,a);return b},
isInsidePlot:function(a,b,c){var d=c?b:a,a=c?a:b;return d>=0&&d<=this.plotWidth&&a>=0&&a<=this.plotHeight},redraw:function(a){var b=this.axes,c=this.series,d=this.pointer,e=this.legend,f=this.isDirtyLegend,g,h,i=this.hasCartesianSeries,j=this.isDirtyBox,k=c.length,l=k,m=this.renderer,o=m.isHidden(),q=[];Ya(a,this);o&&this.cloneRenderTo();for(this.layOutTitles();l--;)if(a=c[l],a.options.stacking&&(g=!0,a.isDirty)){h=!0;break}if(h)for(l=k;l--;)if(a=c[l],a.options.stacking)a.isDirty=!0;n(c,function(a){a.isDirty&&
a.options.legendType==="point"&&(f=!0)});if(f&&e.options.enabled)e.render(),this.isDirtyLegend=!1;g&&this.getStacks();if(i&&!this.isResizing)this.maxTicks=null,n(b,function(a){a.setScale()});this.getMargins();i&&(n(b,function(a){a.isDirty&&(j=!0)}),n(b,function(a){if(a.isDirtyExtremes)a.isDirtyExtremes=!1,q.push(function(){K(a,"afterSetExtremes",x(a.eventArgs,a.getExtremes()));delete a.eventArgs});(j||g)&&a.redraw()}));j&&this.drawChartBox();n(c,function(a){a.isDirty&&a.visible&&(!a.isCartesian||
a.xAxis)&&a.redraw()});d&&d.reset(!0);m.draw();K(this,"redraw");o&&this.cloneRenderTo(!0);n(q,function(a){a.call()})},get:function(a){var b=this.axes,c=this.series,d,e;for(d=0;d<b.length;d++)if(b[d].options.id===a)return b[d];for(d=0;d<c.length;d++)if(c[d].options.id===a)return c[d];for(d=0;d<c.length;d++){e=c[d].points||[];for(b=0;b<e.length;b++)if(e[b].id===a)return e[b]}return null},getAxes:function(){var a=this,b=this.options,c=b.xAxis=pa(b.xAxis||{}),b=b.yAxis=pa(b.yAxis||{});n(c,function(a,
b){a.index=b;a.isX=!0});n(b,function(a,b){a.index=b});c=c.concat(b);n(c,function(b){new O(a,b)})},getSelectedPoints:function(){var a=[];n(this.series,function(b){a=a.concat(hb(b.points||[],function(a){return a.selected}))});return a},getSelectedSeries:function(){return hb(this.series,function(a){return a.selected})},getStacks:function(){var a=this;n(a.yAxis,function(a){if(a.stacks&&a.hasVisibleSeries)a.oldStacks=a.stacks});n(a.series,function(b){if(b.options.stacking&&(b.visible===!0||a.options.chart.ignoreHiddenSeries===
!1))b.stackKey=b.type+p(b.options.stack,"")})},setTitle:function(a,b,c){var g;var d=this,e=d.options,f;f=e.title=y(e.title,a);g=e.subtitle=y(e.subtitle,b),e=g;n([["title",a,f],["subtitle",b,e]],function(a){var b=a[0],c=d[b],e=a[1],a=a[2];c&&e&&(d[b]=c=c.destroy());a&&a.text&&!c&&(d[b]=d.renderer.text(a.text,0,0,a.useHTML).attr({align:a.align,"class":"highcharts-"+b,zIndex:a.zIndex||4}).css(a.style).add())});d.layOutTitles(c)},layOutTitles:function(a){var b=0,c=this.title,d=this.subtitle,e=this.options,
f=e.title,e=e.subtitle,g=this.renderer,h=this.spacingBox.width-44;if(c&&(c.css({width:(f.width||h)+"px"}).align(x({y:g.fontMetrics(f.style.fontSize,c).b-3},f),!1,"spacingBox"),!f.floating&&!f.verticalAlign))b=c.getBBox().height;d&&(d.css({width:(e.width||h)+"px"}).align(x({y:b+(f.margin-13)+g.fontMetrics(f.style.fontSize,d).b},e),!1,"spacingBox"),!e.floating&&!e.verticalAlign&&(b=za(b+d.getBBox().height)));c=this.titleOffset!==b;this.titleOffset=b;if(!this.isDirtyBox&&c)this.isDirtyBox=c,this.hasRendered&&
p(a,!0)&&this.isDirtyBox&&this.redraw()},getChartSize:function(){var a=this.options.chart,b=a.width,a=a.height,c=this.renderToClone||this.renderTo;if(!s(b))this.containerWidth=pb(c,"width");if(!s(a))this.containerHeight=pb(c,"height");this.chartWidth=v(0,b||this.containerWidth||600);this.chartHeight=v(0,p(a,this.containerHeight>19?this.containerHeight:400))},cloneRenderTo:function(a){var b=this.renderToClone,c=this.container;a?b&&(this.renderTo.appendChild(c),Ta(b),delete this.renderToClone):(c&&
c.parentNode===this.renderTo&&this.renderTo.removeChild(c),this.renderToClone=b=this.renderTo.cloneNode(0),M(b,{position:"absolute",top:"-9999px",display:"block"}),b.style.setProperty&&b.style.setProperty("display","block","important"),E.body.appendChild(b),c&&b.appendChild(c))},getContainer:function(){var a,b=this.options.chart,c,d,e;this.renderTo=a=b.renderTo;e="highcharts-"+Hb++;if(Ja(a))this.renderTo=a=E.getElementById(a);a||qa(13,!0);c=C(W(a,"data-highcharts-chart"));!isNaN(c)&&ca[c]&&ca[c].hasRendered&&
ca[c].destroy();W(a,"data-highcharts-chart",this.index);a.innerHTML="";!b.skipClone&&!a.offsetWidth&&this.cloneRenderTo();this.getChartSize();c=this.chartWidth;d=this.chartHeight;this.container=a=aa(Ua,{className:"highcharts-container"+(b.className?" "+b.className:""),id:e},x({position:"relative",overflow:"hidden",width:c+"px",height:d+"px",textAlign:"left",lineHeight:"normal",zIndex:0,"-webkit-tap-highlight-color":"rgba(0,0,0,0)"},b.style),this.renderToClone||a);this._cursor=a.style.cursor;this.renderer=
b.forExport?new na(a,c,d,b.style,!0):new Wa(a,c,d,b.style);ma&&this.renderer.create(this,a,c,d);this.renderer.chartIndex=this.index},getMargins:function(a){var b=this.spacing,c=this.margin,d=this.titleOffset;this.resetMargins();if(d&&!s(c[0]))this.plotTop=v(this.plotTop,d+this.options.title.margin+b[0]);this.legend.adjustMargins(c,b);this.extraBottomMargin&&(this.marginBottom+=this.extraBottomMargin);this.extraTopMargin&&(this.plotTop+=this.extraTopMargin);a||this.getAxisMargins()},getAxisMargins:function(){var a=
this,b=a.axisOffset=[0,0,0,0],c=a.margin;a.hasCartesianSeries&&n(a.axes,function(a){a.getOffset()});n(ob,function(d,e){s(c[e])||(a[d]+=b[e])});a.setChartSize()},reflow:function(a){var b=this,c=b.options.chart,d=b.renderTo,e=c.width||pb(d,"width"),f=c.height||pb(d,"height"),c=a?a.target:T,d=function(){if(b.container)b.setSize(e,f,!1),b.hasUserSize=null};if(!b.hasUserSize&&!b.isPrinting&&e&&f&&(c===T||c===E)){if(e!==b.containerWidth||f!==b.containerHeight)clearTimeout(b.reflowTimeout),a?b.reflowTimeout=
setTimeout(d,100):d();b.containerWidth=e;b.containerHeight=f}},initReflow:function(){var a=this,b=function(b){a.reflow(b)};D(T,"resize",b);D(a,"destroy",function(){U(T,"resize",b)})},setSize:function(a,b,c){var d=this,e,f,g;d.isResizing+=1;g=function(){d&&K(d,"endResize",null,function(){d.isResizing-=1})};Ya(c,d);d.oldChartHeight=d.chartHeight;d.oldChartWidth=d.chartWidth;if(s(a))d.chartWidth=e=v(0,w(a)),d.hasUserSize=!!e;if(s(b))d.chartHeight=f=v(0,w(b));(Ga?qb:M)(d.container,{width:e+"px",height:f+
"px"},Ga);d.setChartSize(!0);d.renderer.setSize(e,f,c);d.maxTicks=null;n(d.axes,function(a){a.isDirty=!0;a.setScale()});n(d.series,function(a){a.isDirty=!0});d.isDirtyLegend=!0;d.isDirtyBox=!0;d.layOutTitles();d.getMargins();d.redraw(c);d.oldChartHeight=null;K(d,"resize");Ga===!1?g():setTimeout(g,Ga&&Ga.duration||500)},setChartSize:function(a){var b=this.inverted,c=this.renderer,d=this.chartWidth,e=this.chartHeight,f=this.options.chart,g=this.spacing,h=this.clipOffset,i,j,k,l;this.plotLeft=i=w(this.plotLeft);
this.plotTop=j=w(this.plotTop);this.plotWidth=k=v(0,w(d-i-this.marginRight));this.plotHeight=l=v(0,w(e-j-this.marginBottom));this.plotSizeX=b?l:k;this.plotSizeY=b?k:l;this.plotBorderWidth=f.plotBorderWidth||0;this.spacingBox=c.spacingBox={x:g[3],y:g[0],width:d-g[3]-g[1],height:e-g[0]-g[2]};this.plotBox=c.plotBox={x:i,y:j,width:k,height:l};d=2*X(this.plotBorderWidth/2);b=za(v(d,h[3])/2);c=za(v(d,h[0])/2);this.clipBox={x:b,y:c,width:X(this.plotSizeX-v(d,h[1])/2-b),height:v(0,X(this.plotSizeY-v(d,h[2])/
2-c))};a||n(this.axes,function(a){a.setAxisSize();a.setAxisTranslation()})},resetMargins:function(){var a=this;n(ob,function(b,c){a[b]=p(a.margin[c],a.spacing[c])});a.axisOffset=[0,0,0,0];a.clipOffset=[0,0,0,0]},drawChartBox:function(){var a=this.options.chart,b=this.renderer,c=this.chartWidth,d=this.chartHeight,e=this.chartBackground,f=this.plotBackground,g=this.plotBorder,h=this.plotBGImage,i=a.borderWidth||0,j=a.backgroundColor,k=a.plotBackgroundColor,l=a.plotBackgroundImage,m=a.plotBorderWidth||
0,o,q=this.plotLeft,t=this.plotTop,p=this.plotWidth,n=this.plotHeight,u=this.plotBox,r=this.clipRect,s=this.clipBox;o=i+(a.shadow?8:0);if(i||j)if(e)e.animate(e.crisp({width:c-o,height:d-o}));else{e={fill:j||Z};if(i)e.stroke=a.borderColor,e["stroke-width"]=i;this.chartBackground=b.rect(o/2,o/2,c-o,d-o,a.borderRadius,i).attr(e).addClass("highcharts-background").add().shadow(a.shadow)}if(k)f?f.animate(u):this.plotBackground=b.rect(q,t,p,n,0).attr({fill:k}).add().shadow(a.plotShadow);if(l)h?h.animate(u):
this.plotBGImage=b.image(l,q,t,p,n).add();r?r.animate({width:s.width,height:s.height}):this.clipRect=b.clipRect(s);if(m)g?g.animate(g.crisp({x:q,y:t,width:p,height:n,strokeWidth:-m})):this.plotBorder=b.rect(q,t,p,n,0,-m).attr({stroke:a.plotBorderColor,"stroke-width":m,fill:Z,zIndex:1}).add();this.isDirtyBox=!1},propFromSeries:function(){var a=this,b=a.options.chart,c,d=a.options.series,e,f;n(["inverted","angular","polar"],function(g){c=I[b.type||b.defaultSeriesType];f=a[g]||b[g]||c&&c.prototype[g];
for(e=d&&d.length;!f&&e--;)(c=I[d[e].type])&&c.prototype[g]&&(f=!0);a[g]=f})},linkSeries:function(){var a=this,b=a.series;n(b,function(a){a.linkedSeries.length=0});n(b,function(b){var d=b.options.linkedTo;if(Ja(d)&&(d=d===":previous"?a.series[b.index-1]:a.get(d)))d.linkedSeries.push(b),b.linkedParent=d})},renderSeries:function(){n(this.series,function(a){a.translate();a.render()})},renderLabels:function(){var a=this,b=a.options.labels;b.items&&n(b.items,function(c){var d=x(b.style,c.style),e=C(d.left)+
a.plotLeft,f=C(d.top)+a.plotTop+12;delete d.left;delete d.top;a.renderer.text(c.html,e,f).attr({zIndex:2}).css(d).add()})},render:function(){var a=this.axes,b=this.renderer,c=this.options,d,e,f,g;this.setTitle();this.legend=new rb(this,c.legend);this.getStacks();this.getMargins(!0);this.setChartSize();d=this.plotWidth;e=this.plotHeight-=13;n(a,function(a){a.setScale()});this.getAxisMargins();f=d/this.plotWidth>1.2;g=e/this.plotHeight>1.1;if(f||g)this.maxTicks=null,n(a,function(a){(a.horiz&&f||!a.horiz&&
g)&&a.setTickInterval(!0)}),this.getMargins();this.drawChartBox();this.hasCartesianSeries&&n(a,function(a){a.render()});if(!this.seriesGroup)this.seriesGroup=b.g("series-group").attr({zIndex:3}).add();this.renderSeries();this.renderLabels();this.showCredits(c.credits);this.hasRendered=!0},showCredits:function(a){if(a.enabled&&!this.credits)this.credits=this.renderer.text(a.text,0,0).on("click",function(){if(a.href)location.href=a.href}).attr({align:a.position.align,zIndex:8}).css(a.style).add().align(a.position)},
destroy:function(){var a=this,b=a.axes,c=a.series,d=a.container,e,f=d&&d.parentNode;K(a,"destroy");ca[a.index]=r;gb--;a.renderTo.removeAttribute("data-highcharts-chart");U(a);for(e=b.length;e--;)b[e]=b[e].destroy();for(e=c.length;e--;)c[e]=c[e].destroy();n("title,subtitle,chartBackground,plotBackground,plotBGImage,plotBorder,seriesGroup,clipRect,credits,pointer,scroller,rangeSelector,legend,resetZoomButton,tooltip,renderer".split(","),function(b){var c=a[b];c&&c.destroy&&(a[b]=c.destroy())});if(d)d.innerHTML=
"",U(d),f&&Ta(d);for(e in a)delete a[e]},isReadyToRender:function(){var a=this;return!ea&&T==T.top&&E.readyState!=="complete"||ma&&!T.canvg?(ma?Tb.push(function(){a.firstRender()},a.options.global.canvasToolsURL):E.attachEvent("onreadystatechange",function(){E.detachEvent("onreadystatechange",a.firstRender);E.readyState==="complete"&&a.firstRender()}),!1):!0},firstRender:function(){var a=this,b=a.options,c=a.callback;if(a.isReadyToRender()){a.getContainer();K(a,"init");a.resetMargins();a.setChartSize();
a.propFromSeries();a.getAxes();n(b.series||[],function(b){a.initSeries(b)});a.linkSeries();K(a,"beforeRender");if(z.Pointer)a.pointer=new Xa(a,b);a.render();a.renderer.draw();c&&c.apply(a,[a]);n(a.callbacks,function(b){a.index!==r&&b.apply(a,[a])});K(a,"load");a.cloneRenderTo(!0)}},splashArray:function(a,b){var c=b[a],c=ia(c)?c:[c,c,c,c];return[p(b[a+"Top"],c[0]),p(b[a+"Right"],c[1]),p(b[a+"Bottom"],c[2]),p(b[a+"Left"],c[3])]}};var fc=z.CenteredSeriesMixin={getCenter:function(){var a=this.options,
b=this.chart,c=2*(a.slicedOffset||0),d=b.plotWidth-2*c,b=b.plotHeight-2*c,e=a.center,e=[p(e[0],"50%"),p(e[1],"50%"),a.size||"100%",a.innerSize||0],f=B(d,b),g,h,i;for(h=0;h<4;++h)i=e[h],g=/%$/.test(i),a=h<2||h===2&&g,e[h]=(g?[d,b,f,e[2]][h]*C(i)/100:C(i))+(a?c:0);return e}},Ca=function(){};Ca.prototype={init:function(a,b,c){this.series=a;this.color=a.color;this.applyOptions(b,c);this.pointAttr={};if(a.options.colorByPoint&&(b=a.options.colors||a.chart.options.colors,this.color=this.color||b[a.colorCounter++],
a.colorCounter===b.length))a.colorCounter=0;a.chart.pointCount++;return this},applyOptions:function(a,b){var c=this.series,d=c.options.pointValKey||c.pointValKey,a=Ca.prototype.optionsToObject.call(this,a);x(this,a);this.options=this.options?x(this.options,a):a;if(d)this.y=this[d];if(this.x===r&&c)this.x=b===r?c.autoIncrement():b;return this},optionsToObject:function(a){var b={},c=this.series,d=c.pointArrayMap||["y"],e=d.length,f=0,g=0;if(typeof a==="number"||a===null)b[d[0]]=a;else if(Ka(a)){if(a.length>
e){c=typeof a[0];if(c==="string")b.name=a[0];else if(c==="number")b.x=a[0];f++}for(;g<e;)b[d[g++]]=a[f++]}else if(typeof a==="object"){b=a;if(a.dataLabels)c._hasPointLabels=!0;if(a.marker)c._hasPointMarkers=!0}return b},destroy:function(){var a=this.series.chart,b=a.hoverPoints,c;a.pointCount--;if(b&&(this.setState(),ua(b,this),!b.length))a.hoverPoints=null;if(this===a.hoverPoint)this.onMouseOut();if(this.graphic||this.dataLabel)U(this),this.destroyElements();this.legendItem&&a.legend.destroyItem(this);
for(c in this)this[c]=null},destroyElements:function(){for(var a="graphic,dataLabel,dataLabelUpper,group,connector,shadowGroup".split(","),b,c=6;c--;)b=a[c],this[b]&&(this[b]=this[b].destroy())},getLabelConfig:function(){return{x:this.category,y:this.y,key:this.name||this.category,series:this.series,point:this,percentage:this.percentage,total:this.total||this.stackTotal}},tooltipFormatter:function(a){var b=this.series,c=b.tooltipOptions,d=p(c.valueDecimals,""),e=c.valuePrefix||"",f=c.valueSuffix||
"";n(b.pointArrayMap||["y"],function(b){b="{point."+b;if(e||f)a=a.replace(b+"}",e+b+"}"+f);a=a.replace(b+"}",b+":,."+d+"f}")});return Ma(a,{point:this,series:this.series})},firePointEvent:function(a,b,c){var d=this,e=this.series.options;(e.point.events[a]||d.options&&d.options.events&&d.options.events[a])&&this.importEvents();a==="click"&&e.allowPointSelect&&(c=function(a){d.select(null,a.ctrlKey||a.metaKey||a.shiftKey)});K(this,a,b,c)}};var P=z.Series=function(){};P.prototype={isCartesian:!0,type:"line",
pointClass:Ca,sorted:!0,requireSorting:!0,pointAttrToOptions:{stroke:"lineColor","stroke-width":"lineWidth",fill:"fillColor",r:"radius"},axisTypes:["xAxis","yAxis"],colorCounter:0,parallelArrays:["x","y"],init:function(a,b){var c=this,d,e,f=a.series,g=function(a,b){return p(a.options.index,a._i)-p(b.options.index,b._i)};c.chart=a;c.options=b=c.setOptions(b);c.linkedSeries=[];c.bindAxes();x(c,{name:b.name,state:"",pointAttr:{},visible:b.visible!==!1,selected:b.selected===!0});if(ma)b.animation=!1;
e=b.events;for(d in e)D(c,d,e[d]);if(e&&e.click||b.point&&b.point.events&&b.point.events.click||b.allowPointSelect)a.runTrackerClick=!0;c.getColor();c.getSymbol();n(c.parallelArrays,function(a){c[a+"Data"]=[]});c.setData(b.data,!1);if(c.isCartesian)a.hasCartesianSeries=!0;f.push(c);c._i=f.length-1;xb(f,g);this.yAxis&&xb(this.yAxis.series,g);n(f,function(a,b){a.index=b;a.name=a.name||"Series "+(b+1)})},bindAxes:function(){var a=this,b=a.options,c=a.chart,d;n(a.axisTypes||[],function(e){n(c[e],function(c){d=
c.options;if(b[e]===d.index||b[e]!==r&&b[e]===d.id||b[e]===r&&d.index===0)c.series.push(a),a[e]=c,c.isDirty=!0});!a[e]&&a.optionalAxis!==e&&qa(18,!0)})},updateParallelArrays:function(a,b){var c=a.series,d=arguments;n(c.parallelArrays,typeof b==="number"?function(d){var f=d==="y"&&c.toYData?c.toYData(a):a[d];c[d+"Data"][b]=f}:function(a){Array.prototype[b].apply(c[a+"Data"],Array.prototype.slice.call(d,2))})},autoIncrement:function(){var a=this.options,b=this.xIncrement,c,d=a.pointIntervalUnit,b=p(b,
a.pointStart,0);this.pointInterval=c=p(this.pointInterval,a.pointInterval,1);if(d==="month"||d==="year")a=new fa(b),a=d==="month"?+a[Cb](a[db]()+c):+a[Db](a[eb]()+c),c=a-b;this.xIncrement=b+c;return b},getSegments:function(){var a=-1,b=[],c,d=this.points,e=d.length;if(e)if(this.options.connectNulls){for(c=e;c--;)d[c].y===null&&d.splice(c,1);d.length&&(b=[d])}else n(d,function(c,g){c.y===null?(g>a+1&&b.push(d.slice(a+1,g)),a=g):g===e-1&&b.push(d.slice(a+1,g+1))});this.segments=b},setOptions:function(a){var b=
this.chart,c=b.options.plotOptions,b=b.userOptions||{},d=b.plotOptions||{},e=c[this.type];this.userOptions=a;c=y(e,c.series,a);this.tooltipOptions=y(F.tooltip,F.plotOptions[this.type].tooltip,b.tooltip,d.series&&d.series.tooltip,d[this.type]&&d[this.type].tooltip,a.tooltip);e.marker===null&&delete c.marker;this.zoneAxis=c.zoneAxis;a=this.zones=(c.zones||[]).slice();if((c.negativeColor||c.negativeFillColor)&&!c.zones)a.push({value:c[this.zoneAxis+"Threshold"]||c.threshold||0,color:c.negativeColor,
fillColor:c.negativeFillColor});a.length&&s(a[a.length-1].value)&&a.push({color:this.color,fillColor:this.fillColor});return c},getCyclic:function(a,b,c){var d=this.userOptions,e="_"+a+"Index",f=a+"Counter";b||(s(d[e])?b=d[e]:(d[e]=b=this.chart[f]%c.length,this.chart[f]+=1),b=c[b]);this[a]=b},getColor:function(){this.options.colorByPoint||this.getCyclic("color",this.options.color||V[this.type].color,this.chart.options.colors)},getSymbol:function(){var a=this.options.marker;this.getCyclic("symbol",
a.symbol,this.chart.options.symbols);if(/^url/.test(this.symbol))a.radius=0},drawLegendSymbol:G.drawLineMarker,setData:function(a,b,c,d){var e=this,f=e.points,g=f&&f.length||0,h,i=e.options,j=e.chart,k=null,l=e.xAxis,m=l&&!!l.categories,o=i.turboThreshold,q=this.xData,t=this.yData,J=(h=e.pointArrayMap)&&h.length,a=a||[];h=a.length;b=p(b,!0);if(d!==!1&&h&&g===h&&!e.cropped&&!e.hasGroupedData&&e.visible)n(a,function(a,b){f[b].update(a,!1,null,!1)});else{e.xIncrement=null;e.pointRange=m?1:i.pointRange;
e.colorCounter=0;n(this.parallelArrays,function(a){e[a+"Data"].length=0});if(o&&h>o){for(c=0;k===null&&c<h;)k=a[c],c++;if(sa(k)){m=p(i.pointStart,0);i=p(i.pointInterval,1);for(c=0;c<h;c++)q[c]=m,t[c]=a[c],m+=i;e.xIncrement=m}else if(Ka(k))if(J)for(c=0;c<h;c++)i=a[c],q[c]=i[0],t[c]=i.slice(1,J+1);else for(c=0;c<h;c++)i=a[c],q[c]=i[0],t[c]=i[1];else qa(12)}else for(c=0;c<h;c++)if(a[c]!==r&&(i={series:e},e.pointClass.prototype.applyOptions.apply(i,[a[c]]),e.updateParallelArrays(i,c),m&&i.name))l.names[i.x]=
i.name;Ja(t[0])&&qa(14,!0);e.data=[];e.options.data=a;for(c=g;c--;)f[c]&&f[c].destroy&&f[c].destroy();if(l)l.minRange=l.userMinRange;e.isDirty=e.isDirtyData=j.isDirtyBox=!0;c=!1}b&&j.redraw(c)},processData:function(a){var b=this.xData,c=this.yData,d=b.length,e;e=0;var f,g,h=this.xAxis,i,j=this.options;i=j.cropThreshold;var k=this.isCartesian,l,m;if(k&&!this.isDirty&&!h.isDirty&&!this.yAxis.isDirty&&!a)return!1;if(h)a=h.getExtremes(),l=a.min,m=a.max;if(k&&this.sorted&&(!i||d>i||this.forceCrop))if(b[d-
1]<l||b[0]>m)b=[],c=[];else if(b[0]<l||b[d-1]>m)e=this.cropData(this.xData,this.yData,l,m),b=e.xData,c=e.yData,e=e.start,f=!0;for(i=b.length-1;i>=0;i--)d=b[i]-b[i-1],d>0&&(g===r||d<g)?g=d:d<0&&this.requireSorting&&qa(15);this.cropped=f;this.cropStart=e;this.processedXData=b;this.processedYData=c;if(j.pointRange===null)this.pointRange=g||1;this.closestPointRange=g},cropData:function(a,b,c,d){var e=a.length,f=0,g=e,h=p(this.cropShoulder,1),i;for(i=0;i<e;i++)if(a[i]>=c){f=v(0,i-h);break}for(;i<e;i++)if(a[i]>
d){g=i+h;break}return{xData:a.slice(f,g),yData:b.slice(f,g),start:f,end:g}},generatePoints:function(){var a=this.options.data,b=this.data,c,d=this.processedXData,e=this.processedYData,f=this.pointClass,g=d.length,h=this.cropStart||0,i,j=this.hasGroupedData,k,l=[],m;if(!b&&!j)b=[],b.length=a.length,b=this.data=b;for(m=0;m<g;m++)i=h+m,j?l[m]=(new f).init(this,[d[m]].concat(pa(e[m]))):(b[i]?k=b[i]:a[i]!==r&&(b[i]=k=(new f).init(this,a[i],d[m])),l[m]=k),l[m].index=i;if(b&&(g!==(c=b.length)||j))for(m=
0;m<c;m++)if(m===h&&!j&&(m+=g),b[m])b[m].destroyElements(),b[m].plotX=r;this.data=b;this.points=l},getExtremes:function(a){var b=this.yAxis,c=this.processedXData,d,e=[],f=0;d=this.xAxis.getExtremes();var g=d.min,h=d.max,i,j,k,l,a=a||this.stackedYData||this.processedYData;d=a.length;for(l=0;l<d;l++)if(j=c[l],k=a[l],i=k!==null&&k!==r&&(!b.isLog||k.length||k>0),j=this.getExtremesFromAll||this.cropped||(c[l+1]||j)>=g&&(c[l-1]||j)<=h,i&&j)if(i=k.length)for(;i--;)k[i]!==null&&(e[f++]=k[i]);else e[f++]=
k;this.dataMin=p(void 0,Sa(e));this.dataMax=p(void 0,Fa(e))},translate:function(){this.processedXData||this.processData();this.generatePoints();for(var a=this.options,b=a.stacking,c=this.xAxis,d=c.categories,e=this.yAxis,f=this.points,g=f.length,h=!!this.modifyValue,i=a.pointPlacement,j=i==="between"||sa(i),k=a.threshold,l,m,o,q=Number.MAX_VALUE,a=0;a<g;a++){var t=f[a],n=t.x,N=t.y;m=t.low;var u=b&&e.stacks[(this.negStacks&&N<k?"-":"")+this.stackKey];if(e.isLog&&N!==null&&N<=0)t.y=N=null,qa(10);t.plotX=
l=c.translate(n,0,0,0,1,i,this.type==="flags");if(b&&this.visible&&u&&u[n])u=u[n],N=u.points[this.index+","+a],m=N[0],N=N[1],m===0&&(m=p(k,e.min)),e.isLog&&m<=0&&(m=null),t.total=t.stackTotal=u.total,t.percentage=u.total&&t.y/u.total*100,t.stackY=N,u.setOffset(this.pointXOffset||0,this.barW||0);t.yBottom=s(m)?e.translate(m,0,1,0,1):null;h&&(N=this.modifyValue(N,t));t.plotY=m=typeof N==="number"&&N!==Infinity?B(v(-1E5,e.translate(N,0,1,0,1)),1E5):r;t.isInside=m!==r&&m>=0&&m<=e.len&&l>=0&&l<=c.len;
t.clientX=j?c.translate(n,0,0,0,1):l;t.negative=t.y<(k||0);t.category=d&&d[t.x]!==r?d[t.x]:t.x;a&&(q=B(q,R(l-o)));o=l}this.closestPointRangePx=q;this.getSegments()},setClip:function(a){var b=this.chart,c=b.renderer,d=b.inverted,e=this.clipBox,f=e||b.clipBox,g=this.sharedClipKey||["_sharedClip",a&&a.duration,a&&a.easing,f.height].join(","),h=b[g],i=b[g+"m"];if(!h){if(a)f.width=0,b[g+"m"]=i=c.clipRect(-99,d?-b.plotLeft:-b.plotTop,99,d?b.chartWidth:b.chartHeight);b[g]=h=c.clipRect(f)}a&&(h.count+=1);
if(this.options.clip!==!1)this.group.clip(a||e?h:b.clipRect),this.markerGroup.clip(i),this.sharedClipKey=g;a||(h.count-=1,h.count<=0&&g&&b[g]&&(e||(b[g]=b[g].destroy()),b[g+"m"]&&(b[g+"m"]=b[g+"m"].destroy())))},animate:function(a){var b=this.chart,c=this.options.animation,d;if(c&&!ia(c))c=V[this.type].animation;a?this.setClip(c):(d=this.sharedClipKey,(a=b[d])&&a.animate({width:b.plotSizeX},c),b[d+"m"]&&b[d+"m"].animate({width:b.plotSizeX+99},c),this.animate=null)},afterAnimate:function(){this.setClip();
K(this,"afterAnimate")},drawPoints:function(){var a,b=this.points,c=this.chart,d,e,f,g,h,i,j,k,l=this.options.marker,m=this.pointAttr[""],o,q,t,n=this.markerGroup,N=p(l.enabled,this.xAxis.isRadial,this.closestPointRangePx>2*l.radius);if(l.enabled!==!1||this._hasPointMarkers)for(f=b.length;f--;)if(g=b[f],d=X(g.plotX),e=g.plotY,k=g.graphic,o=g.marker||{},q=!!g.marker,a=N&&o.enabled===r||o.enabled,t=g.isInside,a&&e!==r&&!isNaN(e)&&g.y!==null)if(a=g.pointAttr[g.selected?"select":""]||m,h=a.r,i=p(o.symbol,
this.symbol),j=i.indexOf("url")===0,k)k[t?"show":"hide"](!0).animate(x({x:d-h,y:e-h},k.symbolName?{width:2*h,height:2*h}:{}));else{if(t&&(h>0||j))g.graphic=c.renderer.symbol(i,d-h,e-h,2*h,2*h,q?o:l).attr(a).add(n)}else if(k)g.graphic=k.destroy()},convertAttribs:function(a,b,c,d){var e=this.pointAttrToOptions,f,g,h={},a=a||{},b=b||{},c=c||{},d=d||{};for(f in e)g=e[f],h[f]=p(a[g],b[f],c[f],d[f]);return h},getAttribs:function(){var a=this,b=a.options,c=V[a.type].marker?b.marker:b,d=c.states,e=d.hover,
f,g=a.color,h=a.options.negativeColor;f={stroke:g,fill:g};var i=a.points||[],j,k=[],l,m=a.pointAttrToOptions;l=a.hasPointSpecificOptions;var o=c.lineColor,q=c.fillColor;j=b.turboThreshold;var t=a.zones,p=a.zoneAxis||"y",N;b.marker?(e.radius=e.radius||c.radius+e.radiusPlus,e.lineWidth=e.lineWidth||c.lineWidth+e.lineWidthPlus):(e.color=e.color||wa(e.color||g).brighten(e.brightness).get(),e.negativeColor=e.negativeColor||wa(e.negativeColor||h).brighten(e.brightness).get());k[""]=a.convertAttribs(c,f);
n(["hover","select"],function(b){k[b]=a.convertAttribs(d[b],k[""])});a.pointAttr=k;g=i.length;if(!j||g<j||l)for(;g--;){j=i[g];if((c=j.options&&j.options.marker||j.options)&&c.enabled===!1)c.radius=0;if(t.length){l=0;for(f=t[l];j[p]>=f.value;)f=t[++l];j.color=j.fillColor=f.color}l=b.colorByPoint||j.color;if(j.options)for(N in m)s(c[m[N]])&&(l=!0);if(l){c=c||{};l=[];d=c.states||{};f=d.hover=d.hover||{};if(!b.marker)f.color=f.color||!j.options.color&&e[j.negative&&h?"negativeColor":"color"]||wa(j.color).brighten(f.brightness||
e.brightness).get();f={color:j.color};if(!q)f.fillColor=j.color;if(!o)f.lineColor=j.color;l[""]=a.convertAttribs(x(f,c),k[""]);l.hover=a.convertAttribs(d.hover,k.hover,l[""]);l.select=a.convertAttribs(d.select,k.select,l[""])}else l=k;j.pointAttr=l}},destroy:function(){var a=this,b=a.chart,c=/AppleWebKit\/533/.test(Ha),d,e,f=a.data||[],g,h,i;K(a,"destroy");U(a);n(a.axisTypes||[],function(b){if(i=a[b])ua(i.series,a),i.isDirty=i.forceRedraw=!0});a.legendItem&&a.chart.legend.destroyItem(a);for(e=f.length;e--;)(g=
f[e])&&g.destroy&&g.destroy();a.points=null;clearTimeout(a.animationTimeout);n("area,graph,dataLabelsGroup,group,markerGroup,tracker,graphNeg,areaNeg,posClip,negClip".split(","),function(b){a[b]&&(d=c&&b==="group"?"hide":"destroy",a[b][d]())});if(b.hoverSeries===a)b.hoverSeries=null;ua(b.series,a);for(h in a)delete a[h]},getSegmentPath:function(a){var b=this,c=[],d=b.options.step;n(a,function(e,f){var g=e.plotX,h=e.plotY,i;b.getPointSpline?c.push.apply(c,b.getPointSpline(a,e,f)):(c.push(f?"L":"M"),
d&&f&&(i=a[f-1],d==="right"?c.push(i.plotX,h):d==="center"?c.push((i.plotX+g)/2,i.plotY,(i.plotX+g)/2,h):c.push(g,i.plotY)),c.push(e.plotX,e.plotY))});return c},getGraphPath:function(){var a=this,b=[],c,d=[];n(a.segments,function(e){c=a.getSegmentPath(e);e.length>1?b=b.concat(c):d.push(e[0])});a.singlePoints=d;return a.graphPath=b},drawGraph:function(){var a=this,b=this.options,c=[["graph",b.lineColor||this.color,b.dashStyle]],d=b.lineWidth,e=b.linecap!=="square",f=this.getGraphPath(),g=this.fillGraph&&
this.color||Z;n(this.zones,function(d,e){c.push(["colorGraph"+e,d.color||a.color,d.dashStyle||b.dashStyle])});n(c,function(c,i){var j=c[0],k=a[j];if(k)ab(k),k.animate({d:f});else if((d||g)&&f.length)k={stroke:c[1],"stroke-width":d,fill:g,zIndex:1},c[2]?k.dashstyle=c[2]:e&&(k["stroke-linecap"]=k["stroke-linejoin"]="round"),a[j]=a.chart.renderer.path(f).attr(k).add(a.group).shadow(i<2&&b.shadow)})},applyZones:function(){var a=this,b=this.chart,c=b.renderer,d=this.zones,e,f,g=this.clips||[],h,i=this.graph,
j=this.area,k=v(b.chartWidth,b.chartHeight),l=this[(this.zoneAxis||"y")+"Axis"],m=l.reversed,o=l.horiz,q=!1;if(d.length&&(i||j))i.hide(),j&&j.hide(),n(d,function(d,i){e=p(f,m?o?b.plotWidth:0:o?0:l.toPixels(l.min));f=w(l.toPixels(p(d.value,l.max),!0));q&&(e=f=l.toPixels(l.max));if(l.isXAxis){if(h={x:m?f:e,y:0,width:Math.abs(e-f),height:k},!o)h.x=b.plotHeight-h.x}else if(h={x:0,y:m?e:f,width:k,height:Math.abs(e-f)},o)h.y=b.plotWidth-h.y;b.inverted&&c.isVML&&(h=l.isXAxis?{x:0,y:m?e:f,height:h.width,
width:b.chartWidth}:{x:h.y-b.plotLeft-b.spacingBox.x,y:0,width:h.height,height:b.chartHeight});g[i]?g[i].animate(h):(g[i]=c.clipRect(h),a["colorGraph"+i].clip(g[i]),j&&a["colorArea"+i].clip(g[i]));q=d.value>l.max}),this.clips=g},invertGroups:function(){function a(){var a={width:b.yAxis.len,height:b.xAxis.len};n(["group","markerGroup"],function(c){b[c]&&b[c].attr(a).invert()})}var b=this,c=b.chart;if(b.xAxis)D(c,"resize",a),D(b,"destroy",function(){U(c,"resize",a)}),a(),b.invertGroups=a},plotGroup:function(a,
b,c,d,e){var f=this[a],g=!f;g&&(this[a]=f=this.chart.renderer.g(b).attr({visibility:c,zIndex:d||0.1}).add(e));f[g?"attr":"animate"](this.getPlotBox());return f},getPlotBox:function(){var a=this.chart,b=this.xAxis,c=this.yAxis;if(a.inverted)b=c,c=this.xAxis;return{translateX:b?b.left:a.plotLeft,translateY:c?c.top:a.plotTop,scaleX:1,scaleY:1}},render:function(){var a=this,b=a.chart,c,d=a.options,e=(c=d.animation)&&!!a.animate&&b.renderer.isSVG&&p(c.duration,500)||0,f=a.visible?"visible":"hidden",g=
d.zIndex,h=a.hasRendered,i=b.seriesGroup;c=a.plotGroup("group","series",f,g,i);a.markerGroup=a.plotGroup("markerGroup","markers",f,g,i);e&&a.animate(!0);a.getAttribs();c.inverted=a.isCartesian?b.inverted:!1;a.drawGraph&&(a.drawGraph(),a.applyZones());n(a.points,function(a){a.redraw&&a.redraw()});a.drawDataLabels&&a.drawDataLabels();a.visible&&a.drawPoints();a.drawTracker&&a.options.enableMouseTracking!==!1&&a.drawTracker();b.inverted&&a.invertGroups();d.clip!==!1&&!a.sharedClipKey&&!h&&c.clip(b.clipRect);
e&&a.animate();if(!h)e?a.animationTimeout=setTimeout(function(){a.afterAnimate()},e):a.afterAnimate();a.isDirty=a.isDirtyData=!1;a.hasRendered=!0},redraw:function(){var a=this.chart,b=this.isDirtyData,c=this.isDirty,d=this.group,e=this.xAxis,f=this.yAxis;d&&(a.inverted&&d.attr({width:a.plotWidth,height:a.plotHeight}),d.animate({translateX:p(e&&e.left,a.plotLeft),translateY:p(f&&f.top,a.plotTop)}));this.translate();this.render();b&&K(this,"updatedData");(c||b)&&delete this.kdTree},kdDimensions:1,kdTree:null,
kdAxisArray:["plotX","plotY"],kdComparer:"distX",searchPoint:function(a){var b=this.xAxis,c=this.yAxis,d=this.chart.inverted;a.plotX=d?b.len-a.chartY+b.pos:a.chartX-b.pos;a.plotY=d?c.len-a.chartX+c.pos:a.chartY-c.pos;return this.searchKDTree(a)},buildKDTree:function(){function a(b,d,g){var h,i;if(i=b&&b.length)return h=c.kdAxisArray[d%g],b.sort(function(a,b){return a[h]-b[h]}),i=Math.floor(i/2),{point:b[i],left:a(b.slice(0,i),d+1,g),right:a(b.slice(i+1),d+1,g)}}function b(){var b=hb(c.points,function(a){return a.y!==
null});c.kdTree=a(b,d,d)}var c=this,d=c.kdDimensions;delete c.kdTree;c.options.kdSync?b():setTimeout(b)},searchKDTree:function(a){function b(a,h,i,j){var k=h.point,l=c.kdAxisArray[i%j],m,o=k;m=s(a[e])&&s(k[e])?Math.pow(a[e]-k[e],2):null;var q=s(a[f])&&s(k[f])?Math.pow(a[f]-k[f],2):null,t=(m||0)+(q||0);m={distX:s(m)?Math.sqrt(m):Number.MAX_VALUE,distY:s(q)?Math.sqrt(q):Number.MAX_VALUE,distR:s(t)?Math.sqrt(t):Number.MAX_VALUE};k.dist=m;l=a[l]-k[l];m=l<0?"left":"right";h[m]&&(m=b(a,h[m],i+1,j),o=m.dist[d]<
o.dist[d]?m:k,k=l<0?"right":"left",h[k]&&Math.sqrt(l*l)<o.dist[d]&&(a=b(a,h[k],i+1,j),o=a.dist[d]<o.dist[d]?a:o));return o}var c=this,d=this.kdComparer,e=this.kdAxisArray[0],f=this.kdAxisArray[1];this.kdTree||this.buildKDTree();if(this.kdTree)return b(a,this.kdTree,this.kdDimensions,this.kdDimensions)}};Qb.prototype={destroy:function(){Na(this,this.axis)},render:function(a){var b=this.options,c=b.format,c=c?Ma(c,this):b.formatter.call(this);this.label?this.label.attr({text:c,visibility:"hidden"}):
this.label=this.axis.chart.renderer.text(c,null,null,b.useHTML).css(b.style).attr({align:this.textAlign,rotation:b.rotation,visibility:"hidden"}).add(a)},setOffset:function(a,b){var c=this.axis,d=c.chart,e=d.inverted,f=this.isNegative,g=c.translate(c.usePercentage?100:this.total,0,0,0,1),c=c.translate(0),c=R(g-c),h=d.xAxis[0].translate(this.x)+a,i=d.plotHeight,f={x:e?f?g:g-c:h,y:e?i-h-b:f?i-g-c:i-g,width:e?c:b,height:e?b:c};if(e=this.label)e.align(this.alignOptions,null,f),f=e.alignAttr,e[this.options.crop===
!1||d.isInsidePlot(f.x,f.y)?"show":"hide"](!0)}};O.prototype.buildStacks=function(){var a=this.series,b=p(this.options.reversedStacks,!0),c=a.length;if(!this.isXAxis){for(this.usePercentage=!1;c--;)a[b?c:a.length-c-1].setStackedPoints();if(this.usePercentage)for(c=0;c<a.length;c++)a[c].setPercentStacks()}};O.prototype.renderStackTotals=function(){var a=this.chart,b=a.renderer,c=this.stacks,d,e,f=this.stackTotalGroup;if(!f)this.stackTotalGroup=f=b.g("stack-labels").attr({visibility:"visible",zIndex:6}).add();
f.translate(a.plotLeft,a.plotTop);for(d in c)for(e in a=c[d],a)a[e].render(f)};P.prototype.setStackedPoints=function(){if(this.options.stacking&&!(this.visible!==!0&&this.chart.options.chart.ignoreHiddenSeries!==!1)){var a=this.processedXData,b=this.processedYData,c=[],d=b.length,e=this.options,f=e.threshold,g=e.stack,e=e.stacking,h=this.stackKey,i="-"+h,j=this.negStacks,k=this.yAxis,l=k.stacks,m=k.oldStacks,o,q,t,p,n,u;for(p=0;p<d;p++){n=a[p];u=b[p];t=this.index+","+p;q=(o=j&&u<f)?i:h;l[q]||(l[q]=
{});if(!l[q][n])m[q]&&m[q][n]?(l[q][n]=m[q][n],l[q][n].total=null):l[q][n]=new Qb(k,k.options.stackLabels,o,n,g);q=l[q][n];q.points[t]=[q.cum||0];e==="percent"?(o=o?h:i,j&&l[o]&&l[o][n]?(o=l[o][n],q.total=o.total=v(o.total,q.total)+R(u)||0):q.total=la(q.total+(R(u)||0))):q.total=la(q.total+(u||0));q.cum=(q.cum||0)+(u||0);q.points[t].push(q.cum);c[p]=q.cum}if(e==="percent")k.usePercentage=!0;this.stackedYData=c;k.oldStacks={}}};P.prototype.setPercentStacks=function(){var a=this,b=a.stackKey,c=a.yAxis.stacks,
d=a.processedXData;n([b,"-"+b],function(b){var e;for(var f=d.length,g,h;f--;)if(g=d[f],e=(h=c[b]&&c[b][g])&&h.points[a.index+","+f],g=e)h=h.total?100/h.total:0,g[0]=la(g[0]*h),g[1]=la(g[1]*h),a.stackedYData[f]=g[1]})};x(Pa.prototype,{addSeries:function(a,b,c){var d,e=this;a&&(b=p(b,!0),K(e,"addSeries",{options:a},function(){d=e.initSeries(a);e.isDirtyLegend=!0;e.linkSeries();b&&e.redraw(c)}));return d},addAxis:function(a,b,c,d){var e=b?"xAxis":"yAxis",f=this.options;new O(this,y(a,{index:this[e].length,
isX:b}));f[e]=pa(f[e]||{});f[e].push(a);p(c,!0)&&this.redraw(d)},showLoading:function(a){var b=this,c=b.options,d=b.loadingDiv,e=c.loading,f=function(){d&&M(d,{left:b.plotLeft+"px",top:b.plotTop+"px",width:b.plotWidth+"px",height:b.plotHeight+"px"})};if(!d)b.loadingDiv=d=aa(Ua,{className:"highcharts-loading"},x(e.style,{zIndex:10,display:Z}),b.container),b.loadingSpan=aa("span",null,e.labelStyle,d),D(b,"redraw",f);b.loadingSpan.innerHTML=a||c.lang.loading;if(!b.loadingShown)M(d,{opacity:0,display:""}),
qb(d,{opacity:e.style.opacity},{duration:e.showDuration||0}),b.loadingShown=!0;f()},hideLoading:function(){var a=this.options,b=this.loadingDiv;b&&qb(b,{opacity:0},{duration:a.loading.hideDuration||100,complete:function(){M(b,{display:Z})}});this.loadingShown=!1}});x(Ca.prototype,{update:function(a,b,c,d){function e(){f.applyOptions(a);if(ia(a)&&!Ka(a))f.redraw=function(){if(h)a&&a.marker&&a.marker.symbol?f.graphic=h.destroy():h.attr(f.pointAttr[f.state||""]);if(a&&a.dataLabels&&f.dataLabel)f.dataLabel=
f.dataLabel.destroy();f.redraw=null};i=f.index;g.updateParallelArrays(f,i);if(l&&f.name)l[f.x]=f.name;k.data[i]=f.options;g.isDirty=g.isDirtyData=!0;if(!g.fixedBox&&g.hasCartesianSeries)j.isDirtyBox=!0;j.legend.display&&k.legendType==="point"&&(g.updateTotals(),j.legend.clearItems());b&&j.redraw(c)}var f=this,g=f.series,h=f.graphic,i,j=g.chart,k=g.options,l=g.xAxis&&g.xAxis.names,b=p(b,!0);d===!1?e():f.firePointEvent("update",{options:a},e)},remove:function(a,b){this.series.removePoint(Oa(this,this.series.data),
a,b)}});x(P.prototype,{addPoint:function(a,b,c,d){var e=this.options,f=this.data,g=this.graph,h=this.area,i=this.chart,j=this.xAxis&&this.xAxis.names,k=g&&g.shift||0,l=e.data,m,o=this.xData;Ya(d,i);c&&n([g,h,this.graphNeg,this.areaNeg],function(a){if(a)a.shift=k+1});if(h)h.isArea=!0;b=p(b,!0);d={series:this};this.pointClass.prototype.applyOptions.apply(d,[a]);g=d.x;h=o.length;if(this.requireSorting&&g<o[h-1])for(m=!0;h&&o[h-1]>g;)h--;this.updateParallelArrays(d,"splice",h,0,0);this.updateParallelArrays(d,
h);if(j&&d.name)j[g]=d.name;l.splice(h,0,a);m&&(this.data.splice(h,0,null),this.processData());e.legendType==="point"&&this.generatePoints();c&&(f[0]&&f[0].remove?f[0].remove(!1):(f.shift(),this.updateParallelArrays(d,"shift"),l.shift()));this.isDirtyData=this.isDirty=!0;b&&(this.getAttribs(),i.redraw())},removePoint:function(a,b,c){var d=this,e=d.data,f=e[a],g=d.points,h=d.chart,i=function(){e.length===g.length&&g.splice(a,1);e.splice(a,1);d.options.data.splice(a,1);d.updateParallelArrays(f||{series:d},
"splice",a,1);f&&f.destroy();d.isDirty=!0;d.isDirtyData=!0;b&&h.redraw()};Ya(c,h);b=p(b,!0);f?f.firePointEvent("remove",null,i):i()},remove:function(a,b){var c=this,d=c.chart,a=p(a,!0);if(!c.isRemoving)c.isRemoving=!0,K(c,"remove",null,function(){c.destroy();d.isDirtyLegend=d.isDirtyBox=!0;d.linkSeries();a&&d.redraw(b)});c.isRemoving=!1},update:function(a,b){var c=this,d=this.chart,e=this.userOptions,f=this.type,g=I[f].prototype,h=["group","markerGroup","dataLabelsGroup"],i;if(a.type&&a.type!==f||
a.zIndex!==void 0)h.length=0;n(h,function(a){h[a]=c[a];delete c[a]});a=y(e,{animation:!1,index:this.index,pointStart:this.xData[0]},{data:this.options.data},a);this.remove(!1);for(i in g)this[i]=r;x(this,I[a.type||f].prototype);n(h,function(a){c[a]=h[a]});this.init(d,a);d.linkSeries();p(b,!0)&&d.redraw(!1)}});x(O.prototype,{update:function(a,b){var c=this.chart,a=c.options[this.coll][this.options.index]=y(this.userOptions,a);this.destroy(!0);this._addedPlotLB=r;this.init(c,x(a,{events:r}));c.isDirtyBox=
!0;p(b,!0)&&c.redraw()},remove:function(a){for(var b=this.chart,c=this.coll,d=this.series,e=d.length;e--;)d[e]&&d[e].remove(!1);ua(b.axes,this);ua(b[c],this);b.options[c].splice(this.options.index,1);n(b[c],function(a,b){a.options.index=b});this.destroy();b.isDirtyBox=!0;p(a,!0)&&b.redraw()},setTitle:function(a,b){this.update({title:a},b)},setCategories:function(a,b){this.update({categories:a},b)}});var Da=ja(P);I.line=Da;V.area=y(Q,{threshold:0});var ya=ja(P,{type:"area",getSegments:function(){var a=
this,b=[],c=[],d=[],e=this.xAxis,f=this.yAxis,g=f.stacks[this.stackKey],h={},i,j,k=this.points,l=this.options.connectNulls,m,o;if(this.options.stacking&&!this.cropped){for(m=0;m<k.length;m++)h[k[m].x]=k[m];for(o in g)g[o].total!==null&&d.push(+o);d.sort(function(a,b){return a-b});n(d,function(b){var d=0,k;if(!l||h[b]&&h[b].y!==null)if(h[b])c.push(h[b]);else{for(m=a.index;m<=f.series.length;m++)if(k=g[b].points[m+","+b]){d=k[1];break}i=e.translate(b);j=f.toPixels(d,!0);c.push({y:null,plotX:i,clientX:i,
plotY:j,yBottom:j,onMouseOver:ha})}});c.length&&b.push(c)}else P.prototype.getSegments.call(this),b=this.segments;this.segments=b},getSegmentPath:function(a){var b=P.prototype.getSegmentPath.call(this,a),c=[].concat(b),d,e=this.options;d=b.length;var f=this.yAxis.getThreshold(e.threshold),g;d===3&&c.push("L",b[1],b[2]);if(e.stacking&&!this.closedStacks)for(d=a.length-1;d>=0;d--)g=p(a[d].yBottom,f),d<a.length-1&&e.step&&c.push(a[d+1].plotX,g),c.push(a[d].plotX,g);else this.closeSegment(c,a,f);this.areaPath=
this.areaPath.concat(c);return b},closeSegment:function(a,b,c){a.push("L",b[b.length-1].plotX,c,"L",b[0].plotX,c)},drawGraph:function(){this.areaPath=[];P.prototype.drawGraph.apply(this);var a=this,b=this.areaPath,c=this.options,d=[["area",this.color,c.fillColor]];n(this.zones,function(b,f){d.push(["colorArea"+f,b.color||a.color,b.fillColor||c.fillColor])});n(d,function(d){var f=d[0],g=a[f];g?g.animate({d:b}):a[f]=a.chart.renderer.path(b).attr({fill:p(d[2],wa(d[1]).setOpacity(p(c.fillOpacity,0.75)).get()),
zIndex:0}).add(a.group)})},drawLegendSymbol:G.drawRectangle});I.area=ya;V.spline=y(Q);Da=ja(P,{type:"spline",getPointSpline:function(a,b,c){var d=b.plotX,e=b.plotY,f=a[c-1],g=a[c+1],h,i,j,k;if(f&&g){a=f.plotY;j=g.plotX;var g=g.plotY,l;h=(1.5*d+f.plotX)/2.5;i=(1.5*e+a)/2.5;j=(1.5*d+j)/2.5;k=(1.5*e+g)/2.5;l=(k-i)*(j-d)/(j-h)+e-k;i+=l;k+=l;i>a&&i>e?(i=v(a,e),k=2*e-i):i<a&&i<e&&(i=B(a,e),k=2*e-i);k>g&&k>e?(k=v(g,e),i=2*e-k):k<g&&k<e&&(k=B(g,e),i=2*e-k);b.rightContX=j;b.rightContY=k}c?(b=["C",f.rightContX||
f.plotX,f.rightContY||f.plotY,h||d,i||e,d,e],f.rightContX=f.rightContY=null):b=["M",d,e];return b}});I.spline=Da;V.areaspline=y(V.area);ya=ya.prototype;Da=ja(Da,{type:"areaspline",closedStacks:!0,getSegmentPath:ya.getSegmentPath,closeSegment:ya.closeSegment,drawGraph:ya.drawGraph,drawLegendSymbol:G.drawRectangle});I.areaspline=Da;V.column=y(Q,{borderColor:"#FFFFFF",borderRadius:0,groupPadding:0.2,marker:null,pointPadding:0.1,minPointLength:0,cropThreshold:50,pointRange:null,states:{hover:{brightness:0.1,
shadow:!1,halo:!1},select:{color:"#C0C0C0",borderColor:"#000000",shadow:!1}},dataLabels:{align:null,verticalAlign:null,y:null},stickyTracking:!1,tooltip:{distance:6},threshold:0});Da=ja(P,{type:"column",pointAttrToOptions:{stroke:"borderColor",fill:"color",r:"borderRadius"},cropShoulder:0,directTouch:!0,trackerGroups:["group","dataLabelsGroup"],negStacks:!0,init:function(){P.prototype.init.apply(this,arguments);var a=this,b=a.chart;b.hasRendered&&n(b.series,function(b){if(b.type===a.type)b.isDirty=
!0})},getColumnMetrics:function(){var a=this,b=a.options,c=a.xAxis,d=a.yAxis,e=c.reversed,f,g={},h,i=0;b.grouping===!1?i=1:n(a.chart.series,function(b){var c=b.options,e=b.yAxis;if(b.type===a.type&&b.visible&&d.len===e.len&&d.pos===e.pos)c.stacking?(f=b.stackKey,g[f]===r&&(g[f]=i++),h=g[f]):c.grouping!==!1&&(h=i++),b.columnIndex=h});var c=B(R(c.transA)*(c.ordinalSlope||b.pointRange||c.closestPointRange||c.tickInterval||1),c.len),j=c*b.groupPadding,k=(c-2*j)/i,l=b.pointWidth,b=s(l)?(k-l)/2:k*b.pointPadding,
l=p(l,k-2*b);return a.columnMetrics={width:l,offset:b+(j+((e?i-(a.columnIndex||0):a.columnIndex)||0)*k-c/2)*(e?-1:1)}},translate:function(){var a=this,b=a.chart,c=a.options,d=a.borderWidth=p(c.borderWidth,a.closestPointRange*a.xAxis.transA<2?0:1),e=a.yAxis,f=a.translatedThreshold=e.getThreshold(c.threshold),g=p(c.minPointLength,5),h=a.getColumnMetrics(),i=h.width,j=a.barW=v(i,1+2*d),k=a.pointXOffset=h.offset,l=-(d%2?0.5:0),m=d%2?0.5:1;b.renderer.isVML&&b.inverted&&(m+=1);c.pointPadding&&(j=za(j));
P.prototype.translate.apply(a);n(a.points,function(c){var d=p(c.yBottom,f),h=B(v(-999-d,c.plotY),e.len+999+d),n=c.plotX+k,r=j,u=B(h,d),s;s=v(h,d)-u;R(s)<g&&g&&(s=g,u=w(R(u-f)>g?d-g:f-(e.translate(c.y,0,1,0,1)<=f?g:0)));c.barX=n;c.pointWidth=i;c.tooltipPos=b.inverted?[e.len+e.pos-b.plotLeft-h,a.xAxis.len-n-r/2]:[n+r/2,h+e.pos-b.plotTop];r=w(n+r)+l;n=w(n)+l;r-=n;d=R(u)<0.5;s=B(w(u+s)+m,9E4);u=w(u)+m;s-=u;d&&(u-=1,s+=1);c.shapeType="rect";c.shapeArgs={x:n,y:u,width:r,height:s}})},getSymbol:ha,drawLegendSymbol:G.drawRectangle,
drawGraph:ha,drawPoints:function(){var a=this,b=this.chart,c=a.options,d=b.renderer,e=c.animationLimit||250,f,g;n(a.points,function(h){var i=h.plotY,j=h.graphic;if(i!==r&&!isNaN(i)&&h.y!==null)f=h.shapeArgs,i=s(a.borderWidth)?{"stroke-width":a.borderWidth}:{},g=h.pointAttr[h.selected?"select":""]||a.pointAttr[""],j?(ab(j),j.attr(i)[b.pointCount<e?"animate":"attr"](y(f))):h.graphic=d[h.shapeType](f).attr(i).attr(g).add(a.group).shadow(c.shadow,null,c.stacking&&!c.borderRadius);else if(j)h.graphic=
j.destroy()})},animate:function(a){var b=this.yAxis,c=this.options,d=this.chart.inverted,e={};if(ea)a?(e.scaleY=0.001,a=B(b.pos+b.len,v(b.pos,b.toPixels(c.threshold))),d?e.translateX=a-b.len:e.translateY=a,this.group.attr(e)):(e.scaleY=1,e[d?"translateX":"translateY"]=b.pos,this.group.animate(e,this.options.animation),this.animate=null)},remove:function(){var a=this,b=a.chart;b.hasRendered&&n(b.series,function(b){if(b.type===a.type)b.isDirty=!0});P.prototype.remove.apply(a,arguments)}});I.column=
Da;V.bar=y(V.column);ya=ja(Da,{type:"bar",inverted:!0});I.bar=ya;V.scatter=y(Q,{lineWidth:0,marker:{enabled:!0},tooltip:{headerFormat:'<span style="color:{series.color}">●</span> <span style="font-size: 10px;"> {series.name}</span><br/>',pointFormat:"x: <b>{point.x}</b><br/>y: <b>{point.y}</b><br/>"}});ya=ja(P,{type:"scatter",sorted:!1,requireSorting:!1,noSharedTooltip:!0,trackerGroups:["group","markerGroup","dataLabelsGroup"],takeOrdinalPosition:!1,kdDimensions:2,kdComparer:"distR",drawGraph:function(){this.options.lineWidth&&
P.prototype.drawGraph.call(this)}});I.scatter=ya;V.pie=y(Q,{borderColor:"#FFFFFF",borderWidth:1,center:[null,null],clip:!1,colorByPoint:!0,dataLabels:{distance:30,enabled:!0,formatter:function(){return this.point.name},x:0},ignoreHiddenPoint:!0,legendType:"point",marker:null,size:null,showInLegend:!1,slicedOffset:10,states:{hover:{brightness:0.1,shadow:!1}},stickyTracking:!1,tooltip:{followPointer:!0}});Q={type:"pie",isCartesian:!1,pointClass:ja(Ca,{init:function(){Ca.prototype.init.apply(this,arguments);
var a=this,b;x(a,{visible:a.visible!==!1,name:p(a.name,"Slice")});b=function(b){a.slice(b.type==="select")};D(a,"select",b);D(a,"unselect",b);return a},setVisible:function(a){var b=this,c=b.series,d=c.chart,e=!c.isDirty&&c.options.ignoreHiddenPoint;b.visible=b.options.visible=a=a===r?!b.visible:a;c.options.data[Oa(b,c.data)]=b.options;n(["graphic","dataLabel","connector","shadowGroup"],function(c){if(b[c])b[c][a?"show":"hide"](!0)});b.legendItem&&(d.hasRendered&&(c.updateTotals(),d.legend.clearItems(),
e||d.legend.render()),d.legend.colorizeItem(b,a));if(e)c.isDirty=!0,d.redraw()},slice:function(a,b,c){var d=this.series;Ya(c,d.chart);p(b,!0);this.sliced=this.options.sliced=a=s(a)?a:!this.sliced;d.options.data[Oa(this,d.data)]=this.options;a=a?this.slicedTranslation:{translateX:0,translateY:0};this.graphic.animate(a);this.shadowGroup&&this.shadowGroup.animate(a)},haloPath:function(a){var b=this.shapeArgs,c=this.series.chart;return this.sliced||!this.visible?[]:this.series.chart.renderer.symbols.arc(c.plotLeft+
b.x,c.plotTop+b.y,b.r+a,b.r+a,{innerR:this.shapeArgs.r,start:b.start,end:b.end})}}),requireSorting:!1,noSharedTooltip:!0,trackerGroups:["group","dataLabelsGroup"],axisTypes:[],pointAttrToOptions:{stroke:"borderColor","stroke-width":"borderWidth",fill:"color"},getColor:ha,animate:function(a){var b=this,c=b.points,d=b.startAngleRad;if(!a)n(c,function(a){var c=a.graphic,a=a.shapeArgs;c&&(c.attr({r:b.center[3]/2,start:d,end:d}),c.animate({r:a.r,start:a.start,end:a.end},b.options.animation))}),b.animate=
null},setData:function(a,b,c,d){P.prototype.setData.call(this,a,!1,c,d);this.processData();this.generatePoints();p(b,!0)&&this.chart.redraw(c)},updateTotals:function(){var a,b=0,c,d,e,f=this.options.ignoreHiddenPoint;c=this.points;d=c.length;for(a=0;a<d;a++){e=c[a];if(e.y<0)e.y=null;b+=f&&!e.visible?0:e.y}this.total=b;for(a=0;a<d;a++)e=c[a],e.percentage=b>0&&(e.visible||!f)?e.y/b*100:0,e.total=b},generatePoints:function(){P.prototype.generatePoints.call(this);this.updateTotals()},translate:function(a){this.generatePoints();
var b=0,c=this.options,d=c.slicedOffset,e=d+c.borderWidth,f,g,h,i=c.startAngle||0,j=this.startAngleRad=va/180*(i-90),i=(this.endAngleRad=va/180*(p(c.endAngle,i+360)-90))-j,k=this.points,l=c.dataLabels.distance,c=c.ignoreHiddenPoint,m,o=k.length,q;if(!a)this.center=a=this.getCenter();this.getX=function(b,c){h=Y.asin(B((b-a[1])/(a[2]/2+l),1));return a[0]+(c?-1:1)*ba(h)*(a[2]/2+l)};for(m=0;m<o;m++){q=k[m];f=j+b*i;if(!c||q.visible)b+=q.percentage/100;g=j+b*i;q.shapeType="arc";q.shapeArgs={x:a[0],y:a[1],
r:a[2]/2,innerR:a[3]/2,start:w(f*1E3)/1E3,end:w(g*1E3)/1E3};h=(g+f)/2;h>1.5*va?h-=2*va:h<-va/2&&(h+=2*va);q.slicedTranslation={translateX:w(ba(h)*d),translateY:w(ga(h)*d)};f=ba(h)*a[2]/2;g=ga(h)*a[2]/2;q.tooltipPos=[a[0]+f*0.7,a[1]+g*0.7];q.half=h<-va/2||h>va/2?1:0;q.angle=h;e=B(e,l/2);q.labelPos=[a[0]+f+ba(h)*l,a[1]+g+ga(h)*l,a[0]+f+ba(h)*e,a[1]+g+ga(h)*e,a[0]+f,a[1]+g,l<0?"center":q.half?"right":"left",h]}},drawGraph:null,drawPoints:function(){var a=this,b=a.chart.renderer,c,d,e=a.options.shadow,
f,g;if(e&&!a.shadowGroup)a.shadowGroup=b.g("shadow").add(a.group);n(a.points,function(h){d=h.graphic;g=h.shapeArgs;f=h.shadowGroup;if(e&&!f)f=h.shadowGroup=b.g("shadow").add(a.shadowGroup);c=h.sliced?h.slicedTranslation:{translateX:0,translateY:0};f&&f.attr(c);d?d.animate(x(g,c)):h.graphic=d=b[h.shapeType](g).setRadialReference(a.center).attr(h.pointAttr[h.selected?"select":""]).attr({"stroke-linejoin":"round"}).attr(c).add(a.group).shadow(e,f);h.visible!==void 0&&h.setVisible(h.visible)})},searchPoint:ha,
sortByAngle:function(a,b){a.sort(function(a,d){return a.angle!==void 0&&(d.angle-a.angle)*b})},drawLegendSymbol:G.drawRectangle,getCenter:fc.getCenter,getSymbol:ha};Q=ja(P,Q);I.pie=Q;P.prototype.drawDataLabels=function(){var a=this,b=a.options,c=b.cursor,d=b.dataLabels,e=a.points,f,g,h=a.hasRendered||0,i,j,k=a.chart.renderer;if(d.enabled||a._hasPointLabels)a.dlProcessOptions&&a.dlProcessOptions(d),j=a.plotGroup("dataLabelsGroup","data-labels",d.defer?"hidden":"visible",d.zIndex||6),p(d.defer,!0)&&
(j.attr({opacity:+h}),h||D(a,"afterAnimate",function(){a.visible&&j.show();j[b.animation?"animate":"attr"]({opacity:1},{duration:200})})),g=d,n(e,function(e){var h,o=e.dataLabel,q,t,n=e.connector,N=!0,u,v={};f=e.dlOptions||e.options&&e.options.dataLabels;h=p(f&&f.enabled,g.enabled);if(o&&!h)e.dataLabel=o.destroy();else if(h){d=y(g,f);u=d.style;h=d.rotation;q=e.getLabelConfig();i=d.format?Ma(d.format,q):d.formatter.call(q,d);u.color=p(d.color,u.color,a.color,"black");if(o)if(s(i))o.attr({text:i}),
N=!1;else{if(e.dataLabel=o=o.destroy(),n)e.connector=n.destroy()}else if(s(i)){o={fill:d.backgroundColor,stroke:d.borderColor,"stroke-width":d.borderWidth,r:d.borderRadius||0,rotation:h,padding:d.padding,zIndex:1};if(u.color==="contrast")v.color=d.inside||d.distance<0||b.stacking?k.getContrast(e.color||a.color):"#000000";if(c)v.cursor=c;for(t in o)o[t]===r&&delete o[t];o=e.dataLabel=k[h?"text":"label"](i,0,-999,d.shape,null,null,d.useHTML).attr(o).css(x(u,v)).add(j).shadow(d.shadow)}o&&a.alignDataLabel(e,
o,d,null,N)}})};P.prototype.alignDataLabel=function(a,b,c,d,e){var f=this.chart,g=f.inverted,h=p(a.plotX,-999),i=p(a.plotY,-999),j=b.getBBox(),k=f.renderer.fontMetrics(c.style.fontSize).b,l=this.visible&&(a.series.forceDL||f.isInsidePlot(h,w(i),g)||d&&f.isInsidePlot(h,g?d.x+1:d.y+d.height-1,g));if(l)d=x({x:g?f.plotWidth-i:h,y:w(g?f.plotHeight-h:i),width:0,height:0},d),x(c,{width:j.width,height:j.height}),c.rotation?(a=f.renderer.rotCorr(k,c.rotation),b[e?"attr":"animate"]({x:d.x+c.x+d.width/2+a.x,
y:d.y+c.y+d.height/2}).attr({align:c.align})):(b.align(c,null,d),g=b.alignAttr,p(c.overflow,"justify")==="justify"?this.justifyDataLabel(b,c,g,j,d,e):p(c.crop,!0)&&(l=f.isInsidePlot(g.x,g.y)&&f.isInsidePlot(g.x+j.width,g.y+j.height)),c.shape&&b.attr({anchorX:a.plotX,anchorY:a.plotY}));if(!l)b.attr({y:-999}),b.placed=!1};P.prototype.justifyDataLabel=function(a,b,c,d,e,f){var g=this.chart,h=b.align,i=b.verticalAlign,j,k,l=a.box?0:a.padding||0;j=c.x+l;if(j<0)h==="right"?b.align="left":b.x=-j,k=!0;j=
c.x+d.width-l;if(j>g.plotWidth)h==="left"?b.align="right":b.x=g.plotWidth-j,k=!0;j=c.y+l;if(j<0)i==="bottom"?b.verticalAlign="top":b.y=-j,k=!0;j=c.y+d.height-l;if(j>g.plotHeight)i==="top"?b.verticalAlign="bottom":b.y=g.plotHeight-j,k=!0;if(k)a.placed=!f,a.align(b,null,e)};if(I.pie)I.pie.prototype.drawDataLabels=function(){var a=this,b=a.data,c,d=a.chart,e=a.options.dataLabels,f=p(e.connectorPadding,10),g=p(e.connectorWidth,1),h=d.plotWidth,i=d.plotHeight,j,k,l=p(e.softConnector,!0),m=e.distance,o=
a.center,q=o[2]/2,t=o[1],s=m>0,r,u,A,x=[[],[]],y,z,D,E,L,C=[0,0,0,0],I=function(a,b){return b.y-a.y};if(a.visible&&(e.enabled||a._hasPointLabels)){P.prototype.drawDataLabels.apply(a);n(b,function(a){a.dataLabel&&a.visible&&x[a.half].push(a)});for(E=2;E--;){var G=[],M=[],H=x[E],K=H.length,F;if(K){a.sortByAngle(H,E-0.5);for(L=b=0;!b&&H[L];)b=H[L]&&H[L].dataLabel&&(H[L].dataLabel.getBBox().height||21),L++;if(m>0){u=B(t+q+m,d.plotHeight);for(L=v(0,t-q-m);L<=u;L+=b)G.push(L);u=G.length;if(K>u){c=[].concat(H);
c.sort(I);for(L=K;L--;)c[L].rank=L;for(L=K;L--;)H[L].rank>=u&&H.splice(L,1);K=H.length}for(L=0;L<K;L++){c=H[L];A=c.labelPos;c=9999;var Q,O;for(O=0;O<u;O++)Q=R(G[O]-A[1]),Q<c&&(c=Q,F=O);if(F<L&&G[L]!==null)F=L;else for(u<K-L+F&&G[L]!==null&&(F=u-K+L);G[F]===null;)F++;M.push({i:F,y:G[F]});G[F]=null}M.sort(I)}for(L=0;L<K;L++){c=H[L];A=c.labelPos;r=c.dataLabel;D=c.visible===!1?"hidden":"visible";c=A[1];if(m>0){if(u=M.pop(),F=u.i,z=u.y,c>z&&G[F+1]!==null||c<z&&G[F-1]!==null)z=B(v(0,c),d.plotHeight)}else z=
c;y=e.justify?o[0]+(E?-1:1)*(q+m):a.getX(z===t-q-m||z===t+q+m?c:z,E);r._attr={visibility:D,align:A[6]};r._pos={x:y+e.x+({left:f,right:-f}[A[6]]||0),y:z+e.y-10};r.connX=y;r.connY=z;if(this.options.size===null)u=r.width,y-u<f?C[3]=v(w(u-y+f),C[3]):y+u>h-f&&(C[1]=v(w(y+u-h+f),C[1])),z-b/2<0?C[0]=v(w(-z+b/2),C[0]):z+b/2>i&&(C[2]=v(w(z+b/2-i),C[2]))}}}if(Fa(C)===0||this.verifyDataLabelOverflow(C))this.placeDataLabels(),s&&g&&n(this.points,function(b){j=b.connector;A=b.labelPos;if((r=b.dataLabel)&&r._pos)D=
r._attr.visibility,y=r.connX,z=r.connY,k=l?["M",y+(A[6]==="left"?5:-5),z,"C",y,z,2*A[2]-A[4],2*A[3]-A[5],A[2],A[3],"L",A[4],A[5]]:["M",y+(A[6]==="left"?5:-5),z,"L",A[2],A[3],"L",A[4],A[5]],j?(j.animate({d:k}),j.attr("visibility",D)):b.connector=j=a.chart.renderer.path(k).attr({"stroke-width":g,stroke:e.connectorColor||b.color||"#606060",visibility:D}).add(a.dataLabelsGroup);else if(j)b.connector=j.destroy()})}},I.pie.prototype.placeDataLabels=function(){n(this.points,function(a){var a=a.dataLabel,
b;if(a)(b=a._pos)?(a.attr(a._attr),a[a.moved?"animate":"attr"](b),a.moved=!0):a&&a.attr({y:-999})})},I.pie.prototype.alignDataLabel=ha,I.pie.prototype.verifyDataLabelOverflow=function(a){var b=this.center,c=this.options,d=c.center,e=c=c.minSize||80,f;d[0]!==null?e=v(b[2]-v(a[1],a[3]),c):(e=v(b[2]-a[1]-a[3],c),b[0]+=(a[3]-a[1])/2);d[1]!==null?e=v(B(e,b[2]-v(a[0],a[2])),c):(e=v(B(e,b[2]-a[0]-a[2]),c),b[1]+=(a[0]-a[2])/2);e<b[2]?(b[2]=e,this.translate(b),n(this.points,function(a){if(a.dataLabel)a.dataLabel._pos=
null}),this.drawDataLabels&&this.drawDataLabels()):f=!0;return f};if(I.column)I.column.prototype.alignDataLabel=function(a,b,c,d,e){var f=this.chart.inverted,g=a.series,h=a.dlBox||a.shapeArgs,i=a.below||a.plotY>p(this.translatedThreshold,g.yAxis.len),j=p(c.inside,!!this.options.stacking);if(h&&(d=y(h),f&&(d={x:g.yAxis.len-d.y-d.height,y:g.xAxis.len-d.x-d.width,width:d.height,height:d.width}),!j))f?(d.x+=i?0:d.width,d.width=0):(d.y+=i?d.height:0,d.height=0);c.align=p(c.align,!f||j?"center":i?"right":
"left");c.verticalAlign=p(c.verticalAlign,f||j?"middle":i?"top":"bottom");P.prototype.alignDataLabel.call(this,a,b,c,d,e)};(function(a){var b=a.Chart,c=a.each,d=HighchartsAdapter.addEvent;b.prototype.callbacks.push(function(a){function b(){var d=[];c(a.series,function(a){var b=a.options.dataLabels;(b.enabled||a._hasPointLabels)&&!b.allowOverlap&&a.visible&&c(a.points,function(a){if(a.dataLabel)a.dataLabel.labelrank=a.labelrank,d.push(a.dataLabel)})});a.hideOverlappingLabels(d)}b();d(a,"redraw",b)});
b.prototype.hideOverlappingLabels=function(a){var b=a.length,c,d,i,j;for(d=0;d<b;d++)if(c=a[d])c.oldOpacity=c.opacity,c.newOpacity=1;for(d=0;d<b;d++){i=a[d];for(c=d+1;c<b;++c)if(j=a[c],i&&j&&i.placed&&j.placed&&i.newOpacity!==0&&j.newOpacity!==0&&!(j.alignAttr.x>i.alignAttr.x+i.width||j.alignAttr.x+j.width<i.alignAttr.x||j.alignAttr.y>i.alignAttr.y+i.height||j.alignAttr.y+j.height<i.alignAttr.y))(i.labelrank<j.labelrank?i:j).newOpacity=0}for(d=0;d<b;d++)if(c=a[d]){if(c.oldOpacity!==c.newOpacity&&
c.placed)c.alignAttr.opacity=c.newOpacity,c[c.isOld&&c.newOpacity?"animate":"attr"](c.alignAttr);c.isOld=!0}}})(z);var jb=z.TrackerMixin={drawTrackerPoint:function(){var a=this,b=a.chart,c=b.pointer,d=a.options.cursor,e=d&&{cursor:d},f=function(a){for(var c=a.target,d;c&&!d;)d=c.point,c=c.parentNode;if(d!==r&&d!==b.hoverPoint)d.onMouseOver(a)};n(a.points,function(a){if(a.graphic)a.graphic.element.point=a;if(a.dataLabel)a.dataLabel.element.point=a});if(!a._hasTracking)n(a.trackerGroups,function(b){if(a[b]&&
(a[b].addClass("highcharts-tracker").on("mouseover",f).on("mouseout",function(a){c.onTrackerMouseOut(a)}).css(e),$a))a[b].on("touchstart",f)}),a._hasTracking=!0},drawTrackerGraph:function(){var a=this,b=a.options,c=b.trackByArea,d=[].concat(c?a.areaPath:a.graphPath),e=d.length,f=a.chart,g=f.pointer,h=f.renderer,i=f.options.tooltip.snap,j=a.tracker,k=b.cursor,l=k&&{cursor:k},k=a.singlePoints,m,o=function(){if(f.hoverSeries!==a)a.onMouseOver()},q="rgba(192,192,192,"+(ea?1.0E-4:0.002)+")";if(e&&!c)for(m=
e+1;m--;)d[m]==="M"&&d.splice(m+1,0,d[m+1]-i,d[m+2],"L"),(m&&d[m]==="M"||m===e)&&d.splice(m,0,"L",d[m-2]+i,d[m-1]);for(m=0;m<k.length;m++)e=k[m],d.push("M",e.plotX-i,e.plotY,"L",e.plotX+i,e.plotY);j?j.attr({d:d}):(a.tracker=h.path(d).attr({"stroke-linejoin":"round",visibility:a.visible?"visible":"hidden",stroke:q,fill:c?q:Z,"stroke-width":b.lineWidth+(c?0:2*i),zIndex:2}).add(a.group),n([a.tracker,a.markerGroup],function(a){a.addClass("highcharts-tracker").on("mouseover",o).on("mouseout",function(a){g.onTrackerMouseOut(a)}).css(l);
if($a)a.on("touchstart",o)}))}};if(I.column)Da.prototype.drawTracker=jb.drawTrackerPoint;if(I.pie)I.pie.prototype.drawTracker=jb.drawTrackerPoint;if(I.scatter)ya.prototype.drawTracker=jb.drawTrackerPoint;x(rb.prototype,{setItemEvents:function(a,b,c,d,e){var f=this;(c?b:a.legendGroup).on("mouseover",function(){a.setState("hover");b.css(f.options.itemHoverStyle)}).on("mouseout",function(){b.css(a.visible?d:e);a.setState()}).on("click",function(b){var c=function(){a.setVisible()},b={browserEvent:b};
a.firePointEvent?a.firePointEvent("legendItemClick",b,c):K(a,"legendItemClick",b,c)})},createCheckboxForItem:function(a){a.checkbox=aa("input",{type:"checkbox",checked:a.selected,defaultChecked:a.selected},this.options.itemCheckboxStyle,this.chart.container);D(a.checkbox,"click",function(b){K(a.series||a,"checkboxClick",{checked:b.target.checked,item:a},function(){a.select()})})}});F.legend.itemStyle.cursor="pointer";x(Pa.prototype,{showResetZoom:function(){var a=this,b=F.lang,c=a.options.chart.resetZoomButton,
d=c.theme,e=d.states,f=c.relativeTo==="chart"?null:"plotBox";this.resetZoomButton=a.renderer.button(b.resetZoom,null,null,function(){a.zoomOut()},d,e&&e.hover).attr({align:c.position.align,title:b.resetZoomTitle}).add().align(c.position,!1,f)},zoomOut:function(){var a=this;K(a,"selection",{resetSelection:!0},function(){a.zoom()})},zoom:function(a){var b,c=this.pointer,d=!1,e;!a||a.resetSelection?n(this.axes,function(a){b=a.zoom()}):n(a.xAxis.concat(a.yAxis),function(a){var e=a.axis,h=e.isXAxis;if(c[h?
"zoomX":"zoomY"]||c[h?"pinchX":"pinchY"])b=e.zoom(a.min,a.max),e.displayBtn&&(d=!0)});e=this.resetZoomButton;if(d&&!e)this.showResetZoom();else if(!d&&ia(e))this.resetZoomButton=e.destroy();b&&this.redraw(p(this.options.chart.animation,a&&a.animation,this.pointCount<100))},pan:function(a,b){var c=this,d=c.hoverPoints,e;d&&n(d,function(a){a.setState()});n(b==="xy"?[1,0]:[1],function(b){var d=a[b?"chartX":"chartY"],h=c[b?"xAxis":"yAxis"][0],i=c[b?"mouseDownX":"mouseDownY"],j=(h.pointRange||0)/2,k=h.getExtremes(),
l=h.toValue(i-d,!0)+j,j=h.toValue(i+c[b?"plotWidth":"plotHeight"]-d,!0)-j,i=i>d;if(h.series.length&&(i||l>B(k.dataMin,k.min))&&(!i||j<v(k.dataMax,k.max)))h.setExtremes(l,j,!1,!1,{trigger:"pan"}),e=!0;c[b?"mouseDownX":"mouseDownY"]=d});e&&c.redraw(!1);M(c.container,{cursor:"move"})}});x(Ca.prototype,{select:function(a,b){var c=this,d=c.series,e=d.chart,a=p(a,!c.selected);c.firePointEvent(a?"select":"unselect",{accumulate:b},function(){c.selected=c.options.selected=a;d.options.data[Oa(c,d.data)]=c.options;
c.setState(a&&"select");b||n(e.getSelectedPoints(),function(a){if(a.selected&&a!==c)a.selected=a.options.selected=!1,d.options.data[Oa(a,d.data)]=a.options,a.setState(""),a.firePointEvent("unselect")})})},onMouseOver:function(a){var b=this.series,c=b.chart,d=c.tooltip,e=c.hoverPoint;if(c.hoverSeries!==b)b.onMouseOver();if(e&&e!==this)e.onMouseOut();this.firePointEvent("mouseOver");d&&(!d.shared||b.noSharedTooltip)&&d.refresh(this,a);this.setState("hover");c.hoverPoint=this},onMouseOut:function(){var a=
this.series.chart,b=a.hoverPoints;this.firePointEvent("mouseOut");if(!b||Oa(this,b)===-1)this.setState(),a.hoverPoint=null},importEvents:function(){if(!this.hasImportedEvents){var a=y(this.series.options.point,this.options).events,b;this.events=a;for(b in a)D(this,b,a[b]);this.hasImportedEvents=!0}},setState:function(a,b){var c=this.plotX,d=this.plotY,e=this.series,f=e.options.states,g=V[e.type].marker&&e.options.marker,h=g&&!g.enabled,i=g&&g.states[a],j=i&&i.enabled===!1,k=e.stateMarkerGraphic,l=
this.marker||{},m=e.chart,o=e.halo,q,a=a||"";q=this.pointAttr[a]||e.pointAttr[a];if(!(a===this.state&&!b||this.selected&&a!=="select"||f[a]&&f[a].enabled===!1||a&&(j||h&&i.enabled===!1)||a&&l.states&&l.states[a]&&l.states[a].enabled===!1)){if(this.graphic)g=g&&this.graphic.symbolName&&q.r,this.graphic.attr(y(q,g?{x:c-g,y:d-g,width:2*g,height:2*g}:{})),k&&k.hide();else{if(a&&i)if(g=i.radius,l=l.symbol||e.symbol,k&&k.currentSymbol!==l&&(k=k.destroy()),k)k[b?"animate":"attr"]({x:c-g,y:d-g});else if(l)e.stateMarkerGraphic=
k=m.renderer.symbol(l,c-g,d-g,2*g,2*g).attr(q).add(e.markerGroup),k.currentSymbol=l;if(k)k[a&&m.isInsidePlot(c,d,m.inverted)?"show":"hide"]()}if((c=f[a]&&f[a].halo)&&c.size){if(!o)e.halo=o=m.renderer.path().add(m.seriesGroup);o.attr(x({fill:wa(this.color||e.color).setOpacity(c.opacity).get()},c.attributes))[b?"animate":"attr"]({d:this.haloPath(c.size)})}else o&&o.attr({d:[]});this.state=a}},haloPath:function(a){var b=this.series,c=b.chart,d=b.getPlotBox(),e=c.inverted;return c.renderer.symbols.circle(d.translateX+
(e?b.yAxis.len-this.plotY:this.plotX)-a,d.translateY+(e?b.xAxis.len-this.plotX:this.plotY)-a,a*2,a*2)}});x(P.prototype,{onMouseOver:function(){var a=this.chart,b=a.hoverSeries;if(b&&b!==this)b.onMouseOut();this.options.events.mouseOver&&K(this,"mouseOver");this.setState("hover");a.hoverSeries=this},onMouseOut:function(){var a=this.options,b=this.chart,c=b.tooltip,d=b.hoverPoint;if(d)d.onMouseOut();this&&a.events.mouseOut&&K(this,"mouseOut");c&&!a.stickyTracking&&(!c.shared||this.noSharedTooltip)&&
c.hide();this.setState();b.hoverSeries=null},setState:function(a){var b=this.options,c=this.graph,d=this.graphNeg,e=b.states,b=b.lineWidth,a=a||"";if(this.state!==a)this.state=a,e[a]&&e[a].enabled===!1||(a&&(b=(e[a].lineWidth||b)+(e[a].lineWidthPlus||0)),c&&!c.dashstyle&&(a={"stroke-width":b},c.attr(a),d&&d.attr(a)))},setVisible:function(a,b){var c=this,d=c.chart,e=c.legendItem,f,g=d.options.chart.ignoreHiddenSeries,h=c.visible;f=(c.visible=a=c.userOptions.visible=a===r?!h:a)?"show":"hide";n(["group",
"dataLabelsGroup","markerGroup","tracker"],function(a){if(c[a])c[a][f]()});if(d.hoverSeries===c||(d.hoverPoint&&d.hoverPoint.series)===c)c.onMouseOut();e&&d.legend.colorizeItem(c,a);c.isDirty=!0;c.options.stacking&&n(d.series,function(a){if(a.options.stacking&&a.visible)a.isDirty=!0});n(c.linkedSeries,function(b){b.setVisible(a,!1)});if(g)d.isDirtyBox=!0;b!==!1&&d.redraw();K(c,f)},show:function(){this.setVisible(!0)},hide:function(){this.setVisible(!1)},select:function(a){this.selected=a=a===r?!this.selected:
a;if(this.checkbox)this.checkbox.checked=a;K(this,a?"select":"unselect")},drawTracker:jb.drawTrackerGraph});S(P.prototype,"init",function(a){var b;a.apply(this,Array.prototype.slice.call(arguments,1));(b=this.xAxis)&&b.options.ordinal&&D(this,"updatedData",function(){delete b.ordinalIndex})});S(O.prototype,"getTimeTicks",function(a,b,c,d,e,f,g,h){var i=0,j=0,k,l={},m,o,q,t=[],n=-Number.MAX_VALUE,p=this.options.tickPixelInterval;if(!this.options.ordinal&&!this.options.breaks||!f||f.length<3||c===r)return a.call(this,
b,c,d,e);for(o=f.length;j<o;j++){q=j&&f[j-1]>d;f[j]<c&&(i=j);if(j===o-1||f[j+1]-f[j]>g*5||q){if(f[j]>n){for(k=a.call(this,b,f[i],f[j],e);k.length&&k[0]<=n;)k.shift();k.length&&(n=k[k.length-1]);t=t.concat(k)}i=j+1}if(q)break}a=k.info;if(h&&a.unitRange<=H.hour){j=t.length-1;for(i=1;i<j;i++)ka("%d",t[i])!==ka("%d",t[i-1])&&(l[t[i]]="day",m=!0);m&&(l[t[0]]="day");a.higherRanks=l}t.info=a;if(h&&s(p)){var h=a=t.length,j=[],u;for(m=[];h--;)i=this.translate(t[h]),u&&(m[h]=u-i),j[h]=u=i;m.sort();m=m[X(m.length/
2)];m<p*0.6&&(m=null);h=t[a-1]>d?a-1:a;for(u=void 0;h--;)i=j[h],d=u-i,u&&d<p*0.8&&(m===null||d<m*0.8)?(l[t[h]]&&!l[t[h+1]]?(d=h+1,u=i):d=h,t.splice(d,1)):u=i}return t});x(O.prototype,{beforeSetTickPositions:function(){var a=this,b,c=[],d=!1,e,f=a.getExtremes(),g=f.min,f=f.max,h;if(a.options.ordinal||a.options.breaks){n(a.series,function(d,e){if(d.visible!==!1&&(d.takeOrdinalPosition!==!1||a.options.breaks))if(c=c.concat(d.processedXData),b=c.length,c.sort(function(a,b){return a-b}),b)for(e=b-1;e--;)c[e]===
c[e+1]&&c.splice(e,1)});b=c.length;if(b>2){e=c[1]-c[0];for(h=b-1;h--&&!d;)c[h+1]-c[h]!==e&&(d=!0);if(!a.options.keepOrdinalPadding&&(c[0]-g>e||f-c[c.length-1]>e))d=!0}d?(a.ordinalPositions=c,e=a.val2lin(v(g,c[0]),!0),h=v(a.val2lin(B(f,c[c.length-1]),!0),1),a.ordinalSlope=f=(f-g)/(h-e),a.ordinalOffset=g-e*f):a.ordinalPositions=a.ordinalSlope=a.ordinalOffset=r;if(a.options.ordinal)a.doPostTranslate=d}a.groupIntervalFactor=null},val2lin:function(a,b){var c=this.ordinalPositions;if(c){var d=c.length,
e,f;for(e=d;e--;)if(c[e]===a){f=e;break}for(e=d-1;e--;)if(a>c[e]||e===0){c=(a-c[e])/(c[e+1]-c[e]);f=e+c;break}return b?f:this.ordinalSlope*(f||0)+this.ordinalOffset}else return a},lin2val:function(a,b){var c=this.ordinalPositions;if(c){var d=this.ordinalSlope,e=this.ordinalOffset,f=c.length-1,g,h;if(b)a<0?a=c[0]:a>f?a=c[f]:(f=X(a),h=a-f);else for(;f--;)if(g=d*f+e,a>=g){d=d*(f+1)+e;h=(a-g)/(d-g);break}return h!==r&&c[f]!==r?c[f]+(h?h*(c[f+1]-c[f]):0):a}else return a},getExtendedPositions:function(){var a=
this.chart,b=this.series[0].currentDataGrouping,c=this.ordinalIndex,d=b?b.count+b.unitName:"raw",e=this.getExtremes(),f,g;if(!c)c=this.ordinalIndex={};if(!c[d])f={series:[],getExtremes:function(){return{min:e.dataMin,max:e.dataMax}},options:{ordinal:!0},val2lin:O.prototype.val2lin},n(this.series,function(c){g={xAxis:f,xData:c.xData,chart:a,destroyGroupedData:ha};g.options={dataGrouping:b?{enabled:!0,forced:!0,approximation:"open",units:[[b.unitName,[b.count]]]}:{enabled:!1}};c.processData.apply(g);
f.series.push(g)}),this.beforeSetTickPositions.apply(f),c[d]=f.ordinalPositions;return c[d]},getGroupIntervalFactor:function(a,b,c){var d=0,c=c.processedXData,e=c.length,f=[],g=this.groupIntervalFactor;if(!g){for(;d<e-1;d++)f[d]=c[d+1]-c[d];f.sort(function(a,b){return a-b});d=f[X(e/2)];a=v(a,c[0]);b=B(b,c[e-1]);this.groupIntervalFactor=g=e*d/(b-a)}return g},postProcessTickInterval:function(a){var b=this.ordinalSlope;return b?this.options.breaks?this.closestPointRange:a/(b/this.closestPointRange):
a}});S(Pa.prototype,"pan",function(a,b){var c=this.xAxis[0],d=b.chartX,e=!1;if(c.options.ordinal&&c.series.length){var f=this.mouseDownX,g=c.getExtremes(),h=g.dataMax,i=g.min,j=g.max,k=this.hoverPoints,l=c.closestPointRange,f=(f-d)/(c.translationSlope*(c.ordinalSlope||l)),m={ordinalPositions:c.getExtendedPositions()},l=c.lin2val,o=c.val2lin,q;if(m.ordinalPositions){if(R(f)>1)k&&n(k,function(a){a.setState()}),f<0?(k=m,q=c.ordinalPositions?c:m):(k=c.ordinalPositions?c:m,q=m),m=q.ordinalPositions,h>
m[m.length-1]&&m.push(h),this.fixedRange=j-i,f=c.toFixedRange(null,null,l.apply(k,[o.apply(k,[i,!0])+f,!0]),l.apply(q,[o.apply(q,[j,!0])+f,!0])),f.min>=B(g.dataMin,i)&&f.max<=v(h,j)&&c.setExtremes(f.min,f.max,!0,!1,{trigger:"pan"}),this.mouseDownX=d,M(this.container,{cursor:"move"})}else e=!0}else e=!0;e&&a.apply(this,Array.prototype.slice.call(arguments,1))});S(P.prototype,"getSegments",function(a){var b,c=this.options.gapSize,d=this.xAxis;a.apply(this,Array.prototype.slice.call(arguments,1));if(c)b=
this.segments,n(b,function(a,f){for(var g=a.length-1;g--;)a[g+1].x-a[g].x>d.closestPointRange*c&&b.splice(f+1,0,a.splice(g+1,a.length-g))})});(function(a){function b(){return Array.prototype.slice.call(arguments,1)}var c=a.pick,d=a.wrap,e=a.extend,f=HighchartsAdapter.fireEvent,g=a.Axis,h=a.Series;e(g.prototype,{isInBreak:function(a,b){var c=a.repeat||Infinity,d=a.from,e=a.to-a.from,c=b>=d?(b-d)%c:c-(d-b)%c;return a.inclusive?c<=e:c<e&&c!==0},isInAnyBreak:function(a,b){if(!this.options.breaks)return!1;
for(var d=this.options.breaks,e=d.length,f=!1,g=!1;e--;)this.isInBreak(d[e],a)&&(f=!0,g||(g=c(d[e].showPoints,this.isXAxis?!1:!0)));return f&&b?f&&!g:f}});d(g.prototype,"setTickPositions",function(a){a.apply(this,Array.prototype.slice.call(arguments,1));if(this.options.breaks){var b=this.tickPositions,c=this.tickPositions.info,d=[],e;if(!(c&&c.totalRange>=this.closestPointRange)){for(e=0;e<b.length;e++)this.isInAnyBreak(b[e])||d.push(b[e]);this.tickPositions=d;this.tickPositions.info=c}}});d(g.prototype,
"init",function(a,b,c){if(c.breaks&&c.breaks.length)c.ordinal=!1;a.call(this,b,c);if(this.options.breaks){var d=this;d.doPostTranslate=!0;this.val2lin=function(a){var b=a,c,e;for(e=0;e<d.breakArray.length;e++)if(c=d.breakArray[e],c.to<=a)b-=c.len;else if(c.from>=a)break;else if(d.isInBreak(c,a)){b-=a-c.from;break}return b};this.lin2val=function(a){var b,c;for(c=0;c<d.breakArray.length;c++)if(b=d.breakArray[c],b.from>=a)break;else b.to<a?a+=b.to-b.from:d.isInBreak(b,a)&&(a+=b.to-b.from);return a};
this.setExtremes=function(a,b,c,d,e){for(;this.isInAnyBreak(a);)a-=this.closestPointRange;for(;this.isInAnyBreak(b);)b-=this.closestPointRange;g.prototype.setExtremes.call(this,a,b,c,d,e)};this.setAxisTranslation=function(a){g.prototype.setAxisTranslation.call(this,a);var b=d.options.breaks,a=[],c=[],e=0,h,i,j=d.userMin||d.min,k=d.userMax||d.max,n,p;for(p in b)i=b[p],d.isInBreak(i,j)&&(j+=i.to%i.repeat-j%i.repeat),d.isInBreak(i,k)&&(k-=k%i.repeat-i.from%i.repeat);for(p in b){i=b[p];n=i.from;for(h=
i.repeat||Infinity;n-h>j;)n-=h;for(;n<j;)n+=h;for(;n<k;n+=h)a.push({value:n,move:"in"}),a.push({value:n+(i.to-i.from),move:"out",size:i.breakSize})}a.sort(function(a,b){return a.value===b.value?(a.move==="in"?0:1)-(b.move==="in"?0:1):a.value-b.value});b=0;n=j;for(p in a){i=a[p];b+=i.move==="in"?1:-1;if(b===1&&i.move==="in")n=i.value;b===0&&(c.push({from:n,to:i.value,len:i.value-n-(i.size||0)}),e+=i.value-n-(i.size||0))}d.breakArray=c;f(d,"afterBreaks");d.transA*=(k-d.min)/(k-j-e);d.min=j;d.max=k}}});
d(h.prototype,"generatePoints",function(a){a.apply(this,b(arguments));var c=this.xAxis,d=this.yAxis,e=this.points,f,g=e.length;if(c&&d&&(c.options.breaks||d.options.breaks))for(;g--;)if(f=e[g],c.isInAnyBreak(f.x,!0)||d.isInAnyBreak(f.y,!0))e.splice(g,1),this.data[g].destroyElements()});d(a.seriesTypes.column.prototype,"drawPoints",function(a){a.apply(this);var a=this.points,b=this.yAxis,c=b.breakArray||[],d,e,g,h,n;for(g=0;g<a.length;g++){d=a[g];n=d.stackY||d.y;for(h=0;h<c.length;h++)if(e=c[h],n<
e.from)break;else n>e.to?f(b,"pointBreak",{point:d,brk:e}):f(b,"pointInBreak",{point:d,brk:e})}})})(z);var da=P.prototype,Q=Kb.prototype,gc=da.processData,hc=da.generatePoints,ic=da.destroy,jc=Q.tooltipFooterHeaderFormatter,kc={approximation:"average",groupPixelWidth:2,dateTimeLabelFormats:{millisecond:["%A, %b %e, %H:%M:%S.%L","%A, %b %e, %H:%M:%S.%L","-%H:%M:%S.%L"],second:["%A, %b %e, %H:%M:%S","%A, %b %e, %H:%M:%S","-%H:%M:%S"],minute:["%A, %b %e, %H:%M","%A, %b %e, %H:%M","-%H:%M"],hour:["%A, %b %e, %H:%M",
"%A, %b %e, %H:%M","-%H:%M"],day:["%A, %b %e, %Y","%A, %b %e","-%A, %b %e, %Y"],week:["Week from %A, %b %e, %Y","%A, %b %e","-%A, %b %e, %Y"],month:["%B %Y","%B","-%B %Y"],year:["%Y","%Y","-%Y"]}},Ub={line:{},spline:{},area:{},areaspline:{},column:{approximation:"sum",groupPixelWidth:10},arearange:{approximation:"range"},areasplinerange:{approximation:"range"},columnrange:{approximation:"range",groupPixelWidth:10},candlestick:{approximation:"ohlc",groupPixelWidth:10},ohlc:{approximation:"ohlc",groupPixelWidth:5}},
Vb=[["millisecond",[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,2,3,4,6,8,12]],["day",[1]],["week",[1]],["month",[1,3,6]],["year",null]],Qa={sum:function(a){var b=a.length,c;if(!b&&a.hasNulls)c=null;else if(b)for(c=0;b--;)c+=a[b];return c},average:function(a){var b=a.length,a=Qa.sum(a);typeof a==="number"&&b&&(a/=b);return a},open:function(a){return a.length?a[0]:a.hasNulls?null:r},high:function(a){return a.length?Fa(a):a.hasNulls?null:r},low:function(a){return a.length?
Sa(a):a.hasNulls?null:r},close:function(a){return a.length?a[a.length-1]:a.hasNulls?null:r},ohlc:function(a,b,c,d){a=Qa.open(a);b=Qa.high(b);c=Qa.low(c);d=Qa.close(d);if(typeof a==="number"||typeof b==="number"||typeof c==="number"||typeof d==="number")return[a,b,c,d]},range:function(a,b){a=Qa.low(a);b=Qa.high(b);if(typeof a==="number"||typeof b==="number")return[a,b]}};da.groupData=function(a,b,c,d){var e=this.data,f=this.options.data,g=[],h=[],i=a.length,j,k,l=!!b,m=[[],[],[],[]],d=typeof d==="function"?
d:Qa[d],o=this.pointArrayMap,q=o&&o.length,n;for(n=0;n<=i;n++)if(a[n]>=c[0])break;for(;n<=i;n++){for(;c[1]!==r&&a[n]>=c[1]||n===i;)if(j=c.shift(),k=d.apply(0,m),k!==r&&(g.push(j),h.push(k)),m[0]=[],m[1]=[],m[2]=[],m[3]=[],n===i)break;if(n===i)break;if(o){j=this.cropStart+n;j=e&&e[j]||this.pointClass.prototype.applyOptions.apply({series:this},[f[j]]);var p;for(k=0;k<q;k++)if(p=j[o[k]],typeof p==="number")m[k].push(p);else if(p===null)m[k].hasNulls=!0}else if(j=l?b[n]:null,typeof j==="number")m[0].push(j);
else if(j===null)m[0].hasNulls=!0}return[g,h]};da.processData=function(){var a=this.chart,b=this.options,c=b.dataGrouping,d=this.allowDG!==!1&&c&&p(c.enabled,a.options._stock),e;this.forceCrop=d;this.groupPixelWidth=null;this.hasProcessed=!0;if(gc.apply(this,arguments)!==!1&&d){this.destroyGroupedData();var f=this.processedXData,g=this.processedYData,h=a.plotSizeX,a=this.xAxis,i=a.options.ordinal,j=this.groupPixelWidth=a.getGroupPixelWidth&&a.getGroupPixelWidth(),d=this.pointRange;if(j){e=!0;this.points=
null;var k=a.getExtremes(),d=k.min,k=k.max,i=i&&a.getGroupIntervalFactor(d,k,this)||1,h=j*(k-d)/h*i,j=a.getTimeTicks(a.normalizeTimeTickInterval(h,c.units||Vb),d,k,a.options.startOfWeek,f,this.closestPointRange),g=da.groupData.apply(this,[f,g,j,c.approximation]),f=g[0],g=g[1];if(c.smoothed){c=f.length-1;for(f[c]=k;c--&&c>0;)f[c]+=h/2;f[0]=d}this.currentDataGrouping=j.info;if(b.pointRange===null)this.pointRange=j.info.totalRange;this.closestPointRange=j.info.totalRange;if(s(f[0])&&f[0]<a.dataMin){if(a.min===
a.dataMin)a.min=f[0];a.dataMin=f[0]}this.processedXData=f;this.processedYData=g}else this.currentDataGrouping=null,this.pointRange=d;this.hasGroupedData=e}};da.destroyGroupedData=function(){var a=this.groupedData;n(a||[],function(b,c){b&&(a[c]=b.destroy?b.destroy():null)});this.groupedData=null};da.generatePoints=function(){hc.apply(this);this.destroyGroupedData();this.groupedData=this.hasGroupedData?this.points:null};Q.tooltipFooterHeaderFormatter=function(a,b){var c=a.series,d=c.tooltipOptions,
e=c.options.dataGrouping,f=d.xDateFormat,g,h=c.xAxis;h&&h.options.type==="datetime"&&e&&sa(a.key)?(c=c.currentDataGrouping,e=e.dateTimeLabelFormats,c?(h=e[c.unitName],c.count===1?f=h[0]:(f=h[1],g=h[2])):!f&&e&&(f=this.getXDateFormat(a,d,h)),f=ka(f,a.key),g&&(f+=ka(g,a.key+c.totalRange-1)),d=d[(b?"footer":"header")+"Format"].replace("{point.key}",f)):d=jc.call(this,a,b);return d};da.destroy=function(){for(var a=this.groupedData||[],b=a.length;b--;)a[b]&&a[b].destroy();ic.apply(this)};S(da,"setOptions",
function(a,b){var c=a.call(this,b),d=this.type,e=this.chart.options.plotOptions,f=V[d].dataGrouping;if(Ub[d])f||(f=y(kc,Ub[d])),c.dataGrouping=y(f,e.series&&e.series.dataGrouping,e[d].dataGrouping,b.dataGrouping);if(this.chart.options._stock)this.requireSorting=!0;return c});S(O.prototype,"setScale",function(a){a.call(this);n(this.series,function(a){a.hasProcessed=!1})});O.prototype.getGroupPixelWidth=function(){var a=this.series,b=a.length,c,d=0,e=!1,f;for(c=b;c--;)(f=a[c].options.dataGrouping)&&
(d=v(d,f.groupPixelWidth));for(c=b;c--;)if((f=a[c].options.dataGrouping)&&a[c].hasProcessed)if(b=(a[c].processedXData||a[c].data).length,a[c].groupPixelWidth||b>this.chart.plotSizeX/d||b&&f.forced)e=!0;return e?d:0};V.ohlc=y(V.column,{lineWidth:1,tooltip:{pointFormat:'<span style="color:{point.color}">●</span> <b> {series.name}</b><br/>Open: {point.open}<br/>High: {point.high}<br/>Low: {point.low}<br/>Close: {point.close}<br/>'},states:{hover:{lineWidth:3}},threshold:null});Q=ja(I.column,{type:"ohlc",
pointArrayMap:["open","high","low","close"],toYData:function(a){return[a.open,a.high,a.low,a.close]},pointValKey:"high",pointAttrToOptions:{stroke:"color","stroke-width":"lineWidth"},upColorProp:"stroke",getAttribs:function(){I.column.prototype.getAttribs.apply(this,arguments);var a=this.options,b=a.states,a=a.upColor||this.color,c=y(this.pointAttr),d=this.upColorProp;c[""][d]=a;c.hover[d]=b.hover.upColor||a;c.select[d]=b.select.upColor||a;n(this.points,function(a){if(a.open<a.close&&!a.options.color)a.pointAttr=
c})},translate:function(){var a=this.yAxis;I.column.prototype.translate.apply(this);n(this.points,function(b){if(b.open!==null)b.plotOpen=a.translate(b.open,0,1,0,1);if(b.close!==null)b.plotClose=a.translate(b.close,0,1,0,1)})},drawPoints:function(){var a=this,b=a.chart,c,d,e,f,g,h,i,j;n(a.points,function(k){if(k.plotY!==r)i=k.graphic,c=k.pointAttr[k.selected?"selected":""]||a.pointAttr[""],f=c["stroke-width"]%2/2,j=w(k.plotX)-f,g=w(k.shapeArgs.width/2),h=["M",j,w(k.yBottom),"L",j,w(k.plotY)],k.open!==
null&&(d=w(k.plotOpen)+f,h.push("M",j,d,"L",j-g,d)),k.close!==null&&(e=w(k.plotClose)+f,h.push("M",j,e,"L",j+g,e)),i?i.attr(c).animate({d:h}):k.graphic=b.renderer.path(h).attr(c).add(a.group)})},animate:null});I.ohlc=Q;V.candlestick=y(V.column,{lineColor:"black",lineWidth:1,states:{hover:{lineWidth:2}},tooltip:V.ohlc.tooltip,threshold:null,upColor:"white"});Q=ja(Q,{type:"candlestick",pointAttrToOptions:{fill:"color",stroke:"lineColor","stroke-width":"lineWidth"},upColorProp:"fill",getAttribs:function(){I.ohlc.prototype.getAttribs.apply(this,
arguments);var a=this.options,b=a.states,c=a.upLineColor||a.lineColor,d=b.hover.upLineColor||c,e=b.select.upLineColor||c;n(this.points,function(a){if(a.open<a.close)a.pointAttr[""].stroke=c,a.pointAttr.hover.stroke=d,a.pointAttr.select.stroke=e})},drawPoints:function(){var a=this,b=a.chart,c,d=a.pointAttr[""],e,f,g,h,i,j,k,l,m,o,q;n(a.points,function(n){m=n.graphic;if(n.plotY!==r)c=n.pointAttr[n.selected?"selected":""]||d,k=c["stroke-width"]%2/2,l=w(n.plotX)-k,e=n.plotOpen,f=n.plotClose,g=Y.min(e,
f),h=Y.max(e,f),q=w(n.shapeArgs.width/2),i=w(g)!==w(n.plotY),j=h!==n.yBottom,g=w(g)+k,h=w(h)+k,o=["M",l-q,h,"L",l-q,g,"L",l+q,g,"L",l+q,h,"Z","M",l,g,"L",l,i?w(n.plotY):g,"M",l,h,"L",l,j?w(n.yBottom):h],m?m.attr(c).animate({d:o}):n.graphic=b.renderer.path(o).attr(c).add(a.group).shadow(a.options.shadow)})}});I.candlestick=Q;var sb=na.prototype.symbols;V.flags=y(V.column,{fillColor:"white",lineWidth:1,pointRange:0,shape:"flag",stackDistance:12,states:{hover:{lineColor:"black",fillColor:"#FCFFC5"}},
style:{fontSize:"11px",fontWeight:"bold",textAlign:"center"},tooltip:{pointFormat:"{point.text}<br/>"},threshold:null,y:-30});I.flags=ja(I.column,{type:"flags",sorted:!1,noSharedTooltip:!0,allowDG:!1,takeOrdinalPosition:!1,trackerGroups:["markerGroup"],forceCrop:!0,init:P.prototype.init,pointAttrToOptions:{fill:"fillColor",stroke:"color","stroke-width":"lineWidth",r:"radius"},translate:function(){I.column.prototype.translate.apply(this);var a=this.chart,b=this.points,c=b.length-1,d,e,f=this.options.onSeries,
f=(d=f&&a.get(f))&&d.options.step,g=d&&d.points,h=g&&g.length,i=this.xAxis,j=i.getExtremes(),k,l,m;if(d&&d.visible&&h){d=d.currentDataGrouping;l=g[h-1].x+(d?d.totalRange:0);for(b.sort(function(a,b){return a.x-b.x});h--&&b[c];)if(d=b[c],k=g[h],k.x<=d.x&&k.plotY!==r){if(d.x<=l)d.plotY=k.plotY,k.x<d.x&&!f&&(m=g[h+1])&&m.plotY!==r&&(d.plotY+=(d.x-k.x)/(m.x-k.x)*(m.plotY-k.plotY));c--;h++;if(c<0)break}}n(b,function(c,d){var f;if(c.plotY===r)c.x>=j.min&&c.x<=j.max?c.plotY=a.chartHeight-i.bottom-(i.opposite?
i.height:0)+i.offset-a.plotTop:c.shapeArgs={};if((e=b[d-1])&&e.plotX===c.plotX){if(e.stackIndex===r)e.stackIndex=0;f=e.stackIndex+1}c.stackIndex=f})},drawPoints:function(){var a,b=this.pointAttr[""],c=this.points,d=this.chart.renderer,e,f,g=this.options,h=g.y,i,j,k,l,m=g.lineWidth%2/2,o,n;for(j=c.length;j--;)if(k=c[j],a=k.plotX>this.xAxis.len,e=k.plotX+(a?m:-m),l=k.stackIndex,i=k.options.shape||g.shape,f=k.plotY,f!==r&&(f=k.plotY+h+m-(l!==r&&l*g.stackDistance)),o=l?r:k.plotX+m,n=l?r:k.plotY,l=k.graphic,
f!==r&&e>=0&&!a)a=k.pointAttr[k.selected?"select":""]||b,l?l.attr({x:e,y:f,r:a.r,anchorX:o,anchorY:n}):k.graphic=d.label(k.options.title||g.title||"A",e,f,i,o,n,g.useHTML).css(y(g.style,k.style)).attr(a).attr({align:i==="flag"?"left":"center",width:g.width,height:g.height}).add(this.markerGroup).shadow(g.shadow),k.tooltipPos=[e,f];else if(l)k.graphic=l.destroy()},drawTracker:function(){var a=this.points;jb.drawTrackerPoint.apply(this);n(a,function(b){var c=b.graphic;c&&D(c.element,"mouseover",function(){if(b.stackIndex>
0&&!b.raised)b._y=c.y,c.attr({y:b._y-8}),b.raised=!0;n(a,function(a){if(a!==b&&a.raised&&a.graphic)a.graphic.attr({y:a._y}),a.raised=!1})})})},animate:ha,buildKDTree:ha,setClip:ha});sb.flag=function(a,b,c,d,e){var f=e&&e.anchorX||a,e=e&&e.anchorY||b;return["M",f,e,"L",a,b+d,a,b,a+c,b,a+c,b+d,a,b+d,"M",f,e,"Z"]};n(["circle","square"],function(a){sb[a+"pin"]=function(b,c,d,e,f){var g=f&&f.anchorX,f=f&&f.anchorY,b=sb[a](b,c,d,e);g&&f&&b.push("M",g,c>f?c:c+e,"L",g,f);return b}});Wa===z.VMLRenderer&&n(["flag",
"circlepin","squarepin"],function(a){ib.prototype.symbols[a]=sb[a]});var Q=[].concat(Vb),tb=function(a){var b=hb(arguments,function(a){return typeof a==="number"});if(b.length)return Math[a].apply(0,b)};Q[4]=["day",[1,2,3,4]];Q[5]=["week",[1,2,3]];x(F,{navigator:{handles:{backgroundColor:"#ebe7e8",borderColor:"#b2b1b6"},height:40,margin:25,maskFill:"rgba(128,179,236,0.3)",maskInside:!0,outlineColor:"#b2b1b6",outlineWidth:1,series:{type:I.areaspline===r?"line":"areaspline",color:"#4572A7",compare:null,
fillOpacity:0.05,dataGrouping:{approximation:"average",enabled:!0,groupPixelWidth:2,smoothed:!0,units:Q},dataLabels:{enabled:!1,zIndex:2},id:"highcharts-navigator-series",lineColor:"#4572A7",lineWidth:1,marker:{enabled:!1},pointRange:0,shadow:!1,threshold:null},xAxis:{tickWidth:0,lineWidth:0,gridLineColor:"#EEE",gridLineWidth:1,tickPixelInterval:200,labels:{align:"left",style:{color:"#888"},x:3,y:-4},crosshair:!1},yAxis:{gridLineWidth:0,startOnTick:!1,endOnTick:!1,minPadding:0.1,maxPadding:0.1,labels:{enabled:!1},
crosshair:!1,title:{text:null},tickWidth:0}},scrollbar:{height:fb?20:14,barBackgroundColor:"#bfc8d1",barBorderRadius:0,barBorderWidth:1,barBorderColor:"#bfc8d1",buttonArrowColor:"#666",buttonBackgroundColor:"#ebe7e8",buttonBorderColor:"#bbb",buttonBorderRadius:0,buttonBorderWidth:1,minWidth:6,rifleColor:"#666",trackBackgroundColor:"#eeeeee",trackBorderColor:"#eeeeee",trackBorderWidth:1,liveRedraw:ea&&!fb}});Eb.prototype={drawHandle:function(a,b){var c=this.chart,d=c.renderer,e=this.elementsToDestroy,
f=this.handles,g=this.navigatorOptions.handles,g={fill:g.backgroundColor,stroke:g.borderColor,"stroke-width":1},h;this.rendered||(f[b]=d.g("navigator-handle-"+["left","right"][b]).css({cursor:"ew-resize"}).attr({zIndex:4-b}).add(),h=d.rect(-4.5,0,9,16,0,1).attr(g).add(f[b]),e.push(h),h=d.path(["M",-1.5,4,"L",-1.5,12,"M",0.5,4,"L",0.5,12]).attr(g).add(f[b]),e.push(h));f[b][c.isResizing?"animate":"attr"]({translateX:this.scrollerLeft+this.scrollbarHeight+parseInt(a,10),translateY:this.top+this.height/
2-8})},drawScrollbarButton:function(a){var b=this.chart.renderer,c=this.elementsToDestroy,d=this.scrollbarButtons,e=this.scrollbarHeight,f=this.scrollbarOptions,g;this.rendered||(d[a]=b.g().add(this.scrollbarGroup),g=b.rect(-0.5,-0.5,e+1,e+1,f.buttonBorderRadius,f.buttonBorderWidth).attr({stroke:f.buttonBorderColor,"stroke-width":f.buttonBorderWidth,fill:f.buttonBackgroundColor}).add(d[a]),c.push(g),g=b.path(["M",e/2+(a?-1:1),e/2-3,"L",e/2+(a?-1:1),e/2+3,e/2+(a?2:-2),e/2]).attr({fill:f.buttonArrowColor}).add(d[a]),
c.push(g));a&&d[a].attr({translateX:this.scrollerWidth-e})},render:function(a,b,c,d){var e=this.chart,f=e.renderer,g,h,i,j,k=this.scrollbarGroup,l=this.navigatorGroup,m=this.scrollbar,l=this.xAxis,o=this.scrollbarTrack,n=this.scrollbarHeight,t=this.scrollbarEnabled,r=this.navigatorOptions,s=this.scrollbarOptions,u=s.minWidth,A=this.height,x=this.top,y=this.navigatorEnabled,z=r.outlineWidth,C=z/2,D=0,E=this.outlineHeight,H=s.barBorderRadius,G=s.barBorderWidth,F=x+C,I;if(!isNaN(a)){this.navigatorLeft=
g=p(l.left,e.plotLeft+n);this.navigatorWidth=h=p(l.len,e.plotWidth-2*n);this.scrollerLeft=i=g-n;this.scrollerWidth=j=j=h+2*n;l.getExtremes&&(I=this.getUnionExtremes(!0))&&(I.dataMin!==l.min||I.dataMax!==l.max)&&l.setExtremes(I.dataMin,I.dataMax,!0,!1);c=p(c,l.translate(a));d=p(d,l.translate(b));if(isNaN(c)||R(c)===Infinity)c=0,d=j;if(!(l.translate(d,!0)-l.translate(c,!0)<e.xAxis[0].minRange)){this.zoomedMax=B(v(c,d),h);this.zoomedMin=v(this.fixedWidth?this.zoomedMax-this.fixedWidth:B(c,d),0);this.range=
this.zoomedMax-this.zoomedMin;c=w(this.zoomedMax);b=w(this.zoomedMin);a=c-b;if(!this.rendered){if(y)this.navigatorGroup=l=f.g("navigator").attr({zIndex:3}).add(),this.leftShade=f.rect().attr({fill:r.maskFill}).add(l),r.maskInside?this.leftShade.css({cursor:"ew-resize "}):this.rightShade=f.rect().attr({fill:r.maskFill}).add(l),this.outline=f.path().attr({"stroke-width":z,stroke:r.outlineColor}).add(l);if(t)this.scrollbarGroup=k=f.g("scrollbar").add(),m=s.trackBorderWidth,this.scrollbarTrack=o=f.rect().attr({x:0,
y:-m%2/2,fill:s.trackBackgroundColor,stroke:s.trackBorderColor,"stroke-width":m,r:s.trackBorderRadius||0,height:n}).add(k),this.scrollbar=m=f.rect().attr({y:-G%2/2,height:n,fill:s.barBackgroundColor,stroke:s.barBorderColor,"stroke-width":G,r:H}).add(k),this.scrollbarRifles=f.path().attr({stroke:s.rifleColor,"stroke-width":1}).add(k)}e=e.isResizing?"animate":"attr";if(y){this.leftShade[e](r.maskInside?{x:g+b,y:x,width:c-b,height:A}:{x:g,y:x,width:b,height:A});if(this.rightShade)this.rightShade[e]({x:g+
c,y:x,width:h-c,height:A});this.outline[e]({d:["M",i,F,"L",g+b-C,F,g+b-C,F+E,"L",g+c-C,F+E,"L",g+c-C,F,i+j,F].concat(r.maskInside?["M",g+b+C,F,"L",g+c-C,F]:[])});this.drawHandle(b+C,0);this.drawHandle(c+C,1)}if(t&&k)this.drawScrollbarButton(0),this.drawScrollbarButton(1),k[e]({translateX:i,translateY:w(F+A)}),o[e]({width:j}),g=n+b,h=a-G,h<u&&(D=(u-h)/2,h=u,g-=D),this.scrollbarPad=D,m[e]({x:X(g)+G%2/2,width:h}),u=n+b+a/2-0.5,this.scrollbarRifles.attr({visibility:a>12?"visible":"hidden"})[e]({d:["M",
u-3,n/4,"L",u-3,2*n/3,"M",u,n/4,"L",u,2*n/3,"M",u+3,n/4,"L",u+3,2*n/3]});this.scrollbarPad=D;this.rendered=!0}}},addEvents:function(){var a=this.chart.container,b=this.mouseDownHandler,c=this.mouseMoveHandler,d=this.mouseUpHandler,e;e=[[a,"mousedown",b],[a,"mousemove",c],[document,"mouseup",d]];$a&&e.push([a,"touchstart",b],[a,"touchmove",c],[document,"touchend",d]);n(e,function(a){D.apply(null,a)});this._events=e},removeEvents:function(){n(this._events,function(a){U.apply(null,a)});this._events=
r;this.navigatorEnabled&&this.baseSeries&&U(this.baseSeries,"updatedData",this.updatedDataHandler)},init:function(){var a=this,b=a.chart,c,d,e=a.scrollbarHeight,f=a.navigatorOptions,g=a.height,h=a.top,i,j,k=a.baseSeries;a.mouseDownHandler=function(d){var d=b.pointer.normalize(d),e=a.zoomedMin,f=a.zoomedMax,h=a.top,j=a.scrollbarHeight,k=a.scrollerLeft,l=a.scrollerWidth,m=a.navigatorLeft,n=a.navigatorWidth,p=a.scrollbarPad,r=a.range,s=d.chartX,v=d.chartY,d=b.xAxis[0],w,x=fb?10:7;if(v>h&&v<h+g+j)if((h=
!a.scrollbarEnabled||v<h+g)&&Y.abs(s-e-m)<x)a.grabbedLeft=!0,a.otherHandlePos=f,a.fixedExtreme=d.max,b.fixedRange=null;else if(h&&Y.abs(s-f-m)<x)a.grabbedRight=!0,a.otherHandlePos=e,a.fixedExtreme=d.min,b.fixedRange=null;else if(s>m+e-p&&s<m+f+p)a.grabbedCenter=s,a.fixedWidth=r,i=s-e;else if(s>k&&s<k+l){f=h?s-m-r/2:s<m?e-r*0.2:s>k+l-j?e+r*0.2:s<m+e?e-r:f;if(f<0)f=0;else if(f+r>=n)f=n-r,w=a.getUnionExtremes().dataMax;if(f!==e)a.fixedWidth=r,e=c.toFixedRange(f,f+r,null,w),d.setExtremes(e.min,e.max,
!0,!1,{trigger:"navigator"})}};a.mouseMoveHandler=function(c){var d=a.scrollbarHeight,e=a.navigatorLeft,f=a.navigatorWidth,g=a.scrollerLeft,h=a.scrollerWidth,k=a.range,l;if(c.pageX!==0)c=b.pointer.normalize(c),l=c.chartX,l<e?l=e:l>g+h-d&&(l=g+h-d),a.grabbedLeft?(j=!0,a.render(0,0,l-e,a.otherHandlePos)):a.grabbedRight?(j=!0,a.render(0,0,a.otherHandlePos,l-e)):a.grabbedCenter&&(j=!0,l<i?l=i:l>f+i-k&&(l=f+i-k),a.render(0,0,l-i,l-i+k)),j&&a.scrollbarOptions.liveRedraw&&setTimeout(function(){a.mouseUpHandler(c)},
0)};a.mouseUpHandler=function(d){var e,f;if(j){if(a.zoomedMin===a.otherHandlePos)e=a.fixedExtreme;else if(a.zoomedMax===a.otherHandlePos)f=a.fixedExtreme;e=c.toFixedRange(a.zoomedMin,a.zoomedMax,e,f);b.xAxis[0].setExtremes(e.min,e.max,!0,!1,{trigger:"navigator",triggerOp:"navigator-drag",DOMEvent:d})}if(d.type!=="mousemove")a.grabbedLeft=a.grabbedRight=a.grabbedCenter=a.fixedWidth=a.fixedExtreme=a.otherHandlePos=j=i=null};var l=b.xAxis.length,m=b.yAxis.length;b.extraBottomMargin=a.outlineHeight+f.margin;
a.navigatorEnabled?(a.xAxis=c=new O(b,y({breaks:k&&k.xAxis.options.breaks,ordinal:k&&k.xAxis.options.ordinal},f.xAxis,{id:"navigator-x-axis",isX:!0,type:"datetime",index:l,height:g,offset:0,offsetLeft:e,offsetRight:-e,keepOrdinalPadding:!0,startOnTick:!1,endOnTick:!1,minPadding:0,maxPadding:0,zoomEnabled:!1})),a.yAxis=d=new O(b,y(f.yAxis,{id:"navigator-y-axis",alignTicks:!1,height:g,offset:0,index:m,zoomEnabled:!1})),k||f.series.data?a.addBaseSeries():b.series.length===0&&S(b,"redraw",function(c,
d){if(b.series.length>0&&!a.series)a.setBaseSeries(),b.redraw=c;c.call(b,d)})):a.xAxis=c={translate:function(a,c){var d=b.xAxis[0],f=d.getExtremes(),g=b.plotWidth-2*e,h=tb("min",d.options.min,f.dataMin),d=tb("max",d.options.max,f.dataMax)-h;return c?a*d/g+h:g*(a-h)/d},toFixedRange:O.prototype.toFixedRange};S(b,"getMargins",function(b){var e=this.legend,f=e.options;b.apply(this,[].slice.call(arguments,1));a.top=h=a.navigatorOptions.top||this.chartHeight-a.height-a.scrollbarHeight-this.spacing[2]-(f.verticalAlign===
"bottom"&&f.enabled&&!f.floating?e.legendHeight+p(f.margin,10):0);if(c&&d)c.options.top=d.options.top=h,c.setAxisSize(),d.setAxisSize()});a.addEvents()},getUnionExtremes:function(a){var b=this.chart.xAxis[0],c=this.xAxis,d=c.options,e=b.options;if(!a||b.dataMin!==null)return{dataMin:tb("min",d&&d.min,e.min,b.dataMin,c.dataMin),dataMax:tb("max",d&&d.max,e.max,b.dataMax,c.dataMax)}},setBaseSeries:function(a){var b=this.chart,a=a||b.options.navigator.baseSeries;this.series&&this.series.remove();this.baseSeries=
b.series[a]||typeof a==="string"&&b.get(a)||b.series[0];this.xAxis&&this.addBaseSeries()},addBaseSeries:function(){var a=this.baseSeries,b=a?a.options:{},c=b.data,d=this.navigatorOptions.series,e;e=d.data;this.hasNavigatorData=!!e;b=y(b,d,{enableMouseTracking:!1,group:"nav",padXAxis:!1,xAxis:"navigator-x-axis",yAxis:"navigator-y-axis",name:"Navigator",showInLegend:!1,isInternal:!0,visible:!0});b.data=e||c;this.series=this.chart.initSeries(b);if(a&&this.navigatorOptions.adaptToUpdatedData!==!1)D(a,
"updatedData",this.updatedDataHandler),a.userOptions.events=x(a.userOptions.event,{updatedData:this.updatedDataHandler})},updatedDataHandler:function(){var a=this.chart.scroller,b=a.baseSeries,c=b.xAxis,d=c.getExtremes(),e=d.min,f=d.max,g=d.dataMin,d=d.dataMax,h=f-e,i,j,k,l,m,o=a.series;i=o.xData;var n=!!c.setExtremes;j=f>=i[i.length-1]-(this.closestPointRange||0);i=e<=g;if(!a.hasNavigatorData)o.options.pointStart=b.xData[0],o.setData(b.options.data,!1),m=!0;i&&(l=g,k=l+h);j&&(k=d,i||(l=v(k-h,o.xData[0])));
n&&(i||j)?isNaN(l)||c.setExtremes(l,k,!0,!1,{trigger:"updatedData"}):(m&&this.chart.redraw(!1),a.render(v(e,g),B(f,d)))},destroy:function(){this.removeEvents();n([this.xAxis,this.yAxis,this.leftShade,this.rightShade,this.outline,this.scrollbarTrack,this.scrollbarRifles,this.scrollbarGroup,this.scrollbar],function(a){a&&a.destroy&&a.destroy()});this.xAxis=this.yAxis=this.leftShade=this.rightShade=this.outline=this.scrollbarTrack=this.scrollbarRifles=this.scrollbarGroup=this.scrollbar=null;n([this.scrollbarButtons,
this.handles,this.elementsToDestroy],function(a){Na(a)})}};z.Scroller=Eb;S(O.prototype,"zoom",function(a,b,c){var d=this.chart,e=d.options,f=e.chart.zoomType,g=e.navigator,e=e.rangeSelector,h;if(this.isXAxis&&(g&&g.enabled||e&&e.enabled))if(f==="x")d.resetZoomButton="blocked";else if(f==="y")h=!1;else if(f==="xy")d=this.previousZoom,s(b)?this.previousZoom=[this.min,this.max]:d&&(b=d[0],c=d[1],delete this.previousZoom);return h!==r?h:a.call(this,b,c)});S(Pa.prototype,"init",function(a,b,c){D(this,
"beforeRender",function(){var a=this.options;if(a.navigator.enabled||a.scrollbar.enabled)this.scroller=new Eb(this)});a.call(this,b,c)});S(P.prototype,"addPoint",function(a,b,c,d,e){var f=this.options.turboThreshold;f&&this.xData.length>f&&ia(b)&&!Ka(b)&&this.chart.scroller&&qa(20,!0);a.call(this,b,c,d,e)});x(F,{rangeSelector:{buttonTheme:{width:28,height:18,fill:"#f7f7f7",padding:2,r:0,"stroke-width":0,style:{color:"#444",cursor:"pointer",fontWeight:"normal"},zIndex:7,states:{hover:{fill:"#e7e7e7"},
select:{fill:"#e7f0f9",style:{color:"black",fontWeight:"bold"}}}},inputPosition:{align:"right"},labelStyle:{color:"#666"}}});F.lang=y(F.lang,{rangeSelectorZoom:"Zoom",rangeSelectorFrom:"From",rangeSelectorTo:"To"});Fb.prototype={clickButton:function(a,b){var c=this,d=c.selected,e=c.chart,f=c.buttons,g=c.buttonOptions[a],h=e.xAxis[0],i=e.scroller&&e.scroller.getUnionExtremes()||h||{},j=i.dataMin,k=i.dataMax,l,m=h&&w(B(h.max,p(k,h.max))),o=new fa(m),q=g.type,t=g.count,i=g._range,s;if(!(j===null||k===
null||a===c.selected)){if(q==="month"||q==="year")l={month:"Month",year:"FullYear"}[q],o["set"+l](o["get"+l]()-t),l=o.getTime(),j=p(j,Number.MIN_VALUE),isNaN(l)||l<j?(l=j,m=B(l+i,k)):i=m-l;else if(i)l=v(m-i,j),m=B(l+i,k);else if(q==="ytd")if(h){if(k===r)j=Number.MAX_VALUE,k=Number.MIN_VALUE,n(e.series,function(a){a=a.xData;j=B(a[0],j);k=v(a[a.length-1],k)}),b=!1;m=new fa(k);s=m.getFullYear();l=s=v(j||0,fa.UTC(s,0,1));m=m.getTime();m=B(k||m,m)}else{D(e,"beforeRender",function(){c.clickButton(a)});
return}else q==="all"&&h&&(l=j,m=k);f[d]&&f[d].setState(0);f[a]&&f[a].setState(2);e.fixedRange=i;h?h.setExtremes(l,m,p(b,1),0,{trigger:"rangeSelectorButton",rangeSelectorButton:g}):(d=e.options.xAxis,d[0]=y(d[0],{range:i,min:s}));c.setSelected(a)}},setSelected:function(a){this.selected=this.options.selected=a},defaultButtons:[{type:"month",count:1,text:"1m"},{type:"month",count:3,text:"3m"},{type:"month",count:6,text:"6m"},{type:"ytd",text:"YTD"},{type:"year",count:1,text:"1y"},{type:"all",text:"All"}],
init:function(a){var b=this,c=a.options.rangeSelector,d=c.buttons||[].concat(b.defaultButtons),e=c.selected,f=b.blurInputs=function(){var a=b.minInput,c=b.maxInput;a&&a.blur&&K(a,"blur");c&&c.blur&&K(c,"blur")};b.chart=a;b.options=c;b.buttons=[];a.extraTopMargin=35;b.buttonOptions=d;D(a.container,"mousedown",f);D(a,"resize",f);n(d,b.computeButtonRange);e!==r&&d[e]&&this.clickButton(e,!1);D(a,"load",function(){D(a.xAxis[0],"afterSetExtremes",function(){b.updateButtonStates(!0)})})},updateButtonStates:function(a){var b=
this,c=this.chart,d=c.xAxis[0],e=c.scroller&&c.scroller.getUnionExtremes()||d,f=e.dataMin,g=e.dataMax,h=b.selected,i=b.options.allButtonsEnabled,j=b.buttons;a&&c.fixedRange!==w(d.max-d.min)&&(j[h]&&j[h].setState(0),b.setSelected(null));n(b.buttonOptions,function(a,c){var e=a._range,o=e>g-f,n=e<d.minRange,p=a.type==="all"&&d.max-d.min>=g-f&&j[c].state!==2,s=a.type==="ytd"&&ka("%Y",f)===ka("%Y",g);e===w(d.max-d.min)&&c!==h?(b.setSelected(c),j[c].setState(2)):!i&&(o||n||p||s)?j[c].setState(3):j[c].state===
3&&j[c].setState(0)})},computeButtonRange:function(a){var b=a.type,c=a.count||1,d={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5};if(d[b])a._range=d[b]*c;else if(b==="month"||b==="year")a._range={month:30,year:365}[b]*864E5*c},setInputValue:function(a,b){var c=this.chart.options.rangeSelector;if(s(b))this[a+"Input"].HCTime=b;this[a+"Input"].value=ka(c.inputEditDateFormat||"%Y-%m-%d",this[a+"Input"].HCTime);this[a+"DateBox"].attr({text:ka(c.inputDateFormat||"%b %e, %Y",this[a+
"Input"].HCTime)})},drawInput:function(a){var b=this,c=b.chart,d=c.renderer.style,e=c.renderer,f=c.options.rangeSelector,g=b.div,h=a==="min",i,j,k,l=this.inputGroup;this[a+"Label"]=j=e.label(F.lang[h?"rangeSelectorFrom":"rangeSelectorTo"],this.inputGroup.offset).attr({padding:2}).css(y(d,f.labelStyle)).add(l);l.offset+=j.width+5;this[a+"DateBox"]=k=e.label("",l.offset).attr({padding:2,width:f.inputBoxWidth||90,height:f.inputBoxHeight||17,stroke:f.inputBoxBorderColor||"silver","stroke-width":1}).css(y({textAlign:"center",
color:"#444"},d,f.inputStyle)).on("click",function(){b[a+"Input"].focus()}).add(l);l.offset+=k.width+(h?10:0);this[a+"Input"]=i=aa("input",{name:a,className:"highcharts-range-selector",type:"text"},x({position:"absolute",border:0,width:"1px",height:"1px",padding:0,textAlign:"center",fontSize:d.fontSize,fontFamily:d.fontFamily,top:c.plotTop+"px"},f.inputStyle),g);i.onfocus=function(){M(this,{left:l.translateX+k.x+"px",top:l.translateY+"px",width:k.width-2+"px",height:k.height-2+"px",border:"2px solid silver"})};
i.onblur=function(){M(this,{border:0,width:"1px",height:"1px"});b.setInputValue(a)};i.onchange=function(){var a=i.value,d=(f.inputDateParser||fa.parse)(a),e=c.xAxis[0],g=e.dataMin,j=e.dataMax;isNaN(d)&&(d=a.split("-"),d=fa.UTC(C(d[0]),C(d[1])-1,C(d[2])));isNaN(d)||(F.global.useUTC||(d+=(new fa).getTimezoneOffset()*6E4),h?d>b.maxInput.HCTime?d=r:d<g&&(d=g):d<b.minInput.HCTime?d=r:d>j&&(d=j),d!==r&&c.xAxis[0].setExtremes(h?d:e.min,h?e.max:d,r,r,{trigger:"rangeSelectorInput"}))}},render:function(a,b){var c=
this,d=c.chart,e=d.renderer,f=d.container,g=d.options,h=g.exporting&&g.navigation&&g.navigation.buttonOptions,i=g.rangeSelector,j=c.buttons,k=F.lang,g=c.div,g=c.inputGroup,l=i.buttonTheme,m=i.buttonPosition||{},o=i.inputEnabled,q=l&&l.states,t=d.plotLeft,r,v,u=c.group;if(!c.rendered&&(c.group=u=e.g("range-selector-buttons").add(),c.zoomText=e.text(k.rangeSelectorZoom,p(m.x,t),p(m.y,d.plotTop-35)+15).css(i.labelStyle).add(u),r=p(m.x,t)+c.zoomText.getBBox().width+5,v=p(m.y,d.plotTop-35),n(c.buttonOptions,
function(a,b){j[b]=e.button(a.text,r,v,function(){c.clickButton(b);c.isActive=!0},l,q&&q.hover,q&&q.select,q&&q.disabled).css({textAlign:"center"}).add(u);r+=j[b].width+p(i.buttonSpacing,5);c.selected===b&&j[b].setState(2)}),c.updateButtonStates(),o!==!1))c.div=g=aa("div",null,{position:"relative",height:0,zIndex:1}),f.parentNode.insertBefore(g,f),c.inputGroup=g=e.g("input-group").add(),g.offset=0,c.drawInput("min"),c.drawInput("max");o!==!1&&(f=d.plotTop-45,g.align(x({y:f,width:g.offset,x:h&&f<(h.y||
0)+h.height-d.spacing[0]?-40:0},i.inputPosition),!0,d.spacingBox),s(o)||(d=u.getBBox(),g[g.translateX<d.x+d.width+10?"hide":"show"]()),c.setInputValue("min",a),c.setInputValue("max",b));c.rendered=!0},destroy:function(){var a=this.minInput,b=this.maxInput,c=this.chart,d=this.blurInputs,e;U(c.container,"mousedown",d);U(c,"resize",d);Na(this.buttons);if(a)a.onfocus=a.onblur=a.onchange=null;if(b)b.onfocus=b.onblur=b.onchange=null;for(e in this)this[e]&&e!=="chart"&&(this[e].destroy?this[e].destroy():
this[e].nodeType&&Ta(this[e])),this[e]=null}};O.prototype.toFixedRange=function(a,b,c,d){var e=this.chart&&this.chart.fixedRange,a=p(c,this.translate(a,!0)),b=p(d,this.translate(b,!0)),c=e&&(b-a)/e;c>0.7&&c<1.3&&(d?a=b-e:b=a+e);return{min:a,max:b}};S(Pa.prototype,"init",function(a,b,c){D(this,"init",function(){if(this.options.rangeSelector.enabled)this.rangeSelector=new Fb(this)});a.call(this,b,c)});z.RangeSelector=Fb;Pa.prototype.callbacks.push(function(a){function b(){f=a.xAxis[0].getExtremes();
g.render(f.min,f.max)}function c(){f=a.xAxis[0].getExtremes();isNaN(f.min)||h.render(f.min,f.max)}function d(a){a.triggerOp!=="navigator-drag"&&g.render(a.min,a.max)}function e(a){h.render(a.min,a.max)}var f,g=a.scroller,h=a.rangeSelector;g&&(D(a.xAxis[0],"afterSetExtremes",d),S(a,"drawChartBox",function(a){var c=this.isDirtyBox;a.call(this);c&&b()}),b());h&&(D(a.xAxis[0],"afterSetExtremes",e),D(a,"resize",c),c());D(a,"destroy",function(){g&&U(a.xAxis[0],"afterSetExtremes",d);h&&(U(a,"resize",c),
U(a.xAxis[0],"afterSetExtremes",e))})});z.StockChart=function(a,b){var c=a.series,d,e=p(a.navigator&&a.navigator.enabled,!0)?{startOnTick:!1,endOnTick:!1}:null,f={marker:{enabled:!1,radius:2},states:{hover:{lineWidth:2}}},g={shadow:!1,borderWidth:0};a.xAxis=Aa(pa(a.xAxis||{}),function(a){return y({minPadding:0,maxPadding:0,ordinal:!0,title:{text:null},labels:{overflow:"justify"},showLastLabel:!0},a,{type:"datetime",categories:null},e)});a.yAxis=Aa(pa(a.yAxis||{}),function(a){d=p(a.opposite,!0);return y({labels:{y:-2},
opposite:d,showLastLabel:!1,title:{text:null}},a)});a.series=null;a=y({chart:{panning:!0,pinchType:"x"},navigator:{enabled:!0},scrollbar:{enabled:!0},rangeSelector:{enabled:!0},title:{text:null,style:{fontSize:"16px"}},tooltip:{shared:!0,crosshairs:!0},legend:{enabled:!1},plotOptions:{line:f,spline:f,area:f,areaspline:f,arearange:f,areasplinerange:f,column:g,columnrange:g,candlestick:g,ohlc:g}},a,{_stock:!0,chart:{inverted:!1}});a.series=c;return new Pa(a,b)};S(Xa.prototype,"init",function(a,b,c){var d=
c.chart.pinchType||"";a.call(this,b,c);this.pinchX=this.pinchHor=d.indexOf("x")!==-1;this.pinchY=this.pinchVert=d.indexOf("y")!==-1;this.hasZoom=this.hasZoom||this.pinchHor||this.pinchVert});S(O.prototype,"autoLabelAlign",function(a){var b=this.chart,c=this.options,b=b._labelPanes=b._labelPanes||{},d=this.options.labels;if(this.chart.options._stock&&this.coll==="yAxis"&&(c=c.top+","+c.height,!b[c]&&d.enabled)){if(d.x===15)d.x=0;if(d.align===void 0)d.align="right";b[c]=1;return"right"}return a.call(this,
[].slice.call(arguments,1))});S(O.prototype,"getPlotLinePath",function(a,b,c,d,e,f){var g=this,h=this.isLinked&&!this.series?this.linkedParent.series:this.series,i=g.chart,j=i.renderer,k=g.left,l=g.top,m,o,q,r,x=[],y=[],u;if(g.coll==="colorAxis")return a.apply(this,[].slice.call(arguments,1));y=g.isXAxis?s(g.options.yAxis)?[i.yAxis[g.options.yAxis]]:Aa(h,function(a){return a.yAxis}):s(g.options.xAxis)?[i.xAxis[g.options.xAxis]]:Aa(h,function(a){return a.xAxis});n(g.isXAxis?i.yAxis:i.xAxis,function(a){if(s(a.options.id)?
a.options.id.indexOf("navigator")===-1:1){var b=a.isXAxis?"yAxis":"xAxis",b=s(a.options[b])?i[b][a.options[b]]:i[b][0];g===b&&y.push(a)}});u=y.length?[]:[g.isXAxis?i.yAxis[0]:i.xAxis[0]];n(y,function(a){Oa(a,u)===-1&&u.push(a)});f=p(f,g.translate(b,null,null,d));isNaN(f)||(g.horiz?n(u,function(a){var b;o=a.pos;r=o+a.len;m=q=w(f+g.transB);if(m<k||m>k+g.width)e?m=q=B(v(k,m),k+g.width):b=!0;b||x.push("M",m,o,"L",q,r)}):n(u,function(a){var b;m=a.pos;q=m+a.len;o=r=w(l+g.height-f);if(o<l||o>l+g.height)e?
o=r=B(v(l,o),g.top+g.height):b=!0;b||x.push("M",m,o,"L",q,r)}));return x.length>0?j.crispPolyLine(x,c||1):null});O.prototype.getPlotBandPath=function(a,b){var c=this.getPlotLinePath(b,null,null,!0),d=this.getPlotLinePath(a,null,null,!0),e=[],f;if(d&&c)for(f=0;f<d.length;f+=6)e.push("M",d[f+1],d[f+2],"L",d[f+4],d[f+5],c[f+4],c[f+5],c[f+1],c[f+2]);else e=null;return e};na.prototype.crispPolyLine=function(a,b){var c;for(c=0;c<a.length;c+=6)a[c+1]===a[c+4]&&(a[c+1]=a[c+4]=w(a[c+1])-b%2/2),a[c+2]===a[c+
5]&&(a[c+2]=a[c+5]=w(a[c+2])+b%2/2);return a};if(Wa===z.VMLRenderer)ib.prototype.crispPolyLine=na.prototype.crispPolyLine;S(O.prototype,"hideCrosshair",function(a,b){a.call(this,b);s(this.crossLabelArray)&&(s(b)?this.crossLabelArray[b]&&this.crossLabelArray[b].hide():n(this.crossLabelArray,function(a){a.hide()}))});S(O.prototype,"drawCrosshair",function(a,b,c){var d,e;a.call(this,b,c);if(s(this.crosshair.label)&&this.crosshair.label.enabled&&s(c)){var a=this.chart,f=this.options.crosshair.label,g=
this.isXAxis?"x":"y",b=this.horiz,h=this.opposite,i=this.left,j=this.top,k=this.crossLabel,l,m,n=f.format,q="";if(!k)k=this.crossLabel=a.renderer.label().attr({align:f.align||(b?"center":h?this.labelAlign==="right"?"right":"left":this.labelAlign==="left"?"left":"center"),zIndex:12,height:b?16:r,fill:f.backgroundColor||this.series[0]&&this.series[0].color||"gray",padding:p(f.padding,2),stroke:f.borderColor||null,"stroke-width":f.borderWidth||0}).css(x({color:"white",fontWeight:"normal",fontSize:"11px",
textAlign:"center"},f.style)).add();b?(l=c.plotX+i,m=j+(h?0:this.height)):(l=h?this.width+i:0,m=c.plotY+j);if(m<j||m>j+this.height)this.hideCrosshair();else{!n&&!f.formatter&&(this.isDatetimeAxis&&(q="%b %d, %Y"),n="{value"+(q?":"+q:"")+"}");k.attr({text:n?Ma(n,{value:c[g]}):f.formatter.call(this,c[g]),x:l,y:m,visibility:"visible"});c=k.getBBox();if(b){if(this.options.tickPosition==="inside"&&!h||this.options.tickPosition!=="inside"&&h)m=k.y-c.height}else m=k.y-c.height/2;b?(d=i-c.x,e=i+this.width-
c.x):(d=this.labelAlign==="left"?i:0,e=this.labelAlign==="right"?i+this.width:a.chartWidth);k.translateX<d&&(l+=d-k.translateX);k.translateX+c.width>=e&&(l-=k.translateX+c.width-e);k.attr({x:l,y:m,visibility:"visible"})}}});var lc=da.init,mc=da.processData,nc=Ca.prototype.tooltipFormatter;da.init=function(){lc.apply(this,arguments);this.setCompare(this.options.compare)};da.setCompare=function(a){this.modifyValue=a==="value"||a==="percent"?function(b,c){var d=this.compareValue;if(b!==r&&(b=a==="value"?
b-d:b=100*(b/d)-100,c))c.change=b;return b}:null;if(this.chart.hasRendered)this.isDirty=!0};da.processData=function(){var a=0,b,c,d;mc.apply(this,arguments);if(this.xAxis&&this.processedYData){b=this.processedXData;c=this.processedYData;for(d=c.length;a<d;a++)if(typeof c[a]==="number"&&b[a]>=this.xAxis.min){this.compareValue=c[a];break}}};S(da,"getExtremes",function(a){a.apply(this,[].slice.call(arguments,1));if(this.modifyValue)this.dataMax=this.modifyValue(this.dataMax),this.dataMin=this.modifyValue(this.dataMin)});
O.prototype.setCompare=function(a,b){this.isXAxis||(n(this.series,function(b){b.setCompare(a)}),p(b,!0)&&this.chart.redraw())};Ca.prototype.tooltipFormatter=function(a){a=a.replace("{point.change}",(this.change>0?"+":"")+z.numberFormat(this.change,p(this.series.tooltipOptions.changeDecimals,2)));return nc.apply(this,[a])};S(P.prototype,"render",function(a){if(this.chart.options._stock)!this.clipBox&&this.animate&&this.animate.toString().indexOf("sharedClip")!==-1?(this.clipBox=y(this.chart.clipBox),
this.clipBox.width=this.xAxis.len,this.clipBox.height=this.yAxis.len):this.chart[this.sharedClipKey]&&(ab(this.chart[this.sharedClipKey]),this.chart[this.sharedClipKey].attr({width:this.xAxis.len,height:this.yAxis.len}));a.call(this)});x(z,{Color:wa,Point:Ca,Tick:Za,Renderer:Wa,SVGElement:$,SVGRenderer:na,arrayMin:Sa,arrayMax:Fa,charts:ca,dateFormat:ka,error:qa,format:Ma,pathAnim:Ib,getOptions:function(){return F},hasBidiBug:Wb,isTouchDevice:fb,setOptions:function(a){F=y(!0,F,a);Nb();return F},addEvent:D,
removeEvent:U,createElement:aa,discardElement:Ta,css:M,each:n,map:Aa,merge:y,splat:pa,extendClass:ja,pInt:C,svg:ea,canvas:ma,vml:!ea&&!ma,product:"Highstock",version:"2.1.4"})})();

/*!
 * typeahead.js 0.9.3
 * https://github.com/twitter/typeahead
 * Copyright 2013 Twitter, Inc. and other contributors; Licensed MIT
 */

!function(a){var b="0.9.3",c={isMsie:function(){var a=/(msie) ([\w.]+)/i.exec(navigator.userAgent);return a?parseInt(a[2],10):!1},isBlankString:function(a){return!a||/^\s*$/.test(a)},escapeRegExChars:function(a){return a.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,"\\$&")},isString:function(a){return"string"==typeof a},isNumber:function(a){return"number"==typeof a},isArray:a.isArray,isFunction:a.isFunction,isObject:a.isPlainObject,isUndefined:function(a){return"undefined"==typeof a},bind:a.proxy,bindAll:function(b){var c;for(var d in b)a.isFunction(c=b[d])&&(b[d]=a.proxy(c,b))},indexOf:function(a,b){for(var c=0;c<a.length;c++)if(a[c]===b)return c;return-1},each:a.each,map:a.map,filter:a.grep,every:function(b,c){var d=!0;return b?(a.each(b,function(a,e){return(d=c.call(null,e,a,b))?void 0:!1}),!!d):d},some:function(b,c){var d=!1;return b?(a.each(b,function(a,e){return(d=c.call(null,e,a,b))?!1:void 0}),!!d):d},mixin:a.extend,getUniqueId:function(){var a=0;return function(){return a++}}(),defer:function(a){setTimeout(a,0)},debounce:function(a,b,c){var d,e;return function(){var f,g,h=this,i=arguments;return f=function(){d=null,c||(e=a.apply(h,i))},g=c&&!d,clearTimeout(d),d=setTimeout(f,b),g&&(e=a.apply(h,i)),e}},throttle:function(a,b){var c,d,e,f,g,h;return g=0,h=function(){g=new Date,e=null,f=a.apply(c,d)},function(){var i=new Date,j=b-(i-g);return c=this,d=arguments,0>=j?(clearTimeout(e),e=null,g=i,f=a.apply(c,d)):e||(e=setTimeout(h,j)),f}},tokenizeQuery:function(b){return a.trim(b).toLowerCase().split(/[\s]+/)},tokenizeText:function(b){return a.trim(b).toLowerCase().split(/[\s\-_]+/)},getProtocol:function(){return location.protocol},noop:function(){}},d=function(){var a=/\s+/;return{on:function(b,c){var d;if(!c)return this;for(this._callbacks=this._callbacks||{},b=b.split(a);d=b.shift();)this._callbacks[d]=this._callbacks[d]||[],this._callbacks[d].push(c);return this},trigger:function(b,c){var d,e;if(!this._callbacks)return this;for(b=b.split(a);d=b.shift();)if(e=this._callbacks[d])for(var f=0;f<e.length;f+=1)e[f].call(this,{type:d,data:c});return this}}}(),e=function(){function b(b){b&&b.el||a.error("EventBus initialized without el"),this.$el=a(b.el)}var d="typeahead:";return c.mixin(b.prototype,{trigger:function(a){var b=[].slice.call(arguments,1);this.$el.trigger(d+a,b)}}),b}(),f=function(){function a(a){this.prefix=["__",a,"__"].join(""),this.ttlKey="__ttl__",this.keyMatcher=new RegExp("^"+this.prefix)}function b(){return(new Date).getTime()}function d(a){return JSON.stringify(c.isUndefined(a)?null:a)}function e(a){return JSON.parse(a)}var f,g;try{f=window.localStorage,f.setItem("~~~","!"),f.removeItem("~~~")}catch(h){f=null}return g=f&&window.JSON?{_prefix:function(a){return this.prefix+a},_ttlKey:function(a){return this._prefix(a)+this.ttlKey},get:function(a){return this.isExpired(a)&&this.remove(a),e(f.getItem(this._prefix(a)))},set:function(a,e,g){return c.isNumber(g)?f.setItem(this._ttlKey(a),d(b()+g)):f.removeItem(this._ttlKey(a)),f.setItem(this._prefix(a),d(e))},remove:function(a){return f.removeItem(this._ttlKey(a)),f.removeItem(this._prefix(a)),this},clear:function(){var a,b,c=[],d=f.length;for(a=0;d>a;a++)(b=f.key(a)).match(this.keyMatcher)&&c.push(b.replace(this.keyMatcher,""));for(a=c.length;a--;)this.remove(c[a]);return this},isExpired:function(a){var d=e(f.getItem(this._ttlKey(a)));return c.isNumber(d)&&b()>d?!0:!1}}:{get:c.noop,set:c.noop,remove:c.noop,clear:c.noop,isExpired:c.noop},c.mixin(a.prototype,g),a}(),g=function(){function a(a){c.bindAll(this),a=a||{},this.sizeLimit=a.sizeLimit||10,this.cache={},this.cachedKeysByAge=[]}return c.mixin(a.prototype,{get:function(a){return this.cache[a]},set:function(a,b){var c;this.cachedKeysByAge.length===this.sizeLimit&&(c=this.cachedKeysByAge.shift(),delete this.cache[c]),this.cache[a]=b,this.cachedKeysByAge.push(a)}}),a}(),h=function(){function b(a){c.bindAll(this),a=c.isString(a)?{url:a}:a,i=i||new g,h=c.isNumber(a.maxParallelRequests)?a.maxParallelRequests:h||6,this.url=a.url,this.wildcard=a.wildcard||"%QUERY",this.filter=a.filter,this.replace=a.replace,this.ajaxSettings={type:"get",cache:a.cache,timeout:a.timeout,dataType:a.dataType||"json",beforeSend:a.beforeSend},this._get=(/^throttle$/i.test(a.rateLimitFn)?c.throttle:c.debounce)(this._get,a.rateLimitWait||300)}function d(){j++}function e(){j--}function f(){return h>j}var h,i,j=0,k={};return c.mixin(b.prototype,{_get:function(a,b){function c(c){var e=d.filter?d.filter(c):c;b&&b(e),i.set(a,c)}var d=this;f()?this._sendRequest(a).done(c):this.onDeckRequestArgs=[].slice.call(arguments,0)},_sendRequest:function(b){function c(){e(),k[b]=null,f.onDeckRequestArgs&&(f._get.apply(f,f.onDeckRequestArgs),f.onDeckRequestArgs=null)}var f=this,g=k[b];return g||(d(),g=k[b]=a.ajax(b,this.ajaxSettings).always(c)),g},get:function(a,b){var d,e,f=this,g=encodeURIComponent(a||"");return b=b||c.noop,d=this.replace?this.replace(this.url,g):this.url.replace(this.wildcard,g),(e=i.get(d))?c.defer(function(){b(f.filter?f.filter(e):e)}):this._get(d,b),!!e}}),b}(),i=function(){function d(b){c.bindAll(this),c.isString(b.template)&&!b.engine&&a.error("no template engine specified"),b.local||b.prefetch||b.remote||a.error("one of local, prefetch, or remote is required"),this.name=b.name||c.getUniqueId(),this.limit=b.limit||5,this.minLength=b.minLength||1,this.header=b.header,this.footer=b.footer,this.valueKey=b.valueKey||"value",this.template=e(b.template,b.engine,this.valueKey),this.local=b.local,this.prefetch=b.prefetch,this.remote=b.remote,this.itemHash={},this.adjacencyList={},this.storage=b.name?new f(b.name):null}function e(a,b,d){var e,f;return c.isFunction(a)?e=a:c.isString(a)?(f=b.compile(a),e=c.bind(f.render,f)):e=function(a){return"<p>"+a[d]+"</p>"},e}var g={thumbprint:"thumbprint",protocol:"protocol",itemHash:"itemHash",adjacencyList:"adjacencyList"};return c.mixin(d.prototype,{_processLocalData:function(a){this._mergeProcessedData(this._processData(a))},_loadPrefetchData:function(d){function e(a){var b=d.filter?d.filter(a):a,e=m._processData(b),f=e.itemHash,h=e.adjacencyList;m.storage&&(m.storage.set(g.itemHash,f,d.ttl),m.storage.set(g.adjacencyList,h,d.ttl),m.storage.set(g.thumbprint,n,d.ttl),m.storage.set(g.protocol,c.getProtocol(),d.ttl)),m._mergeProcessedData(e)}var f,h,i,j,k,l,m=this,n=b+(d.thumbprint||"");return this.storage&&(f=this.storage.get(g.thumbprint),h=this.storage.get(g.protocol),i=this.storage.get(g.itemHash),j=this.storage.get(g.adjacencyList)),k=f!==n||h!==c.getProtocol(),d=c.isString(d)?{url:d}:d,d.ttl=c.isNumber(d.ttl)?d.ttl:864e5,i&&j&&!k?(this._mergeProcessedData({itemHash:i,adjacencyList:j}),l=a.Deferred().resolve()):l=a.getJSON(d.url).done(e),l},_transformDatum:function(a){var b=c.isString(a)?a:a[this.valueKey],d=a.tokens||c.tokenizeText(b),e={value:b,tokens:d};return c.isString(a)?(e.datum={},e.datum[this.valueKey]=a):e.datum=a,e.tokens=c.filter(e.tokens,function(a){return!c.isBlankString(a)}),e.tokens=c.map(e.tokens,function(a){return a.toLowerCase()}),e},_processData:function(a){var b=this,d={},e={};return c.each(a,function(a,f){var g=b._transformDatum(f),h=c.getUniqueId(g.value);d[h]=g,c.each(g.tokens,function(a,b){var d=b.charAt(0),f=e[d]||(e[d]=[h]);!~c.indexOf(f,h)&&f.push(h)})}),{itemHash:d,adjacencyList:e}},_mergeProcessedData:function(a){var b=this;c.mixin(this.itemHash,a.itemHash),c.each(a.adjacencyList,function(a,c){var d=b.adjacencyList[a];b.adjacencyList[a]=d?d.concat(c):c})},_getLocalSuggestions:function(a){var b,d=this,e=[],f=[],g=[];return c.each(a,function(a,b){var d=b.charAt(0);!~c.indexOf(e,d)&&e.push(d)}),c.each(e,function(a,c){var e=d.adjacencyList[c];return e?(f.push(e),(!b||e.length<b.length)&&(b=e),void 0):!1}),f.length<e.length?[]:(c.each(b,function(b,e){var h,i,j=d.itemHash[e];h=c.every(f,function(a){return~c.indexOf(a,e)}),i=h&&c.every(a,function(a){return c.some(j.tokens,function(b){return 0===b.indexOf(a)})}),i&&g.push(j)}),g)},initialize:function(){var b;return this.local&&this._processLocalData(this.local),this.transport=this.remote?new h(this.remote):null,b=this.prefetch?this._loadPrefetchData(this.prefetch):a.Deferred().resolve(),this.local=this.prefetch=this.remote=null,this.initialize=function(){return b},b},getSuggestions:function(a,b){function d(a){f=f.slice(0),c.each(a,function(a,b){var d,e=g._transformDatum(b);return d=c.some(f,function(a){return e.value===a.value}),!d&&f.push(e),f.length<g.limit}),b&&b(f)}var e,f,g=this,h=!1;a.length<this.minLength||(e=c.tokenizeQuery(a),f=this._getLocalSuggestions(e).slice(0,this.limit),f.length<this.limit&&this.transport&&(h=this.transport.get(a,d)),!h&&b&&b(f))}}),d}(),j=function(){function b(b){var d=this;c.bindAll(this),this.specialKeyCodeMap={9:"tab",27:"esc",37:"left",39:"right",13:"enter",38:"up",40:"down"},this.$hint=a(b.hint),this.$input=a(b.input).on("blur.tt",this._handleBlur).on("focus.tt",this._handleFocus).on("keydown.tt",this._handleSpecialKeyEvent),c.isMsie()?this.$input.on("keydown.tt keypress.tt cut.tt paste.tt",function(a){d.specialKeyCodeMap[a.which||a.keyCode]||c.defer(d._compareQueryToInputValue)}):this.$input.on("input.tt",this._compareQueryToInputValue),this.query=this.$input.val(),this.$overflowHelper=e(this.$input)}function e(b){return a("<span></span>").css({position:"absolute",left:"-9999px",visibility:"hidden",whiteSpace:"nowrap",fontFamily:b.css("font-family"),fontSize:b.css("font-size"),fontStyle:b.css("font-style"),fontVariant:b.css("font-variant"),fontWeight:b.css("font-weight"),wordSpacing:b.css("word-spacing"),letterSpacing:b.css("letter-spacing"),textIndent:b.css("text-indent"),textRendering:b.css("text-rendering"),textTransform:b.css("text-transform")}).insertAfter(b)}function f(a,b){return a=(a||"").replace(/^\s*/g,"").replace(/\s{2,}/g," "),b=(b||"").replace(/^\s*/g,"").replace(/\s{2,}/g," "),a===b}return c.mixin(b.prototype,d,{_handleFocus:function(){this.trigger("focused")},_handleBlur:function(){this.trigger("blured")},_handleSpecialKeyEvent:function(a){var b=this.specialKeyCodeMap[a.which||a.keyCode];b&&this.trigger(b+"Keyed",a)},_compareQueryToInputValue:function(){var a=this.getInputValue(),b=f(this.query,a),c=b?this.query.length!==a.length:!1;c?this.trigger("whitespaceChanged",{value:this.query}):b||this.trigger("queryChanged",{value:this.query=a})},destroy:function(){this.$hint.off(".tt"),this.$input.off(".tt"),this.$hint=this.$input=this.$overflowHelper=null},focus:function(){this.$input.focus()},blur:function(){this.$input.blur()},getQuery:function(){return this.query},setQuery:function(a){this.query=a},getInputValue:function(){return this.$input.val()},setInputValue:function(a,b){this.$input.val(a),!b&&this._compareQueryToInputValue()},getHintValue:function(){return this.$hint.val()},setHintValue:function(a){this.$hint.val(a)},getLanguageDirection:function(){return(this.$input.css("direction")||"ltr").toLowerCase()},isOverflow:function(){return this.$overflowHelper.text(this.getInputValue()),this.$overflowHelper.width()>this.$input.width()},isCursorAtEnd:function(){var a,b=this.$input.val().length,d=this.$input[0].selectionStart;return c.isNumber(d)?d===b:document.selection?(a=document.selection.createRange(),a.moveStart("character",-b),b===a.text.length):!0}}),b}(),k=function(){function b(b){c.bindAll(this),this.isOpen=!1,this.isEmpty=!0,this.isMouseOverDropdown=!1,this.$menu=a(b.menu).on("mouseenter.tt",this._handleMouseenter).on("mouseleave.tt",this._handleMouseleave).on("click.tt",".tt-suggestion",this._handleSelection).on("mouseover.tt",".tt-suggestion",this._handleMouseover)}function e(a){return a.data("suggestion")}var f={suggestionsList:'<span class="tt-suggestions"></span>'},g={suggestionsList:{display:"block"},suggestion:{whiteSpace:"nowrap",cursor:"pointer"},suggestionChild:{whiteSpace:"normal"}};return c.mixin(b.prototype,d,{_handleMouseenter:function(){this.isMouseOverDropdown=!0},_handleMouseleave:function(){this.isMouseOverDropdown=!1},_handleMouseover:function(b){var c=a(b.currentTarget);this._getSuggestions().removeClass("tt-is-under-cursor"),c.addClass("tt-is-under-cursor")},_handleSelection:function(b){var c=a(b.currentTarget);this.trigger("suggestionSelected",e(c))},_show:function(){this.$menu.css("display","block")},_hide:function(){this.$menu.hide()},_moveCursor:function(a){var b,c,d,f;if(this.isVisible()){if(b=this._getSuggestions(),c=b.filter(".tt-is-under-cursor"),c.removeClass("tt-is-under-cursor"),d=b.index(c)+a,d=(d+1)%(b.length+1)-1,-1===d)return this.trigger("cursorRemoved"),void 0;-1>d&&(d=b.length-1),f=b.eq(d).addClass("tt-is-under-cursor"),this._ensureVisibility(f),this.trigger("cursorMoved",e(f))}},_getSuggestions:function(){return this.$menu.find(".tt-suggestions > .tt-suggestion")},_ensureVisibility:function(a){var b=this.$menu.height()+parseInt(this.$menu.css("paddingTop"),10)+parseInt(this.$menu.css("paddingBottom"),10),c=this.$menu.scrollTop(),d=a.position().top,e=d+a.outerHeight(!0);0>d?this.$menu.scrollTop(c+d):e>b&&this.$menu.scrollTop(c+(e-b))},destroy:function(){this.$menu.off(".tt"),this.$menu=null},isVisible:function(){return this.isOpen&&!this.isEmpty},closeUnlessMouseIsOverDropdown:function(){this.isMouseOverDropdown||this.close()},close:function(){this.isOpen&&(this.isOpen=!1,this.isMouseOverDropdown=!1,this._hide(),this.$menu.find(".tt-suggestions > .tt-suggestion").removeClass("tt-is-under-cursor"),this.trigger("closed"))},open:function(){this.isOpen||(this.isOpen=!0,!this.isEmpty&&this._show(),this.trigger("opened"))},setLanguageDirection:function(a){var b={left:"0",right:"auto"},c={left:"auto",right:" 0"};"ltr"===a?this.$menu.css(b):this.$menu.css(c)},moveCursorUp:function(){this._moveCursor(-1)},moveCursorDown:function(){this._moveCursor(1)},getSuggestionUnderCursor:function(){var a=this._getSuggestions().filter(".tt-is-under-cursor").first();return a.length>0?e(a):null},getFirstSuggestion:function(){var a=this._getSuggestions().first();return a.length>0?e(a):null},renderSuggestions:function(b,d){var e,h,i,j,k,l="tt-dataset-"+b.name,m='<div class="tt-suggestion">%body</div>',n=this.$menu.find("."+l);0===n.length&&(h=a(f.suggestionsList).css(g.suggestionsList),n=a("<div></div>").addClass(l).append(b.header).append(h).append(b.footer).appendTo(this.$menu)),d.length>0?(this.isEmpty=!1,this.isOpen&&this._show(),i=document.createElement("div"),j=document.createDocumentFragment(),c.each(d,function(c,d){d.dataset=b.name,e=b.template(d.datum),i.innerHTML=m.replace("%body",e),k=a(i.firstChild).css(g.suggestion).data("suggestion",d),k.children().each(function(){a(this).css(g.suggestionChild)}),j.appendChild(k[0])}),n.show().find(".tt-suggestions").html(j)):this.clearSuggestions(b.name),this.trigger("suggestionsRendered")},clearSuggestions:function(a){var b=a?this.$menu.find(".tt-dataset-"+a):this.$menu.find('[class^="tt-dataset-"]'),c=b.find(".tt-suggestions");b.hide(),c.empty(),0===this._getSuggestions().length&&(this.isEmpty=!0,this._hide())}}),b}(),l=function(){function b(a){var b,d,f;c.bindAll(this),this.$node=e(a.input),this.datasets=a.datasets,this.dir=null,this.eventBus=a.eventBus,b=this.$node.find(".tt-dropdown-menu"),d=this.$node.find(".tt-query"),f=this.$node.find(".tt-hint"),this.dropdownView=new k({menu:b}).on("suggestionSelected",this._handleSelection).on("cursorMoved",this._clearHint).on("cursorMoved",this._setInputValueToSuggestionUnderCursor).on("cursorRemoved",this._setInputValueToQuery).on("cursorRemoved",this._updateHint).on("suggestionsRendered",this._updateHint).on("opened",this._updateHint).on("closed",this._clearHint).on("opened closed",this._propagateEvent),this.inputView=new j({input:d,hint:f}).on("focused",this._openDropdown).on("blured",this._closeDropdown).on("blured",this._setInputValueToQuery).on("enterKeyed tabKeyed",this._handleSelection).on("queryChanged",this._clearHint).on("queryChanged",this._clearSuggestions).on("queryChanged",this._getSuggestions).on("whitespaceChanged",this._updateHint).on("queryChanged whitespaceChanged",this._openDropdown).on("queryChanged whitespaceChanged",this._setLanguageDirection).on("escKeyed",this._closeDropdown).on("escKeyed",this._setInputValueToQuery).on("tabKeyed upKeyed downKeyed",this._managePreventDefault).on("upKeyed downKeyed",this._moveDropdownCursor).on("upKeyed downKeyed",this._openDropdown).on("tabKeyed leftKeyed rightKeyed",this._autocomplete)}function e(b){var c=a(g.wrapper),d=a(g.dropdown),e=a(b),f=a(g.hint);c=c.css(h.wrapper),d=d.css(h.dropdown),f.css(h.hint).css({backgroundAttachment:e.css("background-attachment"),backgroundClip:e.css("background-clip"),backgroundColor:e.css("background-color"),backgroundImage:e.css("background-image"),backgroundOrigin:e.css("background-origin"),backgroundPosition:e.css("background-position"),backgroundRepeat:e.css("background-repeat"),backgroundSize:e.css("background-size")}),e.data("ttAttrs",{dir:e.attr("dir"),autocomplete:e.attr("autocomplete"),spellcheck:e.attr("spellcheck"),style:e.attr("style")}),e.addClass("tt-query").attr({autocomplete:"off",spellcheck:!1}).css(h.query);try{!e.attr("dir")&&e.attr("dir","auto")}catch(i){}return e.wrap(c).parent().prepend(f).append(d)}function f(a){var b=a.find(".tt-query");c.each(b.data("ttAttrs"),function(a,d){c.isUndefined(d)?b.removeAttr(a):b.attr(a,d)}),b.detach().removeData("ttAttrs").removeClass("tt-query").insertAfter(a),a.remove()}var g={wrapper:'<span class="twitter-typeahead"></span>',hint:'<input class="tt-hint" type="text" autocomplete="off" spellcheck="off" disabled>',dropdown:'<span class="tt-dropdown-menu"></span>'},h={wrapper:{position:"relative",display:"inline-block"},hint:{position:"absolute",top:"0",left:"0",borderColor:"transparent",boxShadow:"none"},query:{position:"relative",verticalAlign:"top",backgroundColor:"transparent"},dropdown:{position:"absolute",top:"100%",left:"0",zIndex:"100",display:"none"}};return c.isMsie()&&c.mixin(h.query,{backgroundImage:"url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)"}),c.isMsie()&&c.isMsie()<=7&&(c.mixin(h.wrapper,{display:"inline",zoom:"1"}),c.mixin(h.query,{marginTop:"-1px"})),c.mixin(b.prototype,d,{_managePreventDefault:function(a){var b,c,d=a.data,e=!1;switch(a.type){case"tabKeyed":b=this.inputView.getHintValue(),c=this.inputView.getInputValue(),e=b&&b!==c;break;case"upKeyed":case"downKeyed":e=!d.shiftKey&&!d.ctrlKey&&!d.metaKey}e&&d.preventDefault()},_setLanguageDirection:function(){var a=this.inputView.getLanguageDirection();a!==this.dir&&(this.dir=a,this.$node.css("direction",a),this.dropdownView.setLanguageDirection(a))},_updateHint:function(){var a,b,d,e,f,g=this.dropdownView.getFirstSuggestion(),h=g?g.value:null,i=this.dropdownView.isVisible(),j=this.inputView.isOverflow();h&&i&&!j&&(a=this.inputView.getInputValue(),b=a.replace(/\s{2,}/g," ").replace(/^\s+/g,""),d=c.escapeRegExChars(b),e=new RegExp("^(?:"+d+")(.*$)","i"),f=e.exec(h),this.inputView.setHintValue(a+(f?f[1]:"")))},_clearHint:function(){this.inputView.setHintValue("")},_clearSuggestions:function(){this.dropdownView.clearSuggestions()},_setInputValueToQuery:function(){this.inputView.setInputValue(this.inputView.getQuery())},_setInputValueToSuggestionUnderCursor:function(a){var b=a.data;this.inputView.setInputValue(b.value,!0)},_openDropdown:function(){this.dropdownView.open()},_closeDropdown:function(a){this.dropdownView["blured"===a.type?"closeUnlessMouseIsOverDropdown":"close"]()},_moveDropdownCursor:function(a){var b=a.data;b.shiftKey||b.ctrlKey||b.metaKey||this.dropdownView["upKeyed"===a.type?"moveCursorUp":"moveCursorDown"]()},_handleSelection:function(a){var b="suggestionSelected"===a.type,d=b?a.data:this.dropdownView.getSuggestionUnderCursor();d&&(this.inputView.setInputValue(d.value),b?this.inputView.focus():a.data.preventDefault(),b&&c.isMsie()?c.defer(this.dropdownView.close):this.dropdownView.close(),this.eventBus.trigger("selected",d.datum,d.dataset))},_getSuggestions:function(){var a=this,b=this.inputView.getQuery();c.isBlankString(b)||c.each(this.datasets,function(c,d){d.getSuggestions(b,function(c){b===a.inputView.getQuery()&&a.dropdownView.renderSuggestions(d,c)})})},_autocomplete:function(a){var b,c,d,e,f;("rightKeyed"!==a.type&&"leftKeyed"!==a.type||(b=this.inputView.isCursorAtEnd(),c="ltr"===this.inputView.getLanguageDirection()?"leftKeyed"===a.type:"rightKeyed"===a.type,b&&!c))&&(d=this.inputView.getQuery(),e=this.inputView.getHintValue(),""!==e&&d!==e&&(f=this.dropdownView.getFirstSuggestion(),this.inputView.setInputValue(f.value),this.eventBus.trigger("autocompleted",f.datum,f.dataset)))},_propagateEvent:function(a){this.eventBus.trigger(a.type)},destroy:function(){this.inputView.destroy(),this.dropdownView.destroy(),f(this.$node),this.$node=null},setQuery:function(a){this.inputView.setQuery(a),this.inputView.setInputValue(a),this._clearHint(),this._clearSuggestions(),this._getSuggestions()}}),b}();!function(){var b,d={},f="ttView";b={initialize:function(b){function g(){var b,d=a(this),g=new e({el:d});b=c.map(h,function(a){return a.initialize()}),d.data(f,new l({input:d,eventBus:g=new e({el:d}),datasets:h})),a.when.apply(a,b).always(function(){c.defer(function(){g.trigger("initialized")})})}var h;return b=c.isArray(b)?b:[b],0===b.length&&a.error("no datasets provided"),h=c.map(b,function(a){var b=d[a.name]?d[a.name]:new i(a);return a.name&&(d[a.name]=b),b}),this.each(g)},destroy:function(){function b(){var b=a(this),c=b.data(f);c&&(c.destroy(),b.removeData(f))}return this.each(b)},setQuery:function(b){function c(){var c=a(this).data(f);c&&c.setQuery(b)}return this.each(c)}},jQuery.fn.typeahead=function(a){return b[a]?b[a].apply(this,[].slice.call(arguments,1)):b.initialize.apply(this,arguments)}}()}(window.jQuery);
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Use application configuration module to register a new module
ApplicationConfiguration.registerModule('musicsource');

'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('sales');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('users');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('youtube');

'use strict';

// Setting up route
angular.module('core').config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function($stateProvider, $urlRouterProvider, $locationProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		})
		.state('notification', {
			url: '/notification',
			templateUrl: 'modules/core/views/notification.client.view.html'
		})
		.state('urlSale', {
			url: '/core/url-sale?code&message',
			templateUrl: 'modules/core/views/url-sale.client.view.html'
		});

		// use the HTML5 History API
        $locationProvider.html5Mode(true);
	}]
);

'use strict';


angular.module('core').controller('SaleURLController',
	["$scope", "$location", "$window", "Authentication", "$stateParams", "Notifier", "SalesUrl", function($scope, $location, $window, Authentication, $stateParams, Notifier, SalesUrl) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		if (! $scope.authentication.user) {
			$location.path('/signin');
			return;
		}
        $scope.tableHeader = [
            {name: 'SOURCE URL', sort: 'sorting', schema: 'sourceUrl'},
            {name: 'SHORTEN URL', sort: 'sorting', schema: 'shortenUrl'},
			{name: 'COMPANY', sort: 'sorting', schema: 'companyName'},
			{name: 'ACTION', sort: 'sorting', schema: 'sourceUrl'}
        ];
		$scope.totalPages = 1;
		$scope.currentPage = 1;
		$scope.showEntries = [10, 25, 50, 100];
		$scope.numEntryPerPage = 10;
		$scope.sortedSchema = 'partnerName';
		$scope.isAscending = 1;
		$scope.isLoading = true;

		$scope.range=function(min, max, total) {
			min = $window.Math.max(min, 1);
			max = $window.Math.min(max, total);

			for(var input=[],i=min; max>=i;i++) {
				input.push(i);
			}
			return input;
		};

		$scope.setTotalPages = function(total) {
			$scope.totalPages = parseInt(total / $scope.numEntryPerPage);
			if ($scope.totalPages * $scope.numEntryPerPage !== total) {
				$scope.totalPages += 1;
			}
		};

		$scope.loadPageDetail = function (pageNumber) {
			pageNumber = $window.Math.max(pageNumber, 1);
			$scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
			$scope.initLoad();
		};

		$scope.updateEntry = function () {
			//console.log('Showing ' + $scope.numEntryPerPage + ' entries');
			// reload data
			$scope.initLoad();
		};

		$scope.searchEnter = function (keyEvent) {
			if (keyEvent.which === 13) {
				//Enter is pressed
				$scope.search();
			}
		};

		$scope.search = function () {
			if ($scope.isLoading || !$scope.inputSearchText || $scope.inputSearchText && $scope.inputSearchText.trim() === '') {
				return;
			}
			console.log('query with searchText = ' + $scope.inputSearchText);
			// reload data
			$scope.initLoad();
		};

		var toggleSortClass = function (sortClass) {
			switch (sortClass) {
				case 'sorting':
					return 'sorting_asc';
				case 'sorting_asc':
					return 'sorting_desc';
				case 'sorting_desc':
					return 'sorting_asc';
				default:
					return 'sorting';
			}
		};

		var checkIfAscending = function (className) {
			if (className === 'sorting_asc') {
				return 1;
			} else {
				return -1;
			}
		};

		$scope.sortByHeader = function (index) {

			if ($scope.isLoading) {return;}

			console.log('sorting this column ' + index);

			// toggle the sort class at current column `index`
			var newClassName = toggleSortClass($scope.tableHeader[index].sort);
			$scope.tableHeader[index].sort = newClassName;

			// reset other column to 'sorting'
			for (var i = 0; i < $scope.tableHeader.length; i++) {
				if (i === index) { continue; }
				$scope.tableHeader[i].sort = 'sorting';
			}

			// store the sorted state in global variable
			$scope.sortedSchema = $scope.tableHeader[index].schema;
			$scope.isAscending = checkIfAscending(newClassName);

			// reload data
			$scope.initLoad();
		};

		$scope.getParameters = function () {
			var params = {};
			params.page = $scope.currentPage;
			params.numPerPage = $scope.numEntryPerPage;
			params.sort = $scope.sortedSchema;
			params.isAscending = $scope.isAscending;

			if (typeof $scope.inputSearchText !== 'undefined' && $scope.inputSearchText !== '') {
				params.searchText = $scope.inputSearchText;
			}

			return params;
		};

		$scope.initLoad = function() {
			var params = $scope.getParameters();
			SalesUrl.listSaleUrl(params).then(function (response) {
				$scope.setTotalPages(response.total);
				$scope.listUrl = response.data;
				$scope.isLoading = false;
			});
		};

		// Open Simple Modal
		$scope.openModal = function(urlId) {
			$scope.removeurlId = urlId;
		};

		$scope.confirmRemove = function() {
			if($scope.removeurlId) {
				SalesUrl.deleteSaleURLByID({urlId: $scope.removeurlId}).then(function () {
					$scope.initLoad();
				});
			}
		};

		$scope.shortenSaleURL = function() {
			SalesUrl.shortenSaleURL({
				sourceUrl: $scope.sourceUrl,
				companyName: $scope.companyName
			}).then(function () {
				$scope.initLoad();
				Notifier.notify($stateParams.message, 'Success!');
			});
		};
	}]
);

'use strict';


angular.module('core').controller('HomeController',
	["$scope", "$http", "$state", "$location", "Authentication", "Authorization", "$rootScope", function($scope, $http, $state, $location, Authentication, Authorization, $rootScope) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		if (! $scope.authentication.user) {
			$location.path('/signin');
			return;
		} else {
			var state = 'viewSaleCommission';
			if($location.search().redirectState) {
				state = $location.search().redirectState;
				delete $location.search().redirectState;
			}
			var debug = $location.search().debug;
			$rootScope.debug = (debug === 'true');
			$state.go(state, $location.search());
		}

		$scope.auth = Authorization;
		
		$scope.getNewsAlert = function() {
			$scope.cntAlert = 0;
			//$http.get('/albums/status-comission')
			//	.success(function (response) {
			//		$scope.cntAlert = parseInt(response.data);
			//});
		};
	}]
);

'use strict';

angular.module('core').factory('Utility', ["$window", function($window) {

	return {
		albumRoles: [
			{id: 'composer', name: 'Composer'},
			{id: 'sound engineer', name: 'Sound Engineer'},
			{id: 'label', name: 'Label'},
			{id: 'vocalist', name: 'Vocalist'},
			{id: 'musician', name: 'Musician'},
			{id: 'ochestrator', name: 'Ochestrator'}
		],
		channelRoles: [
			{id: 'editor', name: 'Editor'},
			{id: 'motion designer', name: 'Motion Designer'},
			{id: 'graphic designer', name: 'Graphic Designer'}
		],
		apiUrl: $window.apiUrl
	};
}]);

'use strict';

angular.module('core').value('toastr', toastr);

angular.module('core').factory('Notifier', ["toastr", function(toastr) {
	var opts = {
		'closeButton': true,
		'debug': false,
		'positionClass': 'toast-bottom-right',
		'onclick': null,
		'showDuration': '300',
		'hideDuration': '1000',
		'timeOut': '5000',
		'extendedTimeOut': '1000',
		'showEasing': 'swing',
		'hideEasing': 'linear',
		'showMethod': 'fadeIn',
		'hideMethod': 'fadeOut'
	};

	return {
		notify: function(sms, title) {
			toastr.success(sms, title, opts);
		},
		error: function(sms, title) {
			toastr.error(sms, title, opts);
		}
	};
}]);

/**
 * Created by steventran on 5/17/15.
 */

'use strict';

//Sales service used to communicate Sales REST endpoints
angular.module('core').factory('SalesUrl', ["$http", "$q", "Utility", function($http, $q, Utility) {
        return {
            listSaleUrl: function(params) {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl +'/sale-url', {params: params}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            deleteSaleURLByID: function(body) {
                var dfd = $q.defer();
                $http.post(Utility.apiUrl +'/sale-url/delete', body).success(function () {
                    dfd.resolve(true);
                });
                return dfd.promise;
            },

            shortenSaleURL: function(body) {
                var dfd = $q.defer();
                $http.post(Utility.apiUrl +'/bitly-auth', body).success(function () {
                    dfd.resolve(true);
                });
                return dfd.promise;
            },
        };
    }]
);

/**
 * Created by sonthanh on 4/6/15.
 */
'use strict';

//Setting up route
angular.module('musicsource').config(
    ["$stateProvider", function($stateProvider) {
        // Sales state routing
        $stateProvider.
            state('submitmusicsource', {
                url: '/musicsource/submit',
                templateUrl: 'modules/musicsource/views/submit.client.view.html'
            });
    }]
);

/**
 * Created by sonthanh on 4/6/15.
 */
'use strict';

angular.module('musicsource').controller('SubmitController', ["$scope", function($scope){
    console.log('submit controller');
}]);

'use strict';

//Setting up route
angular.module('sales').config(
	["$stateProvider", function($stateProvider) {
		// Sales state routing
		$stateProvider.
		state('importSaleReport', {
			url: '/sale/import-report',
			templateUrl: 'modules/sales/views/import-report.client.view.html'
		}).
		state('viewTrackShares', {
			url: '/sale/view-track-shares',
			templateUrl: 'modules/sales/views/view-track-share.client.view.html'
		}).
		state('salePayment', {
				url: '/sale/payment',
				templateUrl: 'modules/sales/views/payment.client.view.html'
	    }).
		state('viewSaleCommission', {
			url: '/view-sale-comission',
			templateUrl: 'modules/sales/views/view-commission.client.view.html'
		});
	}]
);

'use strict';

// Sales controller
angular.module('sales').controller('ComissionController',
    ["$scope", "$state", "$stateParams", "$location", "$http", "SalesCache", "$upload", "$window", "Authentication", "Notifier", "Utility", "Authorization", function ($scope, $state, $stateParams, $location, $http, SalesCache,
              $upload, $window, Authentication, Notifier, Utility, Authorization) {
        $scope.authentication = Authentication;
        $scope.auth = Authorization;

        $scope.uploadFile = function ($files) {
            if (!$files || !$files[0] || $files[0].type !== 'text/csv') {
                Notifier.error('Invalid CSV File', 'Failed!');
            } else {

                $scope.upload = $upload.upload({
                    url: Utility.apiUrl + '/sales/report',
                    method: 'POST',
                    withCredentials: true,
                    file: $files[0]
                })
                    .progress(function (evt) {
                        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                    })
                    .success(function () {
                        $('#load').css('display', 'none');
                        $scope.success = true;
                        SalesCache.removeCache();
                        Notifier.notify('Kindly waiting until the import process completed...', 'Data Successfully Uploaded!');
                        $window.location.href = '/acc/sale/import-report';
                    })
                    .error(function (response) {
                        $('#load').css('display', 'none');
                        Notifier.error(response.message, 'Failed!');
                    });
                $('#load').css('display', 'block');
            }
        };

        $scope.statusClass = function(status) {
            var label = 'label label-warning';
            if(status === 'Active') {
                label = 'label label-success';
            }
            return label;
        };

        $scope.approveCommission = function(albumId, share, action) {
            $http.post(Utility.apiUrl + '/albums/status-comission', {
                albumId: albumId,
                share: share,
                action: action
            }).success(function (response) {
                $scope.findSaleAlbum();
            }).error(function(err) {
                $scope.findSaleAlbum();
            });
        };
    }]
);

'use strict';

// Sales controller
angular.module('sales').controller('PaymentController',
    ["$scope", "$location", "$window", "SalesCache", "Notifier", "Authentication", "Authorization", function ($scope, $location, $window, SalesCache, Notifier, Authentication, Authorization) {
        $scope.authentication = Authentication;
        $scope.auth = Authorization;
        if (! $scope.authentication.user) {
            $location.path('/signin');
            return;
        }

        $scope.tableHeader = [
            {name: 'DATE', sort: 'sorting_asc', schema: 'sale_date'},
            {name: 'ALBUM', sort: 'sorting', schema: 'album_name'},
            {name: 'USER', sort: 'sorting', schema: 'user_name'},
            {name: 'ROLE', sort: 'sorting', schema: 'role'},
            {name: 'EARN', sort: 'sorting', schema: 'commission'},
            {name: 'STATUS', sort: 'sorting', schema: 'status'}
        ];
        $scope.totalPages = 1;
        $scope.currentPage = 1;
        $scope.showEntries = [10, 25, 50, 100];
        $scope.numEntryPerPage = 10;
        $scope.sortedSchema = 'sale_date';
        $scope.isAscending = 1;
        $scope.isLoading = true;
        $scope.paymentList = [];
        $scope.albumIDByName = [];
        $scope.dateRangeSel = [];
        $scope.paymentStatus = {};
        $scope.totalEarning = 0;
        $scope.totalUnpaid = 0;

        $scope.range=function(min, max, total) {
            min = $window.Math.max(min, 1);
            max = $window.Math.min(max, total);

            for(var input=[],i=min; max>=i;i++) {
                input.push(i);
            }
            return input;
        };

        $scope.setTotalPages = function(total) {
            $scope.totalPages = parseInt(total / $scope.numEntryPerPage);
            if ($scope.totalPages * $scope.numEntryPerPage !== total) {
                $scope.totalPages += 1;
            }
        };

        Array.prototype.sum = function (prop) {
            var total = 0;
            for ( var i = 0, _len = this.length; i < _len; i++ ) {
                total += this[i][prop];
            }
            return total;
        };

        $scope.loadPageDetail = function (pageNumber) {
            pageNumber = $window.Math.max(pageNumber, 1);
            $scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
            $scope.loadUserEarningCommission();
        };

        $scope.updateEntry = function () {
            //console.log('Showing ' + $scope.numEntryPerPage + ' entries');
            // reload data
            $scope.loadUserEarningCommission();
        };

        $scope.searchEnter = function (keyEvent) {
            if (keyEvent.which === 13) {
                //Enter is pressed
                $scope.search();
            }
        };

        $scope.search = function () {
            if ($scope.isLoading || !$scope.inputSearchText || $scope.inputSearchText && $scope.inputSearchText.trim() === '') {
                return;
            }
            console.log('query with searchText = ' + $scope.inputSearchText);
            // reload data
            $scope.loadUserEarningCommission();
        };

        var toggleSortClass = function (sortClass) {
            switch (sortClass) {
                case 'sorting':
                    return 'sorting_asc';
                case 'sorting_asc':
                    return 'sorting_desc';
                case 'sorting_desc':
                    return 'sorting_asc';
                default:
                    return 'sorting';
            }
        };

        var checkIfAscending = function (className) {
            if (className === 'sorting_asc') {
                return 1;
            } else {
                return -1;
            }
        };

        $scope.sortByHeader = function (index) {

            if ($scope.isLoading) {return;}

            console.log('sorting this column ' + index);

            // toggle the sort class at current column `index`
            var newClassName = toggleSortClass($scope.tableHeader[index].sort);
            $scope.tableHeader[index].sort = newClassName;

            // reset other column to 'sorting'
            for (var i = 0; i < $scope.tableHeader.length; i++) {
                if (i === index) { continue; }
                $scope.tableHeader[i].sort = 'sorting';
            }

            // store the sorted state in global variable
            $scope.sortedSchema = $scope.tableHeader[index].schema;
            $scope.isAscending = checkIfAscending(newClassName);

            // reload data
            $scope.loadUserEarningCommission();
        };

        $scope.initLoad = function() {
            SalesCache.getAlbumTrackShareInfo().then(function (response) {
                $scope.listAlbum = response.data;
                $scope.dateRangeSel = response.saleDate;
                $scope.setAlbum();
            });
        };

        $scope.setAlbum = function() {
            $('#s2id_user').find('.select2-chosen').html('All Users');
            $scope.selectedUser = '';
            $scope.setUser();
        };

        $scope.getPaymentClass = function(status) {
            if(status) {
                return 'btn btn-success';
            }
            return 'btn btn-danger';
        };

        $scope.getPaymentButton = function(status) {
            if(status) {
                return 'Paid';
            }
            return 'UnPaid';
        };

        $scope.setUser = function() {
            $('#s2id_role').find('.select2-chosen').html('All Roles');
            $scope.selectedRole = '';
            $scope.setRole();
        };

        $scope.setRole = function () {
            $scope.loadUserEarningCommission();
        };

        $scope.getParameters = function () {
            var userId = '', albumName = '', roleId = '';

            if($scope.selectedRole && $scope.selectedRole !== '') {
                roleId = $scope.selectedRole.role;
            }

            if ($scope.selectedUser && $scope.selectedUser !== '') {
                userId = $scope.selectedUser.userId;
            }

            if($scope.selectedAlbum && $scope.selectedAlbum !== '') {
                albumName = $scope.selectedAlbum.albumName;
            }

            var params = {
                albumName: albumName,
                userId: userId,
                role: roleId
            };

            params.page = $scope.currentPage;
            params.numPerPage = $scope.numEntryPerPage;
            params.sort = $scope.sortedSchema;
            params.isAscending = $scope.isAscending;
            params.startDate = $scope.startDate;
            params.endDate = $scope.endDate;

            if (typeof $scope.inputSearchText !== 'undefined' && $scope.inputSearchText !== '') {
                params.searchText = $scope.inputSearchText;
            }

            return params;
        };

        $scope.loadUserEarningCommission = function() {
            var params = $scope.getParameters();
            SalesCache.loadUserEarningCommission(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.paymentList = response.data;
                $scope.dateRangeSel = response.dateRangeSel;
                $scope.totalEarning = response.totalEarning;
                $scope.totalUnpaid = response.totalUnpaid;
                $scope.isLoading = false;
            });
        };

        $scope.submitPayment = function(index) {
            var body = [];
            if(index === parseInt(index)) {
                body.push({
                    date: new Date($scope.paymentList[index].sale_date).getTime(),
                    albumName: $scope.paymentList[index].album_name,
                    userId: $scope.paymentList[index].user_id,
                    role: $scope.paymentList[index].role,
                    status: ! $scope.paymentList[index].status
                });
            } else {
                if(index === 'true') {
                    index = true;
                } else {
                    index = false;
                }

                body = {
                    albumName: '',
                    userId: '',
                    role: '',
                    status: index
                };

                if($scope.startDate) {
                    body.startDate = new Date($scope.startDate).getTime();
                }
                if($scope.endDate) {
                    body.endDate = new Date($scope.endDate).getTime();
                }
                if($scope.selectedAlbum && $scope.selectedAlbum.albumName) {
                    body.albumName = $scope.selectedAlbum.albumName;
                }
                if($scope.selectedUser.userId) {
                    body.userId = $scope.selectedUser.userId;
                }
                if($scope.selectedRole.role) {
                    body.role = $scope.selectedRole.role;
                }
                body = [body];
            }

            SalesCache.createOrUpdatePayment(body).then(function () {
                if(index === parseInt(index)) {
                    $scope.paymentList[index].status = !$scope.paymentList[index].status;
                    if(! $scope.paymentList[index].status) {
                        $scope.totalUnpaid += $scope.paymentList[index].commission;
                    } else {
                        $scope.totalUnpaid -= $scope.paymentList[index].commission;
                    }
                } else {
                    for(var idx in $scope.paymentList) {
                        $scope.paymentList[idx].status = index;
                    }
                    if(index) {
                        $scope.totalUnpaid = 0;
                    } else {
                        $scope.totalUnpaid = $scope.totalEarning;
                    }
                }
                Notifier.notify('Payment Updated', 'Success!');
            });
        };
    }]
);

'use strict';

// Sales controller
angular.module('sales').controller('SalesController',
    ["$scope", "$location", "$window", "Authentication", "SalesCache", "Authorization", "$filter", function ($scope, $location, $window, Authentication,
              SalesCache, Authorization, $filter) {

        $scope.authentication = Authentication;
        if (! $scope.authentication.user) {
            $location.path('/signin');
            return;
        }

        $scope.auth = Authorization;
        $scope.totalPages = 1;
        $scope.currentPage = 1;
        $scope.listAlbum = [];
        $scope.saleCommission = [];
        $scope.selectedTabDisplay = 'tabChartSummary';
        $scope.selectedAlbum = '';
        $scope.selectedUser = '';
        $scope.selectedRole = '';
        $scope.tableHeader = [];
        $scope.earningSummary = {commission: 0, paid: 0, balance: 0, DA: 0, DT: 0, S: 0};
        $scope.isfirstLoad = true;
        $scope.isLoadedChart = false;
        $scope.userNameByID = [];
        $scope.dateRangeStart = [];
        $scope.dateRangeEnd = [];

        $scope.detailHeader = [
            {name: 'ALBUM', sort: 'sorting', schema: 'album_name'},
            {name: 'TRACK', sort: 'sorting', schema: 'track_name'},
            {name: 'PARTNER', sort: 'sorting', schema: 'partner'},
            {name: 'TYPE', sort: 'sorting', schema: 'type'},
            {name: 'QUANTITY', sort: 'sorting', schema: 'quantity'},
            {name: 'PRICE', sort: 'sorting', schema: 'unit'},
            {name: 'SUBTOTAL', sort: 'sorting', schema: 'profit'},
            {name: 'COMISSION', sort: 'sorting', schema: 'allCommissions.detail.value'},
            {name: 'DATE', sort: 'sorting', schema: 'sale_date'}
        ];

        $scope.trackHeader = [
            {name: 'ALBUM', sort: 'sorting', schema: 'album_name'},
            {name: 'TRACK', sort: 'sorting', schema: 'track_name'},
            {name: 'TYPE', sort: 'sorting', schema: 'type'},
            {name: 'QUANTITY', sort: 'sorting', schema: 'quantity'},
            {name: 'SUBTOTAL', sort: 'sorting', schema: 'profit'},
            {name: 'COMISSION', sort: 'sorting', schema: 'commission'}
        ];

        $scope.tableArtistHeader = [
            {name: 'Artist', sort: 'sorting', schema: 'name'},
            {name: 'ROLE', sort: 'sorting', schema: 'role'},
            {name: 'DA', sort: 'sorting', schema: 'da_quantity'},
            {name: '$DA', sort: 'sorting', schema: 'da_profit'},
            {name: 'DT', sort: 'sorting', schema: 'dt_quantity'},
            {name: '$DT', sort: 'sorting', schema: 'dt_profit'},
            {name: 'S', sort: 'sorting', schema: 's_quantity'},
            {name: '$S', sort: 'sorting', schema: 's_profit'},
            {name: 'SUBTOTAL', sort: 'sorting', schema: 'profit'},
            {name: 'COMISSION', sort: 'sorting', schema: 'commission'}
        ];

        $scope.getGroupHeader = function() {
            return [
                {name: $scope.selTblCol, sort: 'sorting', schema: 'name'},
                {name: 'DA', sort: 'sorting', schema: 'da_quantity'},
                {name: '$DA', sort: 'sorting', schema: 'da_profit'},
                {name: 'DT', sort: 'sorting', schema: 'dt_quantity'},
                {name: '$DT', sort: 'sorting', schema: 'dt_profit'},
                {name: 'S', sort: 'sorting', schema: 's_quantity'},
                {name: '$S', sort: 'sorting', schema: 's_profit'},
                {name: 'SUBTOTAL', sort: 'sorting', schema: 'profit'},
                {name: 'COMISSION', sort: 'sorting', schema: 'commission'}
            ];
        };

        $scope.sortedSchema = 'sale_date';
        $scope.isAscending = 1;
        $scope.isLoading = true;

        $scope.showEntries = [10, 25, 50, 100];
        $scope.numEntryPerPage = 10;

        $scope.inputSearchText = undefined;

        $scope.initLoad = function() {
            SalesCache.getAlbumTrackShareInfo().then(function (response) {
                $scope.listAlbum = response.data;
                if($scope.listAlbum.length === 1) {
                    $scope.selectedAlbum = $scope.listAlbum[0];
                    $('#s2id_album').find('.select2-chosen').html($scope.selectedAlbum.albumName);
                }
                $scope.listAlbum.forEach(function(album) {
                    album.users.forEach(function(user) {
                        $scope.userNameByID[user.userId] = user.userName;
                    });
                });
                $scope.setAlbum();
            });
        };

        $scope.setAlbum = function() {
            $('#s2id_user').find('.select2-chosen').html('All Users');
            $scope.selectedUser = '';
            if($scope.selectedAlbum && $scope.selectedAlbum.users) {
                if($scope.selectedAlbum.users.length === 1) {
                    $scope.selectedUser = $scope.selectedAlbum.users[0];
                    $('#s2id_user').find('.select2-chosen').html($scope.selectedUser.userName);
                }
            }
            $scope.setUser();
        };

        $scope.setUser = function() {
            $('#s2id_role').find('.select2-chosen').html('All Roles');
            $scope.selectedRole = '';
            if($scope.selectedUser && $scope.selectedUser.roles) {
                if($scope.selectedUser.roles.length === 1) {
                    $scope.selectedRole = $scope.selectedUser.roles[0];
                    $('#s2id_role').find('.select2-chosen').html($scope.selectedRole.role);
                }
            }
            $scope.setRole();
        };

        $scope.setRole = function () {
            $scope.loadCommission();
        };

        $scope.setTabDisplay = function(tabName) {
            $scope.selectedTabDisplay = tabName;
            $scope.totalPages = 1;
            $scope.currentPage = 1;
            $scope.isAscending = 1;
            $scope.numEntryPerPage = 10;
            $scope.inputSearchText = undefined;
            $scope.groupName = undefined;
            $scope.sortedSchema = $scope.groupName;

            switch(tabName) {
                case 'tabDetail':
                    $scope.tableHeader = $scope.detailHeader;
                    $scope.sortedSchema = 'sale_date';
                    break;
                case 'tabTrack':
                    $scope.tableHeader = $scope.trackHeader;
                    $scope.sortedSchema = 'track_name';
                    break;
                case 'tabLabel':
                    $scope.selTblCol = 'Label';
                    $scope.groupName = 'label';
                    $scope.tableHeader = $scope.getGroupHeader();
                    break;
                case 'tabArtist':
                    break;
                case 'tabAlbum':
                    $scope.selTblCol = 'Album';
                    $scope.groupName = 'album_name';
                    $scope.tableHeader = $scope.getGroupHeader();
                    break;
                case 'tabStore':
                    $scope.selTblCol = 'Store';
                    $scope.groupName = 'partner';
                    $scope.tableHeader = $scope.getGroupHeader();
                    break;

                case 'tabChartSummary':
                    break;
            }
            $scope.loadCommission();
        };

        $scope.loadPageDetail = function (pageNumber) {
            pageNumber = $window.Math.max(pageNumber, 1);
            $scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
            $scope.loadCommission();
        };

        var toggleSortClass = function (sortClass) {
            switch (sortClass) {
                case 'sorting':
                    return 'sorting_asc';
                case 'sorting_asc':
                    return 'sorting_desc';
                case 'sorting_desc':
                    return 'sorting_asc';
                default:
                    return 'sorting';
            }
        };

        var checkIfAscending = function (className) {
            if (className === 'sorting_asc') {
                return 1;
            } else {
                return -1;
            }
        };

        $scope.sortByHeader = function (index) {

            if ($scope.isLoading) {return;}

            console.log('sorting this column ' + index);

            // toggle the sort class at current column `index`
            var newClassName = toggleSortClass($scope.tableHeader[index].sort);
            $scope.tableHeader[index].sort = newClassName;

            // reset other column to 'sorting'
            for (var i = 0; i < $scope.tableHeader.length; i++) {
                if (i === index) { continue; }
                $scope.tableHeader[i].sort = 'sorting';
            }

            // store the sorted state in global variable
            $scope.sortedSchema = $scope.tableHeader[index].schema;
            $scope.isAscending = checkIfAscending(newClassName);

            // reload data
            $scope.loadCommission();
        };

        $scope.updateEntry = function () {
            //console.log('Showing ' + $scope.numEntryPerPage + ' entries');
            // reload data
            $scope.loadCommission();
        };

        $scope.searchEnter = function (keyEvent) {
            if (keyEvent.which === 13) {
                //Enter is pressed
                $scope.search();
            }
        };

        $scope.search = function () {
            if ($scope.isLoading || !$scope.inputSearchText || $scope.inputSearchText && $scope.inputSearchText.trim() === '') {
                return;
            }
            console.log('query with searchText = ' + $scope.inputSearchText);
            // reload data
            $scope.loadCommission();
        };

        $scope.range=function(min, max, total) {
            min = $window.Math.max(min, 1);
            max = $window.Math.min(max, total);

            for(var input=[],i=min; max>=i;i++) {
                input.push(i);
            }
            return input;
        };

        $scope.setTotalPages = function(total) {
            $scope.totalPages = parseInt(total / $scope.numEntryPerPage);
            if ($scope.totalPages * $scope.numEntryPerPage !== total) {
                $scope.totalPages += 1;
            }
        };

        Array.prototype.sum = function (prop) {
            var total = 0;
            for ( var i = 0, _len = this.length; i < _len; i++ ) {
                total += this[i][prop];
            }
            return total;
        };

        $scope.getParameters = function () {
            var userId = '', albumName = '', roleId = '';
            if ($scope.selectedUser && $scope.selectedUser !== '') {
                userId = $scope.selectedUser.userId;
            }

            if($scope.selectedAlbum && $scope.selectedAlbum !== '') {
                albumName = $scope.selectedAlbum.albumName;
            }

            if($scope.selectedRole && $scope.selectedRole !== '') {
                roleId = $scope.selectedRole.role;
            }

            var params = {
                albumName: albumName,
                userId: userId,
                role: roleId,
                startDate: $scope.startDate,
                endDate: $scope.endDate
            };

            if($scope.selectedTabDisplay !== 'tabChartSummary') {
                params.page = $scope.currentPage;
                params.numPerPage = $scope.numEntryPerPage;
                params.sort = $scope.sortedSchema;
                params.isAscending = $scope.isAscending;

                if($scope.groupName) {
                    params.groupName = $scope.groupName;
                }

                if (typeof $scope.inputSearchText !== 'undefined' && $scope.inputSearchText !== '') {
                    params.searchText = $scope.inputSearchText;
                }
            }

            return params;
        };

        $scope.loadCommission = function() {
            if($scope.listAlbum.length > 0) {

                $scope.saleCommission = [];
                $scope.isLoading = true;

                switch ($scope.selectedTabDisplay) {
                    case 'tabDetail':
                        $scope.loadDetailsCommission();
                        break;
                    case 'tabTrack':
                        $scope.loadCommissionGroupByTrack();
                        break;
                    case 'tabArtist':
                        $scope.loadCommissionGroupByArtist();
                        break;
                    case 'tabLabel':
                    case 'tabAlbum':
                    case 'tabStore':
                        $scope.loadCommissionByGroup();
                        break;
                    case 'tabChartSummary':
                        if ($scope.isfirstLoad || $scope.isLoadedChart) {
                            $scope.loadChartSummary();
                        } else {
                            $scope.loadCommissionSummary();
                        }
                        break;
                }
            }
        };

        $scope.assignDateRange = function(min, max) {
            var start = new Date(min);
            var end = new Date(max);

            $scope.dateRangeStart.forEach(function(item, idx) {
                var dte = new Date(item.value);
                if(dte.getFullYear() === start.getFullYear() && dte.getMonth() === start.getMonth() || $scope.dateRangeStart.length === 1) {
                    if(start.getDate() > 2 && idx + 1 < $scope.dateRangeStart.length) {
                        $scope.startDate = $scope.dateRangeStart[idx + 1].value;
                    } else {
                        $scope.startDate = item.value;
                    }
                }
            });

            $scope.dateRangeEnd.forEach(function(item) {
                var dte = new Date(item.value);
                if(dte.getFullYear() === end.getFullYear() && dte.getMonth() === end.getMonth() || $scope.dateRangeEnd.length === 1) {
                    $scope.endDate = item.value;
                }
            });

            start = $filter('date')(new Date($scope.startDate), 'MMM - yyyy');
            end = $filter('date')(new Date($scope.endDate), 'MMM - yyyy');
            $('#s2id_startDate').find('.select2-chosen').html(start);
            $('#s2id_endDate').find('.select2-chosen').html(end);
        };

        $scope.loadChartSummary = function() {
            var params = $scope.getParameters();
            SalesCache.loadChartSummary(params).then(function (response) {
                var data = [];
                for(var i = 0; i < response.data.length; i++) {
                    var date = new Date(response.data[i]._id.sale_date);
                    date = new Date(date.getFullYear(), date.getMonth(), 2);
                    if(i + 1 === response.data.length) {
                        var int_d = new Date(date.getFullYear(), date.getMonth() + 1, 1);
                        date = new Date(int_d - 1);
                    }
                    data.push([date.getTime(), response.data[i].commission]);
                    if($scope.isfirstLoad) {
                        date = new Date(date.getFullYear(), date.getMonth(), 2);
                        $scope.dateRangeStart.push({
                            value: date.getTime(),
                            text: date
                        });

                        var int_dd = new Date(date.getFullYear(), date.getMonth() + 1, 1);
                        date = new Date(int_dd - 1);
                        $scope.dateRangeEnd.push({
                            value: date.getTime(),
                            text: date
                        });
                    }
                }
                $scope.isLoading = false;

                var chartOpt = {
                    colors: ['#689f38'],
                    title : {text : 'Monthly Earning'},
                    series : [{
                        name: 'Earning',
                        data : data,
                        marker : {enabled : true, radius : 3},
                        tooltip: {valueDecimals: 2}
                    }],
                    xAxis: {
                        events: {
                            setExtremes: function (e) {
                                if(e.trigger === 'rangeSelectorButton' || e.DOMEvent && e.DOMEvent.type === 'mouseup') {
                                    $scope.assignDateRange(e.min, e.max);
                                    $scope.isLoadedChart = false;
                                    $scope.loadCommission();
                                }
                            }
                        },
                        minRange: 30 * 24 * 3600
                    },
                    navigator: {
                        maskInside: false,
                        series: {
                            color: '#ffffff',
                            lineColor: '#689f38'
                        }
                    }
                };
                if($scope.isfirstLoad) {
                    chartOpt.rangeSelector = {selected : 1};
                }


                $('#roll14').highcharts('StockChart', chartOpt, function(e) {
                    $scope.assignDateRange(e.xAxis[0].min, e.xAxis[0].max);
                    if($scope.isfirstLoad || $scope.isLoadedChart) {
                        $scope.isfirstLoad = false;
                        $scope.isLoadedChart = false;
                        $scope.loadCommission();
                    }
                });
            });
        };

        $scope.loadCommissionSummary = function() {
            var params = $scope.getParameters();
            return SalesCache.loadCommissionSummary(params).then(function (response) {
                $scope.earningSummary = response.data;
                $scope.isLoading = false;
                if($scope.isLoadedChart) {
                    $scope.loadChartSummary();
                }

                $scope.isLoadedChart = true;
            });
        };

        $scope.loadCommissionGroupByArtist = function() {
            var params = $scope.getParameters();
            return SalesCache.loadCommissionGroupByArtist(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.saleCommission = response.data;
                $scope.isLoading = false;
            });
        };

        $scope.loadCommissionByGroup = function() {
            var params = $scope.getParameters();
            return SalesCache.loadCommissionByGroup(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.saleCommission = response.data;
                $scope.isLoading = false;
            });
        };

        $scope.loadCommissionGroupByTrack = function() {
            var params = $scope.getParameters();
            return SalesCache.loadCommissionGroupByTrack(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.saleCommission = response.data;
                $scope.isLoading = false;
            });
        };

        $scope.isLink = function(item) {
            if(item.dms && item.dms.indexOf('http') >= 0) {
                return true;
            }

            return false;
        };

        $scope.loadDetailsCommission = function () {

            var params = $scope.getParameters();
            return SalesCache.loadSaleCommission(params).then(function (response) {
                $scope.setTotalPages(response.total);
                //console.log('raw data size = ' + data.length);
                $scope.saleCommission = response.data;
                //console.log('Total Page = ' +  $scope.totalPages);
                $scope.isLoading = false;
                return 0;
            });
        };
    }]
);

'use strict';

// Sales controller
var SalesModule = angular.module('sales');

SalesModule.controller('TrackSharesController',
    ["$scope", "$location", "$http", "$state", "$stateParams", "$modal", "$window", "SalesCache", "Authentication", "Notifier", "TrackShareService", "Authorization", "Utility", "$rootScope", function ($scope, $location, $http, $state, $stateParams, $modal, $window, SalesCache,
              Authentication, Notifier, TrackShareService, Authorization, Utility, $rootScope) {
        $scope.authentication = Authentication;
        $scope.auth = Authorization;
        if (! $scope.authentication.user) {
            $location.path('/signin');
            return;
        }

        // data for all track share
        $scope.allTrackShare = [];
        $scope.currentUser = {};

        // data for adding track share
        $scope.listAlbum = [];
        $scope.selectedAlbum = {};
        $scope.listTrack = [];
        $scope.selectedTrack = {};
        $scope.listUser = [];
        $scope.filterUser = [];
        $scope.selectedUser = {};
        $scope.listRole = Utility.albumRoles;
        $scope.selectedRole = {};
        $scope.selectedCommission = {};
        $scope.filterAlbum = '';
        $scope.numEntryPerPage = 30;
        $scope.showEntries = [30, 50, 100];
        $scope.totalPages = 1;
        $scope.currentPage = 1;

        // variable used for Modal
        $scope.modalInstance = undefined;
        $scope.parentSelected = undefined;
        $scope.selectedTrackShare = undefined;

        // variable control the sort and filter table
        $scope.sortAlbum = 'albumName';
        $scope.sortTrack = 'trackName';
        $scope.searchText = '';

        // variable used for Delete Modal
        $scope.deleteModalInstance = undefined;
        $scope.deletedShare = undefined;
        $scope.deletedIndexShareWith = undefined;
        $scope.deletedIndexItemModal = undefined;
        $scope.isMultipleRemoval = undefined;

        // variable used for Invite Modal
        $scope.inviteModalInstance = undefined;
        $scope.inviteEmails = {};

        // disable some element in UI when loading from server;
        $scope.isLoading = false;

        $scope.checkAll = undefined;
        $scope.expandedAll = undefined;

        $scope.isAdmin = function () {
            return $scope.auth.isAuthorized('admin');
        };

        $scope.initLoad = function() {
            TrackShareService.getAllAlbumUsers({}).then(function (response) {
                $scope.filterUser = response.data;
                if($scope.filterUser.length === 1) {
                    $scope.currentUser = $scope.filterUser[0];
                    $('#s2id_user').find('.select2-chosen').html($scope.currentUser.displayName);
                }

                if($scope.currentUser.albums && $scope.currentUser.albums.length === 1) {
                    $scope.filterAlbum = $scope.currentUser.albums[0];
                    $('#s2id_album').find('.select2-chosen').html($scope.filterAlbum.name);
                }

                $scope.currentUser = $scope.filterUser.filter(function (userWithAlbum) {
                    return userWithAlbum._id === Authentication.user._id;
                })[0];

                $scope.setUser();
            });

            TrackShareService.getAlbumInfo({}).then(function (response) {
                $scope.listAlbum = response.data;
            });

            TrackShareService.getAllCollaborators().then(function (response) {
                if (response.ret_code === 0) {
                    $scope.listUser = response.data;
                } else {
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    $scope.listUser = [];
                }

            });
        };

        $scope.setUser = function() {
            $('#s2id_album').find('.select2-chosen').html('All Albums');
            $scope.filterAlbum = '';
            if($scope.filterUser.length === 1) {
                $scope.currentUser = $scope.filterUser[0];
                $('#s2id_user').find('.select2-chosen').html($scope.currentUser.displayName);
            }

            if($scope.currentUser && $scope.currentUser.albums && $scope.currentUser.albums.length === 1) {
                $scope.filterAlbum = $scope.currentUser.albums[0];
                $('#s2id_album').find('.select2-chosen').html($scope.filterAlbum.name);
            }
            $scope.loadTrackShare();
        };

        $scope.range=function(min, max, total) {
            min = $window.Math.max(min, 1);
            max = $window.Math.min(max, total);

            for(var input=[],i=min; max>=i;i++) {
                input.push(i);
            }
            return input;
        };

        $scope.updateEntry = function () {
            if ($rootScope.debug) {
                console.log('Showing ' + $scope.numEntryPerPage + ' entries');
            }
            // reload data
            $scope.loadTrackShare();
        };

        $scope.setTotalPages = function(total) {
            $scope.totalPages = parseInt(total / $scope.numEntryPerPage);
            if ($scope.totalPages * $scope.numEntryPerPage !== total) {
                $scope.totalPages += 1;
            }
        };

        $scope.loadPageDetail = function (pageNumber) {
            pageNumber = $window.Math.max(pageNumber, 1);
            $scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
            $scope.loadTrackShare();
        };

        $scope.loadTrackShare = function () {
            $scope.allTrackShare = [];
            var sentUserId;
            if ($scope.currentUser && $scope.currentUser._id) {
                sentUserId = $scope.currentUser._id;
            }

            $scope.isLoading = true;
            if ($rootScope.debug) {
                console.log('Current user: ' + $scope.currentUser);
            }

            var sentAlbumId = ($scope.filterAlbum && $scope.filterAlbum._id) || undefined;
            var params = {userId: sentUserId, albumId: sentAlbumId};
            params.numPerPage = $scope.numEntryPerPage;
            params.page = $scope.currentPage;

            TrackShareService.loadTrackShares(params).then(function (response) {
                // reset the data
                $scope.allTrackShare = response.data;
                $scope.setTotalPages(response.total);
                if ($rootScope.debug) {
                    console.log('Result size of allTrackShare = ' + $scope.allTrackShare.length);
                }
                $scope.isLoading = false;
            });
        };

        $scope.setAlbum = function () {
            if (!$scope.selectedAlbum) {return;}
            var sorted = $scope.selectedAlbum.trackList.sort(function (track1, track2) {
                return track1.trackName.localeCompare(track2.trackName);
            });
            $scope.listTrack = [{trackName: 'All', trackId: ''}];
            $scope.listTrack = $scope.listTrack.concat(sorted);
        };

        $scope.addTrackShare = function () {
            var value = parseInt($scope.selectedCommission.value);
            if (invalidCommission(value)) {
                Notifier.error('Commission must be a number from 0 to 100', 'Failed!');
                return;
            }
            var addTracks = [];
            if ($scope.selectedTrack.trackName === 'All') {
                addTracks = $scope.selectedAlbum.trackList;
            } else {
                addTracks.push($scope.selectedTrack);
            }
            var listNewData = addTracks.map(function (track) {
                return {
                    albumId: $scope.selectedAlbum,
                    albumName: $scope.selectedAlbum.name,
                    trackId: track.trackId,
                    trackName: track.trackName,
                    userId: $scope.selectedUser.value,
                    userName: $scope.selectedUser.value.displayName,
                    role: $scope.selectedRole.value,
                    commission: value,
                    shareWith: []
                };
            });

            $scope.isLoading = true;
            var params = {
                album: $scope.selectedAlbum,
                addTracks: addTracks,
                userId: $scope.selectedUser.value,
                role: $scope.selectedRole.value,
                commission: $scope.selectedCommission.value
            };
            TrackShareService.addTrackShares(params).then(function (response) {
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    Notifier.notify('Add new track share successfully', 'Success!');
                    //$scope.allTrackShare = $scope.allTrackShare.concat(listNewData);
                    $scope.allTrackShare = listNewData;

                    $scope.addToFilter();
                    $scope.setTotalPages(listNewData.length);
                    $('#s2id_user').find('.select2-chosen').html($scope.currentUser.displayName);
                    $('#s2id_album').find('.select2-chosen').html($scope.filterAlbum.name);
                } else {
                    Notifier.error('Cannot add new track share because of ' + response.msg, 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        $scope.addToFilter = function(isCollaborator) {
            var isInfilterUser = false, isInFilterAlbum = false;
            var selUser = {}, selAlbum = {};
            $scope.filterUser.forEach(function(filterUser) {
                if(filterUser._id === $scope.selectedUser.value._id) {
                    isInfilterUser = true;
                    selUser = filterUser;
                    filterUser.albums.forEach(function(filterAlbum) {
                        if(filterAlbum._id === $scope.selectedAlbum._id) {
                            isInFilterAlbum = true;
                            selAlbum = filterAlbum;
                        }
                    });

                    if(! isInFilterAlbum) {
                        selAlbum = {
                            _id: $scope.selectedAlbum._id,
                            name: $scope.selectedAlbum.name
                        };
                        filterUser.albums.push(selAlbum);
                    }
                }
            });

            if(! isInfilterUser) {
                selAlbum = {
                    _id: $scope.selectedAlbum._id,
                    name: $scope.selectedAlbum.name
                };
                selUser = {
                    _id: $scope.selectedUser.value._id,
                    displayName: $scope.selectedUser.value.displayName,
                    albums: [selAlbum]
                };
                $scope.filterUser.push(selUser);
            }

            if(! isCollaborator) {
                $scope.currentUser = selUser;
                $scope.filterAlbum = selAlbum;
            }
        };

        $scope.showExpandedAll = function () {
            var sharesContainCollaborator = $scope.allTrackShare.filter(function (share) {
                return share.shareWith.length > 0;
            });
            return sharesContainCollaborator.length > 0;
        };

        $scope.toggleFieldAll = function () {

            $scope.expandedAll = !$scope.expandedAll;
            $scope.allTrackShare.forEach(function (share) {
                if (share.shareWith.length > 0) {
                    share.expanded = $scope.expandedAll;
                }
            });
        };

        $scope.toggleField = function (obj, field) {
            if (!$scope.isLoading) {
                obj[field] = !obj[field];
            }
        };

        $scope.toggleSort = function (field) {
            if (field.startsWith('-')) {
                return field.substring(1);
            } else {
                return '-' + field;
            }
        };

        $scope.isSortReversed = function (field) {
            return field.startsWith('-');
        };

        var escapeRegExp = function (str) {
            return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
        };

        var matchAllRegex = function (listRegex, matchingString) {
            return listRegex.every(function (regex) {
                return matchingString.match(regex);
            });
        };

        $scope.filterFunction = function(trackShare) {
            var multiRegex = $scope.searchText.split(' ').map(function (word) {
                return new RegExp(escapeRegExp(word), 'i');
            });
            var fullText = trackShare.albumName + ' ' + trackShare.trackName + ' ' + trackShare.userName + ' ' +
                           trackShare.role + ' ' + trackShare.commission.toString();
            if (matchAllRegex(multiRegex, fullText)) {
                return true;
            } else {
                // search inside the Collaborator trackshare
                var filter = trackShare.shareWith.filter(function (nestedShare) {
                    var nestedFullText = nestedShare.userName + ' ' +  nestedShare.role + ' ' + nestedShare.commission.toString();
                    return matchAllRegex(multiRegex, nestedFullText);
                });
                return filter.length > 0;
            }
        };

        $scope.oldEditCommission = undefined;
        $scope.keepOldCommission = function (old) {
            $scope.oldEditCommission = old;
        };

        $scope.editCommission = function (trackShare, field, parentShare) {
            var value = parseInt(trackShare.commission);
            if (isNaN(value) || value > 100 || value < 0) {
                Notifier.error('Commission must be a number from 0 to 100', 'Failed!');
                return;
            }
            if (parentShare) {
                // need to check the total % must not exceed 100
                parentShare.shareWith.forEach(function (share) {share.commission = parseInt(share.commission);});
                var total = parentShare.shareWith.reduce(function (total, share) {return total + share.commission;}, 0);
                if (total > 100) {
                    Notifier.error('Total commission of all Collaborator must be from 0 to 100', 'Failed!');
                    return;
                }
            }
            if (value === 0) {
                if (typeof parentShare === 'undefined') {
                    $scope.deletedShare = trackShare;
                    $scope.deletedIndexShareWith = undefined;
                } else {
                    $scope.deletedShare = parentShare;
                    // find index of child in parent.shareWith
                    var foundIndex = -1;
                    parentShare.shareWith.filter(function (child, index) {
                        if (getObjId(child.userId) === getObjId(trackShare.userId) && child.role === trackShare.role) {
                            foundIndex = index;
                        }
                    });
                    if (foundIndex === -1 && $rootScope.debug) {
                        console.log('NOT FOUND child in parent');
                        return;
                    }
                    $scope.deletedIndexShareWith = foundIndex;
                }
                $scope.deletedIndexItemModal = undefined;
                $scope.isMultipleRemoval = undefined;

                // Open new modal
                $scope.deleteModalInstance = $modal.open({
                    templateUrl: 'modules/sales/views/modal/zero-trackshare-modal.client.view.html',
                    size: 'md',
                    scope: $scope
                });
                $scope.deleteModalInstance.result.then(function () {
                    resetDeleteModal();
                }, function () {
                    trackShare.commission = $scope.oldEditCommission;
                    trackShare[field] = !trackShare[field];
                    resetDeleteModal();
                });
            } else {
                $scope.isLoading = true;
                var params = {
                    album: (trackShare.albumId || parentShare.albumId),
                    trackId: (trackShare.trackId || parentShare.trackId),
                    userId: trackShare.userId,
                    role: trackShare.role,
                    commission: value
                };
                TrackShareService.editCommission(params).then(function (response) {
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    if (response.ret_code === 0) {
                        SalesCache.removeCache();
                        Notifier.notify('Edit commission successfully', 'Success!');
                        trackShare[field] = !trackShare[field];
                    } else {
                        Notifier.error('Cannot save commission', 'Failed!');
                    }
                    $scope.isLoading = false;
                });
            }
        };

        $scope.disableEditCollaborator = function () {
            var selected = $scope.allTrackShare.filter(function (share) {return share.selected;});
            return $scope.isLoading || selected.length === 0;
        };

        function getObjId(obj) {
            if (obj && obj._id) {
                return obj._id;
            } else {
                return obj;
            }
        }

        /**
         * Edit Collaborator modal
         */
        $scope.editCollaborator = function () {
            var selected = $scope.allTrackShare.filter(function (share) {return share.selected;});
            var totalShareWith = [];
            for (var i = 0; i < selected.length; i++) {
                for (var j = 0; j < selected[i].shareWith.length; j++) {
                    var share = selected[i];
                    var childShare = selected[i].shareWith[j];
                    // check if this (user,role) exists in the array
                    var found = totalShareWith.filter(function (share) {
                        return getObjId(share.userId) === getObjId(childShare.userId) && share.role === childShare.role;
                    });
                    if (found.length === 0) {
                        totalShareWith.push({
                            userId: childShare.userId,
                            userName: childShare.userName,
                            role: childShare.role,
                            commission: childShare.commission
                        });
                    }
                }
            }

            $scope.parentSelected = selected;
            $scope.selectedTrackShare = totalShareWith;

            // Open new modal
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/sales/views/modal/add-track-share-modal.client.view.html',
                size: 'lg',
                scope: $scope
            });
        };

        $scope.addCollaborator = function () {
            var value = parseInt($scope.selectedCommission.value);
            if (invalidCommission(value)) {
                Notifier.error('Commission must be a number greater than 0 and less or equal to 100', 'Failed!');
                return;
            }
            var checkExists = $scope.selectedTrackShare.filter(function (share) {
                return getObjId(share.userId) === getObjId($scope.selectedUser.value) &&
                       share.role === $scope.selectedRole.value;
            });
            if (checkExists.length > 0) {
                Notifier.error('Duplicate data', 'Failed!');
                return;
            }
            $scope.selectedTrackShare.forEach(function (share) {share.commission = parseInt(share.commission);});
            var total = $scope.selectedTrackShare.reduce(function (total, share) {return total + share.commission;}, 0) + value;
            if (invalidCommission(total)) {
                if ($rootScope.debug) {
                    console.log('Total commission = ' + total);
                }
                Notifier.error('Total commission of all Collaborator must be from 0 to 100', 'Failed!');
                return;
            }
            $scope.selectedTrackShare.push({
                userId: $scope.selectedUser.value,
                userName: $scope.selectedUser.value.displayName,
                role: $scope.selectedRole.value,
                commission: value
            });
            $scope.addToFilter(true);
        };

        function validateEachCollaCommission(commission) {
            var value = parseInt(commission);
            var failed = false;
            var errMsg = '';
            if (!isNaN(value) && value <= 0) {
                errMsg = 'Please remove 0% trackshare';
                failed = true;
            } else if (isNaN(value)) {
                errMsg = 'Please enter a number';
                failed = true;
            }
            return {ok: !failed, msg: errMsg, value: value};
        }

        function invalidCommission(total) {
            return isNaN(total) || total <= 0 || total > 100;
        }

        $scope.validateAllCollaCommission = function (listTrackShare, validateShare) {
            // validate total commission in `listTrackShare` between (0, 100]
            var failed = false;
            var errMsg = '';
            var total = listTrackShare.reduce(function (total, share) {return total + parseInt(share.commission);}, 0);
            if (typeof validateShare !== 'undefined') {
                var result = validateEachCollaCommission(validateShare.commission);
                if (!result.ok) {
                    errMsg = result.msg;
                    failed = true;
                } else if (invalidCommission(total)) {
                    errMsg = 'Total commission of all Collaborator must be from 0 to 100';
                    failed = true;
                }

                if (!failed) {
                    validateShare.commission = result.value;
                }
            } else {
                var existsZeroShare = listTrackShare.filter(function (share) {
                    var value = parseInt(share.commission);
                    return !isNaN(value) && value === 0;
                });
                if (existsZeroShare.length > 0) {
                    errMsg = 'Please remove 0% trackshare';
                    failed = true;
                } else if (invalidCommission(total)) {
                    errMsg = 'Total commission of all Collaborator must be from 0 to 100';
                    failed = true;
                }
            }

            if (failed) {
                Notifier.error(errMsg, 'Failed!');
            }

            return !failed;
        };

        $scope.saveAll = function () {
            if (!$scope.validateAllCollaCommission($scope.selectedTrackShare, undefined)) {
                return;
            }
            $scope.isLoading = true;
            var params = {
                listParent: $scope.parentSelected,
                listCollaborator: $scope.selectedTrackShare
            };
            TrackShareService.addCollaboratorTrackShares(params).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    Notifier.notify('Edit collaborator share successfully', 'Success!');
                    $scope.parentSelected.forEach(function (share) {
                        share.shareWith = JSON.parse(JSON.stringify($scope.selectedTrackShare));
                    });
                    $scope.modalInstance.close('Finish!');
                } else {
                    Notifier.error('Cannot edit', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        $scope.cancel = function () {
            $scope.modalInstance.dismiss('Cancel!');
        };

        /**
         * Delete confirmation modal
         */
        var resetDeleteModal = function () {
            $scope.deleteModalInstance = undefined;
            $scope.deletedShare = undefined;
            $scope.deletedIndexShareWith = undefined;
            $scope.deletedIndexItemModal = undefined;
            $scope.isMultipleRemoval = undefined;
        };

        $scope.openDeleteModal = function (removeTrackShare, removeIndexInShareWith, removeIndexInItemModal, multipleRemove) {
            $scope.deletedShare = removeTrackShare;
            $scope.deletedIndexShareWith = removeIndexInShareWith;
            $scope.deletedIndexItemModal = removeIndexInItemModal;
            $scope.isMultipleRemoval = multipleRemove;
            // Open new modal
            $scope.deleteModalInstance = $modal.open({
                templateUrl: 'modules/sales/views/modal/delete-confirm-modal.client.view.html',
                size: 'md',
                scope: $scope
            });
            $scope.deleteModalInstance.result.then(function () {
                resetDeleteModal();
            }, function () {
                resetDeleteModal();
            });
        };

        var findIndexOfTrackShare = function (listShare, searchShare) {
            var foundIndex = -1;
            for (var i = 0; i < listShare.length; i++) {
                var iteratingShare = listShare[i];
                if (iteratingShare.albumName === searchShare.albumName &&
                    iteratingShare.trackName === searchShare.trackName &&
                    iteratingShare.userName === searchShare.userName &&
                    iteratingShare.role === searchShare.role &&
                    iteratingShare.commission === searchShare.commission) {
                    foundIndex = i;
                    break;
                }
            }
            return foundIndex;
        };

        // remove the main track share
        var removeMainTrackShare = function (trackShare) {
            var removeIndex = findIndexOfTrackShare($scope.allTrackShare, trackShare);
            if (removeIndex !== -1) {
                $scope.isLoading = true;
                TrackShareService.removeTrackShare(trackShare).then(function (response) {
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    if (response.ret_code === 0) {
                        SalesCache.removeCache();
                        Notifier.notify('Remove successfully', 'Success!');
                        $scope.allTrackShare.splice(removeIndex, 1);
                    } else {
                        Notifier.error('Cannot remove', 'Failed!');
                    }
                    $scope.isLoading = false;
                });
            }
        };

        // remove the track share of Collaborator
        var removeCollaboratorTrackShare = function (trackShare, indexInShareWith) {
            $scope.isLoading = true;
            var removeCollaborator = trackShare.shareWith[indexInShareWith];
            var params = {
                userId: removeCollaborator.userId,
                role: removeCollaborator.role,
                listParent: [trackShare]
            };
            TrackShareService.removeCollaboratorTrackShare(params).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    Notifier.notify('Remove successfully', 'Success!');
                    trackShare.shareWith.splice(indexInShareWith, 1);
                } else {
                    Notifier.error('Cannot remove', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        // remove track share in Edit Modal
        var removeItemInModal = function (selectedIndexModal) {
            $scope.isLoading = true;
            var removeCollaborator = $scope.selectedTrackShare[selectedIndexModal];
            var params = {
                userId: removeCollaborator.userId,
                role: removeCollaborator.role,
                listParent: $scope.parentSelected
            };
            TrackShareService.removeCollaboratorTrackShare(params).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    // remove this collaborator in shareWith of all parents
                    $scope.parentSelected.forEach(function (parent) {
                        parent.shareWith = parent.shareWith.filter(function (colla) {
                            return !(getObjId(colla.userId) === getObjId(removeCollaborator.userId) &&
                                    colla.role === removeCollaborator.role);
                        });
                    });

                    Notifier.notify('Remove successfully', 'Success!');
                    $scope.selectedTrackShare.splice(selectedIndexModal, 1);
                } else {
                    Notifier.error('Cannot remove', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        // multiple remove
        var multipleRemove = function () {
            var selectedRemoval = $scope.allTrackShare.filter(function (share) {return share.selected;});
            TrackShareService.removeMultiple(selectedRemoval).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    $scope.allTrackShare = $scope.allTrackShare.filter(function (share) {return !share.selected;});
                    Notifier.notify('Remove successfully', 'Success!');
                } else {
                    Notifier.error('Cannot remove', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        $scope.confirmDelete = function () {
            if (typeof $scope.deletedShare !== 'undefined' && typeof $scope.deletedIndexShareWith === 'undefined') {
                removeMainTrackShare($scope.deletedShare);
            }
            else if (typeof $scope.deletedShare !== 'undefined' && typeof $scope.deletedIndexShareWith !== 'undefined') {
                removeCollaboratorTrackShare($scope.deletedShare, $scope.deletedIndexShareWith);
            } else if (typeof $scope.deletedIndexItemModal !== 'undefined') {
                removeItemInModal($scope.deletedIndexItemModal);
            } else if ($scope.isMultipleRemoval === 'multiple') {
                multipleRemove();
            }
            $scope.deleteModalInstance.close('Yes!');
        };

        $scope.cancelDelete = function () {
            $scope.deleteModalInstance.dismiss('No!');
        };

        var extractField = function (path, obj) {
            return path.split('.').reduce(function (o, p) {
                return o && o[p];
            }, obj);
        };

        var resetUnCheckedAll = function (listShare) {
            listShare.forEach(function (share) {
                share.selected = false;
            });
        };

        $scope.toggleCheckAll = function () {
            resetUnCheckedAll($scope.allTrackShare);
            if (!$scope.checkAll) {
                // if unchecked all
                $scope.searchText = '';
            } else {
                var albumName = extractField('name', $scope.selectedAlbum) || '';
                //var trackName = extractField('trackName', $scope.selectedTrack) || '';
                //var userName = extractField('value.displayName', $scope.selectedUser) || '';
                //var role = extractField('value', $scope.selectedRole) || '';
                //var commission = extractField('value', $scope.selectedCommission) || '';
                //$scope.searchText = (albumName + ' ' + trackName + ' ' + userName + ' ' + role + ' ' + commission).trim();
                $scope.searchText = albumName.trim();
                var filterShare = $scope.allTrackShare;
                if ($scope.searchText !== '') {
                    filterShare = $scope.allTrackShare.filter($scope.filterFunction);
                }
                filterShare.forEach(function (share) {
                    share.selected = true;
                });
            }
        };

        /**
         * Invite Collaborator modal
         */
        $scope.inviteCollaborator = function () {
            // Open new modal
            $scope.inviteModalInstance = $modal.open({
                templateUrl: 'modules/sales/views/modal/track-share-invite-collaborators.client.view.html',
                size: 'md',
                scope: $scope
            });
        };

        $scope.confirmInvite = function () {
            var listEmail = ($scope.inviteEmails.value || '');
            listEmail = listEmail.split(',').map(function (email) {return email.trim();});
            TrackShareService.inviteCollaborator({emails: listEmail}).then(function (response) {
                SalesCache.removeCache();
                if (response.ret_code === 0) {
                    TrackShareService.getAllCollaborators().then(function (response) {
                        if (response.ret_code === 0) {
                            $scope.listUser = response.data;
                        } else {
                            if ($rootScope.debug) {
                                console.log(response);
                            }
                            $scope.listUser = [];
                        }
                        Notifier.notify(response.msg, 'Success!');
                    });
                } else {
                    Notifier.error(response.msg, 'Failed!');
                }
            });
            $scope.inviteModalInstance.close('Yes!');
        };

        $scope.cancelInvite = function () {
            $scope.inviteModalInstance.dismiss('Cancel Invite!');
        };
    }]
);

'use strict';

var SalesModule = angular.module('sales');

SalesModule.directive('headerFilter', function () {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/headerFilter.client.view.html'
	};
});

SalesModule.directive('tablePagination', function () {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/tablePagination.client.view.html'
	};
});

SalesModule.directive('tableSaleGroup', function(){
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/tableSaleGroup.client.view.html'
	};
});

SalesModule.directive('tableYoutubeGroup', function(){
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/tableYoutubeGroup.client.view.html'
	};
});

SalesModule.directive('tabChartSummary', function(){
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/tabChartSummary.client.view.html'
	};
});

SalesModule.directive('paymentInfo', function(){
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/sales/views/layout/paymentInfo.client.view.html'
	};
});

SalesModule.directive('focusMe', ["$timeout", "$parse", function($timeout, $parse) {
	return {
		//scope: true,   // optionally create a child scope
		link: function(scope, element, attrs) {
			var model = $parse(attrs.focusMe);
			scope.$watch(model, function(value) {
				if(value === true) {
					$timeout(function() {
						element[0].focus();
					});
				}
			});
		}
	};
}]);

SalesModule.directive('select2', function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attr, ngModel) {
			//$this becomes element
			element.select2({
				//options removed for clarity
			});
			/*element.on('change', function() {
			 var val = $(this).value;
			 scope.$apply(function(){
			 //will cause the ng-model to be updated.
			 ngModel.$setViewValue(val);
			 });
			 });*/
			/*          ngModel.$render = function() {
			 //if this is called, the model was changed outside of select, and we need to set the value
			 //not sure what the select2 api is, but something like:
			 element.value = ngModel.$viewValue;
			 };*/
		}
	};
});

SalesModule.directive('ngEnter', function() {
	return function(scope, element, attrs) {
		element.bind('keydown keypress', function(event) {
			if(event.which === 13) {
				scope.$apply(function(){
					scope.$eval(attrs.ngEnter, {'event': event});
				});

				event.preventDefault();
			}
		});
	};
});

SalesModule.directive('ngChangeOnBlur', function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elm, attrs, ngModelCtrl) {
			if (attrs.type === 'radio' || attrs.type === 'checkbox')
				return;

			var expressionToCall = attrs.ngChangeOnBlur;

			var oldValue = null;
			elm.bind('focus',function() {
				scope.$apply(function() {
					oldValue = elm.val();
				});
			});
			elm.bind('blur', function() {
				scope.$apply(function() {
					var newValue = elm.val();
					if (newValue.toString() !== oldValue.toString()){
						scope.$eval(expressionToCall);
					}
				});
			});
		}
	};
});

/**
 * Created by steventran on 06/07/2015.
 */

'use strict';

var SalesModule = angular.module('sales');
SalesModule
    .filter('filterUnpaid', function() {
        return function(data, key) {
            if (typeof(data) === 'undefined' || typeof(key) === 'undefined') {
                return 0;
            }

            var sum = 0;
            for (var i = data.length - 1; i >= 0; i--) {
                if(data[i].status === false) {
                    sum += parseFloat(data[i][key]);
                } else if(! data[i].status) {
                    return 0;
                }
            }

            return sum;
        };
    });

/**
 * Created by steventran on 5/17/15.
 */

'use strict';

//Sales service used to communicate Sales REST endpoints
angular.module('sales').factory('SalesCache', ["$http", "$q", "$cacheFactory", "$rootScope", "Utility", function($http, $q, $cacheFactory, $rootScope, Utility) {
        var cache = $cacheFactory('SalesCache');
        var removeCache = function() {
            cache.removeAll();
        };

        return {
            removeCache: removeCache,

            getAlbumTrackShareInfo: function() {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/albumtrackshareinfo', {cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadSaleCommission: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading sale commission with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionGroupByTrack: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading track commission with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission-byTrack', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionGroupByArtist: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading group commission with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission-byArtist', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionByGroup: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading group commission with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission-byGroup', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionSummary: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading summary commission with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission-summary', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadChartSummary: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading chart summary with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-commission-chart', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            createOrUpdatePayment: function (body) {
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/payment', body).success(function (response) {
                    //TrackShareService.removeCache();
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadUserEarningCommission: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading user earning with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/sales-user-earning', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            }
        };
    }]
);

'use strict';

//Sales service used to communicate Sales REST endpoints
angular.module('sales').factory('TrackShareService', ["$http", "$q", "$cacheFactory", "$rootScope", "Utility", function($http, $q, $cacheFactory, $rootScope, Utility) {
        var cache = $cacheFactory('TrackShareService');
        var removeCache = function() {
            cache.removeAll();
        };

        return {
            removeCache: removeCache,

            loadTrackShares: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading track shares with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/track-shares', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getAlbumInfo: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading all album info with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/albuminfo', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getAllAlbumUsers: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading all album users with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/allAlbumUsers', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getAllUser: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading all users info with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/allusers', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getAllCollaborators: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading all collaborators');
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/allcollaborators', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            addTrackShares: function (body) {
                if ($rootScope.debug) {
                    console.log('Add track share with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl +'/track-shares/add', body).success(function (response) {
                    removeCache();
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            addCollaboratorTrackShares: function (body) {
                if ($rootScope.debug) {
                    console.log('Add Collaborator track share with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/track-shares/add-collaborator', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            editCommission: function (body) {
                if ($rootScope.debug) {
                    console.log('Edit track share with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/track-shares/edit', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            removeTrackShare: function (body) {
                if ($rootScope.debug) {
                    console.log('Remove track share with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl +'/track-shares/delete', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            removeCollaboratorTrackShare: function (body) {
                if ($rootScope.debug) {
                    console.log('Remove collaborator track share with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl +'/track-shares/delete-collaborator', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            removeMultiple: function (body) {
                if ($rootScope.debug) {
                    console.log('Remove multiple with params: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/track-shares/delete-multiple', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            inviteCollaborator: function (body) {
                if ($rootScope.debug) {
                    console.log('Invite collaborator: ');
                    console.log(body);
                }
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/track-shares/invite-collaborator', body).success(function (response) {
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            }
        };
    }]
);

'use strict';

// Config HTTP Error Handling
angular.module('users').config(
	["$httpProvider", function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(
			["$q", "$location", "Authentication", function($q, $location, Authentication) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.user = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour 
								break;
						}

						return $q.reject(rejection);
					}
				};
			}]
		);
	}]
);
'use strict';

// Setting up route
angular.module('users').config(
	["$stateProvider", function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('add-user', {
			url: '/settings/add-user',
			templateUrl: 'modules/users/views/settings/add-user.client.view.html'
		}).
		state('edit-user', {
			url: '/settings/edit-user?_id=:',
			templateUrl: 'modules/users/views/settings/edit-user.client.view.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('payment', {
			url: '/settings/payment',
			templateUrl: 'modules/users/views/settings/payment.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		}).
		state('list-user', {
			url: '/user/list-users',
			templateUrl: 'modules/users/views/view-user.client.view.html'
		}).
		state('invite-user', {
			url: '/user/invite-user',
			templateUrl: 'modules/users/views/settings/invite-user.client.view.html'
		});

	}]
);

'use strict';

angular.module('users').controller('AuthenticationController',
	["$scope", "$rootScope", "$http", "$window", "$state", "$location", "Authentication", "Notifier", "Utility", function($scope, $rootScope, $http, $window, $state, $location, Authentication, Notifier, Utility) {
		$rootScope.isLoginPage        = true;
		$rootScope.isMainPage         = false;
		$scope.authentication = Authentication;
		$scope.credentials = {};

		// If user is signed in then redirect back home
		if ($scope.authentication.user) {
			$window.location.href = '/acc';
			return;
		}

		if($state.current.name === 'signup') {
			$http.get(Utility.apiUrl + '/user/invite-user/' + $location.search().token, {params:{
				email: $location.search().email
			}}).success(function(response) {
				$scope.credentials.token = $location.search().token;
				$scope.credentials.email = $location.search().email;
			}).error(function(response) {
				Notifier.error('Unauthorized Request', 'Failed!');
				$state.go('signin');
				return;
			});
		}

		$scope.signup = function() {
			$http.post(Utility.apiUrl + '/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$window.location.href = '/acc';
			}).error(function(response) {
				Notifier.error(response.message, 'Registration Failed!');
			});
		};

		$scope.signin = function() {
			$http.post(Utility.apiUrl + '/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$window.location.href = '/acc';
			}).error(function(response) {
				Notifier.error(response.message, 'Invalid Login!');
			});
		};
	}]
);

'use strict';

angular.module('users').controller('PasswordController',
	["$scope", "$rootScope", "$stateParams", "$http", "$location", "$window", "Authentication", "Notifier", "Utility", function($scope, $rootScope, $stateParams, $http, $location, $window, Authentication, Notifier, Utility) {
		$rootScope.isLoginPage        = true;
		$rootScope.isMainPage         = false;
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/acc');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

			$http.post(Utility.apiUrl + '/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				Notifier.notify(response.message, 'Success!');

			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				Notifier.error(response.message, 'Error!');
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post(Utility.apiUrl + '/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;

				// And redirect to the index page
				$location.path('/password/reset/success');
				Notifier.notify('Password has been sucessfully reset', 'Success!');
			}).error(function(response) {
				Notifier.error(response.message, 'Error!');
			});
		};
	}]
);

angular.module('users').controller('ResetController', ["$scope", "$window", function($scope, $window) {
  $scope.toHomePage = function() {
    $window.location.href = '/acc';
  };
}]);

'use strict';

angular.module('users').controller('SettingsController',
	["$scope", "$http", "$location", "$state", "$stateParams", "$window", "Users", "Authentication", "Notifier", "Authorization", "SalesCache", "TrackShareService", "Utility", function($scope, $http, $location, $state, $stateParams, $window,
			 Users, Authentication, Notifier, Authorization, SalesCache, TrackShareService, Utility) {
		$scope.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/acc');
		$scope.auth = Authorization;

		$scope.tableHeader = [
			{name: 'NAME', sort: 'sorting_asc', schema: 'displayName'},
			{name: 'EMAIL', sort: 'sorting', schema: 'email'},
			{name: 'ROLES', sort: 'sorting', schema: 'roles'},
			{name: 'ACTIONS', sort: 'sorting', schema: 'displayName'}
		];
		$scope.totalPages = 1;
		$scope.currentPage = 1;
		$scope.showEntries = [10, 25, 50, 100];
		$scope.numEntryPerPage = 10;
		$scope.sortedSchema = 'displayName';
		$scope.isAscending = 1;
		$scope.isLoading = true;

		$scope.range=function(min, max, total) {
			min = $window.Math.max(min, 1);
			max = $window.Math.min(max, total);

			for(var input=[],i=min; max>=i;i++) {
				input.push(i);
			}
			return input;
		};

		$scope.setTotalPages = function(total) {
			$scope.totalPages = parseInt(total / $scope.numEntryPerPage);
			if ($scope.totalPages * $scope.numEntryPerPage !== total) {
				$scope.totalPages += 1;
			}
		};

		$scope.loadPageDetail = function (pageNumber) {
			pageNumber = $window.Math.max(pageNumber, 1);
			$scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
			$scope.listUsers();
		};

		$scope.updateEntry = function () {
			//console.log('Showing ' + $scope.numEntryPerPage + ' entries');
			// reload data
			$scope.listUsers();
		};

		$scope.searchEnter = function (keyEvent) {
			if (keyEvent.which === 13) {
				//Enter is pressed
				$scope.search();
			}
		};

		$scope.search = function () {
			if ($scope.isLoading || !$scope.inputSearchText || $scope.inputSearchText && $scope.inputSearchText.trim() === '') {
				return;
			}
			console.log('query with searchText = ' + $scope.inputSearchText);
			// reload data
			$scope.listUsers();
		};

		var toggleSortClass = function (sortClass) {
			switch (sortClass) {
				case 'sorting':
					return 'sorting_asc';
				case 'sorting_asc':
					return 'sorting_desc';
				case 'sorting_desc':
					return 'sorting_asc';
				default:
					return 'sorting';
			}
		};

		var checkIfAscending = function (className) {
			if (className === 'sorting_asc') {
				return 1;
			} else {
				return -1;
			}
		};

		$scope.sortByHeader = function (index) {

			if ($scope.isLoading) {return;}

			console.log('sorting this column ' + index);

			// toggle the sort class at current column `index`
			var newClassName = toggleSortClass($scope.tableHeader[index].sort);
			$scope.tableHeader[index].sort = newClassName;

			// reset other column to 'sorting'
			for (var i = 0; i < $scope.tableHeader.length; i++) {
				if (i === index) { continue; }
				$scope.tableHeader[i].sort = 'sorting';
			}

			// store the sorted state in global variable
			$scope.sortedSchema = $scope.tableHeader[index].schema;
			$scope.isAscending = checkIfAscending(newClassName);

			// reload data
			$scope.listUsers();
		};

		// List all users
		$scope.listUsers = function() {
			var params = {};
			params.page = $scope.currentPage;
			params.numPerPage = $scope.numEntryPerPage;
			params.sort = $scope.sortedSchema;
			params.isAscending = $scope.isAscending;

			if (typeof $scope.inputSearchText !== 'undefined' && $scope.inputSearchText !== '') {
				params.searchText = $scope.inputSearchText;
			}

			Users.get(params, function(response) {
				$scope.isLoading = false;
				$scope.users = response.data;
				$scope.setTotalPages(response.total);
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				var user = new Users($scope.user);

				user.$update(function(response) {
					SalesCache.removeCache();
					TrackShareService.removeCache();
					Notifier.notify('You have successfully updated your profile', 'Profile Updated!');
					Authentication.user = response;
				}, function(response) {
					Notifier.error(response.data.message, 'Profile Update Failed!');
				});
			}
		};

		$scope.addUserProfile = function(isValid) {
			if (isValid) {
				var user = new Users($scope.member);
				user.$save(function() {
					SalesCache.removeCache();
					TrackShareService.removeCache();
					Notifier.notify('New user added', 'Success');
				}, function(response) {
					Notifier.error(response.data.message, 'Failed!');
				});
			}
		};

		$scope.editUserProfile = function(isValid) {
			if (isValid) {
				if($scope.member.cPassword === $scope.member.password) {
					delete $scope.member.cPassword;
					$scope.member.$update(function (response) {
						SalesCache.removeCache();
						TrackShareService.removeCache();
						$scope.member = response;
						if(Authentication.user._id === $scope.member._id) {
							Authentication.user = $scope.member;
						}
						Notifier.notify('User info Updated', 'Success');
					});
				} else {
					Notifier.error('Confirmed Password Mismatched', 'Invalid!');
				}
			}
		};

		$scope.getEditUser = function() {
			$scope.member = Users.get({userId: $stateParams._id});
		};

		$scope.editUserInfo = function(userId) {
			$state.go('edit-user', {'_id': userId});
		};

		// Change user password
		$scope.changeUserPassword = function(isValid) {
			if (isValid) {
				$http.post(Utility.apiUrl + '/users/password', $scope.passwordDetails).success(function(response) {
					// If successful show success message and clear form
					Notifier.notify('You have successfully changed your password', 'Password Changed!');
					$scope.passwordDetails = null;
				}).error(function(response) {
					Notifier.error(response.message, 'Change Password Failed!');
				});
			}
		};
		
		// Set css for role tag
		$scope.setRoleCss = function(role) {
			switch(role) {
				case 'user':
					return 'label label-success';
				case 'admin':
					return 'label label-danger';
				case 'label':
					return 'label label-primary';
				case 'sound engineer':
					return 'label label-warning';
			}
		};
		
		// Open Simple Modal
		$scope.openModal = function(user)
		{	if (user._id !== $scope.user._id) {
				$scope.removeUser = user;
			} else {
				Notifier.error('This is your own account', 'Failed!');
			}
		};

		$scope.confirmRemove = function() {
			if($scope.removeUser) {
				for (var i in $scope.users) {
					if ($scope.users[i] === $scope.removeUser) {
						$scope.users.splice(i, 1);
					}
				}

				$scope.removeUser = new Users($scope.removeUser);
				$scope.removeUser.$remove(function() {
					SalesCache.removeCache();
					TrackShareService.removeCache();
				});
			}
			$scope.removeUser = undefined;
		};

		$scope.getUserPayment = function() {
			$scope.payment = $scope.user.payment;
		};

		$scope.changePayment = function(isValid) {
			if (isValid) {
				$scope.payment.channel = 'paypal';
				$scope.user.payment = $scope.payment;
				var user = new Users($scope.user);

				user.$update(function(response) {
					Notifier.notify('Your payment has been updated', 'Success!');
					Authentication.user = response;
				}, function(response) {
					Notifier.error(response.data.message, 'Payment Update Failed!');
				});
			}
		};

		$scope.inviteUser = function(isValid) {
			if(isValid) {
				$http.post(Utility.apiUrl + '/users/invite-email', {email: $scope.inviteEmail}).success(function(response) {
					// If successful show success message and clear form
					$('#load').css('display', 'none');
					Notifier.notify(response.message, 'Success!');
					$scope.inviteEmail = null;
				}).error(function(response) {
					$('#load').css('display', 'none');
					Notifier.error(response.message, 'Error!');
				});
				$('#load').css('display', 'block');
			}
		};
	}]
);

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication',
	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
);
'use strict';

// Authentication service for user variables
angular.module('users').factory('Authorization', ["Authentication", function(Authentication) {
	var authentication = Authentication;
	return {
		isAuthorized: function(role) {
			return authentication.user && authentication.user.roles.indexOf(role) > -1;
		}
	};
}]);

'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users',
	["$resource", "Utility", function($resource, Utility) {
		return $resource(Utility.apiUrl + '/users/:userId', { userId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}]
);

'use strict';

//Setting up route
angular.module('youtube').config(
	["$stateProvider", function($stateProvider) {
		// Sales state routing
		$stateProvider.
		state('importYoutubeReport', {
			url: '/youtube/import-report',
			templateUrl: 'modules/youtube/views/import-report.client.view.html'
		}).
		state('viewYoutubeReport', {
			url: '/youtube/view-report',
			templateUrl: 'modules/youtube/views/youtube-report.client.view.html'
		}).
		state('viewVideoShare', {
			url: '/youtube/view-video-shares',
			templateUrl: 'modules/youtube/views/view-video-share.client.view.html'
		}).
		state('youtubePayment', {
        	url: '/youtube/payment',
        	templateUrl: 'modules/youtube/views/payment.client.view.html'
        });
	}]
);

'use strict';

// Sales controller
angular.module('youtube').controller('ReportController',
    ["$scope", "$upload", "Authentication", "Notifier", "Authorization", "YouTubeCache", function ($scope,
              $upload, Authentication, Notifier, Authorization, YouTubeCache) {
        $scope.authentication = Authentication;
        $scope.auth = Authorization;
        $scope.originalCommission = 40;

        $scope.uploadFile = function ($files) {
            if (!$files || !$files[0] || $files[0].type !== 'text/csv') {
                Notifier.error('Invalid CSV File', 'Failed!');
            } else {

                $scope.upload = $upload.upload({
                    headers: {'import-date': $scope.import_date, 'original-commission': $scope.originalCommission},
                    url: '/api/youtube/report',
                    method: 'POST',
                    withCredentials: true,
                    file: $files[0]
                })
                    .progress(function (evt) {
                        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                    })
                    .success(function (response) {
                        $('#load').css('display', 'none');
                        $scope.success = true;
                        Notifier.notify('YouTube report successfully imported', 'Success!');
                    })
                    .error(function (response) {
                        $('#load').css('display', 'none');
                        Notifier.error(response.message, 'Failed!');
                    });
                $('#load').css('display', 'block');
            }
        };
    }]
);

'use strict';

// Sales controller
var youtubeModule = angular.module('youtube');

youtubeModule.controller('VideoSharesController',
    ["$scope", "$location", "$http", "$state", "$stateParams", "$modal", "$window", "SalesCache", "Authentication", "Notifier", "TrackShareService", "Authorization", "Utility", "$rootScope", "YouTubeCache", "$timeout", function ($scope, $location, $http, $state, $stateParams, $modal, $window, SalesCache,
              Authentication, Notifier, TrackShareService, Authorization, Utility, $rootScope, YouTubeCache, $timeout) {
        $scope.authentication = Authentication;
        $scope.auth = Authorization;
        if (! $scope.authentication.user) {
            $location.path('/signin');
            return;
        }

        // data for all track share
        $scope.allTrackShare = [];
        $scope.currentUser = {};

        // data for adding track share
        $scope.listAlbum = [];
        $scope.selectedAlbum = {};
        $scope.listTrack = [];
        $scope.selectedTrack = {};
        $scope.listUser = [];
        $scope.filterUser = [];
        $scope.selectedUser = {};
        $scope.listRole = Utility.channelRoles;
        $scope.selectedRole = {};
        $scope.selectedCommission = {};
        $scope.filterAlbum = '';
        $scope.numEntryPerPage = 30;
        $scope.showEntries = [30, 50, 100];
        $scope.totalPages = 1;
        $scope.currentPage = 1;

        // variable used for Modal
        $scope.modalInstance = undefined;
        $scope.parentSelected = undefined;
        $scope.selectedTrackShare = undefined;

        // variable control the sort and filter table
        $scope.sortAlbum = 'albumName';
        $scope.sortTrack = 'trackName';
        $scope.searchText = '';

        // variable used for Delete Modal
        $scope.deleteModalInstance = undefined;
        $scope.deletedShare = undefined;
        $scope.deletedIndexShareWith = undefined;
        $scope.deletedIndexItemModal = undefined;
        $scope.isMultipleRemoval = undefined;

        // variable used for Invite Modal
        $scope.inviteModalInstance = undefined;
        $scope.inviteEmails = {};

        // disable some element in UI when loading from server;
        $scope.isLoading = false;

        $scope.checkAll = undefined;
        $scope.expandedAll = undefined;

        $scope.isAdmin = function () {
            return $scope.auth.isAuthorized('admin');
        };

        $scope.allChannelInfo = [];

        $scope.initLoad = function() {
            YouTubeCache.getVisibleClip().then(function (response) {
                $scope.filterUser = response.data;
                if($scope.filterUser.length === 1) {
                    $scope.currentUser = $scope.filterUser[0];
                    $('#s2id_user').find('.select2-chosen').html($scope.currentUser.displayName);
                }

                if($scope.currentUser.albums && $scope.currentUser.albums.length === 1) {
                    $scope.filterAlbum = $scope.currentUser.albums[0];
                    $('#s2id_album').find('.select2-chosen').html($scope.filterAlbum.name);
                }

                $scope.currentUser = $scope.filterUser.filter(function (userWithAlbum) {
                    return userWithAlbum._id === Authentication.user._id;
                })[0];

                $scope.setUser();
            });

            TrackShareService.getAlbumInfo({api: 'youtube'}).then(function (response) {
                $scope.allChannelInfo = response.data;
            });

            TrackShareService.getAllCollaborators({api: 'youtube'}).then(function (response) {
                if (response.ret_code === 0) {
                    $scope.listUser = response.data;
                } else {
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    $scope.listUser = [];
                }

            });
        };

        $scope.setUser = function() {
            $('#s2id_album').find('.select2-chosen').html('All Albums');
            $scope.filterAlbum = '';
            if($scope.filterUser.length === 1) {
                $scope.currentUser = $scope.filterUser[0];
                $('#s2id_user').find('.select2-chosen').html($scope.currentUser.displayName);
            }

            if($scope.currentUser && $scope.currentUser.albums && $scope.currentUser.albums.length === 1) {
                $scope.filterAlbum = $scope.currentUser.albums[0];
                $('#s2id_album').find('.select2-chosen').html($scope.filterAlbum.name);
            }
            $scope.searchText = "";
            $scope.loadTrackShare();
        };

        $scope.range=function(min, max, total) {
            min = $window.Math.max(min, 1);
            max = $window.Math.min(max, total);

            for(var input=[],i=min; max>=i;i++) {
                input.push(i);
            }
            return input;
        };

        $scope.updateEntry = function () {
            if ($rootScope.debug) {
                console.log('Showing ' + $scope.numEntryPerPage + ' entries');
            }
            // reload data
            $scope.searchText = "";
            $scope.loadTrackShare();
        };

        $scope.setTotalPages = function(total) {
            $scope.totalPages = parseInt(total / $scope.numEntryPerPage);
            if ($scope.totalPages * $scope.numEntryPerPage !== total) {
                $scope.totalPages += 1;
            }
        };

        $scope.loadPageDetail = function (pageNumber) {
            pageNumber = $window.Math.max(pageNumber, 1);
            $scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
            $scope.searchText = "";
            $scope.loadTrackShare();
        };

        $scope.loadTrackShare = function () {
            $scope.allTrackShare = [];
            var sentUserId;
            if ($scope.currentUser && $scope.currentUser._id) {
                sentUserId = $scope.currentUser._id;
            }

            $scope.isLoading = true;
            if ($rootScope.debug) {
                console.log('Current user: ' + $scope.currentUser);
            }

            var sentAlbumId = ($scope.filterAlbum && $scope.filterAlbum._id) || undefined;
            var params = {userId: sentUserId, albumId: sentAlbumId};
            params.numPerPage = $scope.numEntryPerPage;
            params.page = $scope.currentPage;
            params.api = 'youtube';
            if ($scope.searchText && $scope.searchText !== "") {
                params.search = $scope.searchText;
            }

            TrackShareService.loadTrackShares(params).then(function (response) {
                // reset the data
                $scope.allTrackShare = response.data;
                $scope.setTotalPages(response.total);
                if ($rootScope.debug) {
                    console.log('Result size of allTrackShare = ' + $scope.allTrackShare.length);
                }
                $scope.isLoading = false;
            });
        };

        $scope.selectUser = function () {
            if (!$scope.selectedUser.value) {return;}
            console.log($scope.selectedUser.value);
            var userWithClip = $scope.filterUser.filter(function (user) {
                return user._id == $scope.selectedUser.value._id;
            });
            if (userWithClip.length > 0) {
                $scope.listAlbum = userWithClip[0].albums;
            } else {
                $scope.listAlbum = $scope.allChannelInfo;
            }

            //update the $scope.selectedAlbum based on $scope.listAlbum
            var filterChannel = $scope.listAlbum.filter(function (channel) {
                return channel._id == $scope.selectedAlbum._id;
            });
            if (filterChannel.length > 0) {
                $scope.selectedAlbum = filterChannel[0];
                $scope.setAlbum();
            }
        };

        $scope.setAlbum = function () {
            if (!$scope.selectedAlbum) {return;}
            if ($scope.selectedAlbum && !$scope.selectedAlbum.trackList) {return;}
            var all = {trackId: 'All', trackName: 'All tagged video'};
            var result = $scope.selectedAlbum.trackList.sort(function (track1, track2) {
                return track1.trackName.localeCompare(track2.trackName);
            });
            $scope.listTrack = [all].concat(result);
        };

        $scope.addTrackShare = function () {
            var value = parseInt($scope.selectedCommission.value);
            if (invalidCommission(value)) {
                Notifier.error('Commission must be a number from 0 to 100', 'Failed!');
                return;
            }
            var addTracks = [];
            if ($scope.selectedTrack.value.trackId === 'All') {
                addTracks = $scope.selectedAlbum.trackList;
            } else {
                addTracks.push($scope.selectedTrack.value);
            }
            var listNewData = addTracks.map(function (track) {
                return {
                    albumId: $scope.selectedAlbum,
                    albumName: $scope.selectedAlbum.name,
                    trackId: track.trackId,
                    trackName: track.trackName,
                    userId: $scope.selectedUser.value,
                    userName: $scope.selectedUser.value.displayName,
                    role: $scope.selectedRole.value,
                    commission: value,
                    shareWith: []
                };
            });

            $scope.isLoading = true;
            var params = {
                album: $scope.selectedAlbum,
                addTracks: addTracks,
                userId: $scope.selectedUser.value,
                role: $scope.selectedRole.value,
                commission: $scope.selectedCommission.value,
                api: 'youtube'
            };
            TrackShareService.addTrackShares(params).then(function (response) {
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    Notifier.notify('Add new track share successfully', 'Success!');

                    if ($scope.selectedTrack.value.trackId === 'All') {
                        $state.go($state.$current, null, { reload: true });
                    } else {
                        //$scope.allTrackShare = $scope.allTrackShare.concat(listNewData);
                        $scope.allTrackShare = listNewData;

                        $scope.addToFilter();
                        $scope.setTotalPages(listNewData.length);
                        $('#s2id_user').find('.select2-chosen').html($scope.currentUser.displayName);
                        $('#s2id_album').find('.select2-chosen').html($scope.filterAlbum.name);
                    }
                } else {
                    Notifier.error('Cannot add new track share because of ' + response.msg, 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        $scope.addToFilter = function(isCollaborator) {
            var isInfilterUser = false, isInFilterAlbum = false;
            var selUser = {}, selAlbum = {};
            $scope.filterUser.forEach(function(filterUser) {
                if(filterUser._id === $scope.selectedUser.value._id) {
                    isInfilterUser = true;
                    selUser = filterUser;
                    filterUser.albums.forEach(function(filterAlbum) {
                        if(filterAlbum._id === $scope.selectedAlbum._id) {
                            isInFilterAlbum = true;
                            selAlbum = filterAlbum;
                        }
                    });

                    if(! isInFilterAlbum) {
                        selAlbum = {
                            _id: $scope.selectedAlbum._id,
                            name: $scope.selectedAlbum.name
                        };
                        filterUser.albums.push(selAlbum);
                    }
                }
            });

            if(! isInfilterUser) {
                selAlbum = {
                    _id: $scope.selectedAlbum._id,
                    name: $scope.selectedAlbum.name
                };
                selUser = {
                    _id: $scope.selectedUser.value._id,
                    displayName: $scope.selectedUser.value.displayName,
                    albums: [selAlbum]
                };
                $scope.filterUser.push(selUser);
            }

            if(! isCollaborator) {
                $scope.currentUser = selUser;
                $scope.filterAlbum = selAlbum;
            }
        };

        $scope.showExpandedAll = function () {
            var sharesContainCollaborator = $scope.allTrackShare.filter(function (share) {
                return share.shareWith.length > 0;
            });
            return sharesContainCollaborator.length > 0;
        };

        $scope.toggleFieldAll = function () {

            $scope.expandedAll = !$scope.expandedAll;
            $scope.allTrackShare.forEach(function (share) {
                if (share.shareWith.length > 0) {
                    share.expanded = $scope.expandedAll;
                }
            });
        };

        $scope.toggleField = function (obj, field) {
            if (!$scope.isLoading) {
                obj[field] = !obj[field];
            }
        };

        $scope.toggleSort = function (field) {
            if (field.startsWith('-')) {
                return field.substring(1);
            } else {
                return '-' + field;
            }
        };

        $scope.isSortReversed = function (field) {
            return field.startsWith('-');
        };

        var escapeRegExp = function (str) {
            return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
        };

        var matchAllRegex = function (listRegex, matchingString) {
            return listRegex.every(function (regex) {
                return matchingString.match(regex);
            });
        };

        $scope.filterFunction = function(trackShare) {
            var multiRegex = $scope.searchText.split(' ').map(function (word) {
                return new RegExp(escapeRegExp(word), 'i');
            });
            var fullText = trackShare.albumName + ' ' + trackShare.trackName + ' ' + trackShare.userName + ' ' +
                           trackShare.role + ' ' + trackShare.commission.toString();
            if (matchAllRegex(multiRegex, fullText)) {
                return true;
            } else {
                // search inside the Collaborator trackshare
                var filter = trackShare.shareWith.filter(function (nestedShare) {
                    var nestedFullText = nestedShare.userName + ' ' +  nestedShare.role + ' ' + nestedShare.commission.toString();
                    return matchAllRegex(multiRegex, nestedFullText);
                });
                return filter.length > 0;
            }
        };

        $scope.oldEditCommission = undefined;
        $scope.keepOldCommission = function (old) {
            $scope.oldEditCommission = old;
        };

        $scope.editCommission = function (trackShare, field, parentShare) {
            var value = parseInt(trackShare.commission);
            if (isNaN(value) || value > 100 || value < 0) {
                Notifier.error('Commission must be a number from 0 to 100', 'Failed!');
                return;
            }
            if (parentShare) {
                // need to check the total % must not exceed 100
                parentShare.shareWith.forEach(function (share) {share.commission = parseInt(share.commission);});
                var total = parentShare.shareWith.reduce(function (total, share) {return total + share.commission;}, 0);
                if (total > 100) {
                    Notifier.error('Total commission of all Collaborator must be from 0 to 100', 'Failed!');
                    return;
                }
            }
            if (value === 0) {
                if (typeof parentShare === 'undefined') {
                    $scope.deletedShare = trackShare;
                    $scope.deletedIndexShareWith = undefined;
                } else {
                    $scope.deletedShare = parentShare;
                    // find index of child in parent.shareWith
                    var foundIndex = -1;
                    parentShare.shareWith.filter(function (child, index) {
                        if (getObjId(child.userId) === getObjId(trackShare.userId) && child.role === trackShare.role) {
                            foundIndex = index;
                        }
                    });
                    if (foundIndex === -1 && $rootScope.debug) {
                        console.log('NOT FOUND child in parent');
                        return;
                    }
                    $scope.deletedIndexShareWith = foundIndex;
                }
                $scope.deletedIndexItemModal = undefined;
                $scope.isMultipleRemoval = undefined;

                // Open new modal
                $scope.deleteModalInstance = $modal.open({
                    templateUrl: 'modules/sales/views/modal/zero-trackshare-modal.client.view.html',
                    size: 'md',
                    scope: $scope
                });
                $scope.deleteModalInstance.result.then(function () {
                    resetDeleteModal();
                }, function () {
                    trackShare.commission = $scope.oldEditCommission;
                    trackShare[field] = !trackShare[field];
                    resetDeleteModal();
                });
            } else {
                $scope.isLoading = true;
                var params = {
                    album: (trackShare.albumId || parentShare.albumId),
                    trackId: (trackShare.trackId || parentShare.trackId),
                    userId: trackShare.userId,
                    role: trackShare.role,
                    commission: value,
                    api: 'youtube'
                };
                TrackShareService.editCommission(params).then(function (response) {
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    if (response.ret_code === 0) {
                        SalesCache.removeCache();
                        Notifier.notify('Edit commission successfully', 'Success!');
                        trackShare[field] = !trackShare[field];
                    } else {
                        Notifier.error('Cannot save commission', 'Failed!');
                    }
                    $scope.isLoading = false;
                });
            }
        };

        $scope.disableEditCollaborator = function () {
            var selected = $scope.allTrackShare.filter(function (share) {return share.selected;});
            return $scope.isLoading || selected.length === 0;
        };

        function getObjId(obj) {
            if (obj && obj._id) {
                return obj._id;
            } else {
                return obj;
            }
        }

        /**
         * Edit Collaborator modal
         */
        $scope.editCollaborator = function () {
            var selected = $scope.allTrackShare.filter(function (share) {return share.selected;});
            var totalShareWith = [];
            for (var i = 0; i < selected.length; i++) {
                for (var j = 0; j < selected[i].shareWith.length; j++) {
                    var share = selected[i];
                    var childShare = selected[i].shareWith[j];
                    // check if this (user,role) exists in the array
                    var found = totalShareWith.filter(function (share) {
                        return getObjId(share.userId) === getObjId(childShare.userId) && share.role === childShare.role;
                    });
                    if (found.length === 0) {
                        totalShareWith.push({
                            userId: childShare.userId,
                            userName: childShare.userName,
                            role: childShare.role,
                            commission: childShare.commission
                        });
                    }
                }
            }

            $scope.parentSelected = selected;
            $scope.selectedTrackShare = totalShareWith;

            // Open new modal
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/sales/views/modal/add-track-share-modal.client.view.html',
                size: 'lg',
                scope: $scope
            });
        };

        $scope.addCollaborator = function () {
            var value = parseInt($scope.selectedCommission.value);
            if (invalidCommission(value)) {
                Notifier.error('Commission must be a number greater than 0 and less or equal to 100', 'Failed!');
                return;
            }
            var checkExists = $scope.selectedTrackShare.filter(function (share) {
                return getObjId(share.userId) === getObjId($scope.selectedUser.value) &&
                       share.role === $scope.selectedRole.value;
            });
            if (checkExists.length > 0) {
                Notifier.error('Duplicate data', 'Failed!');
                return;
            }
            $scope.selectedTrackShare.forEach(function (share) {share.commission = parseInt(share.commission);});
            var total = $scope.selectedTrackShare.reduce(function (total, share) {return total + share.commission;}, 0) + value;
            if (invalidCommission(total)) {
                if ($rootScope.debug) {
                    console.log('Total commission = ' + total);
                }
                Notifier.error('Total commission of all Collaborator must be from 0 to 100', 'Failed!');
                return;
            }
            $scope.selectedTrackShare.push({
                userId: $scope.selectedUser.value,
                userName: $scope.selectedUser.value.displayName,
                role: $scope.selectedRole.value,
                commission: value
            });
            $scope.addToFilter(true);
        };

        function validateEachCollaCommission(commission) {
            var value = parseInt(commission);
            var failed = false;
            var errMsg = '';
            if (!isNaN(value) && value <= 0) {
                errMsg = 'Please remove 0% trackshare';
                failed = true;
            } else if (isNaN(value)) {
                errMsg = 'Please enter a number';
                failed = true;
            }
            return {ok: !failed, msg: errMsg, value: value};
        }

        function invalidCommission(total) {
            return isNaN(total) || total <= 0 || total > 100;
        }

        $scope.validateAllCollaCommission = function (listTrackShare, validateShare) {
            // validate total commission in `listTrackShare` between (0, 100]
            var failed = false;
            var errMsg = '';
            var total = listTrackShare.reduce(function (total, share) {return total + parseInt(share.commission);}, 0);
            if (typeof validateShare !== 'undefined') {
                var result = validateEachCollaCommission(validateShare.commission);
                if (!result.ok) {
                    errMsg = result.msg;
                    failed = true;
                } else if (invalidCommission(total)) {
                    errMsg = 'Total commission of all Collaborator must be from 0 to 100';
                    failed = true;
                }

                if (!failed) {
                    validateShare.commission = result.value;
                }
            } else {
                var existsZeroShare = listTrackShare.filter(function (share) {
                    var value = parseInt(share.commission);
                    return !isNaN(value) && value === 0;
                });
                if (existsZeroShare.length > 0) {
                    errMsg = 'Please remove 0% trackshare';
                    failed = true;
                } else if (invalidCommission(total)) {
                    errMsg = 'Total commission of all Collaborator must be from 0 to 100';
                    failed = true;
                }
            }

            if (failed) {
                Notifier.error(errMsg, 'Failed!');
            }

            return !failed;
        };

        $scope.saveAll = function () {
            if (!$scope.validateAllCollaCommission($scope.selectedTrackShare, undefined)) {
                return;
            }
            $scope.isLoading = true;
            var params = {
                listParent: $scope.parentSelected,
                listCollaborator: $scope.selectedTrackShare,
                api: 'youtube'
            };
            TrackShareService.addCollaboratorTrackShares(params).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    Notifier.notify('Edit collaborator share successfully', 'Success!');
                    $scope.parentSelected.forEach(function (share) {
                        share.shareWith = JSON.parse(JSON.stringify($scope.selectedTrackShare));
                    });
                    $scope.modalInstance.close('Finish!');
                } else {
                    Notifier.error('Cannot edit', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        $scope.cancel = function () {
            $scope.modalInstance.dismiss('Cancel!');
        };

        /**
         * Delete confirmation modal
         */
        var resetDeleteModal = function () {
            $scope.deleteModalInstance = undefined;
            $scope.deletedShare = undefined;
            $scope.deletedIndexShareWith = undefined;
            $scope.deletedIndexItemModal = undefined;
            $scope.isMultipleRemoval = undefined;
        };

        $scope.openDeleteModal = function (removeTrackShare, removeIndexInShareWith, removeIndexInItemModal, multipleRemove) {
            $scope.deletedShare = removeTrackShare;
            $scope.deletedIndexShareWith = removeIndexInShareWith;
            $scope.deletedIndexItemModal = removeIndexInItemModal;
            $scope.isMultipleRemoval = multipleRemove;
            // Open new modal
            $scope.deleteModalInstance = $modal.open({
                templateUrl: 'modules/sales/views/modal/delete-confirm-modal.client.view.html',
                size: 'md',
                scope: $scope
            });
            $scope.deleteModalInstance.result.then(function () {
                resetDeleteModal();
            }, function () {
                resetDeleteModal();
            });
        };

        var findIndexOfTrackShare = function (listShare, searchShare) {
            var foundIndex = -1;
            for (var i = 0; i < listShare.length; i++) {
                var iteratingShare = listShare[i];
                if (iteratingShare.albumName === searchShare.albumName &&
                    iteratingShare.trackName === searchShare.trackName &&
                    iteratingShare.userName === searchShare.userName &&
                    iteratingShare.role === searchShare.role &&
                    iteratingShare.commission === searchShare.commission) {
                    foundIndex = i;
                    break;
                }
            }
            return foundIndex;
        };

        // remove the main track share
        var removeMainTrackShare = function (trackShare) {
            var removeIndex = findIndexOfTrackShare($scope.allTrackShare, trackShare);
            if (removeIndex !== -1) {
                $scope.isLoading = true;
                trackShare.api = 'youtube';
                TrackShareService.removeTrackShare(trackShare).then(function (response) {
                    if ($rootScope.debug) {
                        console.log(response);
                    }
                    if (response.ret_code === 0) {
                        SalesCache.removeCache();
                        Notifier.notify('Remove successfully', 'Success!');
                        $scope.allTrackShare.splice(removeIndex, 1);
                    } else {
                        Notifier.error('Cannot remove', 'Failed!');
                    }
                    $scope.isLoading = false;
                });
            }
        };

        // remove the track share of Collaborator
        var removeCollaboratorTrackShare = function (trackShare, indexInShareWith) {
            $scope.isLoading = true;
            var removeCollaborator = trackShare.shareWith[indexInShareWith];
            var params = {
                userId: removeCollaborator.userId,
                role: removeCollaborator.role,
                listParent: [trackShare],
                api: 'youtube'
            };
            TrackShareService.removeCollaboratorTrackShare(params).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    Notifier.notify('Remove successfully', 'Success!');
                    trackShare.shareWith.splice(indexInShareWith, 1);
                } else {
                    Notifier.error('Cannot remove', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        // remove track share in Edit Modal
        var removeItemInModal = function (selectedIndexModal) {
            $scope.isLoading = true;
            var removeCollaborator = $scope.selectedTrackShare[selectedIndexModal];
            var params = {
                userId: removeCollaborator.userId,
                role: removeCollaborator.role,
                listParent: $scope.parentSelected,
                api: 'youtube'
            };
            TrackShareService.removeCollaboratorTrackShare(params).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    // remove this collaborator in shareWith of all parents
                    $scope.parentSelected.forEach(function (parent) {
                        parent.shareWith = parent.shareWith.filter(function (colla) {
                            return !(getObjId(colla.userId) === getObjId(removeCollaborator.userId) &&
                                    colla.role === removeCollaborator.role);
                        });
                    });

                    Notifier.notify('Remove successfully', 'Success!');
                    $scope.selectedTrackShare.splice(selectedIndexModal, 1);
                } else {
                    Notifier.error('Cannot remove', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        // multiple remove
        var multipleRemove = function () {
            var selectedRemoval = $scope.allTrackShare.filter(function (share) {share.api = 'youtube'; return share.selected;});

            //var selectedRemoval = $scope.allTrackShare.map(function (share) {share.api = 'youtube';return share;});

            TrackShareService.removeMultiple(selectedRemoval).then(function (response) {
                if ($rootScope.debug) {
                    console.log(response);
                }
                if (response.ret_code === 0) {
                    SalesCache.removeCache();
                    $scope.allTrackShare = $scope.allTrackShare.filter(function (share) {return !share.selected;});
                    Notifier.notify('Remove successfully', 'Success!');
                } else {
                    Notifier.error('Cannot remove', 'Failed!');
                }
                $scope.isLoading = false;
            });
        };

        $scope.confirmDelete = function () {
            if (typeof $scope.deletedShare !== 'undefined' && typeof $scope.deletedIndexShareWith === 'undefined') {
                removeMainTrackShare($scope.deletedShare);
            }
            else if (typeof $scope.deletedShare !== 'undefined' && typeof $scope.deletedIndexShareWith !== 'undefined') {
                removeCollaboratorTrackShare($scope.deletedShare, $scope.deletedIndexShareWith);
            } else if (typeof $scope.deletedIndexItemModal !== 'undefined') {
                removeItemInModal($scope.deletedIndexItemModal);
            } else if ($scope.isMultipleRemoval === 'multiple') {
                multipleRemove();
            }
            $scope.deleteModalInstance.close('Yes!');
        };

        $scope.cancelDelete = function () {
            $scope.deleteModalInstance.dismiss('No!');
        };

        var extractField = function (path, obj) {
            return path.split('.').reduce(function (o, p) {
                return o && o[p];
            }, obj);
        };

        var resetUnCheckedAll = function (listShare) {
            listShare.forEach(function (share) {
                share.selected = false;
            });
        };

        $scope.toggleCheckAll = function () {
            resetUnCheckedAll($scope.allTrackShare);
            if (!$scope.checkAll) {
                // if unchecked all
                $scope.searchText = '';
            } else {
                var albumName = extractField('name', $scope.selectedAlbum) || '';
                //var trackName = extractField('trackName', $scope.selectedTrack) || '';
                //var userName = extractField('value.displayName', $scope.selectedUser) || '';
                //var role = extractField('value', $scope.selectedRole) || '';
                //var commission = extractField('value', $scope.selectedCommission) || '';
                //$scope.searchText = (albumName + ' ' + trackName + ' ' + userName + ' ' + role + ' ' + commission).trim();
                $scope.searchText = albumName.trim();
                var filterShare = $scope.allTrackShare;
                if ($scope.searchText !== '') {
                    filterShare = $scope.allTrackShare.filter($scope.filterFunction);
                }
                filterShare.forEach(function (share) {
                    share.selected = true;
                });
            }
        };

        /**
         * Invite Collaborator modal
         */
        $scope.inviteCollaborator = function () {
            // Open new modal
            $scope.inviteModalInstance = $modal.open({
                templateUrl: 'modules/sales/views/modal/track-share-invite-collaborators.client.view.html',
                size: 'md',
                scope: $scope
            });
        };

        $scope.confirmInvite = function () {
            var listEmail = ($scope.inviteEmails.value || '');
            listEmail = listEmail.split(',').map(function (email) {return email.trim();});
            TrackShareService.inviteCollaborator({emails: listEmail, api: 'youtube'}).then(function (response) {
                SalesCache.removeCache();
                if (response.ret_code === 0) {
                    TrackShareService.getAllCollaborators({api: 'youtube'}).then(function (response) {
                        if (response.ret_code === 0) {
                            $scope.listUser = response.data;
                        } else {
                            if ($rootScope.debug) {
                                console.log(response);
                            }
                            $scope.listUser = [];
                        }
                        Notifier.notify(response.msg, 'Success!');
                    });
                } else {
                    Notifier.error(response.msg, 'Failed!');
                }
            });
            $scope.inviteModalInstance.close('Yes!');
        };

        $scope.cancelInvite = function () {
            $scope.inviteModalInstance.dismiss('Cancel Invite!');
        };

        $scope.searchVideo = function (input) {
            if ($rootScope.debug) {
                console.log('Search video with query: ' + input);
            }
            if (input === '' || !$scope.selectedAlbum.channelId) { return; }
            var path = '/api/youtube/search-video';
            var query = '?channelId=' + $scope.selectedAlbum.channelId + '&search=' + input + '&limit=5';
            $http.get(path + query).then(
                function (response) {
                    var result = response.data;
                    if (result.ret_code === 0) {
                        var all = {trackId: 'All', trackName: 'All tagged video'};
                        $scope.listTrack = [all].concat(result.data);
                    }
                },
                function (err) {
                    if ($rootScope.debug) {
                        console.log('Something wrong in searcing video: ' + err);
                    }
                });
        };

        /*
        filter:filterVideo($select.search)
        $scope.filterVideo = function (typing) {
            return function(video) {
                if (typing && typing !== '') {
                    var multiRegex = typing.split(/\s+/).map(function (word) {
                        return new RegExp(escapeRegExp(word), 'i');
                    });
                    return matchAllRegex(multiRegex, video.trackName);
                } else {
                    return true;
                }
            };
        };
        */

        $scope.delay = (function() {
            var promise = null;
            return function(callback, ms) {
                $timeout.cancel(promise); //clearTimeout(timer);
                promise = $timeout(callback, ms); //timer = setTimeout(callback, ms);
            };
        })();

    }]
);

'use strict';

// Sales controller
angular.module('youtube').controller('YouTubePaymentController',
    ["$scope", "$location", "$window", "YouTubeCache", "Notifier", "Authentication", "Authorization", function ($scope, $location, $window, YouTubeCache, Notifier, Authentication, Authorization) {
        $scope.authentication = Authentication;
        $scope.auth = Authorization;
        if (! $scope.authentication.user) {
            $location.path('/signin');
            return;
        }

        $scope.tableHeader = [
            {name: 'DATE', sort: 'sorting_asc', schema: 'sale_date'},
            {name: 'ALBUM', sort: 'sorting', schema: 'album_name'},
            {name: 'USER', sort: 'sorting', schema: 'user_name'},
            {name: 'ROLE', sort: 'sorting', schema: 'role'},
            {name: 'EARN', sort: 'sorting', schema: 'commission'},
            {name: 'STATUS', sort: 'sorting', schema: 'status'}
        ];
        $scope.totalPages = 1;
        $scope.currentPage = 1;
        $scope.showEntries = [10, 25, 50, 100];
        $scope.numEntryPerPage = 10;
        $scope.sortedSchema = 'sale_date';
        $scope.isAscending = 1;
        $scope.isLoading = true;
        $scope.paymentList = [];
        $scope.albumIDByName = [];
        $scope.dateRangeSel = [];
        $scope.paymentStatus = {};
        $scope.totalEarning = 0;
        $scope.totalUnpaid = 0;

        $scope.range=function(min, max, total) {
            min = $window.Math.max(min, 1);
            max = $window.Math.min(max, total);

            for(var input=[],i=min; max>=i;i++) {
                input.push(i);
            }
            return input;
        };

        $scope.setTotalPages = function(total) {
            $scope.totalPages = parseInt(total / $scope.numEntryPerPage);
            if ($scope.totalPages * $scope.numEntryPerPage !== total) {
                $scope.totalPages += 1;
            }
        };

        Array.prototype.sum = function (prop) {
            var total = 0;
            for ( var i = 0, _len = this.length; i < _len; i++ ) {
                total += this[i][prop];
            }
            return total;
        };

        $scope.loadPageDetail = function (pageNumber) {
            pageNumber = $window.Math.max(pageNumber, 1);
            $scope.currentPage = $window.Math.min(pageNumber, $scope.totalPages);
            $scope.loadUserEarningCommission();
        };

        $scope.updateEntry = function () {
            //console.log('Showing ' + $scope.numEntryPerPage + ' entries');
            // reload data
            $scope.loadUserEarningCommission();
        };

        $scope.searchEnter = function (keyEvent) {
            if (keyEvent.which === 13) {
                //Enter is pressed
                $scope.search();
            }
        };

        $scope.search = function () {
            if ($scope.isLoading || !$scope.inputSearchText || $scope.inputSearchText && $scope.inputSearchText.trim() === '') {
                return;
            }
            console.log('query with searchText = ' + $scope.inputSearchText);
            // reload data
            $scope.loadUserEarningCommission();
        };

        var toggleSortClass = function (sortClass) {
            switch (sortClass) {
                case 'sorting':
                    return 'sorting_asc';
                case 'sorting_asc':
                    return 'sorting_desc';
                case 'sorting_desc':
                    return 'sorting_asc';
                default:
                    return 'sorting';
            }
        };

        var checkIfAscending = function (className) {
            if (className === 'sorting_asc') {
                return 1;
            } else {
                return -1;
            }
        };

        $scope.sortByHeader = function (index) {

            if ($scope.isLoading) {return;}

            console.log('sorting this column ' + index);

            // toggle the sort class at current column `index`
            var newClassName = toggleSortClass($scope.tableHeader[index].sort);
            $scope.tableHeader[index].sort = newClassName;

            // reset other column to 'sorting'
            for (var i = 0; i < $scope.tableHeader.length; i++) {
                if (i === index) { continue; }
                $scope.tableHeader[i].sort = 'sorting';
            }

            // store the sorted state in global variable
            $scope.sortedSchema = $scope.tableHeader[index].schema;
            $scope.isAscending = checkIfAscending(newClassName);

            // reload data
            $scope.loadUserEarningCommission();
        };

        $scope.initLoad = function() {
            YouTubeCache.getChannelVideoShareInfo().then(function (response) {
                $scope.listAlbum = response.data;
                $scope.listAlbum.forEach(function(row) {
                    row['albumId'] = row['channelId'];
                    row['albumName'] = row['channelName'];
                    delete row['channelId'];
                    delete row['channelName'];
                });
                $scope.dateRangeSel = response.saleDate;
                $scope.setAlbum();
            });
        };

        $scope.setAlbum = function() {
            $('#s2id_user').find('.select2-chosen').html('All Users');
            $scope.selectedUser = '';
            $scope.setUser();
        };

        $scope.getPaymentClass = function(status) {
            if(status) {
                return 'btn btn-success';
            }
            return 'btn btn-danger';
        };

        $scope.getPaymentButton = function(status) {
            if(status) {
                return 'Paid';
            }
            return 'UnPaid';
        };

        $scope.setUser = function() {
            $('#s2id_role').find('.select2-chosen').html('All Roles');
            $scope.selectedRole = '';
            $scope.setRole();
        };

        $scope.setRole = function () {
            $scope.loadUserEarningCommission();
        };

        $scope.getParameters = function () {
            var userId = '', albumName = '', roleId = '';

            if($scope.selectedRole && $scope.selectedRole !== '') {
                roleId = $scope.selectedRole.role;
            }

            if ($scope.selectedUser && $scope.selectedUser !== '') {
                userId = $scope.selectedUser.userId;
            }

            if($scope.selectedAlbum && $scope.selectedAlbum !== '') {
                albumName = $scope.selectedAlbum.albumName;
            }

            var params = {
                albumName: albumName,
                userId: userId,
                role: roleId
            };

            params.page = $scope.currentPage;
            params.numPerPage = $scope.numEntryPerPage;
            params.sort = $scope.sortedSchema;
            params.isAscending = $scope.isAscending;
            params.startDate = $scope.startDate;
            params.endDate = $scope.endDate;

            if (typeof $scope.inputSearchText !== 'undefined' && $scope.inputSearchText !== '') {
                params.searchText = $scope.inputSearchText;
            }

            return params;
        };

        $scope.loadUserEarningCommission = function() {
            var params = $scope.getParameters();
            YouTubeCache.loadUserEarningCommission(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.paymentList = response.data;
                $scope.dateRangeSel = response.dateRangeSel;
                $scope.totalEarning = response.totalEarning;
                $scope.totalUnpaid = response.totalUnpaid;
                $scope.isLoading = false;
            });
        };

        $scope.submitPayment = function(index) {
            var body = [];
            if(index === parseInt(index)) {
                body.push({
                    date: new Date($scope.paymentList[index].sale_date).getTime(),
                    albumName: $scope.paymentList[index].album_name,
                    userId: $scope.paymentList[index].user_id,
                    role: $scope.paymentList[index].role,
                    status: ! $scope.paymentList[index].status
                });
            } else {
                if(index === 'true') {
                    index = true;
                } else {
                    index = false;
                }

                body = {
                    albumName: '',
                    userId: '',
                    role: '',
                    status: index
                };

                if($scope.startDate) {
                    body.startDate = new Date($scope.startDate).getTime();
                }
                if($scope.endDate) {
                    body.endDate = new Date($scope.endDate).getTime();
                }
                if($scope.selectedAlbum && $scope.selectedAlbum.albumName) {
                    body.albumName = $scope.selectedAlbum.albumName;
                }
                if($scope.selectedUser.userId) {
                    body.userId = $scope.selectedUser.userId;
                }
                if($scope.selectedRole.role) {
                    body.role = $scope.selectedRole.role;
                }
                body = [body];
            }

            YouTubeCache.createOrUpdatePayment(body).then(function () {
                if(index === parseInt(index)) {
                    $scope.paymentList[index].status = !$scope.paymentList[index].status;
                    if(! $scope.paymentList[index].status) {
                        $scope.totalUnpaid += $scope.paymentList[index].commission;
                    } else {
                        $scope.totalUnpaid -= $scope.paymentList[index].commission;
                    }
                } else {
                    for(var idx in $scope.paymentList) {
                        $scope.paymentList[idx].status = index;
                    }
                    if(index) {
                        $scope.totalUnpaid = 0;
                    } else {
                        $scope.totalUnpaid = $scope.totalEarning;
                    }
                }
                Notifier.notify('Payment Updated', 'Success!');
            });
        };
    }]
);

'use strict';

// YouTube controller
angular.module('youtube').controller('YouTubeController',
    ["$scope", "$location", "$window", "Authentication", "YouTubeCache", "Authorization", "$filter", function ($scope, $location, $window, Authentication,
              YouTubeCache, Authorization, $filter) {

        $scope.authentication = Authentication;
        if (! $scope.authentication.user) {
            $location.path('/signin');
            return;
        }

        $scope.auth = Authorization;
        $scope.isLoadedChart = false;
        $scope.totalPages = 1;
        $scope.currentPage = 1;
        $scope.listChannel = [];
        $scope.saleCommission = [];
        $scope.selectedTabDisplay = 'tabChartSummary';
        $scope.selectedChannel = '';
        $scope.selectedUser = '';
        $scope.selectedRole = '';
        $scope.tableHeader = [];
        $scope.earningSummary = {commission: 0, paid: 0, balance: 0, quantity: 0, removedProfit: 0, removedQuantity: 0};
        $scope.isfirstLoad = true;
        $scope.userNameByID = [];
        $scope.dateRangeStart = [];
        $scope.dateRangeEnd = [];

        $scope.videoHeader = [
            {name: 'CHANNEL', sort: 'sorting', schema: 'channelName'},
            {name: 'VIDEO', sort: 'sorting', schema: 'videoName'},
            {name: 'SUBTOTAL', sort: 'sorting', schema: 'profit'},
            {name: 'COMISSION', sort: 'sorting', schema: 'commission'}
        ];

        $scope.getGroupHeader = function() {
            return [
                {name: $scope.selTblCol, sort: 'sorting', schema: 'name'},
                {name: 'ROLE', sort: 'sorting', schema: 'role'},
                {name: 'SUBTOTAL', sort: 'sorting', schema: 'profit'},
                {name: 'COMISSION', sort: 'sorting', schema: 'commission'}
            ];
        };

        $scope.sortedSchema = 'sale_date';
        $scope.isAscending = 1;
        $scope.isLoading = true;

        $scope.showEntries = [10, 25, 50, 100];
        $scope.numEntryPerPage = 10;

        $scope.inputSearchText = undefined;

        $scope.initLoad = function() {
            YouTubeCache.getChannelVideoShareInfo().then(function (response) {
                $scope.listChannel = response.data;
                if($scope.listChannel.length === 1) {
                    $scope.selectedChannel = $scope.listChannel[0];
                    $('#s2id_channel').find('.select2-chosen').html($scope.selectedChannel.channelName);
                }
                $scope.listChannel.forEach(function(channel) {
                    channel.users.forEach(function(user) {
                        $scope.userNameByID[user.userId] = user.userName;
                    });
                });
                $scope.setChannel();
            });
        };

        $scope.getColName = function(name) {
            if($scope.selTblCol === 'Artist') {
                return $scope.userNameByID[name];
            }
            return name;
        };

        $scope.setChannel = function() {
            $('#s2id_user').find('.select2-chosen').html('All Users');
            $scope.selectedUser = '';
            if($scope.selectedChannel && $scope.selectedChannel.users) {
                if($scope.selectedChannel.users.length === 1) {
                    $scope.selectedUser = $scope.selectedChannel.users[0];
                    $('#s2id_user').find('.select2-chosen').html($scope.selectedUser.userName);
                }
            }
            $scope.setUser();
        };

        $scope.setUser = function() {
            $('#s2id_role').find('.select2-chosen').html('All Roles');
            $scope.selectedRole = '';
            if($scope.selectedUser && $scope.selectedUser.roles) {
                if($scope.selectedUser.roles.length === 1) {
                    $scope.selectedRole = $scope.selectedUser.roles[0];
                    $('#s2id_role').find('.select2-chosen').html($scope.selectedRole.role);
                }
            }
            $scope.setRole();
        };

        $scope.setRole = function () {
            $scope.loadCommission();
        };

        $scope.setTabDisplay = function(tabName) {
            $scope.selectedTabDisplay = tabName;
            $scope.totalPages = 1;
            $scope.currentPage = 1;
            $scope.isAscending = 1;
            $scope.numEntryPerPage = 10;
            $scope.inputSearchText = undefined;
            $scope.groupName = undefined;
            $scope.sortedSchema = $scope.groupName;

            switch(tabName) {
                case 'tabVideo':
                case 'tabDeleteVideo':
                    $scope.tableHeader = $scope.videoHeader;
                    $scope.sortedSchema = 'videoName';
                    break;
                case 'tabArtist':
                    $scope.selTblCol = 'Artist';
                    $scope.groupName = 'userId';
                    $scope.tableHeader = $scope.getGroupHeader();
                    break;
                case 'tabChannel':
                    $scope.selTblCol = 'Channel';
                    $scope.groupName = 'album_name';
                    $scope.tableHeader = $scope.getGroupHeader();
                    break;

                case 'tabChartSummary':
                    break;
            }
            $scope.loadCommission();
        };

        var toggleSortClass = function (sortClass) {
            switch (sortClass) {
                case 'sorting':
                    return 'sorting_asc';
                case 'sorting_asc':
                    return 'sorting_desc';
                case 'sorting_desc':
                    return 'sorting_asc';
                default:
                    return 'sorting';
            }
        };

        var checkIfAscending = function (className) {
            if (className === 'sorting_asc') {
                return 1;
            } else {
                return -1;
            }
        };

        $scope.sortByHeader = function (index) {

            if ($scope.isLoading) {return;}

            console.log('sorting this column ' + index);

            // toggle the sort class at current column `index`
            var newClassName = toggleSortClass($scope.tableHeader[index].sort);
            $scope.tableHeader[index].sort = newClassName;

            // reset other column to 'sorting'
            for (var i = 0; i < $scope.tableHeader.length; i++) {
                if (i === index) { continue; }
                $scope.tableHeader[i].sort = 'sorting';
            }

            // store the sorted state in global variable
            $scope.sortedSchema = $scope.tableHeader[index].schema;
            $scope.isAscending = checkIfAscending(newClassName);

            // reload data
            $scope.loadCommission();
        };

        $scope.updateEntry = function () {
            //console.log('Showing ' + $scope.numEntryPerPage + ' entries');
            // reload data
            $scope.loadCommission();
        };

        $scope.searchEnter = function (keyEvent) {
            if (keyEvent.which === 13) {
                //Enter is pressed
                $scope.search();
            }
        };

        $scope.search = function () {
            if ($scope.isLoading || !$scope.inputSearchText || $scope.inputSearchText && $scope.inputSearchText.trim() === '') {
                return;
            }
            console.log('query with searchText = ' + $scope.inputSearchText);
            // reload data
            $scope.loadCommission();
        };

        $scope.range=function(min, max, total) {
            min = $window.Math.max(min, 1);
            max = $window.Math.min(max, total);

            for(var input=[],i=min; max>=i;i++) {
                input.push(i);
            }
            return input;
        };

        $scope.setTotalPages = function(total) {
            $scope.totalPages = parseInt(total / $scope.numEntryPerPage);
            if ($scope.totalPages * $scope.numEntryPerPage !== total) {
                $scope.totalPages += 1;
            }
        };

        Array.prototype.sum = function (prop) {
            var total = 0;
            for ( var i = 0, _len = this.length; i < _len; i++ ) {
                total += this[i][prop];
            }
            return total;
        };

        $scope.getParameters = function () {
            var userId = '', channelName = '', roleId = '';
            if ($scope.selectedUser && $scope.selectedUser !== '') {
                userId = $scope.selectedUser.userId;
            }

            if($scope.selectedChannel && $scope.selectedChannel !== '') {
                channelName = $scope.selectedChannel.channelName;
            }

            if($scope.selectedRole && $scope.selectedRole !== '') {
                roleId = $scope.selectedRole.role;
            }

            var params = {
                channelName: channelName,
                userId: userId,
                role: roleId,
                startDate: $scope.startDate,
                endDate: $scope.endDate
            };

            if($scope.selectedTabDisplay !== 'tabChartSummary') {
                params.page = $scope.currentPage;
                params.numPerPage = $scope.numEntryPerPage;
                params.sort = $scope.sortedSchema;
                params.isAscending = $scope.isAscending;

                if($scope.groupName) {
                    params.groupName = $scope.groupName;
                }

                if (typeof $scope.inputSearchText !== 'undefined' && $scope.inputSearchText !== '') {
                    params.searchText = $scope.inputSearchText;
                }
            }

            return params;
        };

        $scope.loadCommission = function() {

            $scope.saleCommission = [];
            $scope.isLoading = true;

            switch($scope.selectedTabDisplay) {
                case 'tabVideo':
                case 'tabDeleteVideo':
                    $scope.videoType = $scope.selectedTabDisplay;
                    $scope.loadCommissionGroupByVideo();
                    break;
                case 'tabArtist':
                case 'tabChannel':
                    $scope.loadCommissionGroup();
                    break;
                case 'tabChartSummary':
                    if($scope.isfirstLoad || $scope.isLoadedChart) {
                        $scope.loadChartSummary();
                    } else {
                        $scope.loadCommissionSummary();
                    }
                    break;
            }
        };

        $scope.assignDateRange = function(min, max) {
            var start = new Date(min);
            var end = new Date(max);

            $scope.dateRangeStart.forEach(function(item, idx) {
                var dte = new Date(item.value);
                if(dte.getFullYear() === start.getFullYear() && dte.getMonth() === start.getMonth() || $scope.dateRangeStart.length === 1) {
                    if(start.getDate() > 2 && idx + 1 < $scope.dateRangeStart.length) {
                        $scope.startDate = $scope.dateRangeStart[idx + 1].value;
                    } else {
                        $scope.startDate = item.value;
                    }
                }
            });

            $scope.dateRangeEnd.forEach(function(item) {
                var dte = new Date(item.value);
                if(dte.getFullYear() === end.getFullYear() && dte.getMonth() === end.getMonth() || $scope.dateRangeEnd.length === 1) {
                    $scope.endDate = item.value;
                }
            });

            start = $filter('date')(new Date($scope.startDate), 'MMM - yyyy');
            end = $filter('date')(new Date($scope.endDate), 'MMM - yyyy');
            $('#s2id_startDate').find('.select2-chosen').html(start);
            $('#s2id_endDate').find('.select2-chosen').html(end);
        };

        $scope.loadChartSummary = function() {
            var params = $scope.getParameters();
            YouTubeCache.loadChartSummary(params).then(function (response) {
                var data = [];
                for(var i = 0; i < response.data.length; i++) {
                    var date = new Date(response.data[i]._id.sale_date);
                    date = new Date(date.getFullYear(), date.getMonth(), 2);
                    if(i + 1 === response.data.length && i > 0) {
                        var int_d = new Date(date.getFullYear(), date.getMonth() + 1, 1);
                        date = new Date(int_d - 1);
                    }
                    data.push([date.getTime(), response.data[i].commission]);
                    if($scope.isfirstLoad) {
                        date = new Date(date.getFullYear(), date.getMonth(), 2);
                        $scope.dateRangeStart.push({
                            value: date.getTime(),
                            text: date
                        });

                        var int_dd = new Date(date.getFullYear(), date.getMonth() + 1, 1);
                        date = new Date(int_dd - 1);
                        $scope.dateRangeEnd.push({
                            value: date.getTime(),
                            text: date
                        });
                    }
                }
                $scope.isLoading = false;

                var chartOpt = {
                    colors: ['#689f38'],
                    title : {text : 'Monthly Earning'},
                    series : [{
                        name: 'Earning',
                        data : data,
                        marker : {enabled : true, radius : 3},
                        tooltip: {valueDecimals: 3}
                    }],
                    xAxis: {
                        events: {
                            setExtremes: function (e) {
                                if(e.trigger === 'rangeSelectorButton' || e.DOMEvent && e.DOMEvent.type === 'mouseup') {
                                    $scope.assignDateRange(e.min, e.max);
                                    $scope.isLoadedChart = false;
                                    $scope.loadCommission();
                                }
                            }
                        },
                        minRange: 30 * 24 * 3600
                    },
                    navigator: {
                        maskInside: false,
                        series: {
                            color: '#ffffff',
                            lineColor: '#689f38'
                        }
                    }
                };
                if($scope.isfirstLoad) {
                    chartOpt.rangeSelector = {selected : 1};
                }


                $('#roll14').highcharts('StockChart', chartOpt, function(e) {
                    $scope.assignDateRange(e.xAxis[0].min, e.xAxis[0].max);
                    if($scope.isfirstLoad || $scope.isLoadedChart) {
                        $scope.isfirstLoad = false;
                        $scope.isLoadedChart = false;
                        $scope.loadCommission();
                    }
                });
            });
        };

        $scope.loadCommissionSummary = function() {
            var params = $scope.getParameters();
            return YouTubeCache.loadCommissionSummary(params).then(function (response) {
                $scope.earningSummary = response.data;
                $scope.isLoading = false;
                if($scope.isLoadedChart) {
                    $scope.loadChartSummary();
                }
                $scope.isLoadedChart = true;
            });
        };

        $scope.loadCommissionGroup = function() {
            var params = $scope.getParameters();
            return YouTubeCache.loadCommissionGroup(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.saleCommission = response.data;
                $scope.isLoading = false;
            });
        };

        $scope.loadCommissionGroupByVideo = function() {
            var params = $scope.getParameters();
            params.type = $scope.selectedTabDisplay;
            return YouTubeCache.loadCommissionGroupByVideo(params).then(function (response) {
                $scope.setTotalPages(response.total);
                $scope.saleCommission = response.data;
                $scope.isLoading = false;
            });
        };
    }]
);

'use strict';

//YouTube service used to communicate YouTube REST endpoints
angular.module('youtube').factory('YouTubeCache', ["$http", "$q", "$cacheFactory", "$rootScope", "Utility", function($http, $q, $cacheFactory, $rootScope, Utility) {
        var cache = $cacheFactory('YouTubeCache');
        var removeCache = function() {
            cache.removeAll();
        };

        return {
            loadYouTubeSale: function(params) {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube/report', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getVideoTag: function(params) {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube/video-tag', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            listChannels: function() {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube/channels', {cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getYouTubeSaleByChannel: function() {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube/sale-by-channel', {cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            removeCache: function() {
                cache.removeAll();
            },

            getChannelVideoShareInfo: function() {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/channelvideoshareinfo', {cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadChartSummary: function (params) {
                //console.log('Loading chart summary with params: ');
                //console.log(params);
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube-commission-chart', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionSummary: function (params) {
                //console.log('Loading summary commission with params: ');
                //console.log(params);
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube-commission-summary', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionGroup: function (params) {
                //console.log('Loading group commission with params: ');
                //console.log(params);
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube-commission-byGroup', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadCommissionGroupByVideo: function (params) {
                //console.log('Loading video commission with params: ');
                //console.log(params);
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube-commission-byVideo', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            loadUserEarningCommission: function (params) {
                if ($rootScope.debug) {
                    console.log('Loading user earning with params: ');
                    console.log(params);
                }
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube-user-earning', {params: params, cache: cache}).success(function (response) {
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            createOrUpdatePayment: function (body) {
                var dfd = $q.defer();
                $http.post(Utility.apiUrl + '/youtube-payment', body).success(function (response) {
                    //TrackShareService.removeCache();
                    removeCache();
                    dfd.resolve(response);
                });
                return dfd.promise;
            },

            getVisibleClip: function () {
                var dfd = $q.defer();
                $http.get(Utility.apiUrl + '/youtube/allUserClip').success(function (response) {
                    if ($rootScope.debug) {
                        console.log('Result from YouTubeCache.getVisibleClip()');
                        console.log(response);
                    }
                    dfd.resolve(response);
                });
                return dfd.promise;
            }
        };
    }]
);
