#!/bin/bash

export LC_ALL=C

if [ ! $1 || ! $2]
then
        echo " Example of use: $0 database_name [dir_to_restore]"
        exit 1
fi
db=$1
out_dir=$2

mongorestore --db $db $out_dir
