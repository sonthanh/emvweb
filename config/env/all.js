'use strict';

module.exports = {
	app: {
		title: 'EPICMUSICVN',
		description: 'Full-Stack JavaScript with MongoDB, Express, AngularJS, and Node.js',
		keywords: 'mongodb, express, angularjs, node.js, mongoose, passport'
	},
	port: process.env.PORT || 5000,
	templateEngine: 'swig',
	// The secret should be set to a non-guessable string that
	// is used to compute a session hash
	sessionSecret: 'MEAN',
	// The name of the MongoDB collection to store sessions in
	sessionCollection: 'sessions',
	// The session cookie settings
	sessionCookie: { 
		path: '/',
		httpOnly: true,
		// If secure is set to true then it will cause the cookie to be set
		// only when SSL-enabled (HTTPS) is used, and otherwise it won't
		// set a cookie. 'true' is recommended yet it requires the above
		// mentioned pre-requisite.
		secure: false,
		// Only set the maxAge to null if the cookie shouldn't be expired
		// at all. The cookie will expunge when the browser is closed.
		maxAge: null
		// To set the cookie in a specific domain uncomment the following 
		// setting:
		// domain: 'yourdomain.com'
	},
	// The session cookie name
	sessionName: 'connect.sid',
	log: {
		// Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
		format: 'combined',
		// Stream defaults to process.stdout
		// Uncomment to enable logging to a log on the file system
		options: {
			stream: 'access.log'
		}
	},
	assets: {
		lib: {
			css: [
				'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400',
				'public/lib/bootstrap/dist/css/bootstrap.min.css',
				'public/cube/css/libs/font-awesome.min.css',
				'public/lib/nanoscroller/bin/css/nanoscroller.css',
				'public/cube/css/compiled/theme_styles.css',
				'public/cube/css/libs/datepicker.css',
				'public/cube/css/libs/select2.css',
				'public/cube/css/angular.css',
				'public/cube/css/compiled/custom.css',
				'public/lib/toastr/toastr.min.css',
				'public/lib/angular-ui-select/dist/select.css'
			],
			js: [
				'public/lib/jquery/dist/jquery.min.js',
				'public/lib/bootstrap/dist/js/bootstrap.min.js',
				'public/cube/js/bootstrap-datepicker.js',
				'public/lib/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js',
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.min.js',
				'public/lib/angular-animate/angular-animate.min.js',
				'public/lib/angular-ui-router/release/angular-ui-router.min.js',
				'public/lib/ng-file-upload/angular-file-upload.min.js',
				'public/lib/toastr/toastr.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
				'public/lib/angular-ui-select/dist/select.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/cube/app/directives.js',
			'public/cube/js/scripts.js',
			'public/cube/js/select2.min.js',
			'public/cube/js/moment.min.js',
			'public/cube/js/daterangepicker.js',
			'public/cube/js/highstock.js',
			'public/cube/js/typeahead.min.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
