'use strict';

module.exports = {
	db: {
		uri: 'mongodb://127.0.0.1/emvn',
		options: {
			user: '',
			pass: ''
		}
	},
	log: {
		// Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
		format: 'dev',
		// Stream defaults to process.stdout
		// Uncomment to enable logging to a log on the file system
		options: {
			//stream: 'access.log'
		}
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || '696413590474262',
		clientSecret: process.env.FACEBOOK_SECRET || 'db7f11c82bc30570e8e90789142df3b7',
		callbackURL: '/auth/facebook/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || '1014322263795-8n0fld1chgu997i9gsgf7c5kcoabbe3m.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || 'XVolSj2EOoEi6ugDFMQgh45Y',
		callbackURL: '/auth/google/callback'
	},
	youtube: {
		clientID: process.env.YOUTUBE_ID || '1014322263795-m9umvikmo8fqsu0adq1t9n6crsqh67m3.apps.googleusercontent.com',
		clientSecret: process.env.YOUTUBE_SECRET || 'SWunIxeRIyDpzxkLGzQ6rlO5',
		callbackURL: 'http://dev.emvn.co/auth/youtube/callback'
	},
	bitly: {
		clientID: process.env.BITLY_ID || 'b760de980c2a28cb12ae8086873689f0a9ad1c9d',
		clientSecret: process.env.BITLY_SECRET || '3e6d22a703b42adc4e6341d4fc385fafb0f88e5e',
		callbackURL: 'http://dev.emvn.co/sale-url-update'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'epicmusicvn4@gmail.com',
				pass: process.env.MAILER_PASSWORD || 'Garden_1'
			}
		}
	}
};
