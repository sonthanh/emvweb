'use strict';

module.exports = {
	db: {
		uri: 'mongodb://127.0.0.1/emvn',
		options: {
			user: '',
			pass: ''
		}
	},
	log: {
		// Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
		format: 'combined',
		// Stream defaults to process.stdout
		// Uncomment to enable logging to a log on the file system
		options: {
			stream: 'access.log'
		}
	},
	assets: {
		lib: {
			css: [
				'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400',
				'public/lib/bootstrap/dist/css/bootstrap.min.css',
				'public/cube/css/libs/font-awesome.min.css',
				'public/lib/nanoscroller/bin/css/nanoscroller.css',
				'public/cube/css/compiled/theme_styles.css',
				'public/cube/css/libs/datepicker.css',
				'public/cube/css/libs/select2.css',
				'public/cube/css/angular.css',
				'public/cube/css/compiled/custom.css',
				'public/lib/toastr/toastr.min.css',
				'public/lib/angular-ui-select/dist/select.min.css'
			],
			js: [
				'public/lib/jquery/dist/jquery.min.js',
				'public/lib/bootstrap/dist/js/bootstrap.min.js',
				'public/cube/js/bootstrap-datepicker.js',
				'public/lib/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js',
				'public/lib/angular/angular.min.js',
				'public/lib/angular-resource/angular-resource.min.js',
				'public/lib/angular-animate/angular-animate.min.js',
				'public/lib/angular-ui-router/release/angular-ui-router.min.js',
				'public/lib/ng-file-upload/angular-file-upload.min.js',
				'public/lib/toastr/toastr.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
				'public/lib/angular-ui-select/dist/select.min.js'
			]
		},
		js: 'public/dist/application.min.js'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || '696413590474262',
		clientSecret: process.env.FACEBOOK_SECRET || 'db7f11c82bc30570e8e90789142df3b7',
		callbackURL: '/auth/facebook/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || '1014322263795-sbdfqv0cnrg6c8mvdbd3thfo3krbn7ns.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || 'My4mUAd4LNftZGISWvh49wZd',
		callbackURL: '/auth/google/callback'
	},
	youtube: {
		clientID: process.env.YOUTUBE_ID || '1014322263795-234c9rj54hh3c96ffadoh9eqevr7hb8a.apps.googleusercontent.com',
		clientSecret: process.env.YOUTUBE_SECRET || '9-khQi2Eb4qFRARu8nYrIvDc',
		callbackURL: 'http://test.emvn.co/auth/youtube/callback'
	},
	bitly: {
		clientID: process.env.BITLY_ID || 'aff8cf53387ffa6e2fa3a1dba88ab98096104a75',
		clientSecret: process.env.BITLY_SECRET || 'a019add2975c4823ae8c811e700025d72883fea5',
		callbackURL: 'http://test.emvn.co/sale-url-update'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'epicmusicvn4@gmail.com',
				pass: process.env.MAILER_PASSWORD || 'Garden_1'
			}
		}
	}
};
