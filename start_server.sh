#!/usr/bin/env bash

kill -9 $(lsof -t -i:5000)
cd `pwd`
rm -rf ~/.npm
npm cache clear
npm install
bower install --allow-root
NODE_ENV=$NODE_ENV grunt build
NODE_ENV=$NODE_ENV node --expose-gc --max-old-space-size=1024 server.js
